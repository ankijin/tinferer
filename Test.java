public boolean contains(T item) {
	int key = item.hashCode();
	head.lock();
	Node pred = head;
	try {
		Node curr = pred.next;
		curr.lock();
		try {
			while (curr.key < key) {
				pred.unlock();
				pred = curr;
				curr = curr.next;
				curr.lock();
			}
			//check contains of item by key comparison
			return (curr.key == key);
		} finally {
			curr.unlock();
		}
	} finally {
		pred.unlock();
	}
}


