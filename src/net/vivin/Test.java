package net.vivin;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

public class Test {
	public static String readFileToString(String filePath) throws IOException {
		StringBuilder fileData = new StringBuilder(1000);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));

		char[] buf = new char[10];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
			buf = new char[1024];
		}

		reader.close();

		return  fileData.toString();	
	}
	
    /*
    Tree looks like:
        A
       / \
      B  C
          \
           D

     ST
     arg = ST     arg = ST
            arg1 = ST
     ST
    For the following tests
   */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		ASTParser parser = ASTParser.newParser(AST.JLS8);
		String str= readFileToString("/Users/kijin/Documents/workspace_mars/GumTreeTester"
				+ "/charts/android_test4/ViewPortHandler.java");
		STGroup group = new STGroupFile("/Users/kijin/Documents/workspace_mars/GumTreeTester/cross_templates/if_statement.stg");
		ST if_statement = group.getInstanceOf("if_statement");
		ST expression = group.getInstanceOf("expression");
		expression.add("expression", "hasNext()");
		ST statement = group.getInstanceOf("statements");
		ST else_statement = group.getInstanceOf("else_clause");
		
        GenericTree<ST> tree = new GenericTree<ST>();

        GenericTreeNode<ST> rootA = new GenericTreeNode<ST>(if_statement);
        GenericTreeNode<ST> childB = new GenericTreeNode<ST>(expression);
        
        GenericTreeNode<ST> childC = new GenericTreeNode<ST>(else_statement);
//        childC.getData().add("condition_clause", "z==y");
        if_statement = group.getInstanceOf("if_statement");
        GenericTreeNode<ST> childD = new GenericTreeNode<ST>(group.getInstanceOf("statements"));
        GenericTreeNode<ST> childE = new GenericTreeNode<ST>(group.getInstanceOf("statements"));
        
    	
        /*
        Tree looks like:
            A
           / \
          B  C
  			/ \
           D  E
        For the following tests
       */
        
       // if_statement(condition_clause, statements, else_clause)
        if_statement.add("expression", "x>y");
        if_statement.add("statements", "x=y");
        childD.getData().add("expression", if_statement.render());
//        System.err.println(if_statement.render());
        childE.getData().add("expression", "return _rect.size");
//        System.out.println(childD.getData().render());
        childC.addChild(childD);
        childC.addChild(childE);
        rootA.addChild(childB);
        rootA.addChild(childC);
        tree.setRoot(rootA);

        
        /**
         * if
         * expr else
         *        if
         */


        // Instead of checking equalities on the lists themselves, we can check equality on the toString's
        // they should generate the same toString's
//       List<GenericTreeNode<ST>> tt = tree.build(GenericTreeTraversalOrderEnum.PRE_ORDER);
        System.out.println(tree.build(GenericTreeTraversalOrderEnum.POST_ORDER).toString());
//        System.out.println(tree.build(GenericTreeTraversalOrderEnum.POST_ORDER).toString());
//        System.err.println(preOrderList.toString());

        
        for(GenericTreeNode<ST> tt1:tree.build(GenericTreeTraversalOrderEnum.POST_ORDER)){
        	ST data = tt1.getData();
        	GenericTreeNode<ST> p= tt1.getParent();
        	
        	if(p!=null){
        		ST pdata = p.getData();
        		Map<String, Object> att = pdata.getAttributes();
        		System.out.println("\n"+att+"->"+data.getName().replace("/", "")+","+data.render());
        //		System.err.println(data.getName().replace("/", "")+","+data.render());
        		if(att.containsKey(data.getName().replace("/", ""))){
        			pdata.add(data.getName().replace("/", ""), data.render());
        		}
        	}
        }
        
        //good standing
//        System.out.println("final atts\n"+rootA.getData().getAttributes());
        System.out.println("final\n"+rootA.getData().render());
	}
}
