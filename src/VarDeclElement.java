import templates.ConTextTemplate;

public class VarDeclElement {
	private String modifier;
	private String type;
	private String name;
	private String init;

	public VarDeclElement(String m, String t, String n, String i){
		modifier 	= m;
		type 		= t;
		name 		= n;
		init 		= i;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return name.equals(((VarDeclElement)obj).name);
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "nvarname:"+name+", modifier:"+modifier+", type:"+type+",init:"+init+"\n";
	}
	
	public String getType(){
		return type;
	}
	
	public String getInit(){
		return init;
	}
	
	public String getVarDeclAndroid(){
		return ConTextTemplate.varDeclAndroid(modifier,type, name, init);
	}
	
	public String getVarDeclSwift(){
		return ConTextTemplate.varDeclSwift(modifier,type, name, init);
	}
	
}
