package migration;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import alignment.common.CommonOperators;
import alignment.common.MappingOutput;
import alignment.common.OpProperty;
import alignment.ruleinfer.StructureMappingAntlr4_for_stmt;
import alignment.ruleinfer.StructureMappingAntlr4_for_stmt.Pair;
import antlr_parsers.javaparser.JavaLexer;
import antlr_parsers.javaparser.JavaParser;
import templatechecker.DSLErrorListener;
import templatechecker.TemplateCBaseListener;
import templatechecker.TemplateCBaseVisitor;
import templatechecker.TemplateCLexer;
import templatechecker.TemplateCParser;
import templatechecker.TemplateCParser.ArgContext;
import templates.MatchingTemplate;

public class IterativeMapping4_0510 {

	public static MappingOutput iteration(ParserRuleContext ctxt_java, CommonTokenStream javaCommonStream, ParserRuleContext ctxt_swft, CommonTokenStream swiftCommonStream, int cycle,String afile, String sfile){

		//		LinkedHashMap<OpProperty, OpProperty> nextmapping = new LinkedHashMap<OpProperty, OpProperty>();
		StructureMappingAntlr4_for_stmt mapping=null;
		if(ctxt_java!=null &&ctxt_swft!=null){
			try {
				mapping = new StructureMappingAntlr4_for_stmt(ctxt_java, ctxt_swft);
				mapping.swiftCommonStream = swiftCommonStream;
				mapping.androidCommonStream = javaCommonStream;
				mapping.afilename = afile;
				mapping.sfilename = sfile;

			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}



			/** rule1
			 * stop when cost equals
			 */
			List<MappingOutput> outputs = new ArrayList<MappingOutput>();
			double c1 = 1.0000, c2 = 1.0001;
			for(int q=0;q<=cycle && mapping!=null  ;q++){
				boolean templateOk = false;
				boolean flip = true;

				//				for(int q=0;q<=cycle && mapping!=null ;q++){
				try {
					//					System.out.println("test"+q);


					//initialize get next candidates








					mapping.initialize();



					//					Iterator<OpProperty> ita = mapping.android_listener.op_a_nodes;





					//					System.out.println("termsMapOnly"+mapping.android_listener.termsMap);
					//					System.out.println("swft termsMapOnly"+mapping.swift_listner.termsMapOnly);
					//					System.err.println("scope_constraint"+mapping.android_listener.scope_constraint);
					//					System.out.println("blcklista"+mapping.android_listener.blacklist);

					//					System.out.println("blcklists"+mapping.swift_listner.blacklist);
					//					System.out.println("op-a"+mapping.android_listener.op_a_nodes);
					//					System.out.println("op-s"+mapping.swift_listner.op_s_nodes);

					//					mapping.nextMapping.clear();
					//					if(mapping.constraint.size()>0)
					//						System.out.println("constraint"+mapping.constraint);
					//						mapping.amap_ct
					//mappings
					//						if(mapping.android_listener.op_a_nodes.size()==mapping.swift_listner.op_s_nodes.size())
					c2 =c1;

					for(int i=0; i< mapping.android_listener.op_a_nodes.size(); i++){
						if(!mapping.android_listener.op_a_nodes.get(i).strASTNode.equals("bubbleData")){
							//							 OpProperty opa = mapping.android_listener.op_a_nodes.get(i);
							//							 opa.property="constant_a";
							//							 mapping.android_listener.op_a_nodes.set(i, opa);
							mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(i));
						}

						if(mapping.android_listener.op_a_nodes.get(i).strASTNode.equals("getDataSets")|mapping.android_listener.op_a_nodes.get(i).strASTNode.equals("getDataSets")){
							OpProperty opa = mapping.android_listener.op_a_nodes.get(i);
							opa.property="constant_a";
							mapping.android_listener.op_a_nodes.set(i, opa);

						}

					}

					for(int i=0; i< mapping.swift_listner.op_s_nodes.size(); i++){
						if(mapping.swift_listner.op_s_nodes.get(i).strASTNode.equals("dataSets")){
							//							 OpProperty ops = mapping.swift_listner.op_s_nodes.get(i);
							//							 ops.property="constant_s";
							//							 mapping.swift_listner.op_s_nodes.set(i, ops);

						}
					}


					MappingOutput output = mapping.compute();
					//					System.out.println("op_a_nodes"+mapping.android_listener.op_a_nodes);
					//					System.out.println("op_s_nodes"+mapping.swift_listner.op_s_nodes);

					if(output!=null){
						c1=output.cost;

						//						System.out.println("template a"+output.template_a.replace("{...}", "{}").replaceAll("\\<arg[0-9]+\\>", "arg"));
						//						System.out.println("template s"+output.template_s.replace("{...}", "{}").replaceAll("\\<arg[0-9]+\\>", "arg"));
						//						String template_a = output.template_a.replace("{...}", "{}").replaceAll("<","").replaceAll(">", "");

						//						System.out.println("template a"+output.template_a.replace("{...}", "{}").replaceAll("<","").replaceAll(">", ""));
						//						System.out.println("template s"+output.template_s.replace("{...}", "{}").replaceAll("<","").replaceAll(">", ""));


						Lexer lexer1 				= new TemplateCLexer((CharStream)new ANTLRInputStream(output.template_a));
						DSLErrorListener listner 	= new DSLErrorListener();
						lexer1.addErrorListener(listner);


						TemplateCParser parser 		= new TemplateCParser(new CommonTokenStream(lexer1));
						//				toskip = false;
						//				ExceptionErrorStrategy error_handler = new ExceptionErrorStrategy();
						//				parser.removeErrorListeners();
						//				parser.setErrorHandler(error_handler);

						TemplateCParser.TemplateContext comj = parser.template();
						List<String> argSwift = new LinkedList<String>(), argAndroid = new LinkedList<String>();
						comj.accept(new TemplateCBaseVisitor(){
							@Override
							public Object visitArg(ArgContext ctx) {
								// TODO Auto-generated method stub
								argAndroid.add(ctx.getText());
								return super.visitArg(ctx);
							}
						});

						//						if(listner.hasErrors()){
						//												System.out.println("error in"+tempa);
						//						}


						TemplateCLexer lexer2 = new TemplateCLexer((CharStream)new ANTLRInputStream(output.template_s));
						DSLErrorListener listner_swift = new DSLErrorListener();
						lexer2.addErrorListener(listner_swift);



						TemplateCParser parser2 = new TemplateCParser(new CommonTokenStream(lexer2));
						//				parser.setErrorHandler(error_handler);
						TemplateCParser.TemplateContext com = parser2.template();

						//				System.out.println("comcom"+com.getText());
						com.accept(new TemplateCBaseVisitor(){
							@Override
							public Object visitArg(ArgContext ctx) {
								// TODO Auto-generated method stub
								//						System.out.println("visitArg");
								argSwift.add(ctx.getText());
								return super.visitArg(ctx);
							}
							public Object visitJava_punc(templatechecker.TemplateCParser.Java_puncContext ctx) {

								return super.visitJava_punc(ctx);
							};
						});

						if(!listner_swift.hasErrors() && !listner.hasErrors() && !(output.template_a.equals("<arg0>") && output.template_s.equals("<arg0>") )){
							outputs.add(output);
						}
					}
					//						output.doupdate();




					//						System.out.println("c1:"+c1+"c2:"+c2+"min:"+Math.min(c1,c2));
					//						if(c2==c1)

					//						templateOk=mapping.templateSolver;

					//						Iterator<Pair> mapps 		= mapping.android_to_swift_point.keySet().iterator();
					//						Iterator<Interval> aaaaaa 	= mapping.amap_ct.keySet().iterator();
					Iterator<OpProperty> aaaaaa = mapping.android_listener.op_a_nodes.iterator();
					List<Interval> listc 		= new LinkedList<Interval>();
					HashSet<OpProperty> set 	= new HashSet<OpProperty>();
					LinkedHashMap<String, List<Interval>> constraint = mapping.android_listener.scope_constraint;

					Iterator<String> decls = constraint.keySet().iterator();
					//					System.out.println("constraint"+constraint);
					while(decls.hasNext()){
						listc = constraint.get(decls.next());
						//													System.out.println("listc"+listc);;
						//						boolean isMet = true;
						//check if template have same args
						while(aaaaaa.hasNext()){
							OpProperty anode = aaaaaa.next();
							Interval nnnnn = anode.interval;
							for(int i = 0; i< listc.size();i++){

								if(!listc.get(i).equals(nnnnn) && !listc.get(i).disjoint(nnnnn)){
									//										System.out.println("nnnnn"+mapping.amap_ct.get(nnnnn));
									set.add(anode);
									if(!mapping.android_listener.blacklist.contains(anode))
										mapping.android_listener.blacklist.add(anode);
								}
							}
						}
					}//scoping constraints in java
					//don't exist




					List<Interval> listc_s 		= new LinkedList<Interval>();
					HashSet<OpProperty> set_s 			= new HashSet<OpProperty>();
					LinkedHashMap<String, List<Interval>> constraint_s = mapping.swift_listner.scope_constraint;

					Iterator<String> decls_s = constraint_s.keySet().iterator();
					//										System.out.println("constraint_s"+constraint_s);
					while(decls_s.hasNext()){
						listc_s = constraint_s.get(decls_s.next());
						//						System.out.println("listc_s"+listc_s);;
						Iterator<OpProperty> ssssss = mapping.swift_listner.op_s_nodes.iterator();
						//						boolean isMet = true;
						//check if template have same args
						while(ssssss.hasNext()){
							OpProperty snode = ssssss.next();
							Interval nnnnn = snode.interval;
							//							System.out.println("nnnnn"+nnnnn);
							for(int i = 0; i< listc_s.size();i++){

								if(!listc_s.get(i).equals(nnnnn) && !listc_s.get(i).disjoint(nnnnn)){
									//										System.out.println("nnnnn"+mapping.amap_ct.get(nnnnn));
									set_s.add(snode);
									if(!mapping.swift_listner.blacklist.contains(snode))
										mapping.swift_listner.blacklist.add(snode);
								}
							}
						}
					}//scoping constraints in swift
					//don't exist

					//					System.out.println("set_s"+set_s);
					/*
					if(set_s.size()>=1){
						for(int i = 0; i< listc_s.size();i++){
							for(OpProperty op: mapping.swift_listner.op_s_nodes)
								if(!listc_s.get(i).equals(op.interval) && !listc_s.get(i).disjoint(op.interval)){
//									mapping.swift_listner.blacklist.addAll(set_s);
																			System.out.println(set_s+" size_s");
								}
						}


						if(mapping.android_to_swift_point.size()>0){
							Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
								public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
									return entry1.getValue().compareTo(entry2.getValue());
								}
							});

//							if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(min.getKey().b))&& !mapping.swift_listner.op_s_nodes.get(min.getKey().b).property.equals("enclosed{}"))
//								mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
							//								System.out.println("black_s"+mapping.swift_listner.blacklist);

						}
					} // un-met .. cases

					 */

					//												System.out.println(set+" sizeof"+set.size()+"amap_ct"+set);
					//if .. not in the same arguments 
					/*
					if(set.size()>=1){
						for(int i = 0; i< listc.size();i++){
							for(OpProperty op: mapping.android_listener.op_a_nodes)
								if(!listc.get(i).equals(op.interval) && !listc.get(i).disjoint(op.interval)){
//									mapping.android_listener.blacklist.addAll(set);
									//										System.out.println(set+" size");
								}
						}


						if(mapping.android_to_swift_point.size()>0){
							Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
								public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
									return entry1.getValue().compareTo(entry2.getValue());
								}
							});

//							if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(min.getKey().b))&& !mapping.swift_listner.op_s_nodes.get(min.getKey().b).property.equals("enclosed{}"))
//								mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
							//								System.out.println("black_s"+mapping.swift_listner.blacklist);

						}
					} // un-met .. cases


					 */


					if(mapping.android_listener.op_a_nodes.size() < mapping.swift_listner.op_s_nodes.size()){
						//							if(flip){
						//							System.out.println("android mapping	"+mapping.android_to_swift_point);
						//							System.out.println("larger"+mapping.android_listener.op_a_nodes);
						//							System.err.println("larger"+mapping.swift_listner.op_s_nodes);

						//							System.out.println("mapping	"+mapping.android_to_swift_point);
						if(mapping.android_to_swift_point.size()>0){

							Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
								public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
									return entry1.getValue().compareTo(entry2.getValue());
								}
							});


							//								System.out.println(min.getKey()+"\n"+);
							//								black_a.clear();
							//								System.out.println("mmmmin	"+min+"  "+min.getKey()+"\n"+mapping.swift_listner.op_s_nodes.get(min.getKey().b)+"\n"+mapping.android_listener.op_a_nodes.get(min.getKey().a));
							//								
							//												System.out.println("mmmmin"+mapping.android_listener.op_a_nodes.get(min.getKey().a));
							if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(min.getKey().a))&& !mapping.android_listener.op_a_nodes.get(min.getKey().a).property.equals("enclosed{}"))
								mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(min.getKey().a));
							int h = mapping.android_listener.op_a_nodes.get(min.getKey().a).height;
							for(OpProperty op: mapping.android_listener.op_a_nodes){
								if(op.height==h && !op.property.equals("enclosed{}")){
									mapping.android_listener.blacklist.add(op);

								}
							}

							//								System.out.println("black_a"+mapping.android_listener.blacklist);
							//								black_a.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
							//								if(min!=null)
							//									mapping.android_listener.op_a_nodes.remove(min.getKey().a);
						}
						//							black_s.addAll(mapping.swift_listner.op_s_nodes);
						//							black_a.addAll(mapping.android_listener.op_a_nodes);
						//						flip = false;
					}

					/** return <>; return <>
					 * for ( <arg0> <arg1> : <arg2> ) {...}  to for <arg1> in <arg2> {...}
					 * 없으면 이대로 .. 있으면 .. 계속. best matching
					 */

					else if(mapping.android_listener.op_a_nodes.size() > mapping.swift_listner.op_s_nodes.size()){
						//							System.out.println("swift mapping	"+mapping.android_to_swift_point);
						//							System.out.println(mapping.android_listener.op_a_nodes);
						//							System.out.println(mapping.swift_listner.op_s_nodes);
						if(mapping.android_to_swift_point.size()>0){
							Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
								public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
									return entry1.getValue().compareTo(entry2.getValue());
								}
							});
							//								System.out.println("|a|<|s|"+min.getKey()+"\n"+mapping.swift_listner.op_s_nodes.get(min.getKey().b)+"\n"+mapping.android_listener.op_a_nodes.get(min.getKey().a));
							//								black_s.clear();


							if(mapping.swift_listner.op_s_nodes.get(min.getKey().b)!=null && !mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(min.getKey().b))&& !mapping.swift_listner.op_s_nodes.get(min.getKey().b).property.equals("enclosed{}")){
								System.out.println("checkinggg"+mapping.swift_listner.op_s_nodes.get(min.getKey().b));
								mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
							}
							int h = mapping.android_listener.op_a_nodes.get(min.getKey().b).height;
							for(OpProperty op: mapping.swift_listner.op_s_nodes){
								if(op.height==h && !op.property.equals("enclosed{}")){
									mapping.swift_listner.blacklist.add(op);
									System.out.println("adding"+op);
								}
							}

							//							mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));



							Iterator<Integer> a_keys = mapping.android_to_swift.keySet().iterator();
							while(a_keys.hasNext()){
								int aa=a_keys.next();
								int ss = mapping.android_to_swift.get(aa);

								if(aa >=0 && ss>=0){
									int decision = CommonOperators.determineGoDownbyNum(mapping.android_listener.op_a_nodes.get(aa).strASTNode, mapping.swift_listner.op_s_nodes.get(ss).strASTNode);
									//																			System.out.println("eq_size decision:"+decision+"   "+mapping.android_listener.op_a_nodes.get(aa)+"   "+mapping.swift_listner.op_s_nodes.get(ss));

									if(decision ==2){
										if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
											Iterator<Interval> itttt = mapping.constraint.values().iterator();
											//									   while(itttt.hasNext()){
											//										   Interval constrainttt = itttt.next();
											//										   if(constrainttt.properlyContains(mapping.swift_listner.op_s_nodes.get(ss).interval)&& constrainttt.length()>mapping.swift_listner.op_s_nodes.get(ss).interval.length())
											mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
											System.out.println("adding blck"+mapping.swift_listner.op_s_nodes.get(ss));
											//									   }

										}
										//												if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
										//													mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
										//												}
										//												System.out.println("SSSBBBB"+mapping.swift_listner.op_s_nodes.get(ss));
									}
									else if(decision ==1){
										if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
											mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
										}
									}
									else if(decision == 0){
										if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
											mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
										}
										if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
											mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
										}
									}
								}
							}

							/*
							Iterator<Integer> a_keys = mapping.android_to_swift.keySet().iterator();
							while(a_keys.hasNext()){
								int aa=a_keys.next();
								int ss = mapping.android_to_swift.get(aa);
								//									ParserRuleContext adrd = (ParserRuleContext)mapping.android_listener.op_a_nodes.get(aa).node;
								//									ParserRuleContext swft = (ParserRuleContext)mapping.swift_listner.op_s_nodes.get(ss).node;

								//0 both
								//1 aop
								//2 sop
								//									int decision = CommonOperators.determineGoDownbyNum(mapping.android_listener.op_a_nodes.get(aa), mapping.swift_listner.op_s_nodes.get(ss));

								if(aa>=0 && ss>=0){
									int decision = CommonOperators.determineGoDownbyNum(mapping.android_listener.op_a_nodes.get(aa), mapping.swift_listner.op_s_nodes.get(ss));
									//										if(decision!=-1){
									System.out.println("SSBBBB"+decision+"   "+mapping.android_listener.op_a_nodes.get(aa)+ " "+mapping.swift_listner.op_s_nodes.get(ss));
									if(decision ==2){
										if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
											mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
										}
//										if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
//											mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
//										}
										//												System.out.println("SSSBBBB"+mapping.swift_listner.op_s_nodes.get(ss));
									}
									else if(decision ==1){
										if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
											mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
										}
									}
									else if(decision == 0){
										if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
											mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
										}
										if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
											mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
										}
									}
									//										}
								}

							}
							 */
							//								black_s.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
							//								if(min!=null)
							//									mapping.swift_listner.op_s_nodes.remove(min.getKey().b);
						}
						//							black_s.clear();
						//							black_s.addAll(mapping.swift_listner.op_s_nodes);
						//							System.out.println("black_s.addAll(mapping.swift_listner.op_s_nodes)");
						//						flip = true;
					}
					else{






						//							mapping.swift_listner.op_s_nodes.get
						Iterator<Integer> a_keys = mapping.android_to_swift.keySet().iterator();
						while(a_keys.hasNext()){
							int aa=a_keys.next();
							int ss = mapping.android_to_swift.get(aa);
							int decision = CommonOperators.determineGoDownbyNum(mapping.android_listener.op_a_nodes.get(aa).strASTNode, mapping.swift_listner.op_s_nodes.get(ss).strASTNode);
							//														System.out.println("eq_size decision:"+decision+"   "+mapping.android_listener.op_a_nodes.get(aa)+"   "+mapping.swift_listner.op_s_nodes.get(ss));
							if(decision ==2){
								if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
									Iterator<Interval> itttt = mapping.constraint.values().iterator();
									//									   while(itttt.hasNext()){
									//										   Interval constrainttt = itttt.next();
									//										   if(constrainttt.properlyContains(mapping.swift_listner.op_s_nodes.get(ss).interval)&& constrainttt.length()>mapping.swift_listner.op_s_nodes.get(ss).interval.length())
									mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
									//										System.out.println("adding blck"+mapping.swift_listner.op_s_nodes.get(ss));
									//									   }

								}
								//												if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
								//													mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
								//												}
								//												System.out.println("SSSBBBB"+mapping.swift_listner.op_s_nodes.get(ss));
							}
							else if(decision ==1){
								if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){

									mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
								}
							}
							else if(decision == 0){
								if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
									mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
									System.out.println("tttt"+mapping.swift_listner.op_s_nodes.get(ss));
								}
								if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
									mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
								}
							}

						}

					}//if same # of
					//					black_a = mapping.android_listener.op_a_nodes;
					//						System.out.println("black_a"+black_a);
					//						System.err.println("black_s"+black_s);

					//					black_s = mapping.swift_listner.op_s_nodes;

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(outputs.size()>0){
				//				System.out.println("MappingOutput\n"+outputs);
				MappingOutput minOutput = Collections.min(outputs);
				//										minOutput.doupdate();
				//					outputs.size();

				MappingOutput last =outputs.get(outputs.size()-1);
				//				last.doupdate();
				//				nextmapping = last.nextMapping;
				//updating next level
				//				MappingOutput.updateNextMapping(connection, nextmapping);
				System.out.println("111outputs"+outputs);
				return minOutput;
			}
			else return null;
		}
		return null;
	}



	public static MappingOutput iterationWithConstants(ParserRuleContext ctxt_java, CommonTokenStream javaCommonStream, ParserRuleContext ctxt_swft, CommonTokenStream swiftCommonStream, int cycle
			,List<String> variable_list, List<String> constant_list	,List<String> svariable_list, List<String> sconstant_list, String afile, String sfile
			){

		//		LinkedHashMap<OpProperty, OpProperty> nextmapping = new LinkedHashMap<OpProperty, OpProperty>();
		StructureMappingAntlr4_for_stmt mapping=null;
		if(ctxt_java!=null &&ctxt_swft!=null){
			try {
				mapping = new StructureMappingAntlr4_for_stmt(ctxt_java, ctxt_swft);
				mapping.swiftCommonStream = swiftCommonStream;
				mapping.androidCommonStream = javaCommonStream;
				mapping.sfilename = sfile;
				mapping.afilename = afile;

			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}



			/** rule1
			 * stop when cost equals
			 */
			List<MappingOutput> outputs = new ArrayList<MappingOutput>();
			double c1 = 1.0000, c2 = 1.0001;
			for(int q=0;q<=cycle && mapping!=null  && c1!=c2;q++){
				boolean templateOk = false;
				boolean flip = true;

				//				for(int q=0;q<=cycle && mapping!=null ;q++){
				try {
					//					System.out.println("test"+q);


					//initialize get next candidates








					mapping.initialize();



					//					Iterator<OpProperty> ita = mapping.android_listener.op_a_nodes;




/*
					System.out.println("termsMapOnly"+mapping.android_listener.termsMap);
					System.out.println("a termsMapOnly"+mapping.android_listener.termsMapOnly);
					System.out.println("s termsMapOnly"+mapping.swift_listner.termsMap);
					System.out.println("swft termsMapOnly"+mapping.swift_listner.termsMapOnly);
					System.err.println("constraint"+mapping.constraint);
					System.err.println("aaaablcklista"+mapping.android_listener.blacklist);

					System.out.println("blcklists"+mapping.swift_listner.blacklist);
*/

					//					mapping.nextMapping.clear();
//					if(mapping.constraint.size()>0)
//						System.out.println("constraint"+mapping.constraint);
					//						mapping.amap_ct
					//mappings
					//						if(mapping.android_listener.op_a_nodes.size()==mapping.swift_listner.op_s_nodes.size())
					c2 =c1;
					if(variable_list !=null){
						for(String varaible:variable_list){
//							System.out.println("ttttttesting");
							for(int i=0; i< mapping.android_listener.op_a_nodes.size(); i++){
								if(mapping.android_listener.op_a_nodes.get(i).strASTNode.contains(varaible) && !mapping.android_listener.op_a_nodes.get(i).strASTNode.equals(varaible))
								{
									if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(i))){
										//										mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(i));
									}
									else{

									}
								}
								mapping.android_listener.blacklist.removeIf( (OpProperty op) -> op.orginTxt.equals(varaible));
							}
						}
					}
					if(constant_list !=null){
						for(String constant:constant_list){
							for(int i=0; i< mapping.android_listener.op_a_nodes.size(); i++){
								if(mapping.android_listener.op_a_nodes.get(i).orginTxt.equals(constant)){
//									System.err.println("testing");
									OpProperty opa = mapping.android_listener.op_a_nodes.get(i);
									opa.property="constant_a";
									//								System.out.println("constant_a	"+opa);
									mapping.android_listener.op_a_nodes.set(i, opa);
								}


								if(mapping.android_listener.op_a_nodes.get(i).orginTxt.contains(constant) && !mapping.android_listener.op_a_nodes.get(i).orginTxt.equals(constant))
								{
									if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(i))){
										mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(i));
									}

								}


							}
						}
					}

					/*
					if(svariable_list !=null){
						for(String varaible:svariable_list){
							for(int i=0; i< mapping.swift_listner.op_s_nodes.size(); i++){
								if(!mapping.swift_listner.op_s_nodes.get(i).strASTNode.contains(varaible) && !mapping.swift_listner.op_s_nodes.get(i).strASTNode.equals(varaible))
								{
									mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(i));
								}
							}
						}
					}
					 */

//					System.err.println("op-aaaaaaa"+mapping.android_listener.op_a_nodes);
//					System.err.println("op-ssssssss"+mapping.swift_listner.op_s_nodes);


					Iterator<Interval> c_a = mapping.constraint.keySet().iterator();
					while(c_a.hasNext()){
						Interval i_c_a = c_a.next();
						Interval i_c_s = mapping.constraint.get(i_c_a);
						Iterator<Pair> iiit = mapping.android_to_swift_point.keySet().iterator();
						while(iiit.hasNext()){
							Pair p = iiit.next();
							if(!p.a_intval.disjoint(i_c_a) && p.b_intval.disjoint(i_c_s)){
								mapping.android_listener.blacklist.add(p.a_op);
								//								count_a++;
							}
							else if(p.a_intval.disjoint(i_c_a) && !p.b_intval.disjoint(i_c_s)){
								mapping.swift_listner.blacklist.add(p.s_op);
								//								count_s++;
								System.out.println("testing11");
							}
							//							Double aa = mapping.android_to_swift_point.get(p);
						}
						for(OpProperty op: mapping.android_listener.op_a_nodes){
							for(OpProperty os: mapping.swift_listner.op_s_nodes){
								if(!op.interval.disjoint(i_c_a) && os.interval.disjoint(i_c_s)){
									mapping.swift_listner.blacklist.add(os);

									//									System.out.println(i_c_a+" "+i_c_s+"@@@@2"+op);

								}
								if(op.interval.disjoint(i_c_a) && !os.interval.disjoint(i_c_s)){
									mapping.android_listener.blacklist.add(op);
									//									System.err.println("adding"+os+" const not met");
									//									mapping.swift_listner.blacklist.add(os);


								}
							}
						}
					}




					/*
					if(sconstant_list !=null){
						for(String constant:sconstant_list){
							for(int i=0; i< mapping.swift_listner.op_s_nodes.size(); i++){
								if(mapping.swift_listner.op_s_nodes.get(i).strASTNode.equals(constant)){
									OpProperty opa = mapping.swift_listner.op_s_nodes.get(i);
									opa.property="constant_s";
									//								System.out.println("constant_a	"+opa);
									mapping.swift_listner.op_s_nodes.set(i, opa);
								}

							}
						}
					}
					 */


					/*
					for(int i=0; i< mapping.swift_listner.op_s_nodes.size(); i++){
						if(mapping.swift_listner.op_s_nodes.get(i).strASTNode.equals("dataSets")){
							//							 OpProperty ops = mapping.swift_listner.op_s_nodes.get(i);
							//							 ops.property="constant_s";
							//							 mapping.swift_listner.op_s_nodes.set(i, ops);

						}
					}
					 */

					MappingOutput output = mapping.compute();
					//					System.out.println("op_a_nodes"+mapping.android_listener.op_a_nodes);
					//					System.out.println("op_s_nodes"+mapping.swift_listner.op_s_nodes);

					if(output!=null){
						c1=output.cost;

						//						System.out.println("template a"+output.template_a.replace("{...}", "{}").replaceAll("\\<arg[0-9]+\\>", "arg"));
						//						System.out.println("template s"+output.template_s.replace("{...}", "{}").replaceAll("\\<arg[0-9]+\\>", "arg"));
						//						String template_a = output.template_a.replace("{...}", "{}").replaceAll("<","").replaceAll(">", "");

						//						System.out.println("template a"+output.template_a.replace("{...}", "{}").replaceAll("<","").replaceAll(">", ""));
						//						System.out.println("template s"+output.template_s.replace("{...}", "{}").replaceAll("<","").replaceAll(">", ""));


						Lexer lexer1 				= new TemplateCLexer((CharStream)new ANTLRInputStream(output.template_a));
						DSLErrorListener listner 	= new DSLErrorListener();
						lexer1.addErrorListener(listner);


						TemplateCParser parser 		= new TemplateCParser(new CommonTokenStream(lexer1));
						//				toskip = false;
						//				ExceptionErrorStrategy error_handler = new ExceptionErrorStrategy();
						//				parser.removeErrorListeners();
						//				parser.setErrorHandler(error_handler);

						TemplateCParser.TemplateContext comj = parser.template();



						TemplateCLexer lexer2 = new TemplateCLexer((CharStream)new ANTLRInputStream(output.template_s));
						DSLErrorListener listner_swift = new DSLErrorListener();
						lexer2.addErrorListener(listner_swift);



						TemplateCParser parser2 = new TemplateCParser(new CommonTokenStream(lexer2));
						//				parser.setErrorHandler(error_handler);
						TemplateCParser.TemplateContext com = parser2.template();

						List<String> argSwift = new LinkedList<String>(), argAndroid = new LinkedList<String>();
						ParseTreeWalker walker_android = new ParseTreeWalker(), walker_swift = new ParseTreeWalker();

						walker_android.walk(new TemplateCBaseListener(){
							public void enterArg(ArgContext ctx) {
								//								 System.out.println("ArgContext"+ctx.getText());
								argAndroid.add(ctx.getText());
								//								argA.add(ctx.getText());
								//								diffArgs.add(ctx.getText().replaceAll("\\<", "").replaceAll("\\>", ""));
							};
						}, comj);




						walker_swift.walk(new TemplateCBaseListener(){
							public void enterArg(ArgContext ctx) {
								//								 System.out.println("ArgContext"+ctx.getText());
								argSwift.add(ctx.getText());
								//								argS.add(ctx.getText());
								//								String arg=ctx.getText().replaceAll("\\<", "").replaceAll("\\>", "");
								//								diffArgs.removeIf((String emp) -> emp.equals(arg));
							};
						}, com);


						//						if(!listner_swift.hasErrors() && !listner.hasErrors()){

						if(!(output.template_a.equals("<arg0>") && output.template_s.equals("<arg0>")))
							if(mapping.android_listener.op_a_nodes.size() == mapping.swift_listner.op_s_nodes.size())
								outputs.add(output);

						//						}
					}
					//						output.doupdate();




					//						System.out.println("c1:"+c1+"c2:"+c2+"min:"+Math.min(c1,c2));
					//						if(c2==c1)

					//						templateOk=mapping.templateSolver;

					//						Iterator<Pair> mapps 		= mapping.android_to_swift_point.keySet().iterator();
					//						Iterator<Interval> aaaaaa 	= mapping.amap_ct.keySet().iterator();
					Iterator<OpProperty> aaaaaa = mapping.android_listener.op_a_nodes.iterator();
					List<Interval> listc 		= new LinkedList<Interval>();
					HashSet<OpProperty> set 	= new HashSet<OpProperty>();
					LinkedHashMap<String, List<Interval>> constraint = mapping.android_listener.scope_constraint;

					Iterator<String> decls = constraint.keySet().iterator();
					//					System.out.println("constraint"+constraint);
					while(decls.hasNext()){
						listc = constraint.get(decls.next());
						//													System.out.println("listc"+listc);;
						//						boolean isMet = true;
						//check if template have same args
						while(aaaaaa.hasNext()){
							OpProperty anode = aaaaaa.next();
							Interval nnnnn = anode.interval;
							for(int i = 0; i< listc.size();i++){

								if(!listc.get(i).equals(nnnnn) && !listc.get(i).disjoint(nnnnn)){
									//										System.out.println("nnnnn"+mapping.amap_ct.get(nnnnn));
									set.add(anode);
									if(!mapping.android_listener.blacklist.contains(anode))
										mapping.android_listener.blacklist.add(anode);
								}
							}
						}
					}//scoping constraints in java
					//don't exist




					List<Interval> listc_s 		= new LinkedList<Interval>();
					HashSet<OpProperty> set_s 			= new HashSet<OpProperty>();
					LinkedHashMap<String, List<Interval>> constraint_s = mapping.swift_listner.scope_constraint;

					Iterator<String> decls_s = constraint_s.keySet().iterator();
					//										System.out.println("constraint_s"+constraint_s);
					while(decls_s.hasNext()){
						listc_s = constraint_s.get(decls_s.next());
						//						System.out.println("listc_s"+listc_s);;
						Iterator<OpProperty> ssssss = mapping.swift_listner.op_s_nodes.iterator();
						//						boolean isMet = true;
						//check if template have same args
						while(ssssss.hasNext()){
							OpProperty snode = ssssss.next();
							Interval nnnnn = snode.interval;
							//							System.out.println("nnnnn"+nnnnn);
							for(int i = 0; i< listc_s.size();i++){

								if(!listc_s.get(i).equals(nnnnn) && !listc_s.get(i).disjoint(nnnnn)){
									//										System.out.println("nnnnn"+mapping.amap_ct.get(nnnnn));
									set_s.add(snode);
									if(!mapping.swift_listner.blacklist.contains(snode)){
										mapping.swift_listner.blacklist.add(snode);
										System.out.println("nnnnn"+mapping.amap_ct.get(nnnnn));
									}
								}
							}
						}
					}//scoping constraints in swift
					//don't exist

					//					System.out.println("set_s"+set_s);
					/*
					if(set_s.size()>=1){
						for(int i = 0; i< listc_s.size();i++){
							for(OpProperty op: mapping.swift_listner.op_s_nodes)
								if(!listc_s.get(i).equals(op.interval) && !listc_s.get(i).disjoint(op.interval)){
//									mapping.swift_listner.blacklist.addAll(set_s);
																			System.out.println(set_s+" size_s");
								}
						}


						if(mapping.android_to_swift_point.size()>0){
							Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
								public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
									return entry1.getValue().compareTo(entry2.getValue());
								}
							});

//							if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(min.getKey().b))&& !mapping.swift_listner.op_s_nodes.get(min.getKey().b).property.equals("enclosed{}"))
//								mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
							//								System.out.println("black_s"+mapping.swift_listner.blacklist);

						}
					} // un-met .. cases

					 */

					//												System.out.println(set+" sizeof"+set.size()+"amap_ct"+set);
					//if .. not in the same arguments 
					/*
					if(set.size()>=1){
						for(int i = 0; i< listc.size();i++){
							for(OpProperty op: mapping.android_listener.op_a_nodes)
								if(!listc.get(i).equals(op.interval) && !listc.get(i).disjoint(op.interval)){
//									mapping.android_listener.blacklist.addAll(set);
									//										System.out.println(set+" size");
								}
						}


						if(mapping.android_to_swift_point.size()>0){
							Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
								public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
									return entry1.getValue().compareTo(entry2.getValue());
								}
							});

//							if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(min.getKey().b))&& !mapping.swift_listner.op_s_nodes.get(min.getKey().b).property.equals("enclosed{}"))
//								mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
							//								System.out.println("black_s"+mapping.swift_listner.blacklist);

						}
					} // un-met .. cases


					 */
					int anum =0, snum=0;
					if(anum==0 || snum==0){
						anum = mapping.android_listener.op_a_nodes.size();
						snum = mapping.swift_listner.op_s_nodes.size();
					}
					if(anum < snum){
						//							if(flip){
						//						System.out.println("android mapping	"+mapping.android_to_swift_point);
						//							System.out.println("larger"+mapping.android_listener.op_a_nodes);
						//							System.err.println("larger"+mapping.swift_listner.op_s_nodes);

						//							System.out.println("mapping	"+mapping.android_to_swift_point);
						if(mapping.android_to_swift_point.size()>0){

							Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
								public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
									return entry1.getValue().compareTo(entry2.getValue());
								}
							});


							//								System.out.println(min.getKey()+"\n"+);
							//								black_a.clear();
							//								System.out.println("mmmmin	"+min+"  "+min.getKey()+"\n"+mapping.swift_listner.op_s_nodes.get(min.getKey().b)+"\n"+mapping.android_listener.op_a_nodes.get(min.getKey().a));
							//								
							//							System.out.println("mmmmin"+mapping.android_listener.op_a_nodes.get(min.getKey().a));
							if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(min.getKey().a))&& !mapping.android_listener.op_a_nodes.get(min.getKey().a).property.equals("enclosed{}")){
								mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(min.getKey().a));
								//								System.out.println("@@@@4"+mapping.android_listener.op_a_nodes.get(min.getKey().a));
							}
							int h = mapping.android_listener.op_a_nodes.get(min.getKey().a).height;
							for(OpProperty op: mapping.android_listener.op_a_nodes){
								if(op.height==h && !op.property.equals("enclosed{}")){
									if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(min.getKey().a)))
										mapping.android_listener.blacklist.add(op);
									//									System.out.println("@@@@5"+mapping.android_listener.op_a_nodes.get(min.getKey().a));

								}
							}

							//								System.out.println("black_a"+mapping.android_listener.blacklist);
							//								black_a.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
							//								if(min!=null)
							//									mapping.android_listener.op_a_nodes.remove(min.getKey().a);
						}
						//							black_s.addAll(mapping.swift_listner.op_s_nodes);
						//							black_a.addAll(mapping.android_listener.op_a_nodes);
						//						flip = false;
					}

					/** return <>; return <>
					 * for ( <arg0> <arg1> : <arg2> ) {...}  to for <arg1> in <arg2> {...}
					 * 없으면 이대로 .. 있으면 .. 계속. best matching
					 */

					else if(anum > snum){
						//					else if(mapping.android_listener.op_a_nodes.size() > mapping.swift_listner.op_s_nodes.size()){
						//							System.out.println("swift mapping	"+mapping.android_to_swift_point);
						//							System.out.println(mapping.android_listener.op_a_nodes);
						//							System.out.println(mapping.swift_listner.op_s_nodes);
						if(mapping.android_to_swift_point.size()>0){
							Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
								public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
									return entry1.getValue().compareTo(entry2.getValue());
								}
							});
							//								System.out.println("|a|<|s|"+min.getKey()+"\n"+mapping.swift_listner.op_s_nodes.get(min.getKey().b)+"\n"+mapping.android_listener.op_a_nodes.get(min.getKey().a));
							//								black_s.clear();

							//							if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(min.getKey().b))){

							//							}

							if(mapping.swift_listner.op_s_nodes.get(min.getKey().b)!=null && !mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(min.getKey().b))&& !mapping.swift_listner.op_s_nodes.get(min.getKey().b).property.equals("enclosed{}")){
								//								mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
//								System.err.println("adding op_a>op_s"+mapping.swift_listner.op_s_nodes.get(min.getKey().b));
								mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
							}
							int h = mapping.swift_listner.op_s_nodes.get(min.getKey().b).height;
							for(OpProperty op: mapping.swift_listner.op_s_nodes){
								if(op.height==h && !op.property.equals("enclosed{}") && !mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(min.getKey().b))){
									mapping.swift_listner.blacklist.add(op);
//									System.err.println("adding op_a>op_s+same_hight"+mapping.swift_listner.op_s_nodes.get(min.getKey().b));

								}
							}



							Iterator<Integer> a_keys = mapping.android_to_swift.keySet().iterator();
							while(a_keys.hasNext()){
								int aa=a_keys.next();
								int ss = mapping.android_to_swift.get(aa);

								if(aa >=0 && ss>=0){
									int decision = CommonOperators.determineGoDownbyNum(mapping.android_listener.op_a_nodes.get(aa).strASTNode, mapping.swift_listner.op_s_nodes.get(ss).strASTNode);
									//									System.out.println("eq_size decision:"+decision+"   "+mapping.android_listener.op_a_nodes.get(aa)+"   "+mapping.swift_listner.op_s_nodes.get(ss));

									if(decision ==2){
										if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
											Iterator<Interval> itttt = mapping.constraint.values().iterator();
											//									   while(itttt.hasNext()){
											//										   Interval constrainttt = itttt.next();
											//										   if(constrainttt.properlyContains(mapping.swift_listner.op_s_nodes.get(ss).interval)&& constrainttt.length()>mapping.swift_listner.op_s_nodes.get(ss).interval.length())
											mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
											System.err.println("adding blck"+mapping.swift_listner.op_s_nodes.get(ss));
											//									   }

										}
										//												if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
										//													mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
										//												}
										//												System.out.println("SSSBBBB"+mapping.swift_listner.op_s_nodes.get(ss));
									}
									else if(decision ==1){
										if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
											mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
											//											System.out.println("@@@@6"+mapping.android_listener.op_a_nodes.get(aa));

										}
									}
									else if(decision == 0){
										if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
											System.err.println("decision == 0			"+mapping.swift_listner.op_s_nodes.get(ss));
											mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
										}
										if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
											mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
											//											System.out.println("@@@decision == 0");
										}
									}
								}
							}

							/*
							Iterator<Integer> a_keys = mapping.android_to_swift.keySet().iterator();
							while(a_keys.hasNext()){
								int aa=a_keys.next();
								int ss = mapping.android_to_swift.get(aa);
								//									ParserRuleContext adrd = (ParserRuleContext)mapping.android_listener.op_a_nodes.get(aa).node;
								//									ParserRuleContext swft = (ParserRuleContext)mapping.swift_listner.op_s_nodes.get(ss).node;

								//0 both
								//1 aop
								//2 sop
								//									int decision = CommonOperators.determineGoDownbyNum(mapping.android_listener.op_a_nodes.get(aa), mapping.swift_listner.op_s_nodes.get(ss));

								if(aa>=0 && ss>=0){
									int decision = CommonOperators.determineGoDownbyNum(mapping.android_listener.op_a_nodes.get(aa), mapping.swift_listner.op_s_nodes.get(ss));
									//										if(decision!=-1){
									System.out.println("SSBBBB"+decision+"   "+mapping.android_listener.op_a_nodes.get(aa)+ " "+mapping.swift_listner.op_s_nodes.get(ss));
									if(decision ==2){
										if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
											mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
										}
//										if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
//											mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
//										}
										//												System.out.println("SSSBBBB"+mapping.swift_listner.op_s_nodes.get(ss));
									}
									else if(decision ==1){
										if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
											mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
										}
									}
									else if(decision == 0){
										if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
											mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
										}
										if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
											mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
										}
									}
									//										}
								}

							}
							 */
							//								black_s.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
							//								if(min!=null)
							//									mapping.swift_listner.op_s_nodes.remove(min.getKey().b);
						}
						//							black_s.clear();
						//							black_s.addAll(mapping.swift_listner.op_s_nodes);
						//							System.out.println("black_s.addAll(mapping.swift_listner.op_s_nodes)");
						//						flip = true;
					}
					else{






						//							mapping.swift_listner.op_s_nodes.get
						Iterator<Integer> a_keys = mapping.android_to_swift.keySet().iterator();
						while(a_keys.hasNext()){
							int aa=a_keys.next();
							int ss = mapping.android_to_swift.get(aa);
							int decision = CommonOperators.determineGoDownbyNum(mapping.android_listener.op_a_nodes.get(aa).strASTNode, mapping.swift_listner.op_s_nodes.get(ss).strASTNode);

							//							System.out.println("eq_size decision:"+decision+"   "+mapping.android_listener.op_a_nodes.get(aa)+"   "+mapping.swift_listner.op_s_nodes.get(ss));

							if(decision ==-1){
								//do nothiing

							}
							if(decision ==2){
								if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
									//									Iterator<Interval> itttt = mapping.constraint.values().iterator();
									//									   while(itttt.hasNext()){
									//										   Interval constrainttt = itttt.next();
									//										   if(constrainttt.properlyContains(mapping.swift_listner.op_s_nodes.get(ss).interval)&& constrainttt.length()>mapping.swift_listner.op_s_nodes.get(ss).interval.length())
									mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
									System.err.println("decision ==2"+mapping.swift_listner.op_s_nodes.get(ss));
									//									   }

								}
								//												if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
								//													mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
								//												}
								//												System.out.println("SSSBBBB"+mapping.swift_listner.op_s_nodes.get(ss));
							}
							else if(decision ==1){
								if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){

									mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
									//									System.out.println("@@@@ 111");
								}
							}
							else if(decision == 0){
								if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
									mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
									System.err.println("aaaa"+mapping.swift_listner.op_s_nodes.get(ss));
								}
								if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
									mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
									//									System.out.println("@@@@1424");
								}
							}

							else{
								Iterator<Interval> ittt = mapping.constraint_cluster.keySet().iterator();
								while(ittt.hasNext()){
									Interval aaa = ittt.next();
									Interval sss = mapping.constraint_cluster.get(aaa);
									Interval ca = mapping.android_listener.op_a_nodes.get(aa).interval;
									Interval cs = mapping.swift_listner.op_s_nodes.get(ss).interval;
									Interval diff_a=ca.differenceNotProperlyContained(aaa);
									Interval diff_s=cs.differenceNotProperlyContained(sss);
									if(!ca.disjoint(aaa) && !cs.disjoint(sss)){
										System.out.println("ttttt"+mapping.android_listener.op_a_nodes.get(aa)+"  "+mapping.swift_listner.op_s_nodes.get(ss)+"\n"+diff_a+" "+diff_s);;
										if(diff_a.length() < diff_s.length() && !mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss))){
											mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
										}else if(diff_a.length() == diff_s.length() && diff_s.length()!=0){
											if(mapping.swift_listner.op_s_nodes.get(ss).strASTNode.length()>mapping.android_listener.op_a_nodes.get(aa).strASTNode.length() && !mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss))){
												mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
											}else if(mapping.swift_listner.op_s_nodes.get(ss).strASTNode.length()<mapping.android_listener.op_a_nodes.get(aa).strASTNode.length() && !mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))){
												mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
											}
										}
									}
									//									;
								}
							}



						}
						if(output.template_a.equals("<arg0>") && output.template_s.equals("<arg0>")){
							//							mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(0));
							//							System.err.println("cccc");
							//							mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(0));
						}
					}//if same # of




					//					black_a = mapping.android_listener.op_a_nodes;
					//						System.out.println("black_a"+black_a);
					//						System.err.println("black_s"+black_s);

					//					black_s = mapping.swift_listner.op_s_nodes;

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(outputs.size()>0){
				//				System.out.println("MappingOutput\n"+outputs);
				MappingOutput minOutput = Collections.min(outputs);
				//										minOutput.doupdate();
				//					outputs.size();
				//				minOutput.template_a = template;
				MappingOutput last =outputs.get(outputs.size()-1);
				//				last.doupdate();
				//				nextmapping = last.nextMapping;
				//updating next level
				//				MappingOutput.updateNextMapping(connection, nextmapping);
				System.out.println("outputs"+outputs);
				return minOutput;
			}
			else return null;
		}
		return null;
	}


	public static MappingOutput iterationWithConstants(ParserRuleContext ctxt_java, CommonTokenStream javaCommonStream, ParserRuleContext ctxt_swft, CommonTokenStream swiftCommonStream, int cycle
			,List<String> variable_list, List<String> constant_list	,List<String> svariable_list, List<String> sconstant_list
			,String template, String afile, String sfile){

		//		LinkedHashMap<OpProperty, OpProperty> nextmapping = new LinkedHashMap<OpProperty, OpProperty>();
		StructureMappingAntlr4_for_stmt mapping=null;
		if(ctxt_java!=null &&ctxt_swft!=null){
			try {
				mapping = new StructureMappingAntlr4_for_stmt(ctxt_java, ctxt_swft);
				mapping.swiftCommonStream = swiftCommonStream;
				mapping.androidCommonStream = javaCommonStream;

			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			Map<String, String> argmap = MatchingTemplate.getAttributesMap(template, ctxt_java.getText());
			List<String> vals = new LinkedList<String>();
			if(!argmap.isEmpty()){
				System.err.println("temp args"+argmap);

				vals.addAll(argmap.values());
				System.out.println("listring"+vals);

			}

			/** rule1
			 * stop when cost equals
			 */
			List<MappingOutput> outputs = new ArrayList<MappingOutput>();
			double c1 = 1.0000, c2 = 1.0001;
			MappingOutput last_output=null;
			for(int q=0;q<=cycle && mapping!=null;q++){
				boolean templateOk = false;
				boolean flip = true;
				mapping.android_listener.gathering = vals;
				//				for(int q=0;q<=cycle && mapping!=null ;q++){
				try {
					//					System.out.println("test"+q);


					//initialize get next candidates








					mapping.initialize();

					//					mapping.constraint.clear();

					//					Iterator<OpProperty> ita = mapping.android_listener.op_a_nodes;

					System.out.println("temp	"+template);

					System.err.println("op-aaaaaaa"+mapping.android_listener.op_a_nodes);
					System.err.println("op-ssssssss"+mapping.swift_listner.op_s_nodes);

					Iterator<OpProperty> ios = mapping.swift_listner.op_s_nodes.iterator();
					while(ios.hasNext()){
						OpProperty nexts = ios.next();
						if(nexts.interval.length()==1 && Character.isUpperCase(nexts.strASTNode.charAt(0))){
							ios.remove();
						}
					}
					for(OpProperty ops:mapping.swift_listner.op_s_nodes){
						if(Character.isUpperCase(ops.strASTNode.charAt(0))){
							ops.property = "constant_s";
						}
					}
					System.out.println("termsMapOnly"+mapping.android_listener.termsMap);
					System.out.println("a termsMapOnly"+mapping.android_listener.termsMapOnly);
					System.out.println("s termsMapOnly"+mapping.swift_listner.termsMap);
					System.out.println("swft termsMapOnly"+mapping.swift_listner.termsMapOnly);
					System.err.println("constraint"+mapping.constraint);
					System.err.println("aaaablcklista"+mapping.android_listener.blacklist);

					System.out.println("blcklists"+mapping.swift_listner.blacklist);


					//					mapping.nextMapping.clear();
					if(mapping.constraint.size()>0)
						System.out.println("constraint"+mapping.constraint);
					//						mapping.amap_ct
					//mappings
					//						if(mapping.android_listener.op_a_nodes.size()==mapping.swift_listner.op_s_nodes.size())
					c2 =c1;
					if(variable_list !=null){
						for(String varaible:variable_list){
							System.out.println("ttttttesting");
							for(int i=0; i< mapping.android_listener.op_a_nodes.size(); i++){
								if(mapping.android_listener.op_a_nodes.get(i).strASTNode.contains(varaible) && !mapping.android_listener.op_a_nodes.get(i).strASTNode.equals(varaible))
								{
									if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(i))){
										//										mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(i));
									}
									else{

									}
								}
								mapping.android_listener.blacklist.removeIf( (OpProperty op) -> op.orginTxt.equals(varaible));
							}
						}
					}
					if(constant_list !=null){
						for(String constant:constant_list){
							for(int i=0; i< mapping.android_listener.op_a_nodes.size(); i++){
								if(mapping.android_listener.op_a_nodes.get(i).orginTxt.equals(constant)){
									System.err.println("testing");
									OpProperty opa = mapping.android_listener.op_a_nodes.get(i);
									opa.property="constant_a";
									//								System.out.println("constant_a	"+opa);
									mapping.android_listener.op_a_nodes.set(i, opa);
								}


								if(mapping.android_listener.op_a_nodes.get(i).orginTxt.contains(constant) && !mapping.android_listener.op_a_nodes.get(i).orginTxt.equals(constant))
								{
									if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(i))){
										mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(i));
									}

								}


							}
						}
					}

					/*
					if(svariable_list !=null){
						for(String varaible:svariable_list){
							for(int i=0; i< mapping.swift_listner.op_s_nodes.size(); i++){
								if(!mapping.swift_listner.op_s_nodes.get(i).strASTNode.contains(varaible) && !mapping.swift_listner.op_s_nodes.get(i).strASTNode.equals(varaible))
								{
									mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(i));
								}
							}
						}
					}
					 */



					Iterator<Interval> c_a = mapping.constraint.keySet().iterator();
					while(c_a.hasNext()){
						Interval i_c_a = c_a.next();
						Interval i_c_s = mapping.constraint.get(i_c_a);
						Iterator<Pair> iiit = mapping.android_to_swift_point.keySet().iterator();
						while(iiit.hasNext()){
							Pair p = iiit.next();
							if(!p.a_intval.disjoint(i_c_a) && p.b_intval.disjoint(i_c_s)){
								mapping.android_listener.blacklist.add(p.a_op);
								//								count_a++;
							}
							else if(p.a_intval.disjoint(i_c_a) && !p.b_intval.disjoint(i_c_s)){
								mapping.swift_listner.blacklist.add(p.s_op);
								//								count_s++;
								System.out.println("testing11");
							}
							//							Double aa = mapping.android_to_swift_point.get(p);
						}
						for(OpProperty op: mapping.android_listener.op_a_nodes){
							for(OpProperty os: mapping.swift_listner.op_s_nodes){
								if(!op.interval.disjoint(i_c_a) && os.interval.disjoint(i_c_s)){
									mapping.swift_listner.blacklist.add(os);

									//									System.out.println(i_c_a+" "+i_c_s+"@@@@2"+op);

								}
								if(op.interval.disjoint(i_c_a) && !os.interval.disjoint(i_c_s)){
									mapping.android_listener.blacklist.add(op);
									//									System.err.println("adding"+os+" const not met");
									//									mapping.swift_listner.blacklist.add(os);


								}
							}
						}
					}




					/*
					if(sconstant_list !=null){
						for(String constant:sconstant_list){
							for(int i=0; i< mapping.swift_listner.op_s_nodes.size(); i++){
								if(mapping.swift_listner.op_s_nodes.get(i).strASTNode.equals(constant)){
									OpProperty opa = mapping.swift_listner.op_s_nodes.get(i);
									opa.property="constant_s";
									//								System.out.println("constant_a	"+opa);
									mapping.swift_listner.op_s_nodes.set(i, opa);
								}

							}
						}
					}
					 */


					/*
					for(int i=0; i< mapping.swift_listner.op_s_nodes.size(); i++){
						if(mapping.swift_listner.op_s_nodes.get(i).strASTNode.equals("dataSets")){
							//							 OpProperty ops = mapping.swift_listner.op_s_nodes.get(i);
							//							 ops.property="constant_s";
							//							 mapping.swift_listner.op_s_nodes.set(i, ops);

						}
					}
					 */

					MappingOutput output = mapping.compute();
					//					System.out.println("op_a_nodes"+mapping.android_listener.op_a_nodes);
					//					System.out.println("op_s_nodes"+mapping.swift_listner.op_s_nodes);

					if(output!=null){
						c1=output.cost;
						last_output = output;
						//						System.out.println("template a"+output.template_a.replace("{...}", "{}").replaceAll("\\<arg[0-9]+\\>", "arg"));
						//						System.out.println("template s"+output.template_s.replace("{...}", "{}").replaceAll("\\<arg[0-9]+\\>", "arg"));
						//						String template_a = output.template_a.replace("{...}", "{}").replaceAll("<","").replaceAll(">", "");

						//						System.out.println("template a"+output.template_a.replace("{...}", "{}").replaceAll("<","").replaceAll(">", ""));
						//						System.out.println("template s"+output.template_s.replace("{...}", "{}").replaceAll("<","").replaceAll(">", ""));


						Lexer lexer1 				= new TemplateCLexer((CharStream)new ANTLRInputStream(output.template_a));
						DSLErrorListener listner 	= new DSLErrorListener();
						lexer1.addErrorListener(listner);


						TemplateCParser parser 		= new TemplateCParser(new CommonTokenStream(lexer1));
						//				toskip = false;
						//				ExceptionErrorStrategy error_handler = new ExceptionErrorStrategy();
						//				parser.removeErrorListeners();
						//				parser.setErrorHandler(error_handler);

						TemplateCParser.TemplateContext comj = parser.template();



						TemplateCLexer lexer2 = new TemplateCLexer((CharStream)new ANTLRInputStream(output.template_s));
						DSLErrorListener listner_swift = new DSLErrorListener();
						lexer2.addErrorListener(listner_swift);



						TemplateCParser parser2 = new TemplateCParser(new CommonTokenStream(lexer2));
						//				parser.setErrorHandler(error_handler);
						TemplateCParser.TemplateContext com = parser2.template();

						List<String> argSwift = new LinkedList<String>(), argAndroid = new LinkedList<String>();
						ParseTreeWalker walker_android = new ParseTreeWalker(), walker_swift = new ParseTreeWalker();

						walker_android.walk(new TemplateCBaseListener(){
							public void enterArg(ArgContext ctx) {
								//								 System.out.println("ArgContext"+ctx.getText());
								argAndroid.add(ctx.getText());
								//								argA.add(ctx.getText());
								//								diffArgs.add(ctx.getText().replaceAll("\\<", "").replaceAll("\\>", ""));
							};
						}, comj);




						walker_swift.walk(new TemplateCBaseListener(){
							public void enterArg(ArgContext ctx) {
								//								 System.out.println("ArgContext"+ctx.getText());
								argSwift.add(ctx.getText());
								//								argS.add(ctx.getText());
								//								String arg=ctx.getText().replaceAll("\\<", "").replaceAll("\\>", "");
								//								diffArgs.removeIf((String emp) -> emp.equals(arg));
							};
						}, com);


						//						if(!listner_swift.hasErrors() && !listner.hasErrors()){

						if(!(output.template_a.equals("<arg0>") && output.template_s.equals("<arg0>")))
							if(argmap.size()==mapping.ssmap.size())
								outputs.add(output);

						//						}
					}
					//						output.doupdate();




					//						System.out.println("c1:"+c1+"c2:"+c2+"min:"+Math.min(c1,c2));
					//						if(c2==c1)

					//						templateOk=mapping.templateSolver;

					//						Iterator<Pair> mapps 		= mapping.android_to_swift_point.keySet().iterator();
					//						Iterator<Interval> aaaaaa 	= mapping.amap_ct.keySet().iterator();
					Iterator<OpProperty> aaaaaa = mapping.android_listener.op_a_nodes.iterator();
					List<Interval> listc 		= new LinkedList<Interval>();
					HashSet<OpProperty> set 	= new HashSet<OpProperty>();
					LinkedHashMap<String, List<Interval>> constraint = mapping.android_listener.scope_constraint;

					Iterator<String> decls = constraint.keySet().iterator();
					//					System.out.println("constraint"+constraint);
					while(decls.hasNext()){
						listc = constraint.get(decls.next());
						//													System.out.println("listc"+listc);;
						//						boolean isMet = true;
						//check if template have same args
						while(aaaaaa.hasNext()){
							OpProperty anode = aaaaaa.next();
							Interval nnnnn = anode.interval;
							for(int i = 0; i< listc.size();i++){

								if(!listc.get(i).equals(nnnnn) && !listc.get(i).disjoint(nnnnn)){
									//										System.out.println("nnnnn"+mapping.amap_ct.get(nnnnn));
									set.add(anode);
									if(!mapping.android_listener.blacklist.contains(anode))
										mapping.android_listener.blacklist.add(anode);
								}
							}
						}
					}//scoping constraints in java
					//don't exist




					List<Interval> listc_s 		= new LinkedList<Interval>();
					HashSet<OpProperty> set_s 			= new HashSet<OpProperty>();
					LinkedHashMap<String, List<Interval>> constraint_s = mapping.swift_listner.scope_constraint;

					Iterator<String> decls_s = constraint_s.keySet().iterator();
					//										System.out.println("constraint_s"+constraint_s);
					while(decls_s.hasNext()){
						listc_s = constraint_s.get(decls_s.next());
						//						System.out.println("listc_s"+listc_s);;
						Iterator<OpProperty> ssssss = mapping.swift_listner.op_s_nodes.iterator();
						//						boolean isMet = true;
						//check if template have same args
						while(ssssss.hasNext()){
							OpProperty snode = ssssss.next();
							Interval nnnnn = snode.interval;
							//							System.out.println("nnnnn"+nnnnn);
							for(int i = 0; i< listc_s.size();i++){

								if(!listc_s.get(i).equals(nnnnn) && !listc_s.get(i).disjoint(nnnnn)){
									//										System.out.println("nnnnn"+mapping.amap_ct.get(nnnnn));
									set_s.add(snode);
									if(!mapping.swift_listner.blacklist.contains(snode)){
										mapping.swift_listner.blacklist.add(snode);
										System.out.println("nnnnn"+mapping.amap_ct.get(nnnnn));
									}
								}
							}
						}
					}//scoping constraints in swift
					//don't exist

					//					System.out.println("set_s"+set_s);
					/*
					if(set_s.size()>=1){
						for(int i = 0; i< listc_s.size();i++){
							for(OpProperty op: mapping.swift_listner.op_s_nodes)
								if(!listc_s.get(i).equals(op.interval) && !listc_s.get(i).disjoint(op.interval)){
//									mapping.swift_listner.blacklist.addAll(set_s);
																			System.out.println(set_s+" size_s");
								}
						}


						if(mapping.android_to_swift_point.size()>0){
							Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
								public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
									return entry1.getValue().compareTo(entry2.getValue());
								}
							});

//							if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(min.getKey().b))&& !mapping.swift_listner.op_s_nodes.get(min.getKey().b).property.equals("enclosed{}"))
//								mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
							//								System.out.println("black_s"+mapping.swift_listner.blacklist);

						}
					} // un-met .. cases

					 */

					//												System.out.println(set+" sizeof"+set.size()+"amap_ct"+set);
					//if .. not in the same arguments 
					/*
					if(set.size()>=1){
						for(int i = 0; i< listc.size();i++){
							for(OpProperty op: mapping.android_listener.op_a_nodes)
								if(!listc.get(i).equals(op.interval) && !listc.get(i).disjoint(op.interval)){
//									mapping.android_listener.blacklist.addAll(set);
									//										System.out.println(set+" size");
								}
						}


						if(mapping.android_to_swift_point.size()>0){
							Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
								public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
									return entry1.getValue().compareTo(entry2.getValue());
								}
							});

//							if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(min.getKey().b))&& !mapping.swift_listner.op_s_nodes.get(min.getKey().b).property.equals("enclosed{}"))
//								mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
							//								System.out.println("black_s"+mapping.swift_listner.blacklist);

						}
					} // un-met .. cases


					 */
					int anum =0, snum=0;
					if(anum==0 || snum==0){
						anum = mapping.android_listener.op_a_nodes.size();
						snum = mapping.swift_listner.op_s_nodes.size();
					}
					if(anum < snum){
						//							if(flip){
						//						System.out.println("android mapping	"+mapping.android_to_swift_point);
						//							System.out.println("larger"+mapping.android_listener.op_a_nodes);
						//							System.err.println("larger"+mapping.swift_listner.op_s_nodes);

						//							System.out.println("mapping	"+mapping.android_to_swift_point);
						if(mapping.android_to_swift_point.size()>0){

							Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
								public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
									return entry1.getValue().compareTo(entry2.getValue());
								}
							});


							//								System.out.println(min.getKey()+"\n"+);
							//								black_a.clear();
							//								System.out.println("mmmmin	"+min+"  "+min.getKey()+"\n"+mapping.swift_listner.op_s_nodes.get(min.getKey().b)+"\n"+mapping.android_listener.op_a_nodes.get(min.getKey().a));
							//								
							//							System.out.println("mmmmin"+mapping.android_listener.op_a_nodes.get(min.getKey().a));
							if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(min.getKey().a))&& !mapping.android_listener.op_a_nodes.get(min.getKey().a).property.equals("enclosed{}")){
								mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(min.getKey().a));
								//								System.out.println("@@@@4"+mapping.android_listener.op_a_nodes.get(min.getKey().a));
							}
							int h = mapping.android_listener.op_a_nodes.get(min.getKey().a).height;
							for(OpProperty op: mapping.android_listener.op_a_nodes){
								if(op.height==h && !op.property.equals("enclosed{}")){
									if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(min.getKey().a)))
										mapping.android_listener.blacklist.add(op);
									//									System.out.println("@@@@5"+mapping.android_listener.op_a_nodes.get(min.getKey().a));

								}
							}

							//								System.out.println("black_a"+mapping.android_listener.blacklist);
							//								black_a.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
							//								if(min!=null)
							//									mapping.android_listener.op_a_nodes.remove(min.getKey().a);
						}
						//							black_s.addAll(mapping.swift_listner.op_s_nodes);
						//							black_a.addAll(mapping.android_listener.op_a_nodes);
						//						flip = false;
					}

					/** return <>; return <>
					 * for ( <arg0> <arg1> : <arg2> ) {...}  to for <arg1> in <arg2> {...}
					 * 없으면 이대로 .. 있으면 .. 계속. best matching
					 */

					else if(anum >= snum){
						//					else if(mapping.android_listener.op_a_nodes.size() > mapping.swift_listner.op_s_nodes.size()){
						//							System.out.println("swift mapping	"+mapping.android_to_swift_point);
						//							System.out.println(mapping.android_listener.op_a_nodes);
						//							System.out.println(mapping.swift_listner.op_s_nodes);
						if(mapping.android_to_swift_point.size()>0){
							Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
								public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
									return entry1.getValue().compareTo(entry2.getValue());
								}
							});
							//								System.out.println("|a|<|s|"+min.getKey()+"\n"+mapping.swift_listner.op_s_nodes.get(min.getKey().b)+"\n"+mapping.android_listener.op_a_nodes.get(min.getKey().a));
							//								black_s.clear();

							//							if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(min.getKey().b))){

							//							}

							if(mapping.swift_listner.op_s_nodes.get(min.getKey().b)!=null && !mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(min.getKey().b))&& !mapping.swift_listner.op_s_nodes.get(min.getKey().b).property.equals("enclosed{}")){
								//								mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
								System.err.println("adding op_a>op_s"+mapping.swift_listner.op_s_nodes.get(min.getKey().b));
								mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
							}


							else if(mapping.swift_listner.op_s_nodes.get(min.getKey().b)!=null && mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(min.getKey().b))){
								//								mapping.swift_listner.blacklist.add(
								mapping.android_to_swift_point.keySet().remove(min.getKey());
								if(!mapping.android_to_swift_point.isEmpty()){
									min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
										public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
											return entry1.getValue().compareTo(entry2.getValue());
										}
									});


									if(mapping.swift_listner.op_s_nodes.get(min.getKey().b)!=null){
										mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
										int h = mapping.swift_listner.op_s_nodes.get(min.getKey().b).height;
										for(OpProperty op: mapping.swift_listner.op_s_nodes){
											if(op.height==h && !op.property.equals("enclosed{}") && !mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(min.getKey().b))){
												mapping.swift_listner.blacklist.add(op);
												System.err.println("adding op_a>op_s+same_hight"+mapping.swift_listner.op_s_nodes.get(min.getKey().b));

											}
										}

									}
								}

							}
							int h = mapping.swift_listner.op_s_nodes.get(min.getKey().b).height;
							for(OpProperty op: mapping.swift_listner.op_s_nodes){
								if(op.height==h && !op.property.equals("enclosed{}") && !mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(min.getKey().b))){
									mapping.swift_listner.blacklist.add(op);
									System.err.println("adding op_a>op_s+same_hight"+mapping.swift_listner.op_s_nodes.get(min.getKey().b));

								}
							}



							Iterator<Integer> a_keys = mapping.android_to_swift.keySet().iterator();
							while(a_keys.hasNext()){
								int aa=a_keys.next();
								int ss = mapping.android_to_swift.get(aa);

								if(aa >=0 && ss>=0){
									int decision = CommonOperators.determineGoDownbyNum(mapping.android_listener.op_a_nodes.get(aa).strASTNode, mapping.swift_listner.op_s_nodes.get(ss).strASTNode);
									System.out.println("eq_size decision:"+decision+"   "+mapping.android_listener.op_a_nodes.get(aa)+"   "+mapping.swift_listner.op_s_nodes.get(ss));

									if(decision ==2){
										if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
											Iterator<Interval> itttt = mapping.constraint.values().iterator();
											//									   while(itttt.hasNext()){
											//										   Interval constrainttt = itttt.next();
											//										   if(constrainttt.properlyContains(mapping.swift_listner.op_s_nodes.get(ss).interval)&& constrainttt.length()>mapping.swift_listner.op_s_nodes.get(ss).interval.length())
											mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
											System.err.println("adding blck"+mapping.swift_listner.op_s_nodes.get(ss));
											//									   }

										}
										//												if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
										//													mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
										//												}
										//												System.out.println("SSSBBBB"+mapping.swift_listner.op_s_nodes.get(ss));
									}
									else if(decision ==1){
										if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
											mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
											//											System.out.println("@@@@6"+mapping.android_listener.op_a_nodes.get(aa));

										}
									}
									else if(decision == 0){
										if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
											System.err.println("decision == 0			"+mapping.swift_listner.op_s_nodes.get(ss));
											mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
										}
										if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
											mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
											//											System.out.println("@@@decision == 0");
										}
									}
								}
							}

							/*
							Iterator<Integer> a_keys = mapping.android_to_swift.keySet().iterator();
							while(a_keys.hasNext()){
								int aa=a_keys.next();
								int ss = mapping.android_to_swift.get(aa);
								//									ParserRuleContext adrd = (ParserRuleContext)mapping.android_listener.op_a_nodes.get(aa).node;
								//									ParserRuleContext swft = (ParserRuleContext)mapping.swift_listner.op_s_nodes.get(ss).node;

								//0 both
								//1 aop
								//2 sop
								//									int decision = CommonOperators.determineGoDownbyNum(mapping.android_listener.op_a_nodes.get(aa), mapping.swift_listner.op_s_nodes.get(ss));

								if(aa>=0 && ss>=0){
									int decision = CommonOperators.determineGoDownbyNum(mapping.android_listener.op_a_nodes.get(aa), mapping.swift_listner.op_s_nodes.get(ss));
									//										if(decision!=-1){
									System.out.println("SSBBBB"+decision+"   "+mapping.android_listener.op_a_nodes.get(aa)+ " "+mapping.swift_listner.op_s_nodes.get(ss));
									if(decision ==2){
										if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
											mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
										}
//										if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
//											mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
//										}
										//												System.out.println("SSSBBBB"+mapping.swift_listner.op_s_nodes.get(ss));
									}
									else if(decision ==1){
										if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
											mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
										}
									}
									else if(decision == 0){
										if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
											mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
										}
										if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
											mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
										}
									}
									//										}
								}

							}
							 */
							//								black_s.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
							//								if(min!=null)
							//									mapping.swift_listner.op_s_nodes.remove(min.getKey().b);
						}
						//							black_s.clear();
						//							black_s.addAll(mapping.swift_listner.op_s_nodes);
						//							System.out.println("black_s.addAll(mapping.swift_listner.op_s_nodes)");
						//						flip = true;
					}
					else{






						//							mapping.swift_listner.op_s_nodes.get
						Iterator<Integer> a_keys = mapping.android_to_swift.keySet().iterator();
						while(a_keys.hasNext()){
							int aa=a_keys.next();
							int ss = mapping.android_to_swift.get(aa);
							int decision = CommonOperators.determineGoDownbyNum(mapping.android_listener.op_a_nodes.get(aa).strASTNode, mapping.swift_listner.op_s_nodes.get(ss).strASTNode);

							//							System.out.println("eq_size decision:"+decision+"   "+mapping.android_listener.op_a_nodes.get(aa)+"   "+mapping.swift_listner.op_s_nodes.get(ss));

							if(decision ==-1){
								//do nothiing

							}
							if(decision ==2){
								if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
									//									Iterator<Interval> itttt = mapping.constraint.values().iterator();
									//									   while(itttt.hasNext()){
									//										   Interval constrainttt = itttt.next();
									//										   if(constrainttt.properlyContains(mapping.swift_listner.op_s_nodes.get(ss).interval)&& constrainttt.length()>mapping.swift_listner.op_s_nodes.get(ss).interval.length())
									mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
									System.err.println("decision ==2"+mapping.swift_listner.op_s_nodes.get(ss));
									//									   }

								}
								//												if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
								//													mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
								//												}
								//												System.out.println("SSSBBBB"+mapping.swift_listner.op_s_nodes.get(ss));
							}
							else if(decision ==1){
								if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){

									mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
									//									System.out.println("@@@@ 111");
								}
							}
							else if(decision == 0){
								if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
									mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
									System.err.println("aaaa"+mapping.swift_listner.op_s_nodes.get(ss));
								}
								if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
									mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
									//									System.out.println("@@@@1424");
								}
							}

							else{
								Iterator<Interval> ittt = mapping.constraint_cluster.keySet().iterator();
								while(ittt.hasNext()){
									Interval aaa = ittt.next();
									Interval sss = mapping.constraint_cluster.get(aaa);
									Interval ca = mapping.android_listener.op_a_nodes.get(aa).interval;
									Interval cs = mapping.swift_listner.op_s_nodes.get(ss).interval;
									Interval diff_a=ca.differenceNotProperlyContained(aaa);
									Interval diff_s=cs.differenceNotProperlyContained(sss);
									if(!ca.disjoint(aaa) && !cs.disjoint(sss)){
										System.out.println("ttttt"+mapping.android_listener.op_a_nodes.get(aa)+"  "+mapping.swift_listner.op_s_nodes.get(ss)+"\n"+diff_a+" "+diff_s);;
										if(diff_a.length() < diff_s.length() && !mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss))){
											mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
										}else if(diff_a.length() == diff_s.length() && diff_s.length()!=0){
											if(mapping.swift_listner.op_s_nodes.get(ss).strASTNode.length()>mapping.android_listener.op_a_nodes.get(aa).strASTNode.length() && !mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss))){
												mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
											}else if(mapping.swift_listner.op_s_nodes.get(ss).strASTNode.length()<mapping.android_listener.op_a_nodes.get(aa).strASTNode.length() && !mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))){
												mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
											}
										}
									}
									//									;
								}
							}



						}
						if(output.template_s.equals("<arg0>")){
							mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(0));
							//							System.err.println("cccc");
							//							mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(0));
						}
					}//if same # of




					//					black_a = mapping.android_listener.op_a_nodes;
					//						System.out.println("black_a"+black_a);
					//						System.err.println("black_s"+black_s);

					//					black_s = mapping.swift_listner.op_s_nodes;

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(outputs.size()>0){
				//				System.out.println("MappingOutput\n"+outputs);
				MappingOutput minOutput = Collections.min(outputs);
				//										minOutput.doupdate();
				//					outputs.size();
				minOutput.template_a = template;
				MappingOutput last =outputs.get(outputs.size()-1);
				//				last.doupdate();
				//				nextmapping = last.nextMapping;
				//updating next level
				//				MappingOutput.updateNextMapping(connection, nextmapping);
//				System.out.println("outputs"+outputs);
				minOutput.template_a = template;
				return minOutput;
			}
			else {
				if(last_output !=null){
					last_output.template_a = template;
					last_output.template_s = ctxt_swft.getText();
				}
				return last_output;
			}
		}
		return null;
	}




	public List<String> checkJavaSyntax(String template, int atype) throws ClassNotFoundException{



		JavaLexer lexerj = new JavaLexer((CharStream)new ANTLRInputStream(template));
		CommonTokenStream javaCommonStream = new CommonTokenStream(lexerj);
		JavaParser parserj = new JavaParser(javaCommonStream);
		ParserRuleContext comj=null;

		Class<? extends Parser> jparserClass = null;
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		jparserClass = cl.loadClass("javaparser.JavaParser").asSubclass(Parser.class);

		try {
			Method jstartRule = null;

			/**
			 * get ParserRuleContexts by type ids
			 */
			jstartRule = jparserClass.getMethod(JavaParser.ruleNames[atype]);
			//			sstartRule = sparserClass.getMethod(SwiftParser.ruleNames[stype]);

			//			System.out.println("rulej"+JavaParser.ruleNames[atype]);
			//			System.out.println("rules"+SwiftParser.ruleNames[stype]);
			comj 	= (ParserRuleContext)jstartRule.invoke(parserj, (Object[])null);
			//			com 	= (ParserRuleContext)sstartRule.invoke(parser, (Object[])null);
			System.out.println(comj.getText());
		} catch (NoSuchMethodException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (SecurityException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		return null;
	}
	/*
	public List<String> checkSwiftSyntax(String template, int type){

		swiftparser.SwiftLexer lexer1 = new swiftparser.SwiftLexer((CharStream)new ANTLRInputStream(bbb));
		CommonTokenStream swiftCommonStream = new CommonTokenStream(lexer1);
		swiftparser.SwiftParser parser = new swiftparser.SwiftParser(swiftCommonStream);


		JavaLexer lexerj = new JavaLexer((CharStream)new ANTLRInputStream(aaa));
		CommonTokenStream javaCommonStream = new CommonTokenStream(lexerj);
		JavaParser parserj = new JavaParser(javaCommonStream);
		ParserRuleContext com = null, comj=null;

		Class<? extends Parser> jparserClass = null, sparserClass = null;
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		jparserClass = cl.loadClass("javaparser.JavaParser").asSubclass(Parser.class);
		sparserClass = cl.loadClass("swiftparser.SwiftParser").asSubclass(Parser.class);

		try {
			Method jstartRule = null, sstartRule =null;


			jstartRule = jparserClass.getMethod(JavaParser.ruleNames[atype]);
			sstartRule = sparserClass.getMethod(SwiftParser.ruleNames[stype]);

			System.out.println("rulej"+JavaParser.ruleNames[atype]);
			System.out.println("rules"+SwiftParser.ruleNames[stype]);
			comj 	= (ParserRuleContext)jstartRule.invoke(parserj, (Object[])null);
			com 	= (ParserRuleContext)sstartRule.invoke(parser, (Object[])null);

		} catch (NoSuchMethodException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (SecurityException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return null;
	}
	 */
}
