package migration;
import static org.simmetrics.builders.StringMetricBuilder.with;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.InfixExpression.Operator;
import org.simmetrics.StringMetric;
import org.simmetrics.metrics.JaroWinkler;

import alignment.common.HungarianAlgorithm;
import sqldb.ProtoSQLiteJDBC;

//import SwiftParser.Binary_expressionContext;

import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;

//import SwiftParser.Parenthesized_expressionContext;

//import SwiftParser.OperatorContext;
//import SwiftParser.Prefix_expressionContext;

public class MappingExpression {
	public int asize=0;
	public int ssize=0;
	public static String[] OPERATOR={"!", "==","||","&&"};
	Expression androidExpr;
	ParserRuleContext swiftExpr;
	Map<Integer, Integer> typeMap = new HashMap<Integer, Integer>();
	List<String> compA = new LinkedList<String>();
	List<String> compS = new LinkedList<String>();
	List<Expression> compA_ast = new LinkedList<Expression>();
	List<ParserRuleContext> compS_ast = new LinkedList<ParserRuleContext>();

	public String swiftTemplate="";

	List<String> opA = new LinkedList<String>();
	List<String> opS = new LinkedList<String>();

	public void init(){
		//			typeMap.put(Expression.INFIX_EXPRESSION, SwiftParser.);
	}
	public MappingExpression(){

	}
	public MappingExpression(Expression aExpr, ParserRuleContext sExpr){
		androidExpr = aExpr;
		swiftExpr = sExpr;

	}

	public boolean isDividable(Expression androidExpr, SwiftParser.ExpressionContext swiftExpr){

		return false;
	}
	public boolean isDividable(){

		return false;
	}

	public  void recordingExpr(Map<Expression, ParserRuleContext> exprMapping){


		Iterator<Expression> iter = exprMapping.keySet().iterator();

		while(iter.hasNext()){
			Expression and = iter.next();
			ParserRuleContext swft = exprMapping.get(and);
			MappingExpression exprr=new MappingExpression();

			if(and instanceof MethodInvocation && swft instanceof SwiftParser.Function_call_expressionContext){
				System.out.println("SwiftParser.Function_call_expressionContext"+and.toString()+"  "+swft.getText());
			}

			swft.accept(new BaseSwiftRecorder(){
				public Object visitFunction_call_expression(SwiftParser.Function_call_expressionContext ctx) {
					if(and instanceof MethodInvocation){
						exprr.androidExpr 	= and;
						exprr.swiftExpr 	= ctx;
						exprr.computeMethod();
						//						exprr = new MappingExpression(and, ctx);
						//						sexprr.computeMethod();
					}
					return null;
				}
			});

			//two are function call
			if(exprr.androidExpr!=null && exprr.swiftExpr!=null){
				exprr.computeMethod();
			} else if((and instanceof MethodInvocation) && swft instanceof SwiftParser.Postfix_expressionContext){
				System.out.println("not func"+swft.getText()+"  "+and);
				exprr.androidExpr = and; 
				exprr.swiftExpr = swft;
				exprr.computeMethodPrefix();
			}
			else{

				//				exprr.androidExpr = and;

				String e12 = ProtoSQLiteJDBC.insertTableExprAndroidV(MappingSingleStatement.expr_id, and.toString(),  and.toString(), and.toString(), Main.androidClassName+"."+MappingDeclaration.methodnameAndroid);
				String e11 = ProtoSQLiteJDBC.insertTableExprSwiftV(MappingSingleStatement.expr_id,  swft.getText(),  swft.getText(), MappingSingleStatement.expr_id, "");


				try{
					Connection c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
					java.sql.Statement sql_stmt = c.createStatement();
					//					sql_stmt.executeUpdate(e12);	
					//					sql_stmt.executeUpdate(e11);
					//					MappingSingleStatement.expr_id++;
				} catch(java.sql.SQLException e){
					e.printStackTrace();
					System.out.println(e11);
					System.out.println(e12);
				}
			}
		}
	}
	public void computeMethodPrefix(){
		StringMetric emetric =
				with(new JaroWinkler())
				.build();
		//		String receiver="";
		List<String> expr_aa = new LinkedList<String>();
		List<String> expr_ss = new LinkedList<String>();
		if(((MethodInvocation) androidExpr).getExpression()!=null)
			expr_aa.add(((MethodInvocation) androidExpr).getExpression().toString());
		List expraa = ((MethodInvocation) androidExpr).arguments();
		for(Object o:expraa){
			expr_aa.add(o.toString());
		}
		StringBuffer buffer = new StringBuffer();
		swiftExpr.accept(new BaseSwiftRecorder(){
			@Override
			public Object visitPrimary_expression(SwiftParser.Primary_expressionContext ctx) {
				// TODO Auto-generated method stub
				String swift_receiver=ctx.getText();
				//				swift_receiver.charAt(0)
				if(Character.isLowerCase(swift_receiver.charAt(0))){
					expr_ss.add(swift_receiver);
				}
				return super.visitPrimary_expression(ctx);
			}
		});

		//		System.out.println("buffer"+buffer);

		double[][] ecostMatrix = new double[expr_aa.size()][expr_ss.size()];
		for(int i=0;i<expr_aa.size();i++){
			for(int j=0;j<expr_ss.size();j++){
				ecostMatrix[i][j]= 1- emetric.compare(expr_aa.get(i).toString(), expr_ss.get(j));
			}
		}
		HungarianAlgorithm ehung = null;
		int[] eresult = new int[0];
		if(expr_aa.size() > 0 && expr_ss.size() >0){
			ehung = new HungarianAlgorithm(ecostMatrix);
			eresult = ehung.execute();
		}
		//		List<String> ids = new LinkedList<String>();




		Map<String, String> amap = new HashMap<String, String>();
		Map<String, String> aamap = new HashMap<String, String>();
		Map<String, String> smap = new HashMap<String, String>();
		int argnum = 0;
		for(int p=0; p<eresult.length;p++){
			if(eresult[p]!=-1){
				String aifexpr= expr_aa.get(p).toString().replace(" ", "");	
				String sifexpr=expr_ss.get(eresult[p]).replace(" ", "");	
				System.out.println(expr_aa.size()+aifexpr+" &&&&& "+expr_ss.size()+sifexpr);
				amap.put(aifexpr, "<arg"+argnum+">");
				//				aamap.put(aifexpr, "arg"+argnum);
				smap.put(sifexpr, "<arg"+argnum+">");
				argnum++;
			}
		}

		Iterator<String> aiter = amap.keySet().iterator();
		Iterator<String> siter = smap.keySet().iterator();
		String aexp=androidExpr.toString().replace(" ", "");
		String sexp=swiftExpr.getText().replace(" ", "");
		while(aiter.hasNext()){
			String expr=aiter.next();
			//			amap.get(expr);
			String ptt = expr.replace(" ", "");
			expr=expr.replaceAll("\\(", "\\\\(");
			expr=expr.replaceAll("\\)", "\\\\)");
			expr=expr.replaceAll("\\.", "\\\\.");

			expr=expr.replaceAll("\\*", "\\\\*");
			expr=expr.replaceAll("/", "\\\\/");
			expr=expr.replaceAll("\\[", "\\\\[");
			expr=expr.replaceAll("\\]", "\\\\]");

			//			expr=expr.replaceAll("\\", "\\b");

			//			androidExpr.re
			System.out.println("aexp"+aexp+"  "+expr+"  "+amap.get(ptt)+"  "+ptt);
			aexp = aexp.replaceAll("\\b"+expr+"\\b",amap.get(ptt));
		}

		while(siter.hasNext()){
			String expr=siter.next();
			//			smap.get(expr);
			String ptt = expr;
			expr=expr.replaceAll("\\(", "\\\\(");
			expr=expr.replaceAll("\\)", "\\\\)");
			expr=expr.replaceAll("\\.", "\\\\.");
			//			expr=expr.replaceAll("\\+", "\\\\+");
			//			expr=expr.replaceAll("\\-", "\\\\-");
			expr=expr.replaceAll("\\*", "\\\\*");
			expr=expr.replaceAll("\\[", "\\\\[");
			expr=expr.replaceAll("\\]", "\\\\]");
			expr=expr.replaceAll("/", "\\\\/");
			//			expr=expr.replaceAll("\\", "\\b");

			//			androidExpr.re
			sexp = sexp.replaceAll("\\b"+expr+"\\b",smap.get(ptt));
		}

		MethodInvocation mmmm = (MethodInvocation)androidExpr;
		//		mmmm.argume
		IMethodBinding m11=null;



		if(mmmm.resolveMethodBinding()!=null)
			m11= mmmm.resolveMethodBinding().getMethodDeclaration();
		String mbinding ="";
		String atemp="";
		if(m11!=null){
			mbinding=m11.getDeclaringClass().getName()+".";
			mbinding+=m11.getName()+"(";

			if(mmmm.getExpression()!=null){
				String expm=amap.get(mmmm.getExpression().toString());
				System.out.println(expr_aa+"  "+expm+"(mmmm.getExpression()"+mmmm.getExpression().toString());
				if(expm!=null){
					atemp =expm+"."+m11.getName()+"(";
				} else{
					atemp =mmmm.getExpression().toString()+"."+m11.getName()+"(";
				}
			}else{
				atemp = mmmm.getName()+"(";
			}

			ITypeBinding[] pt = m11.getParameterTypes();
			//			StringUtils.join(pt, ", ");
			ArrayList<String> types = new ArrayList<String>();

			for(ITypeBinding b:pt){
				types.add(b.getName());

			}
			mbinding+= StringUtils.join(types, ",");
			mbinding+=")";
			//			return mbinding;
		} //DB Key of android, ex) AxisRenderer.computeAxisValues(float, float)


		System.out.println(sexp+" sexpsexp "+aexp);



		String e12 = ProtoSQLiteJDBC.insertTableExprAndroidV(MappingSingleStatement.expr_id, androidExpr.toString(),  aexp,  mbinding, Main.androidClassName+"."+MappingDeclaration.methodnameAndroid);
		String e11 = ProtoSQLiteJDBC.insertTableExprSwiftV(MappingSingleStatement.expr_id,  swiftExpr.getText(),  sexp, MappingSingleStatement.expr_id, "");


		try{
			Connection c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
			java.sql.Statement sql_stmt = c.createStatement();

			sql_stmt.executeUpdate(e12);	
			sql_stmt.executeUpdate(e11);
			MappingSingleStatement.expr_id++;
		} catch(java.sql.SQLException e){
			e.printStackTrace();
			System.out.println(e11);
			System.out.println(e12);
		}



	}


	public void computeMethod(){

		try{
		StringMetric emetric =
				with(new JaroWinkler())
				.build();
		//		String receiver="";
		List<String> expr_aa = new LinkedList<String>();
		List<String> expr_ss = new LinkedList<String>();
		if(((MethodInvocation) androidExpr).getExpression()!=null)
			expr_aa.add(((MethodInvocation) androidExpr).getExpression().toString());
		List expraa = ((MethodInvocation) androidExpr).arguments();
		for(Object o:expraa){
			expr_aa.add(o.toString());
		}






		//		expr_aa.add(method);
		SwiftParser.Function_call_expressionContext func_swiftExpr=(SwiftParser.Function_call_expressionContext) swiftExpr;
		List<SwiftParser.Expression_elementContext> exprss = null;
		
//		if(func_swiftExpr.parenthesized_expression() !=null && func_swiftExpr.parenthesized_expression().expression_element_list()!=null);
			exprss =func_swiftExpr.parenthesized_expression().expression_element_list().expression_element();

		for(SwiftParser.Expression_elementContext e:exprss){
			expr_ss.add(e.expression().getText());
		}
		
		BaseSwiftRecorder record =  new BaseSwiftRecorder(){
			@Override
			public Object visitPrimary_expression(SwiftParser.Primary_expressionContext ctx) {
				// TODO Auto-generated method stub
				String swift_receiver=ctx.getText();
				//				swift_receiver.charAt(0)
				if(Character.isLowerCase(swift_receiver.charAt(0))){
					saveValue = swift_receiver;
				}
				return null;
			}
		};
		func_swiftExpr.postfix_expression().accept(record);
		if(!record.saveValue.equals(""))
			expr_ss.add(record.saveValue);

		//		expr_ss.add(func_swiftExpr.postfix_expression().getText());


		//				int m = blocks_a.size() < blocks_s.size()?blocks_a.size():  blocks_s.size();
		double[][] ecostMatrix = new double[expr_aa.size()][expr_ss.size()];

		//				int aaa = blocks_aa.size();
		//				int bbb = blocks_ss.size();

		for(int i=0;i<expr_aa.size();i++){
			for(int j=0;j<expr_ss.size();j++){
				ecostMatrix[i][j]= 1- emetric.compare(expr_aa.get(i).toString(), expr_ss.get(j));
			}
		}
		HungarianAlgorithm ehung = null;
		int[] eresult = new int[0];
		if(expr_aa.size() > 0 && expr_ss.size() >0){
			ehung = new HungarianAlgorithm(ecostMatrix);
			eresult = ehung.execute();
		}
		//		List<String> ids = new LinkedList<String>();




		Map<String, String> amap = new HashMap<String, String>();
		Map<String, String> aamap = new HashMap<String, String>();
		Map<String, String> smap = new HashMap<String, String>();
		int argnum = 0;
		for(int p=0; p<eresult.length;p++){
			if(eresult[p]!=-1){
				String aifexpr= expr_aa.get(p).toString().replace(" ", "");	
				String sifexpr=expr_ss.get(eresult[p]).replace(" ", "");	
				System.out.println(expr_aa.size()+aifexpr+" @@@@***** "+expr_ss.size()+sifexpr);
				amap.put(aifexpr, "<arg"+argnum+">");
				aamap.put(aifexpr, "arg"+argnum);
				smap.put(sifexpr, "<arg"+argnum+">");
				argnum++;
			}
		}
		String exprFunc = func_swiftExpr.postfix_expression().getText();
		//		String aa= record.saveValue;
		if(smap.get(record.saveValue)!=null){
			exprFunc = exprFunc.replaceAll("\\b"+record.saveValue+"\\b",smap.get(record.saveValue));
		}
		exprFunc +="(";
		if(func_swiftExpr.parenthesized_expression()!=null &&func_swiftExpr.parenthesized_expression().expression_element_list()!=null){
			//							ctx.parenthesized_expression().expression_element_list()

			List<SwiftParser.Expression_elementContext> ele = func_swiftExpr.parenthesized_expression().expression_element_list().expression_element();
			//ele.expression_element()
			//			mappingExpr.ssize=ele.size();
			List<String> ids = new LinkedList<String>();
			for(SwiftParser.Expression_elementContext ee: ele){
				//				if(ee.expression()!=null)
				//					System.err.println("expr	"+ee.expression().getText());
				if(ee.identifier()!=null){
					//					System.err.println("identifier	"+ee.identifier().getText());
					//									exprFunc +=ee.identifier().getText()+" ,";
					String args = smap.get(ee.expression().getText());
					if(args!=null){
						ids.add(ee.identifier().getText()+":"+args);
					}else{
						ids.add(ee.identifier().getText()+":"+ee.identifier().getText());
					}
				}else{
					String args = smap.get(ee.expression().getText());
					if(args!=null){
						ids.add(args);
					}else{
						if(ee.expression()!=null)
							ids.add(ee.expression().getText());
					}
					//									ids.add(ee.identifier().getText()+":"+ee.identifier().getText());
				}
				//				System.out.println(ee.getText());

			}
			exprFunc+= StringUtils.join(ids, ",");
			//							System.out.println("final"+exprFunc+")");
			exprFunc+=")";
			//			mappingExpr.swiftTemplate = exprFunc;

		}else{
			exprFunc+=")";
		}

		//		expr= expr.replaceAll("\\b("+varname+")\\b",type);


		MethodInvocation mmmm = (MethodInvocation)androidExpr;
		//		mmmm.argume
		IMethodBinding m11=null;



		if(mmmm.resolveMethodBinding()!=null)
			m11= mmmm.resolveMethodBinding().getMethodDeclaration();
		String mbinding ="";
		String atemp="";
		if(m11!=null){
			mbinding=m11.getDeclaringClass().getName()+".";
			mbinding+=m11.getName()+"(";

			if(mmmm.getExpression()!=null){
				String expm=amap.get(mmmm.getExpression().toString());
				System.out.println(expr_aa+"  "+expm+"(mmmm.getExpression()"+mmmm.getExpression().toString());
				if(expm!=null){
					atemp =expm+"."+m11.getName()+"(";
				} else{
					atemp =mmmm.getExpression().toString()+"."+m11.getName()+"(";
				}
			}else{
				atemp = mmmm.getName()+"(";
			}

			ITypeBinding[] pt = m11.getParameterTypes();
			//			StringUtils.join(pt, ", ");
			ArrayList<String> types = new ArrayList<String>();

			for(ITypeBinding b:pt){
				types.add(b.getName());

			}
			mbinding+= StringUtils.join(types, ",");
			mbinding+=")";
			//			return mbinding;
		} //DB Key of android, ex) AxisRenderer.computeAxisValues(float, float)
		mmmm.arguments();
		ArrayList<String> args_a = new ArrayList<String>();
		for(Object arg :mmmm.arguments()){
			String ggg=amap.get(arg.toString().replace(" ", ""));
			if(ggg==null){
				args_a.add(arg.toString());
			}else{
				args_a.add(ggg);
			}
		}

		atemp += StringUtils.join(args_a, ",");
		atemp += ")";
		Iterator<String> aiter = amap.keySet().iterator();
		Iterator<String> siter = smap.keySet().iterator();
		String aexp=androidExpr.toString().replace(" ", "");
		String sexp=swiftExpr.getText().replace(" ", "");
		while(aiter.hasNext()){
			String expr=aiter.next();
			//			amap.get(expr);
			String ptt = expr.replace(" ", "");
			expr=expr.replaceAll("\\(", "\\\\(");
			expr=expr.replaceAll("\\)", "\\\\)");
			expr=expr.replaceAll("\\.", "\\\\.");

			expr=expr.replaceAll("\\*", "\\\\*");
			expr=expr.replaceAll("/", "\\\\/");
			expr=expr.replaceAll("\\[", "\\\\[");
			expr=expr.replaceAll("\\]", "\\\\]");

			//			expr=expr.replaceAll("\\", "\\b");

			//			androidExpr.re
			System.out.println("aexp"+aexp+"  "+expr+"  "+amap.get(ptt)+"  "+ptt);
			aexp = aexp.replaceAll("\\b"+expr+"\\b",amap.get(ptt));
		}

		while(siter.hasNext()){
			String expr=siter.next();
			//			smap.get(expr);
			String ptt = expr;
			expr=expr.replaceAll("\\(", "\\\\(");
			expr=expr.replaceAll("\\)", "\\\\)");
			expr=expr.replaceAll("\\.", "\\\\.");
			//			expr=expr.replaceAll("\\+", "\\\\+");
			//			expr=expr.replaceAll("\\-", "\\\\-");
			expr=expr.replaceAll("\\*", "\\\\*");
			expr=expr.replaceAll("\\[", "\\\\[");
			expr=expr.replaceAll("\\]", "\\\\]");
			expr=expr.replaceAll("/", "\\\\/");
			//			expr=expr.replaceAll("\\", "\\b");

			//			androidExpr.re
			sexp = sexp.replaceAll("\\b"+expr+"\\b",smap.get(ptt));
		}


		/*
		androidExpr.accept(new ASTVisitor() {

			@Override
			public boolean visit(SimpleName node) {
				// TODO Auto-generated method stub
				String token=aamap.get(node.toString());
				if(token!=null){
					node.setIdentifier(token);
				}
				return super.visit(node);
			}
//			visit(MethodInvocation
			//			visit

		});
		 */

		System.out.println("making template of method map"+atemp+" "+mbinding+" "+exprFunc+"  "+aexp+"  "+sexp);


		String e12 = ProtoSQLiteJDBC.insertTableExprAndroidV(MappingSingleStatement.expr_id, androidExpr.toString(),  atemp,  mbinding, Main.androidClassName+"."+MappingDeclaration.methodnameAndroid);
		String e11 = ProtoSQLiteJDBC.insertTableExprSwiftV(MappingSingleStatement.expr_id,  swiftExpr.getText(),  exprFunc, MappingSingleStatement.expr_id, "");


		try{
			Connection c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
			java.sql.Statement sql_stmt = c.createStatement();

			sql_stmt.executeUpdate(e12);	
			sql_stmt.executeUpdate(e11);
			MappingSingleStatement.expr_id++;
		} catch(java.sql.SQLException e){
			e.printStackTrace();
			System.out.println(e11);
			System.out.println(e12);
		}
		
		}catch (java.lang.NullPointerException e) {
			System.out.println("computeMethod()");
			e.printStackTrace();
		}
	}//method invocation, function call

	public void compute(){
		if(swiftExpr==null || androidExpr==null ) return;

		swiftExpr.accept(new BaseSwiftRecorder(){

			@Override
			public Object visitBinary_expression(SwiftParser.Binary_expressionContext ctx) {
				// TODO Auto-generated method stub
				if(ctx.binary_operator()==null){
					//					compS.add(ctx.binary_operator().getText());

					//					compS_ast.add(ctx.binary_operator());
					//					opS.add(ctx.binary_operator().getText());

				}
				//				compS_ast.add(ctx.prefix_expression());
				return super.visitBinary_expression(ctx);
			}
			@Override
			public Object visitPrefix_expression(SwiftParser.Prefix_expressionContext ctx) {
				// TODO Auto-generated method stub


				String expr =ctx.getText();
				if(!StringUtils.isNumeric(expr)){
					this.saveValue = null;
					ctx.postfix_expression().accept(new BaseSwiftRecorder(){
						public Object visitFunction_call_expression(SwiftParser.Function_call_expressionContext ctx) {
							compS_ast.add(ctx);
							this.saveValue = "not";
							return null;
						};
					});
					if(this.saveValue==null)
						compS_ast.add(ctx.postfix_expression());
				}

				if(ctx.prefix_operator()==null){
					//					compS.add(ctx.prefix_operator().getText());
					//					opS.add(ctx.prefix_operator().getText());

					//					compS_ast.add(ctx.prefix_operator());
				}
				compS.add(ctx.postfix_expression().getText());

				return super.visitPrefix_expression(ctx);
			}


			@Override
			public Object visitOperator(SwiftParser.OperatorContext ctx) {
				// TODO Auto-generated method stub
				//					compS.add(ctx.getText());
				opS.add(ctx.getText());
				return super.visitOperator(ctx);
			}

		});


		androidExpr.accept(new ASTVisitor() {
			@Override
			public boolean visit(QualifiedName node) {
				// TODO Auto-generated method stub
				compA.add(node.toString());
				compA_ast.add(node);
				return false;
			}
			@Override
			public boolean visit(MethodInvocation node) {
				// TODO Auto-generated method stub
				compA.add(node.toString());
				compA_ast.add(node);
				return false;
			}
			@Override
			public boolean visit(SimpleName node) {
				// TODO Auto-generated method stub
				compA.add(node.toString());
				compA_ast.add(node);
				return super.visit(node);
			}

			@Override
			public boolean visit(InfixExpression node) {
				// TODO Auto-generated method stub
				//				compA.add(node.getOperator().toString());
				//					Operator o = node.getOperator();
				//				compA_ast.add(node.getOperator());
				opA.add(node.getOperator().toString());
				return super.visit(node);
			}

			@Override
			public boolean visit(PrefixExpression node) {
				// TODO Auto-generated method stub
				//				compA.add(node.getOperator().toString());
				//				compA_ast.add(node.getOperator());
				opA.add(node.getOperator().toString());
				return super.visit(node);
			}

			@Override
			public boolean visit(PostfixExpression node) {
				// TODO Auto-generated method stub
				//				compA.add(node.getOperator().toString());
				//				compA_ast.add(node.getOperator());
				opA.add(node.getOperator().toString());
				return super.visit(node);
			}

		});
		//			swiftExpr.prefix_expression();
		//			swiftExpr.binary_expressions().binary_expression();
		//		System.out.println("test Assign"+compA_ast.size()+"	"+compS_ast.size()+(opA.containsAll(opS)&&opS.containsAll(opA)));
		System.out.println(androidExpr.toString()+"#***"+swiftExpr.getText());
		System.out.println("OPS"+opA+"  "+opS+"  "+(opA.containsAll(opS)&&opS.containsAll(opA)&&opA.size()>0));
		System.out.println("ast"+compA_ast);
		for(ParserRuleContext a:compS_ast){
			System.out.println("asts"+a.getText());
		}
		recordingExpr(getASTMap());
	}

	public Map<Expression, ParserRuleContext> getASTMap(){
		if(opA.containsAll(opS)&&opS.containsAll(opA)&&(opA.size()>0)){ //divide it
			return AssignmentList.assignmentAst(compA_ast, compS_ast);
		} else{
			Map<Expression, ParserRuleContext> hclassmapping = new HashMap<Expression, ParserRuleContext>();
			hclassmapping.put(androidExpr, swiftExpr);
			System.out.println("same"+swiftExpr.getText()+"  "+androidExpr);
			return hclassmapping;
		}
	}


	public static void main(String arg[]){
		String expr ="isInBoundsX(x)&&isInBoundsY(y);";
		String ptt ="isInBoundsX(x)&&isInBoundsY(y)";
		ptt=ptt.replaceAll("\\(", "\\\\(");
		ptt=ptt.replaceAll("\\)", "\\\\)");
		ptt=ptt.replaceAll("\\.", "\\\\.");
		ptt=ptt.replaceAll("\\+", "\\\\+");
		ptt=ptt.replaceAll("\\-", "\\\\-");
//		ptt=ptt.replaceAll("\\s", "\\\\s");
		ptt=ptt.replaceAll("/", "\\\\/");
		ptt=ptt.replaceAll("\\&", "\\\\&");
		//ptt=ptt.replaceAll("\b", "\\b");
		//		"\\b"+expr+"\\b"
		String pt = "\\b"+ptt+"\\b";
		//		aexpmLimitLineClippingRect.inset(0.f,-l.getLineWidth()/2.f)  \-l\.getLineWidth\(\)\/2\.f  <arg2>
		System.out.println(pt+"			"+expr.replaceAll(ptt, "<arg2>"));
		System.out.println(ptt);
	}
}
