package migration;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.TerminalNode;

import alignment.common.LineCharIndex;
import alignment.common.OpProperty;
import antlr_parsers.javaparser.JavaBaseListener;
import antlr_parsers.javaparser.JavaParser;
import antlr_parsers.javaparser.JavaParser.CompilationUnitContext;

public class RewriterJava extends JavaBaseListener{
	public String orginText;
	public Stack<ParserRuleContext> pstack = new Stack<ParserRuleContext>();
//	public Stack/<ParserRuleContext> pstack = new Stack<ParserRuleContext>();
	public LinkedHashMap<LineCharIndex, OpProperty> maps = new LinkedHashMap<LineCharIndex, OpProperty>();
	public List<String> test = new LinkedList<String>();
	public List<OpProperty>  constants = new LinkedList<OpProperty>();
	public Stack<String> stack = new Stack<String>();
	public Map<String, OpProperty>   vars = null;
//	JavaParser jparser;
	public RewriterJava(){
	//			plist = list;
	//			jparser=parser;
	}
	
	@Override
	public void enterEveryRule(ParserRuleContext ctx) {
//		int az = ctx.start.getStartIndex();
//		int bz = ctx.stop.getStopIndex();
//		Interval interval = new Interval(az,bz);
//		System.out.println("enterEveryRule"+ctx.getText()+" :"+ctx.getRuleIndex()+ctx.getText());
		
		// TODO Auto-generated method stub

		super.enterEveryRule(ctx);
	}
	
	@Override
	public void visitTerminal(TerminalNode node) {
		// TODO Auto-generated method stub
		LineCharIndex lineindex=new LineCharIndex(node.getSymbol().getLine(), node.getSymbol().getCharPositionInLine(),
				node.getSymbol().getLine(),node.getSymbol().getStopIndex());
//		Interval interval = new Interval(az,bz);
//		pstack.push(ctx);
//		LineCharIndex lineindex=new LineCharIndex(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(),
//				ctx.getStop().getLine(),ctx.getStop().getStopIndex());
		maps.put(lineindex, new OpProperty(node, node.getText(),"none"));
		
		if(vars!=null){
			boolean isDis = true;
			Iterator<OpProperty> vv = vars.values().iterator();
			while(vv.hasNext() && isDis){
				OpProperty nxt = vv.next();
				isDis = nxt.interval.disjoint(node.getSourceInterval());
			}
			if(isDis==true){
				constants.add(new OpProperty(node, node.getText(),"none"));
			}
	
		}
		super.visitTerminal(node);
	}
	@Override
	public void exitEveryRule(ParserRuleContext ctx) {
//		MigrationExamples.typeJ = ctx;
//		jparser.setContext(ctx);
//		System.out.println("every"+jparser.getContext().getRuleIndex());
		int az = ctx.start.getStartIndex();
		int bz = ctx.stop.getStopIndex();
		Interval interval = new Interval(az,bz);
		pstack.push(ctx);
		LineCharIndex lineindex=new LineCharIndex(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(),
				ctx.getStop().getLine(),ctx.getStop().getStopIndex());
		maps.put(lineindex, new OpProperty(ctx, ctx.getStart().getInputStream().getText(interval),"none"));
		
//		if(test.size()>0)
//			test.add(test.size(), ctx.getText());
		
		stack.push(ctx.getText());
//		System.err.println("exitEveryRule"+ctx.getText()+" ->> "+lineindex);
//		plist.add(ctx);
//		System.err.println("plist"+plist);
		// TODO Auto-generated method stub
		super.exitEveryRule(ctx);
	}
}
