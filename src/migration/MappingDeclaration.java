package migration;

import static org.simmetrics.builders.StringMetricBuilder.with;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.StringJoiner;
import java.util.TreeMap;
import java.util.TreeSet;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.eclipse.jdt.core.compiler.InvalidInputException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.BooleanLiteral;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jdt.core.dom.TextElement;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.internal.compiler.parser.Scanner;
import org.eclipse.jdt.internal.compiler.parser.TerminalTokens;
import org.simmetrics.StringMetric;
import org.simmetrics.metrics.JaroWinkler;

import alignment.common.HungarianAlgorithm;
//import SwiftParser.ExpressionContext;
//import SwiftParser.Variable_declarationContext;
import android.database.CharArrayBuffer;
import mapping.MappingElementExpr;
import mapping.MappingMethods;
//import recording.SwiftParser;
import sqldb.SQLiteJDBC;

public class MappingDeclaration{
	FieldDeclaration android;
	SwiftParser.Variable_declarationContext swift;
	public static Map<String, String> localtypeMapAndroid 		= new HashMap<String, String>();
	public static Map<String, String> localtypeMapSwift 		= new HashMap<String, String>();
	public static Map<MappingElement, MappingElement> mappingElements1 = new HashMap<MappingElement, MappingElement>();
	public static Map<String, ASTNode> varDeclAndroid 			= new HashMap<String, ASTNode>();
	public static Map<String, ParserRuleContext> varDeclSwift 	= new HashMap<String, ParserRuleContext>();
	public static String methodnameAndroid;
	public static String methodnameSwift;

	//	public static MappingStatements  map_stmts = new MappingStatements();
	public static int sid;

	public String getType(FieldDeclaration f, String name){
		RecordVarDeclAndroid recordAndroid = new RecordVarDeclAndroid();
		f.accept(recordAndroid);
		if(name.equals(recordAndroid.record.get(2).label)){
			return recordAndroid.record.get(1).label;
		}
		return null;
	}

	public String getTypeSwift(SwiftParser.Variable_declarationContext swift, String name){

		RecordVarDeclSwift recordSwift = new RecordVarDeclSwift<>();
		swift.accept(recordSwift);

		if(name.equals(((MappingElement)recordSwift.record.get(2)).label.toString())){
			return ((MappingElement)recordSwift.record.get(3)).label;
		}
		return null;
	}

	public MappingDeclaration(FieldDeclaration a, SwiftParser.Variable_declarationContext s){
		android = a;
		swift 	= s;
	}

	Map<Integer, Integer> map_types	= new HashMap<Integer, Integer>();
	public MappingDeclaration(){
		map_types.put(25, 18);
		map_types.put(21, 76);
		map_types.put(41, 36);
	}

	String params_android="";
	String stms_android ="";
	String name_android ="";
	String modifiers_android="";
	String type_android="";

	String params_swift="";
	String stms_swift ="";
	String name_swift ="";
	String modifiers_swift="";
	String type_swift="";
	String return_android="NULL";

	List<org.eclipse.jdt.core.dom.Statement> android_stmts_list =new LinkedList<org.eclipse.jdt.core.dom.Statement>();

	public void setElementMethodDecl(ASTNode a, ParserRuleContext s) throws IOException, SQLException, ClassNotFoundException, InterruptedException{
		Map<String, String> recordAndroidParams	= new HashMap<String, String>();
		Map<String, String> recordSwiftParams	= new HashMap<String, String>();
		LinkedList<MappingElement> recordStatement = new LinkedList<MappingElement> ();

		
		StructureMapping strt = new StructureMapping(a, s);
		strt.compute();
		
		LinkedList<MappingElement> record = new LinkedList<MappingElement> ();
		recordAndroidParams.clear();
		recordSwiftParams.clear();
		a.accept(new ASTVisitor() {

			@Override
			public boolean visit(MethodDeclaration node) {
				// TODO Auto-generated method stub
				name_android 		= node.getName().toString();



				List<SingleVariableDeclaration> params = node.parameters();
				for(SingleVariableDeclaration varr:params){		
					recordAndroidParams.put(varr.getName().toString(), varr.getType().toString());
				}
				if(node.getReturnType2()!=null)
					type_android 		= node.getReturnType2().toString();
				modifiers_android 	= node.modifiers().toString();
				if(!node.parameters().isEmpty()){
					params_android 	= node.parameters().toString().replace("\n", " ");
				}
				if( node.getBody()!=null && node.getBody().statements()!=null){
					stms_android 	= node.getBody().statements().toString().replace("\n", "");
					android_stmts_list = node.getBody().statements();
				}
				if(node.getReturnType2()!=null){
					return_android =node.getReturnType2().toString(); 
				}
				return super.visit(node);
			}
		});//record android method


		Connection c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		Statement stmt = c.createStatement();

		c.setAutoCommit(true);

		//		System.out.println("recordAndroidParams\n"+recordAndroidParams);

		//		Iterator<String> pa_iter = recordAndroidParams.keySet().iterator();
		RecordMethodSwift rrr = new RecordMethodSwift<>();
		s.accept(rrr);

		name_swift = RecordMethodSwift.name_swift;


		Iterator<String> ps_iter = rrr.recordSwiftParams.keySet().iterator();
		MappingStatements map_stmts = new MappingStatements();
		//		android_stmts_list, rrr.stmt_list
		//		map_stmts.localtypeMapAndroid = recordAndroidParams;
		//		map_stmts.localtypeMapSwift = rrr.recordSwiftParams;
		map_stmts.swift_stmts_list = rrr.stmt_list;
		map_stmts.android_stmts_list =android_stmts_list;
		//		map_stmts.returnAndroid = return_android;
		//		map_stmts.returnSwift = rrr.returnType;
		//		map_stmts.classnameAndroid = Main.androidClassName;
		//		map_stmts.classnameSwift = Main.swiftClassName;

		methodnameAndroid = name_android;
		methodnameSwift = name_swift;


		map_stmts.mapping();
		Map<String, String> mm_va = new HashMap<String, String>();
		//		System.out.println(mm_va+"::::"+recordAndroidParams+",,"+rrr.recordSwiftParams);
		if(recordAndroidParams.size()==rrr.recordSwiftParams.size()){
			mm_va = MappingMethods.mapping(recordAndroidParams, rrr.recordSwiftParams);
			//			System.out.println(map_stmts.localtypeMapAndroid+":::"+map_stmts.localtypeMapSwift);
//			System.out.println(mm_va+"+::::+"+recordAndroidParams+",,"+rrr.recordSwiftParams);
			Iterator<String> iter = mm_va.keySet().iterator();
			if(iter.hasNext()){
				String a_param=iter.next();
				String t_param= recordAndroidParams.get(a_param);
				String s_param =(String) rrr.recordSwiftParams.get(mm_va.get(a_param));

				//				System.out.println(t_param+"####"+s_param);

				String i3 = SQLiteJDBC.insertTableAndroid("var_android", id,t_param,name_android,"","","");
				String i4 = SQLiteJDBC.insertTableSwift("var_swift", id, s_param,name_swift,rrr.func_decl,"","",id);

				stmt.executeUpdate(i3);
				stmt.executeUpdate(i4);	
				id++;

			}
		}else{

		}

		Iterator<String> dddd = mm_va.keySet().iterator();
		int aaaaa = rrr.recordStatement.size();
		int bbbbb = recordStatement.size();
		String i3="";
		String i4="";
		mid++;		
		if(return_android=="NULL" || rrr.returnType=="NULL"){
			i3 = SQLiteJDBC.insertTableAndroidM("var_android", id,Main.androidClassName,name_android,"");
			i4 = SQLiteJDBC.insertTableSwiftM("var_swift", id, Main.swiftClassName,name_swift,"",id);
		}else{
			//			insertTableAndroidM
			i3 = SQLiteJDBC.insertTableAndroid("var_android", id,Main.androidClassName,name_android,"",return_android,"");
			i4 = SQLiteJDBC.insertTableSwift("var_swift", id, Main.swiftClassName,name_swift,"",rrr.returnType,"",id);
		}
		stmt.executeUpdate(i3);
		stmt.executeUpdate(i4);	
		id++;
	}

	static LinkedList<VarDeclElement> alist = new LinkedList<VarDeclElement>();
	static LinkedList<VarDeclElement> slist = new LinkedList<VarDeclElement>();

	static Map<String, VarDeclElement> amapping 	= new HashMap<String, VarDeclElement>();
	static Map<String, VarDeclElement> smapping 	= new HashMap<String, VarDeclElement>();
	static Map<String, String> mappingKeysVarDecl 	= new HashMap<String, String>();
	static int id 	= 1;
	static int mid 	= 1;
	public <T> void mappingVarDecl() throws FileNotFoundException, IOException, ClassNotFoundException, SQLException{
		RecordVarDeclSwift recordSwift = new RecordVarDeclSwift<>();
		swift.accept(recordSwift);

		RecordVarDeclAndroid recordAndroid = new RecordVarDeclAndroid();
		android.accept(recordAndroid);

		Connection connection = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		Statement stmt = connection.createStatement();
		
		StructureMapping strt = new StructureMapping(android, swift);
		strt.compute();

		/*

		List<String> 		aanodes 	= new LinkedList<String>();
		List<ASTNode> 		taanodes 	= new LinkedList<ASTNode>();


		List<String> 	ssnodes 					= new LinkedList<String>();
		List<ParserRuleContext> 	tssnodes 		= new LinkedList<ParserRuleContext>();
		Scanner scanner = new Scanner();
		scanner.recordLineSeparator = true;
		char[] source = android.toString().toCharArray();

		//		StringBuffer b = new StringBuffer();
		scanner.setSource(source);
		//				scanner.getCurrentTokenString()
		SwiftBaseListener lis = new SwiftBaseListener(){
			@Override
			public void enterEveryRule(ParserRuleContext ctx) {
				// TODO Auto-generated method stub
				if(!ssnodes.contains(ctx.getText()) && !(ctx instanceof SwiftParser.Variable_declarationContext) && !ctx.getText().contains("?")&& !ctx.getText().contains("=")&& !ctx.getText().contains(":")){
					ssnodes.add(ctx.getText());
					tssnodes.add(ctx);

				}
				super.enterEveryRule(ctx);
			}
		};
		ParseTreeWalker walker = new ParseTreeWalker();
		walker.walk(lis, swift);

		android.accept(new ASTVisitor() {
			@Override
			public void preVisit(ASTNode node) {
				// TODO Auto-generated method stub
				if(!(node instanceof FieldDeclaration) && !aanodes.contains(node.toString())&& !node.toString().contains("=") &&!node.toString().contains(";"))
					aanodes.add(node.toString());
				super.preVisit(node);
			}
		});
		
		*/
	/*
		android.accept(new ASTVisitor() {
			@Override
			public boolean visit(FieldDeclaration node) {
				// TODO Auto-generated method stub
				if(node.getType()!=null){
					taanodes.add(node.getType());
					aanodes.add(node.getType().toString());
				}
				if(node.modifiers()!=null){
					List modis = node.modifiers();
					for(int j=0; j<modis.size();j++){
						taanodes.add((ASTNode)modis.get(j));
						aanodes.add(modis.get(j).toString());
					}
				}
				List frgs = node.fragments();
				for(int i=0;i<frgs.size();i++){
					VariableDeclarationFragment fg = (VariableDeclarationFragment)frgs.get(i);
					if(fg.getInitializer()!=null){
						taanodes.add(fg.getInitializer());
						aanodes.add(fg.getInitializer().toString());
					}
					if(fg.getName()!=null){
						taanodes.add(fg.getName());
						aanodes.add(fg.getName().toString());
					}
				}

				return super.visit(node);
			}

		});
*/
/*
		double[][] ecostMatrix = new double[aanodes.size()][ssnodes.size()];


		StringMetric emetric =
				with(new JaroWinkler())
				.build();

		for(int i=0;i<aanodes.size();i++){
			for(int j=0;j<ssnodes.size();j++){
				ecostMatrix[i][j]= 1- emetric.compare(aanodes.get(i), ssnodes.get(j));
			}
		}
		HungarianAlgorithm ehung = null;
		int[] eresult = new int[0];
		if(aanodes.size() > 0 && ssnodes.size() >0){
			ehung = new HungarianAlgorithm(ecostMatrix);
			eresult = ehung.execute();
		}

		Map<String, String> amap = new HashMap<String, String>();
		Map<String, String> smap = new HashMap<String, String>();
		int argnum = 0;
		for(int p=0; p<eresult.length;p++){
			if(eresult[p]!=-1){
				amap.put(aanodes.get(p), "<arg"+argnum+">");
				smap.put(ssnodes.get(eresult[p]), "<arg"+argnum+">");
				argnum++;
			}
		}

		int az = swift.start.getStartIndex();
		int bz = swift.stop.getStopIndex();
		Interval interval = new Interval(az,bz);

		String swift_str = swift.start.getInputStream().getText(interval);
		
		
		Comparator<String> comparator = new Comparator<String>() {
			  public int compare(String o1, String o2) {
				  if(o1.length()>=o2.length()) return -1; 
				  else return 1;
			  }
			};
			
			SortedSet<String> keys = new TreeSet<String>(comparator);
			keys.addAll(amap.keySet());
			
			SortedSet<String> skeys = new TreeSet<String>(comparator);
			skeys.addAll(smap.keySet());
		
//		amap = sortByValues(amap, -1);  // Ascending order
//		smap = sortByValues(smap, -1);  // Ascending order

		Iterator<String> ait = keys.iterator();
		Iterator<String> sit = skeys.iterator();

		swift.start.getInputStream();
		android.setJavadoc(null);

		String android_str = android.toString().replace("\n", "");
		System.out.println("keys"+keys);
		int aaa=0;
		while(ait.hasNext()){
			String key = ait.next();

			String expr = key;
			String arg = amap.get(key);
			//			System.err.println(astnode+" "+arg);
			expr=expr.replaceAll("\\(", "\\\\(");
			expr=expr.replaceAll("\\)", "\\\\)");
			expr=expr.replaceAll("\\.", "\\\\.");
			//			expr=expr.replaceAll("\\+", "\\\\+");
			//			expr=expr.replaceAll("\\-", "\\\\-");
			expr=expr.replaceAll("\\*", "\\\\*");
			expr=expr.replaceAll("\\[", "\\\\[");
			expr=expr.replaceAll("\\]", "\\\\]");
			expr=expr.replaceAll("\\}", "\\\\}");
			expr=expr.replaceAll("\\{", "\\\\{");
//			expr=expr.replaceAll("\\ ", "\\\\ ");
			expr=expr.replaceAll("/", "\\\\/");
			expr=expr.replaceAll("\\s", "\\\\s");
//			android_str=android_str.replaceAll("\b"+expr+"\b", arg);
			android_str=android_str.replaceAll(expr, arg);
			System.err.println(android_str+" "+"\b"+expr+"\b");
			aaa++;
		}

		int sss=0;
		while(sit.hasNext()){
			String key = sit.next();
			String expr = key;
			String arg = smap.get(key);

			expr=expr.replaceAll("\\(", "\\\\(");
			expr=expr.replaceAll("\\)", "\\\\)");
			expr=expr.replaceAll("\\.", "\\\\.");
			//			expr=expr.replaceAll("\\+", "\\\\+");
			//			expr=expr.replaceAll("\\-", "\\\\-");
			expr=expr.replaceAll("\\*", "\\\\*");
			expr=expr.replaceAll("\\[", "\\\\[");
			expr=expr.replaceAll("\\]", "\\\\]");
			expr=expr.replaceAll("\\}", "\\\\}");
			expr=expr.replaceAll("\\{", "\\\\{");
			expr=expr.replaceAll("/", "\\\\/");
			expr=expr.replaceAll("\\s", "\\\\s");
			
			//			System.err.println(astnode+" "+arg);
//			swift_str=swift_str.replaceAll("\\b"+expr+"\\b", arg);
			swift_str=swift_str.replaceAll(expr, arg);
			sss++;
		}

		System.out.println("android_str"+android_str);

		System.out.println("swift_str"+swift_str);
		
		String e12 = SQLiteJDBC.insertTableGrammarAndroid(StructureMapping.grammar_id, android_str, android.getNodeType(), android.getParent().getNodeType(), "TEST");
		String e11 = SQLiteJDBC.insertTableGrammarSwift(StructureMapping.grammar_id, swift_str, swift.getRuleIndex(), swift.getParent().getRuleIndex(), "TEST", StructureMapping.grammar_id);
		//		String e12 = ProtoSQLiteJDBC.insertTableExprAndroidV(grammar_id, mappingExpr.androidExpr.toString(),binding, bbb, Main.androidClassName+"."+MappingDeclaration.methodnameAndroid);
//		String e11 = ProtoSQLiteJDBC.insertTableExprSwiftV(grammar_id,  mappingExpr.swiftExpr.getText(),  mappingExpr.swiftExpr.getText(), expr_id, genVarsS);


		Connection c;
		try {

			c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
			java.sql.Statement sql_stmt = c.createStatement();
			c.setAutoCommit(true);
			sql_stmt.executeUpdate(e12);	
			sql_stmt.executeUpdate(e11);
			
			StructureMapping.grammar_id++;
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			System.out.println(e12);
			System.out.println(e11);
			e1.printStackTrace();
		}
		
		
		
		
		if(aaa==sss){
			BufferedWriter out = new BufferedWriter(new FileWriter("sample2.txt", true));
			//		out.write(android.toString()+"	"+swift.getText());
//			out.write(amap.toString()+"\n");
//			out.write(smap.toString()+"\n");
			out.write(android_str.toString()+"\n");
			out.write(" "+swift_str.toString()+"\n");
			out.close();
		}
		*/
		
		int na = recordAndroid.record.size();
		int ns = recordSwift.record.size();

		if(na==ns && na==4){

			String a1 = recordAndroid.recordStr.get(0);
			String a2 = recordAndroid.recordStr.get(1);
			String a3 = recordAndroid.recordStr.get(2);
			String a4 = recordAndroid.recordStr.get(3);
			alist.add(new VarDeclElement(a1, a2, a3, a4));
			amapping.put(a3, new VarDeclElement(a1, a2, a3, a4));
			String s1 = ((MappingElement) recordSwift.record.get(0)).label;
			String s2 = ((MappingElement) recordSwift.record.get(1)).label;
			String s3 = ((MappingElement) recordSwift.record.get(2)).label;
			String s4 = ((MappingElement) recordSwift.record.get(3)).label;
			slist.add(new VarDeclElement(s1, s2, s3, s4));

			smapping.put(s3, new VarDeclElement(s1, s2, s3, s4));

			mappingKeysVarDecl.put(a3, s3);
			String i3 = SQLiteJDBC.insertTableAndroid("var_android", id,Main.androidClassName,a3,a1,a2,a4);
			String i4 = SQLiteJDBC.insertTableSwift("var_swift", id, Main.swiftClassName,s3,s1,s2,s4,id);
			stmt.executeUpdate(i3);
			stmt.executeUpdate(i4);	
			id++;
		}
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "[android:"+android.toString()+", swift:"+swift.getText()+"]";
	}



}
