package migration;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

//import SwiftParser.Function_call_expressionContext;

//import SwiftParser.ParameterContext;

//import SwiftParser.Pattern_initializerContext;

//import SwiftParser.ExpressionContext;

//import SwiftParser.Constant_declarationContext;

//import SwiftParser.StatementsContext;

public class VarFuncSwiftListener extends SwiftBaseListener{
	LinkedList<String> record = new LinkedList<String> ();
	static LinkedList<String> exprrecord = new LinkedList<String> ();
	
	@Override
	public void enterFunction_call_expression(SwiftParser.Function_call_expressionContext ctx) {
		// TODO Auto-generated method stub
		System.out.println("enterFunction_call_expression"+ctx.postfix_expression().getText());//name of 
		System.out.println("enterFunction_call_expression"+ctx.parenthesized_expression().expression_element_list().getText());
		;
		super.enterFunction_call_expression(ctx);
	}
	@Override
	public void enterStatements(SwiftParser.StatementsContext ctx) {
		// TODO Auto-generated method stub
		
		super.enterStatements(ctx);
	}
	@Override
	public void enterPattern_initializer(SwiftParser.Pattern_initializerContext ctx) {
		// TODO Auto-generated method stub
//		ctx.pattern();
		record.add(ctx.pattern().getText());
		record.add(ctx.initializer().expression().getText());
		
		super.enterPattern_initializer(ctx);
	}
	@Override
	public void enterExpression(SwiftParser.ExpressionContext ctx) {
		// TODO Auto-generated method stub
//		record.add(ctx.getText());
//		System.out.println(ctx.getText());
		exprrecord.add(ctx.getText());
		super.enterExpression(ctx);
	}
	
	@Override
	public void exitConstant_declaration(SwiftParser.Constant_declarationContext ctx) {
		// TODO Auto-generated method stub
		System.err.println(ctx.getRuleIndex()+"\n"+ctx.getText());
		System.err.println(record);
		super.exitConstant_declaration(ctx);
	}
	
	public static void main(String[] args) throws FileNotFoundException, IOException{
		VarFuncSwiftListener rrr = new VarFuncSwiftListener();
		//		System.out.println("RECORD Swift\n"+s.toStringTree());
		//			s.accept(rrr);
		String path = "/Users/kijin/Documents/workspace_mars/GumTreeTester/charts/test_set/test.swift";

		//			Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();
		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new SwiftLexer((CharStream)stream1);
		SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
		SwiftParser.StatementsContext root1 = parser1.statements();

		//			SwiftParser.Function_declarationContext root_func = parser1.function_declaration();

		ParseTreeWalker walker = new ParseTreeWalker();
		//			    AntlrDrinkListener listener = new AntlrDrinkListener();
		walker.walk(rrr, root1);
		
		System.out.println(exprrecord);
		//			root1.accept(rrr);

		//			System.out.println(rrr.record);
		//[public, func, offsetLeft, CGFloat, _contentRect.origin.x]
		//[public, var, offsetLeft, CGFloat, _contentRect.origin.x]
	}
}
