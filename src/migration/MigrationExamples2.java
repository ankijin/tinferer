package migration;

import static org.hamcrest.CoreMatchers.instanceOf;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.misc.TestRig;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.collections4.ListUtils;
import org.sqlite.Function;
import org.stringtemplate.v4.ST;

import com.googlecode.concurrenttrees.radix.node.concrete.DefaultCharSequenceNodeFactory;
import com.googlecode.concurrenttrees.solver.LCSubstringSolver;

import alignment.common.CommonOperators;
import alignment.common.LineCharIndex;
import alignment.common.OpProperty;
import alignment.ruleinfer.StructureMappingAntlr4_for_stmt;
import antlr_parsers.javaparser.JavaBaseListener;
import antlr_parsers.javaparser.JavaBaseVisitor;
import antlr_parsers.javaparser.JavaLexer;
import antlr_parsers.javaparser.JavaParser;
import antlr_parsers.javaparser.JavaParser.StatementContext;
import antlr_parsers.pcommon.SimpleFormat;
import antlr_parsers.pcommon.SimpleFormatListener;
import migration.StructureMappingAntlr4.Pair;
import migration.SwiftParser.Return_statementContext;
import rulesapp.TemplateEDB;
import sqldb.DBTablePrinter;
import sqldb.SQLiteJDBC;
import templatechecker.DSLErrorListener;
import templatechecker.TemplateCLexer;
//import templatechecker.TemplateCParser;
import templates.MatchingTemplate;
import templates.MatchingTemplate2;

import org.apache.commons.collections4.ListUtils;

public class MigrationExamples2 {
	public static ParserRuleContext typeJ=null;
	private int i2;
	public static int idd;
	private static boolean flip = true;
	//		public static final String db_name = "test_record11.db";
	//	public static final Strinfg db_name = "test_record0208.db";

	//	public static final String db_name = "train_record8.db";
	//	public static final String db_name = "test_record0210.db";
	//	public static final String db_name = "train_record7.db";
	//	public static final String db_name = "test_record_charts.db";
	//	public static final String db_name = "test_record_others.db";
	//	public static final String db_name = "db_result/total_examples.db";
	public static final String db_name = "db_result/total_examples0404.db";
	private static final int Iterator = 0;


	private static boolean templateOk = false;

	public static ResultSet queryExampleResultRandom(String tableName, int num) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select TEMPLATE, AST_TYPE, PARENT_TYPE, COUNT, EXAMPLE, AFILENAME, ALOC, SFILENAME, SLOC , ID from "+tableName+ " ORDER BY RANDOM() LIMIT "+num +";";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		//		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);
		return resultSet;
	}


	public static String query(String tableName, int android, int swift){
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, count from "+tableName+ " where parent_type="+swift +" order by count;";
		return query;
	}


	public static ResultSet queryExampleResult(String tableName, int android, int swift) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where ast_type="+android +" and parent_type="+swift +" order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		//		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);
		return resultSet;
	}

	public static ResultSet queryExampleResultbyID(String tableName, int id) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where id="+id+ " order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		//		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		//		connection2.prepareStatement( "ATTACH DATABASE 'test_record.db' AS template").execute();

		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;
	}


	public static ResultSet queryExampleResultbyTestingSet(String tableName, String testing) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where afilename in (\'"+testing+ "\') and ast_type in (19)"+ ";";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		//		connection2.prepareStatement( "ATTACH DATABASE 'test_record.db' AS template").execute();

		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;
	}

	public static ResultSet queryExampleResultbyTestingSet(String tableName, String[] afiles, int [] atype, int [] stype) throws SQLException{

		String [] newa = new String[afiles.length];
		int i =0;
		for(String a:afiles){
			String f="\'"+a+"\'";
			newa[i] = f;
			i++;

		}
		String test=Arrays.toString(newa).replace("[","").replace("]","");


		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		//		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where afilename in ("+test+ ") and ast_type in (19) and parent_type in (97)"+ ";";

		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where afilename in ("+test+ ") and  ast_type in ("+Arrays.toString(atype).replace("[","").replace("]","")+") and parent_type in ("+Arrays.toString(stype).replace("[","").replace("]","")+")"+ ";";

		//		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where afilename in ("+test+ ") and  ast_type in (3) and parent_type in (133,130)"+ ";";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		//		connection2.prepareStatement( "ATTACH DATABASE 'test_record.db' AS template").execute();

		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;
	}

	public static ResultSet queryExampleResultbyTestingSet(String tableName, String[] afiles, int sample_num, int [] atype, int [] stype) throws SQLException{

		String [] newa = new String[afiles.length];
		int i =0;
		for(String a:afiles){
			String f="\'"+a+"\'";
			newa[i] = f;
			i++;

		}
		String test=Arrays.toString(newa).replace("[","").replace("]","");

		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		//		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where afilename in ("+test+ ") and ast_type in (19) and parent_type in (97)"+ ";";
		int num= (int) Math.round(sample_num*0.6);
		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where afilename in ("+test+ ") and  ast_type in ("+Arrays.toString(atype).replace("[","").replace("]","")+") and parent_type in ("+Arrays.toString(stype).replace("[","").replace("]","")+")"+ " ORDER BY RANDOM() LIMIT "+num+";";

		//		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where afilename in ("+test+ ") and  ast_type in (3) and parent_type in (133,130)"+ ";";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		//		connection2.prepareStatement( "ATTACH DATABASE 'test_record.db' AS template").execute();

		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;
	}


	public static ResultSet queryTemplateByType(String dbName, int type) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		//		String query = "select template, example, parent_type, ast_type, count, ID from grammar_android_stat where ast_type="+type+ " order by count;";
		String query = "select template, example, parent_type, ast_type, count, ID from grammar_android_stat  where id=1;";

		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+"test_record.db");
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;
	}

	/**
	 * return templates
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static ResultSet queryTemplateByExampleID(int id) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";

		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+"test_record0415.db");
		//		connection2.prepareStatement( "attach 'total_example.db' as tempp").execute();BY RANDOM() LIMIT xgroup by ast_type, parent_type order by c

		String query = "SELECT template, example, id, ast_type, parent_type, count, rform , count(*) as c FROM grammar_android_stat "
				+ "\'ViewPortHandler.java\') and (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform group by template, example order by c DESC LIMIT 10;";
		//		String query = "SELECT * FROM grammar_android_stat where (SELECT template FROM exams.grammar_swift_stat where id="+id+") REGEXP rform order by count DESC;";

		//		String query = "SELECT * FROM grammar_android_stat where (SELECT template FROM exams.grammar_swift_stat BY RANDOM() LIMIT ) REGEXP rform order by count DESC LIMIT 1;";

		//		String query = "select ast_type, parent_type, count(*) as c from grammar_swift_stat group by ast_type, parent_type order by c DESC;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		//		System.out.println("qquery\n"+query);


		//		connection2.setAutoCommit(true);




		Statement stmt2 = connection2.createStatement();
		stmt2.execute("ATTACH 'db_result/total_examples0404.db' as exams;");
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";

		Function.create(connection2, "REGEXP", new Function() {
			@Override
			protected void xFunc() throws SQLException {
				String expression = value_text(0);
				String value = value_text(1);
				//				System.out.println(value);
				if (value == null){
					value = "";
				}
				Pattern pattern=Pattern.compile(expression, Pattern.DOTALL);
				result(pattern.matcher(value).find() ? 1 : 0);
			}
		});


		Function.create(connection2, "SIMPLE", new Function() {
			@Override
			protected void xFunc() throws SQLException {
				String expression = value_text(0);
				//				String value = value_text(1);
				//				System.out.println("simple\n"+SimpleFormat.getSimpleFormat(expression));
				//				if (value == null){
				//					value = "";
				//				}
				//				Pattern pattern=Pattern.compile(expression);
				result(SimpleFormat.getSimpleFormat(expression));
			}
		});




		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;
	}

	public static ResultSet queryTemplateByExampleID(int id, String [] afiles) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		//		StringUtils.join(null, " ");
		String [] newa = new String[afiles.length];
		int i =0;
		for(String a:afiles){
			String f="\'"+a+"\'";
			newa[i] = f;
			i++;

		}
		String test=Arrays.toString(newa).replace("[","").replace("]","");
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+"test_record0415.db");
		//		connection2.prepareStatement( "attach 'total_example.db' as tempp").execute();BY RANDOM() LIMIT xgroup by ast_type, parent_type order by c

		String query = "SELECT template, example, id, ast_type, parent_type, count, rform , count(*) as c FROM grammar_android_stat where (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform and afilename not in ("+ test +") group by example, template order by c DESC LIMIT 1;";
		//		String query = "SELECT template, example, id, ast_type, parent_type, count, rform , count(*) as c FROM grammar_android_stat where sfilename not in ("+ test +") and (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform group by template, example order by c DESC LIMIT 5;";

		//		String query = "SELECT template, example, id, ast_type, parent_type, count, rform , count(*) as c FROM grammar_android_stat where afilename not in ("+ test +") and (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform group by template, example order by c DESC;";
		//		String query = "SELECT template, example, id, ast_type, parent_type, count, rform FROM grammar_android_stat where afilename not in ("+ test +") and SIMPLE((SELECT template FROM exams.grammar_swift_stat where id="+id+")) REGEXP rform group by template, example;";

		//		String query = "SELECT * FROM grammar_android_stat where (SELECT template FROM exams.grammar_swift_stat where id="+id+") REGEXP rform order by count DESC;";

		//		String query = "SELECT * FROM grammar_android_stat where (SELECT template FROM exams.grammar_swift_stat BY RANDOM() LIMIT ) REGEXP rform order by count DESC LIMIT 1;";

		//		String query = "select ast_type, parent_type, count(*) as c from grammar_swift_stat group by ast_type, parent_type order by c DESC;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";



		//		connection2.setAutoCommit(true);




		Statement stmt2 = connection2.createStatement();
		stmt2.execute("ATTACH 'db_result/total_examples0404.db' as exams;");
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";

		Function.create(connection2, "REGEXP", new Function() {
			@Override
			protected void xFunc() throws SQLException {
				String expression = value_text(0);
				String value = value_text(1);
				//				System.out.println(value);
				if (value == null){
					value = "";
				}
				Pattern pattern=Pattern.compile(expression, Pattern.DOTALL);
				//				System.out.println("pattern"+pattern+"   value  "+value+"  "+pattern.matcher(value).find());
				result(pattern.matcher(value).find() ? 1 : 0);
			}
		});


		Function.create(connection2, "SIMPLE", new Function() {
			@Override
			protected void xFunc() throws SQLException {
				String expression = value_text(0);
				//				String value = value_text(1);
				//				System.out.println("simple\n"+SimpleFormat.getSimpleFormat(expression));
				//				if (value == null){
				//					value = "";
				//				}
				//				Pattern pattern=Pattern.compile(expression);
				result(SimpleFormat.getSimpleFormat(expression));
			}
		});




		ResultSet resultSet = stmt2.executeQuery(query);
		//		System.out.println("qquery	"+query);
		return resultSet;
	}



	public static ResultSet queryTemplateByExample(String example) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";

		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+"test_record0415.db");
		//		connection2.prepareStatement( "attach 'total_example.db' as tempp").execute();BY RANDOM() LIMIT x

		String query = "SELECT * FROM grammar_android_stat where '"+example+"' REGEXP rform order by count;";
		//		String query = "SELECT * FROM grammar_android_stat where (SELECT template FROM exams.grammar_swift_stat where id="+id+") REGEXP rform order by count DESC;";

		//		String query = "SELECT * FROM grammar_android_stat where (SELECT template FROM exams.grammar_swift_stat BY RANDOM() LIMIT ) REGEXP rform order by count DESC LIMIT 1;";

		//		String query = "select ast_type, parent_type, count(*) as c from grammar_swift_stat group by ast_type, parent_type order by c DESC;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		//		System.out.println("query	"+query);


		//		connection2.setAutoCommit(true);




		Statement stmt2 = connection2.createStatement();
		//		stmt2.execute("ATTACH 'db_result/total_examples0404.db' as exams;");
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";

		Function.create(connection2, "REGEXP", new Function() {
			@Override
			protected void xFunc() throws SQLException {
				String expression = value_text(0);
				String value = value_text(1);
				//				System.out.println(value);
				if (value == null){
					value = "";
				}
				Pattern pattern=Pattern.compile(expression, Pattern.COMMENTS);
				result(pattern.matcher(value).find() ? 1 : 0);
			}
		});



		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;
	}

	public static ResultSet queryRegex(String dbname, String [] afiles) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";


		String [] newa = new String[afiles.length];
		int i =0;
		for(String a:afiles){
			String f="\'"+a+"\'";
			newa[i] = f;
			i++;

		}
		String test=Arrays.toString(newa).replace("[","").replace("]","");

		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+dbname);//group by template, example
		//		connection2.prepareStatement( "attach 'total_example.db' as tempp").execute();BY RANDOM() LIMIT x
		//		String query = "SELECT template, example, id, ast_type, parent_type, count, rform , count(*) as c FROM grammar_android_stat where afilename not in ("+ test +") and (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform group by template, example order by c DESC LIMIT 5;";

		String query = "SELECT rform, template, example, count(*) as c FROM grammar_android_stat where afilename not in ("+test+") group by template, example order by c;";
		//		String query = "SELECT * FROM grammar_android_stat where (SELECT template FROM exams.grammar_swift_stat BY RANDOM() LIMIT ) REGEXP rform order by count DESC LIMIT 1;";

		//		String query = "select ast_type, parent_type, count(*) as c from grammar_swift_stat group by ast_type, parent_type order by c DESC;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";


		Statement stmt2 = connection2.createStatement();
		ResultSet resultSet = stmt2.executeQuery(query);
		System.out.println("query	"+resultSet.getFetchSize()+"   "+query);

		return resultSet;
	}


	public static ResultSet queryExampleResultbyExample(String tableName, String a_example, String s_example) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, parent_type, count from "+tableName+ " where template=\'"+a_example +"\' and example=\'"+s_example +"\' order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		//		System.out.println(query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;

	}

	public static ResultSet queryExampleResultbyExample(String tableName, String a_example) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, parent_type, count from "+tableName+ " where template=\'"+a_example +"\' order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		//		System.out.println(query);
		//		test_record11.db
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;

	}



	/*
	public static ResultSet queryExampleResultbyExample(String tableName, String example) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, parent_type, count from "+tableName+ " where template=\'"+example +"\' order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println(query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+"train_record.db");
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;

	}
	 */



	public static String getLongestCommonSubstring(Collection<String> strings) {
		LCSubstringSolver solver = new LCSubstringSolver(new DefaultCharSequenceNodeFactory());

		for (String s: strings) {
			solver.add(s);
		}
		return solver.getLongestCommonSubstring().toString();
	}

	public static Statement initConnection(String dbname) throws ClassNotFoundException, SQLException{

		Class.forName("org.sqlite.JDBC");
		//		Connection connection = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		Connection connection = DriverManager.getConnection("jdbc:sqlite:"+dbname);
		connection.setAutoCommit(true);
		Statement stmt = connection.createStatement();
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift"));

		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_binding_stat"));

		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroidStat());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwiftStat());
		stmt.executeUpdate(SQLiteJDBC.createBindingStat());


		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroid());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwift());

		return stmt;
	}


	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		// TODO Auto-generated method stub
		//query examples
		//generates template to except for bindings
		//when there are no binding .. just syntax change
		//Goals: removal syntax errors

		//set of training
		//query .. 
		//		Statement stmt = initConnection("test_record.db");
		//		DBTablePrinter.printTable(DriverManager.getConnection("jdbc:sqlite:"+"test_record0406_1811.db"), "grammar_android_stat");
		//		DBTablePrinter.printTable(DriverManager.getConnection("jdbc:sqlite:"+"test_record0406_1811.db"), "grammar_swift_stat");



		/*
		int []testset= {
				8,
				9,
				16,
				17,
				20

		};
		 */
		int [] testset={

				4,
				102,
				152,
				594,
				1037,
				1109,
				1114,
				1342,
				1768,
				1929,
				1933,
				1342,
				2433,
				2187,
				2309,
				1931,
				1840,
				1839
				//				2433,
				//				2433

		};

		/*		
		int []testset= {
				1345,
				1347,
				1358,
				1358,
				1367,
				1385,
				1438,
				1440,
				1442,
				1444,
				1447,
				1448,
				1476,
				1480
		};
		 */


		//2/2
		//		String[] testingset ={"LineChartRenderer.java",
		//				"PieRadarChartBase.java"};
		//2/2
		//		String[] testingset ={"LegendRenderer.java",
		//				"AxisBase.java",
		//				"CandleStickChartRenderer.java"};
		//2/2
		//		String[] testingset ={"YAxisRendererRadarChart.java",
		//				"BarChartRenderer.java",
		//				"XAxisRendererHorizontalBarChart.java",
		//				"HorizontalBarChart.java"};

		/*
		String[] testingset ={
		"ChartHighlighter.java",
		"RadarChartRenderer.java",
		"YAxisRendererHorizontalBarChart.java",
		"CombinedChartRenderer.java",
		"Transformer.java"
		};
		 */	

		String[][] testingsett ={
				{"BarLineChartBase.java","IMarker.java",	"IAxisValueFormatter.java"}, //fold 1
				{"ChartData.java","IHighlighter.java"}, //fold 2
				{"ViewPortHandler.java","PieChartRenderer.java","IScatterDataSet.java"}, //fold 3
				{"LineChartRenderer.java","PieRadarChartBase.java"}, //fold 4
				{"PieChart.java","CombinedData.java","HorizontalBarChartRenderer.java",			"IFillFormatter.java"	
				}, //fold 5
				{"LegendRenderer.java","AxisBase.java","CandleStickChartRenderer.java","ILineRadarDataSet.java"}, //fold 6
				{"YAxisRendererRadarChart.java","BarChartRenderer.java","XAxisRendererHorizontalBarChart.java","HorizontalBarChart.java"}, //fold 7
				{"ChartHighlighter.java","RadarChartRenderer.java","YAxisRendererHorizontalBarChart.java","CombinedChartRenderer.java","Transformer.java","IShapeRenderer.java"},	//fold 8	
				{
					"YAxis.java",
					"CombinedChart.java",
					"BarChart.java",
					"YAxisRenderer.java",
					"AxisRenderer.java",
					"BubbleChartRenderer.java",
					"MarkerImage.java",
					"AnimatedZoomJob.java",
					"ChartAnimator.java",

					"IValueFormatter.java"
				}, ////fold 9

				{
					"LineRadarDataSet.java",
					"PieRadarHighlighter.java",
					"ScatterChartRenderer.java",
					"LargeValueFormatter.java",
					"LegendEntry.java",
					"DefaultFillFormatter.java",
					"ZoomJob.java",
					"CombinedHighlighter.java",
					"LineScatterCandleRadarDataSet.java",
					"ScatterData.java",
					"XAxisRendererRadarChart.java",
					"Range.java",
					"CircleShapeRenderer.java",
					"BarHighlighter.java",
					"HorizontalBarHighlighter.java",
					"ViewPortJob.java",
					"AnimatedMoveViewJob.java",
					"BubbleData.java",
					"PieHighlighter.java",
					"RadarChart.java",
					"ComponentBase.java",
					"MoveViewJob.java",
					"CandleData.java",
					"DefaultValueFormatter.java",
					"LineChart.java",
					"Renderer.java",
					"TriangleShapeRenderer.java",
					"BarLineScatterCandleBubbleDataSet.java",
					"BubbleChart.java",
					"DefaultAxisValueFormatter.java",
					"ICandleDataSet.java",

					"MarkerView.java",
					"ScatterChart.java",
					"ScatterDataProvider.java",
					"TransformerHorizontalBarChart.java",
					"BarLineScatterCandleBubbleData.java",
					"BarLineScatterCandleBubbleDataProvider.java",
					"CandleStickChart.java",
					"IBubbleDataSet.java",
					"ILineScatterCandleRadarDataSet.java",

					"LineScatterCandleRadarRenderer.java",
					"SquareShapeRenderer.java",
					"CombinedDataProvider.java",
				}

		};
		int num=0;
		int hit=0;
		int nohit=0;
		//		int hit=0;
		int cc=0;
		int atype, stype;
		int aatype[] ={70};
		int sstype[] ={6};
		for(int id=0 ; id<testingsett.length;id++){
			//		Map<String, String> item_map = new HashMap<String, String>();
			LinkedHashMap<TemplateEDB, TemplateEDB> item_map = new LinkedHashMap<TemplateEDB, TemplateEDB>();
			ResultSet example_resultSet 	=  queryExampleResultbyTestingSet("grammar_swift_stat", testingsett[id],aatype, sstype);
			while(example_resultSet.next()){
				cc++;
			}
			example_resultSet 	=  queryExampleResultbyTestingSet("grammar_swift_stat", testingsett[id], cc, aatype, sstype);
			//			ResultSet resultSet4 	= queryTemplateByExample("public class ChartHighlighter<T extends BarLineScatterCandleBubbleDataProvider> implements Highlighter {}");
			//			System.out.println("dddcc\n"+cc);
			cc = (int) Math.round(cc*0.6);
			Vector<String> migrations = new Vector<String>();
			String aaa="";
			String bbb ="";

			ResultSet regex = queryRegex("test_record0415.db",testingsett[id]);
			while(regex.next()){
				String raaa = regex.getString("rform");
				String tttt = regex.getString("template");
				String eeee = regex.getString("example");
				int c = Integer.parseInt(regex.getString("c"));
				PatternSyntaxException exc = null;

				System.err.println(tttt+"  "+eeee+"  "+c+":"+raaa+"  ");
				try {
					Pattern.compile(raaa);
				} catch (PatternSyntaxException e) {
					exc = e;
					System.out.println("errrrrr"+raaa);
				}
				if (exc != null) {
					exc.printStackTrace();
				} else {
				}

			}

			while(example_resultSet.next()){




				num++;
				int templateT = 0;
				aaa 	= example_resultSet.getString("template");
				bbb 	= example_resultSet.getString("example");
				atype 	= Integer.parseInt(example_resultSet.getString("AST_TYPE"));
				stype 	= Integer.parseInt(example_resultSet.getString("PARENT_TYPE"));
				idd 	= Integer.parseInt(example_resultSet.getString("ID"));

				int ccc = idd;

				ResultSet template_resultSet = queryTemplateByExampleID(idd, testingsett[id]);
				//				System.out.println("resultSet33.getFetchSize()	"+resultSet33.getFetchSize());
				while(template_resultSet.next()){
					String aaaa = template_resultSet.getString("template");
					String bbbb = template_resultSet.getString("example");
					String rform = template_resultSet.getString("rform");
					//					System.out.println("ccccc	\n"+rform+"\n"+aaaa+"\n"+bbbb);

					templateT = Integer.parseInt(template_resultSet.getString("AST_TYPE"));
					stype = Integer.parseInt(template_resultSet.getString("PARENT_TYPE"));
					idd = Integer.parseInt(template_resultSet.getString("ID"));




					if(templateT==atype){
						//						System.out.println("TEMPP	"+	c+"\n"+aaaa+" \n"+bbbb);
						//						System.out.println("aaa	"+aaaa);
						//						System.out.println("bbb	"+bbbb);
						//						System.out.println("rform\n"+rform);
						aaaa = aaaa.replaceAll("@ Override", "@Override");
						aaaa =getStdFormTemplate(aaaa);
						//					TemplateEDB key =;
						item_map.put(new TemplateEDB(idd,aaaa), new TemplateEDB(idd,bbbb));
					}

				}


				//			aaa = aaa.replaceAll("\\.\\.\\.", "\\<arg10\\>");
				//			bbb = bbb.replaceAll("\\.\\.\\.", "\\<arg10\\>");
				//				System.out.println(bbb);
				//				System.err.println(aaa);
				//			item_map.put(aaa, bbb);

				antlr_parsers.swiftparser.SwiftLexer lexer1 = new antlr_parsers.swiftparser.SwiftLexer((CharStream)new ANTLRInputStream(bbb));
				CommonTokenStream swiftCommonStream = new CommonTokenStream(lexer1);
				antlr_parsers.swiftparser.SwiftParser parser = new antlr_parsers.swiftparser.SwiftParser(swiftCommonStream);

				aaa = aaa.replaceAll("@Override[\\s|\\t|\\n]* ", "@Override ");
				JavaLexer lexerj = new JavaLexer((CharStream)new ANTLRInputStream(aaa));
				CommonTokenStream javaCommonStream = new CommonTokenStream(lexerj);
				List<ParserRuleContext> plist = new LinkedList<ParserRuleContext>();

				JavaParser parserj = new JavaParser(javaCommonStream);
				//			Lock lock = new ReentrantLock();
				RewriterJava rw = new RewriterJava(parserj);
				ParseTreeWalker walker_swift = new ParseTreeWalker();
				//			walker_swift.walk(new JavaBaseListener(), parserj.statement());
				//		s	parserj.
				//			parserj.compilationUnit();
				Class<? extends Parser> parserClass = null;
				ClassLoader cl = Thread.currentThread().getContextClassLoader();
				parserClass = cl.loadClass("javaparser.JavaParser").asSubclass(Parser.class);


				try {
					Method startRule = parserClass.getMethod(JavaParser.ruleNames[atype]);
					ParserRuleContext tree = (ParserRuleContext)startRule.invoke(parserj, (Object[])null);
					//					System.out.println(tree.getRuleIndex()+" \n"+tree);

					walker_swift.walk(rw, tree);
					//				while(!rw.pstack.isEmpty()){
					ParserRuleContext ctxx = rw.pstack.pop();
					LineCharIndex lineindex=new LineCharIndex(ctxx.getStart().getLine(), ctxx.getStart().getCharPositionInLine(),
							ctxx.getStop().getLine(),ctxx.getStop().getStopIndex());

					int az = ctxx.start.getStartIndex();
					int bz = ctxx.stop.getStopIndex();
					Interval interval = new Interval(az, bz);
					String context = ctxx.start.getInputStream().getText(interval);
					//					System.out.println("pop		"+context+" "+context.length());
					//get example context... 
					//query templates..
					//down load all templates
					Iterator<TemplateEDB> iter = item_map.keySet().iterator();
					//					String rterm = "@Override public void (.+?)\\((.+?) (.+?)\\) \\{((.?|\n)+)\\}";


					while(iter.hasNext()){

						TemplateEDB kkey = iter.next();
						//						if(kkey.count>1){
						TemplateEDB skey = item_map.get(kkey);


						//						Map<String, String> argmap = MatchingTemplate.getAttributesMap(kkey, context);
						//						Map<LineCharIndex, String> argmaplines = MatchingTemplate.getAttributesMapLineIndex(kkey, context);
						//						System.out.println("argmaplines"+argmaplines);
						//						System.out.println("argmaplines.get(lineindex)	"+lineindex+" "+ctxx.getText()+"   "+argmaplines.get(lineindex));
						//						MatchingTemplate2.getAttributesMapLineIndex(kkey, context);
						//						System.out.println("MatchingTemplate2"+MatchingTemplate2.getAttributesMapLineIndex(kkey, context));
						//						System.out.println("rw.maps"+rw.maps);
						//						Iterator<LineCharIndex> iitt = argmaplines.keySet().iterator();

						Map<LineCharIndex, String> abc = MatchingTemplate2.getAttributesMapLineIndex(kkey.template, context);
						//						Iterator<LineCharIndex> iitt2 = abc.keySet().iterator();

						/*
						while(iitt2.hasNext()){
							LineCharIndex nnxt = iitt2.next();
//							String vlu=abc.get(nnxt);
							String text = rw.maps.get(nnxt)!=null?rw.maps.get(nnxt).strASTNode:"none";
							System.out.println("nnxt	" + nnxt+" "+lineindex+"  "+text+"  "+abc.get(nnxt));
//							rw.maps(nnxt);
						}
						 */
						boolean discard= false;
						if(abc!=null && !abc.isEmpty()){
							String stemplate=skey.template.replaceAll("< ", "\\\\< ");
							ST st_swift = new ST(stemplate);
							System.out.println("abcc	"+kkey+"   "+skey+"  "+abc.keySet()+"  \n"+SimpleFormat.getSimpleFormat(context)+"  "+stemplate);
							//								System.err.println("rw.maps"+rw.maps);


							java.util.Iterator<LineCharIndex> kitt = rw.maps.keySet().iterator();
							while(kitt.hasNext()){
								LineCharIndex keyy = kitt.next();
								//								rw.maps.get(keyy).strASTNode;
								//																System.out.println("keyy	"+keyy+"   "+rw.maps.get(keyy).strASTNode);
							}
							Iterator<LineCharIndex> iterator = abc.keySet().iterator();
							//							int count =	0;
							Vector<String> checkoverlap = new Vector<String>();
							while(iterator.hasNext()){
								LineCharIndex key=iterator.next();

								//searching in context
								//								String texto = text;
								//								String text = rw.maps.get(key)!=null?rw.maps.get(key).strASTNode:"none";
								//check it has a context in source platform
								String text = rw.maps.get(key)!=null? rw.maps.get(key).strASTNode:key.example;
								//								String text = key.example;
								//								String subtree=argmap.get(key);
								String subs ="";
								
								//in case single token
								if(rw.maps.get(key)!=null && rw.maps.get(key).interval.length()==1){
									subs = text;
								}else{
									//const map.. just scan the value.
									String constantMatch=getConstant(text,testingsett[id]);
									if(constantMatch==null){
										subs = text;
									}else{
										subs = constantMatch;
									}
//									subs = getConstant(text, testingsett[id])==null?text:getConstant(text,testingsett[id]);
								}
								//								System.out.println("getConstant("+text+")"+getConstant(text,id));
								//								if(getConstant(text, testingsett[id])==null){
								//									subs = text;
								//								}
								String arg = abc.get(key);
								if(true){
									
								}
								if(arg.startsWith("<") && arg.endsWith(">") && !checkoverlap.contains(arg)){
									checkoverlap.add(arg);
									st_swift.add(arg.replaceAll("<", "").replaceAll(">", ""), subs);
								}
								if(rw.maps.get(key)==null && !arg.equals("{...}")){
									//									System.err.println("key"+arg+" "+key.example);
									discard = true;
								}
								if(StructureMappingAntlr4_for_stmt.java_keywords_list.contains(subs)){
									discard = true;
								}
							}
							//							if(count >0){

							//							System.out.println(aaa);
							//							System.out.println("st_android\n"+kkey+"   "+skey+"\n");
							//							System.err.println("final\n"+st_swift.render()+"\n");
							if(!discard){
								migrations.add(st_swift.render()+"\n");
								//								System.out.println("st_swift.render()\n"+st_swift.render());
							}
							//								replace( ctx, st_android.render());
							//							}
						}




					}
					if(migrations.size()>0){
						hit++;
						System.out.println(hit+" : "+(nohit+hit)+" out of "+cc);
						System.out.println(migrations);
						System.err.println(SimpleFormat.getSimpleFormat(bbb));
						System.out.println(SimpleFormat.getSimpleFormat(aaa));


					}
					if(migrations.size()==0){
						nohit++;
						System.out.println("no hit	"+(nohit+hit)+" out of "+cc);
						System.out.println(SimpleFormat.getSimpleFormat(aaa));
						System.err.println(SimpleFormat.getSimpleFormat(bbb));
					}

					migrations.clear();
					item_map.clear();
					//					}
				} catch (NoSuchMethodException | SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 

			}




			//			System.err.println(migrations.firstElement());
			System.out.println((id+1)+" folded	"+ hit+" out of "+cc);
			hit = 0;nohit = 0;

			cc = 0;
		} //itemset


		//		DBTablePrinter.printTable(DriverManager.getConnection("jdbc:sqlite:"+"test_record0413.db"), "grammar_android_stat");
		//		DBTablePrinter.printTable(DriverManager.getConnection("jdbc:sqlite:"+"test_record0413.db"), "constant_stat");
		//		DBTablePrinter.printTable(DriverManager.getConnection("jdbc:sqlite:"+"test_record0413.db"), "grammar_swift_stat");

		//		DBTablePrinter.printTable(connection, "grammar_swift_stat");

		//		queryExampleResultRandom("", 10);
	}

	public static String getConstant(String aConst, String [] id) throws SQLException{
		String sConst=null;

		String [] newa = new String[id.length];
		int i =0;

		for(String a:id){
			String f="\'"+a+"\'";
			newa[i] = f;
			i++;

		}

		String test=Arrays.toString(newa).replace("[","").replace("]","");

		String query ="SELECT example from constant_stat where template=\'"+aConst+"\' and afilename not in ("+test+") order by count desc LIMIT 1;";
		//		String query ="SELECT example, count(*) as c from constant_stat  where template=\'"+aConst+"\' group by template, example order by c desc LIMIT 1;";

		//				System.out.println("qqq"+query);
		Connection connection2 =DriverManager.getConnection("jdbc:sqlite:"+"test_record0415.db");
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		while(resultSet.next()){
			//			templateOk = false;
			//			flip = true;
			//			double c1 = 1.0000, c2 = 1.0001;

			//			for(int q=0;q<=cycle && !templateOk;q++){

			//				black_a =new LinkedList<OpProperty>();
			//				black_s=new LinkedList<OpProperty>();
			//			num++;
			sConst = resultSet.getString("example");
			//			System.out.println("Integer.parseInt"+Integer.parseInt(resultSet.getString("example")));
		}
		//		if(sConst!=null)
		return sConst;
		//		else return aConst;
	}
	public static String getStdFormTemplate(String template){

		String pattern = "( )(.)( )";
		Pattern r = Pattern.compile(pattern);
		//		String aaa = "@Override public void <arg0> ( <arg1> <arg2> ) - > <arg4> {...}";
		//		CharSequence text;
		Matcher m = r.matcher(template);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			//			System.out.println("g1"+m.group(1)+ " g0"+m.group(0)+"g2"+m.group(2));
			//			System.err.println(aaa.replaceAll("( ).( )",m.group(2)));
			//		    m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(1)), ""));
			m.appendReplacement(sb, m.group(0).replaceAll(Pattern.quote(m.group(1)), "").replaceFirst(Pattern.quote(m.group(3)), ""));
		}

		m.appendTail(sb);
		///	System.out.println(sb.toString());


		//		return sb.toString();
		return template;
	}



}
