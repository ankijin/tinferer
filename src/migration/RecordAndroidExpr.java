package migration;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;

import recording.MappingElement;

public class RecordAndroidExpr extends ASTVisitor{
	@Override
	public boolean visit(ExpressionStatement node) {
		// TODO Auto-generated method stub
		System.out.println("ExpressionStatement\n"+node.toString());
		return super.visit(node);
	}
	
	@Override
	public boolean visit(InfixExpression node) {
		// TODO Auto-generated method stub
//		System.out.println(node.getOperator());
		return super.visit(node);
	}
	
	@Override
	public boolean visit(SingleVariableDeclaration node) {
		// TODO Auto-generated method stub
		System.out.println("SingleVariableDeclaration");
		System.out.println(node.getType());
		System.out.println(node.getName());
		return super.visit(node);
	}
	
	@Override
	public boolean visit(Assignment node) {
		// TODO Auto-generated method stub
//		System.out.println(node.getLeftHandSide());
//		System.out.println(node.getRightHandSide());
		return super.visit(node);
	}
	
	public boolean visit(org.eclipse.jdt.core.dom.VariableDeclarationExpression node) {
		return super.visit(node);
	}
	
	//record return statement
	public boolean visit(org.eclipse.jdt.core.dom.ReturnStatement node) {
		Expression sss = node.getExpression();
		node.getParent().getNodeType();
		MappingElement e = new MappingElement(sss.toString(), sss.getNodeType());
		
//		recordStatement.add(e);
//		record.add(e);
		System.out.println("ReturnStatement\n"+node);
		return true;
	};
	
	@Override
	public void endVisit(ExpressionStatement node) {
		// TODO Auto-generated method stub
		super.endVisit(node);
	}

	@Override
	public boolean visit(IfStatement node) {
		// TODO Auto-generated method stub
		System.out.println("IfStatement\n"+node);
		return super.visit(node);
	}
	
	public static String readFileToString(String filePath) throws IOException {
		StringBuilder fileData = new StringBuilder(1000);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));

		char[] buf = new char[10];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
			buf = new char[1024];
		}
		reader.close();
		return  fileData.toString();	
	}
	
	public static void main(String[] args) throws FileNotFoundException, IOException{
	ASTParser parser = ASTParser.newParser(AST.JLS8);
	String path = "/Users/kijin/Documents/workspace_mars/GumTreeTester/charts/test_set/ViewPortHandler3.java";
	String str= readFileToString(path);
	parser.setSource(str.toCharArray());
	parser.setKind(ASTParser.K_COMPILATION_UNIT);
	Map<String, ASTNode> nodeAndroid = new HashMap<String, ASTNode>();
	final CompilationUnit cu = (CompilationUnit) parser.createAST(null);
	cu.accept(new RecordAndroidExpr());
	}
}
