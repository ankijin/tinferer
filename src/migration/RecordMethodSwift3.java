package migration;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;

import recording.MappingElement;

//import SwiftParser.Declaration_modifiersContext;

//import SwiftParser.Variable_declarationContext;

//import SwiftParser.ParameterContext;

//import SwiftParser.StatementContext;


//import SwiftParser.TypeContext;

//[public, var, offsetLeft, CGFloat, _contentRect.origin.x]
//import SwiftParser.Function_declarationContext;

public class RecordMethodSwift3<T> extends BaseSwiftRecorder<T>{

	public Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();
	LinkedList<MappingElement> record = new LinkedList<MappingElement> ();

	LinkedList<MappingElement> recordStatement = new LinkedList<MappingElement> ();
	Map<String, String> recordSwiftParams	= new HashMap<String, String>();
	@Override
	public T visitFunction_declaration(SwiftParser.Function_declarationContext ctx) {
		// TODO Auto-generated method stub
		SwiftParser.Declaration_modifiersContext ms = ctx.function_head().declaration_modifiers();
		String aaa = ms.getParent().getChild(1).getText();
		record.add(new MappingElement(ms.getText(), 1));
		record.add(new MappingElement(aaa, 1));
		
		record.add(new MappingElement(ctx.function_name().getText(), 1));
		record.add(new MappingElement(ctx.function_signature().function_result().type().getText(), 1));
		return super.visitFunction_declaration(ctx);
	}
//	
//	@Override
//	public T visitStatement(SwiftParser.StatementContext ctx) {
		// TODO Auto-generated method stub
//		System.err.println("visitStatement\n"+ctx.getText());
//		System.err.println("tree"+ ctx.getRuleIndex());
//		return super.visitStatement(ctx);
//	}

	
	@Override
	public T visitVariable_declaration(SwiftParser.Variable_declarationContext ctx) {
		// TODO Auto-generated method stub
		ctx.variable_name().getText();
		System.out.println();
//		ctx.variable_declaration_head().declaration_modifiers();
		SwiftParser.Declaration_modifiersContext ms = ctx.variable_declaration_head().declaration_modifiers();
		String aaa = ms.getParent().getChild(1).getText();
		record.add(new MappingElement(ms.getText(), 1));
		record.add(new MappingElement(aaa, 1));
		record.add(new MappingElement(ctx.variable_name().getText(), 1));
		record.add(new MappingElement(ctx.type_annotation().get(0).type().getText(), 1));
		return super.visitVariable_declaration(ctx);
		
//		return null;
	}



	
	@Override
	public T visitExpression(SwiftParser.ExpressionContext ctx) {
		// TODO Auto-generated method stub
		if(ctx.getParent() instanceof SwiftParser.Return_statementContext){
//			System.out.println("instanceof SwiftParser.Return_statementContext");
			record.add(new MappingElement(ctx.getText(), ctx.getRuleIndex()));
		}
		return super.visitExpression(ctx);
	}
	

	//[private, float, mMaxScaleX, Float.MAX_VALUE]
	
	@Override
	public T visitParameter(SwiftParser.ParameterContext ctx) {
		// TODO Auto-generated method stub
		System.out.println("exitParameter");
		System.out.println(ctx.local_parameter_name().getText());
		System.out.println(ctx.type_annotation().type().getText());
		
		recordSwiftParams.put(ctx.local_parameter_name().getText(), ctx.type_annotation().type().getText());
		
		return super.visitParameter(ctx);
	}
	
	
	public static void main(String[] args) throws FileNotFoundException, IOException{
		RecordMethodSwift3 rrr = new RecordMethodSwift3<>();
		//		System.out.println("RECORD Swift\n"+s.toStringTree());
//		s.accept(rrr);
		String path = "/Users/kijin/Documents/workspace_mars/GumTreeTester/charts/test_set/test.swift";
		
		Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();
		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new SwiftLexer((CharStream)stream1);
		SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
		SwiftParser.StatementsContext root1 = parser1.statements();
		
//		SwiftParser.Function_declarationContext root_func = parser1.function_declaration();
		
		root1.accept(rrr);
		
		
		System.out.println(rrr.record);
		System.out.println(rrr.recordSwiftParams);
		
		//[public, func, offsetLeft, CGFloat, _contentRect.origin.x]
		//[public, var, offsetLeft, CGFloat, _contentRect.origin.x]
		//[public, var, offsetLeft, _contentRect.origin.x]
		//[public, func, isInBoundsTop, _contentRect.origin.y<=y?true:false]
	}
	
}
