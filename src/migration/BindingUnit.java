package migration;

public class BindingUnit {
	public String mbinding;
	public String vars;
	public String consts;
	public String template;
	public String receiver;
	public String params;
	
	public BindingUnit(String m, String v, String c){
		mbinding = m;
		vars = v;
		consts = c;
	}
	public BindingUnit(String m, String v, String c,String t){
		mbinding 	= m;
		vars 		= v;
		consts 		= c;
		template 	= t;
	}
}
