package migration;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import com.github.gumtreediff.actions.ActionGenerator;
import com.github.gumtreediff.actions.model.Action;
import com.github.gumtreediff.matchers.Matcher;
import com.github.gumtreediff.matchers.Matchers;
import com.github.gumtreediff.tree.ITree;

public class Tester {
	
    public void testFileParsing() throws IOException {

    //	ITree src = new RhinoTreeGenerator().generateFromFile("gumtree_v0.js").getRoot();
   // 	ITree dst = new RhinoTreeGenerator().generateFromFile("gumtree_v2.js").getRoot();
    //	System.out.println(dst.toTreeString());
    //	ITree src = new RhinoTreeGenerator().generateFromFile("gumtree_v0.js").getRoot();
    	
    	ITree src1 = new SwiftTreeGen().generateFromFile("Empty2.swift").getRoot();
    	//System.err.println("getlabel");
    //	System.out.println(src1.toTreeString());
    
    	ITree dst1 = new SwiftTreeGen().generateFromFile("GPSAdded2.swift").getRoot();
    //  	System.out.println(dst1.toTreeString());
      	
      	
  
    //	ITree dst1 = new AntlrSwiftTreeGenerator().generate(new FileReader("Empty2.swift")).getRoot();
    
    	Matcher m = Matchers.getInstance().getMatcher(src1, dst1); // retrieve the default matcher
        m.match();
        ActionGenerator g = new ActionGenerator(src1, dst1, m.getMappings());
        g.generate();
        List<Action> actions = g.getActions(); // return the actions
		for (Action a:actions){
			System.out.println(a);
		}
		
		
//    	ITree test = new SwiftTreeGen().generateFromFile("Empty.swift").getRoot();
//      	System.out.println(test.toTreeString());
    }

	public static void main(String[] args) throws UnsupportedOperationException, IOException {

		Tester tester = new Tester();
		tester.testFileParsing();

	}
}
