package migration;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.antlr.v4.runtime.ParserRuleContext;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionMethodReference;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.VariableDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;
import org.simmetrics.StringMetric;
import org.simmetrics.StringMetrics;
import org.stringtemplate.v4.ST;

//import SwiftParser.If_statementContext;
import javassist.compiler.ast.Visitor;
//import SwiftParser.Expression_elementContext;
//import SwiftParser.Expression_element_listContext;
import net.vivin.GenericTree;
import net.vivin.GenericTreeNode;
import net.vivin.GenericTreeTraversalOrderEnum;
import recording.SwiftParser.Superclass_expressionContext;
import sqldb.DBTablePrinter;
import sqldb.ProtoSQLiteJDBC;
import sqldb.SQLiteJDBC;

//import SwiftParser.Function_call_expressionContext;

//import SwiftParser.StatementContext;

//import recording.SwiftParser;
//import recording.SwiftParser.Return_statementContext;
//import recording.SwiftParser.StatementContext;

public class MappingStatements2 {
	public Map<String, String> localtypeMapAndroid 		= new HashMap<String, String>();
	public Map<String, String> localtypeMapSwift 		= new HashMap<String, String>();
	static int stmts_id =1;
	static int umatched_s_id =1;
	static int umatched_a_id =1;
	public String returnAndroid="";
	public String returnSwift="";
	static public String classnameAndroid="";
	static public String classnameSwift="";

	static public String methodnameAndroid="";
	static public String methodnameSwift="";

	class FeatureStatement{
		Integer type = -1;
		String 	name ="name";

		public FeatureStatement(Integer t){
			type = t;	
			name ="name";
		}
		public FeatureStatement(String n){
			name = n;
		}
		public FeatureStatement(Integer t, String n){
			type = t;
			name = n;
		}
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return type+":"+name;
		}

	}
	static int expr_id=1;
	public static Map<Integer, Object> map_a= new HashMap<Integer, Object>();
	public List<SwiftParser.StatementContext> swift_stmts_list;
	public List<org.eclipse.jdt.core.dom.Statement>  android_stmts_list;
	Map<String, String> s_mapping = new HashMap<String, String>();
	public void MappingStatements(List<org.eclipse.jdt.core.dom.Statement>  android, List<SwiftParser.StatementContext> swift){

		android_stmts_list 	= android;
		swift_stmts_list 	= swift;

		//		map_a.put(ReturnStatement.RETURN_STATEMENT, ReturnStatement);
	}

	public void mapping() throws ClassNotFoundException, SQLException{

		List<org.eclipse.jdt.core.dom.Statement> android_stmts_list_copy = new LinkedList<org.eclipse.jdt.core.dom.Statement>();

		List<SwiftParser.StatementContext>	swift_stmts_list_copy 	= new LinkedList<SwiftParser.StatementContext>();
		//		System.out.println("mapping"+android_stmts_list);
		//		System.out.println("mapping"+swift_stmts_list);



		int a_num = 0;
		int s_num = 0;
		try {
			a_num = android_stmts_list.size();
			s_num = swift_stmts_list.size();
		} catch (NullPointerException e){
//			System.out.println("android_stmts_list size"+android_stmts_list==null);
		}
		//		Vector<Integer> a_list = new Vector<Integer>();
		//		Vector<Integer> s_list = new Vector<Integer>();
		StringBuffer a_body = new StringBuffer();
		Map<Integer,FeatureStatement> a_list = new HashMap<Integer, FeatureStatement>();
		Map<Integer,FeatureStatement> s_list = new HashMap<Integer, FeatureStatement>();
		for(int i=0;i<android_stmts_list.size();i++){
			Statement a_stmt = android_stmts_list.get(i);
			android_stmts_list_copy.add(i, a_stmt);
			a_body.append(a_stmt);

			if(a_stmt instanceof ForStatement){
				System.out.println("ForStatement"+a_stmt);
			}
			if(a_stmt instanceof ReturnStatement){
				//				a_list.add(i,  ReturnStatement.RETURN_STATEMENT);
				a_list.put(i, new FeatureStatement(ReturnStatement.RETURN_STATEMENT));
			}
			if(a_stmt instanceof ExpressionStatement){
				//				a_list.add(i, ExpressionStatement.EXPRESSION_STATEMENT);
				ExpressionStatement aaa = (ExpressionStatement)a_stmt;

				a_list.put(i, new FeatureStatement(ExpressionStatement.EXPRESSION_STATEMENT, aaa.getExpression().toString()));
			}
			if( a_stmt instanceof IfStatement){
				//				a_list.add(i, IfStatement.IF_STATEMENT);
				a_list.put(i, new FeatureStatement(IfStatement.IF_STATEMENT));
			}
			if( a_stmt instanceof VariableDeclarationStatement){
				VariableDeclarationStatement var_decl_stmt = (VariableDeclarationStatement) a_stmt; 
				VariableDeclarationFragment frag = (VariableDeclarationFragment)var_decl_stmt.fragments().get(0);
				a_list.put(i, new FeatureStatement(VariableDeclaration.VARIABLE_DECLARATION_STATEMENT, frag.getName().toString()));
			}
			if( a_stmt instanceof ForStatement){
				ForStatement for_stmt = (ForStatement) a_stmt; 
				//				VariableDeclarationFragment frag = (VariableDeclarationFragment)var_decl_stmt.
				a_list.put(i, new FeatureStatement(ForStatement.FOR_STATEMENT, for_stmt.toString()));
			}

			if( a_stmt instanceof WhileStatement){
				WhileStatement while_stmt = (WhileStatement) a_stmt; 
				//				VariableDeclarationFragment frag = (VariableDeclarationFragment)var_decl_stmt.
				a_list.put(i, new FeatureStatement(WhileStatement.WHILE_STATEMENT, while_stmt.toString()));
			}
		}

		//		System.out.println(a_num+"!STATEMENT"+a_list);

		StringBuffer body = new StringBuffer();
		for(int j=0;j<swift_stmts_list.size();j++){
			SwiftParser.StatementContext stmt = swift_stmts_list.get(j);
			swift_stmts_list_copy.add(j, stmt);
			//			System.out.println("j"+j);
			body.append(stmt.getText());
			//			System.out.println(stmt.getText());
			body.append("\n");
			
			
			//classify stmts type
			if(stmt.expression()!=null){
				s_list.put(j, new FeatureStatement(SwiftParser.RULE_expression, stmt.expression().getText()));
//				System.err.println("RULE_expression"+SwiftParser.RULE_expression);
			}
			if(stmt.branch_statement() !=null && stmt.branch_statement().if_statement()!=null){
				s_list.put(j, new FeatureStatement(SwiftParser.RULE_if_statement));
			}
			if(stmt.control_transfer_statement() !=null && stmt.control_transfer_statement().return_statement()!=null){
				s_list.put(j, new FeatureStatement(SwiftParser.RULE_return_statement));
			}
			if(stmt.declaration() !=null && stmt.declaration().constant_declaration()!=null){
				String name = stmt.declaration().constant_declaration().pattern_initializer_list().pattern_initializer(0).pattern().getText();
				s_list.put(j, new FeatureStatement(SwiftParser.RULE_constant_declaration, name));
//				System.err.println("RULE_constant_declaration"+SwiftParser.RULE_constant_declaration);
			}
			if(stmt.declaration() !=null && stmt.declaration().variable_declaration()!=null){
				s_list.put(j, new FeatureStatement(SwiftParser.RULE_variable_declaration));
//				System.err.println("RULE_variable_declaration"+SwiftParser.RULE_variable_declaration);
			}

			if(stmt.declaration() !=null && stmt.loop_statement()!=null && stmt.loop_statement().for_in_statement()!=null){
				//				stmt.declaration().
				s_list.put(j, new FeatureStatement(SwiftParser.RULE_for_in_statement));
				//				System.err.println("RULE_variable_declaration"+SwiftParser.RULE_variable_declaration);
			}

			if(stmt.declaration() !=null && stmt.loop_statement()!=null && stmt.loop_statement().for_statement()!=null){
				//				stmt.declaration().
				s_list.put(j, new FeatureStatement(SwiftParser.RULE_for_statement));
				//				System.err.println("RULE_variable_declaration"+SwiftParser.RULE_variable_declaration);
			}
		}

		//		Connection c = null;
		//		java.sql.Statement sql_stmt = null;
		//		Class.forName("org.sqlite.JDBC");
		//		c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		//		c.setAutoCommit(false);
		//System.out.println("Opened database successfully");

		//		sql_stmt = c.createStatement();

		//		sql_stmt.execute(ProtoSQLiteJDBC.dropTable("expr_swift"));
		//		sql_stmt.execute(ProtoSQLiteJDBC.dropTable("expr_android"));
		//				sql_stmt.executeUpdate(ProtoSQLiteJDBC.createExprTablesAndroid());
		//		sql_stmt.executeUpdate(ProtoSQLiteJDBC.createExprTablesSwift());
		MappingSingleStatement.stmtsMap = this;
		//		System.out.println("#variable mappings");
		//		System.out.println(Main.typeMapAndroid);
		//		System.out.println(Main.typeMapSwift);

		Vector<String> vect_a = new Vector<String>();
		Vector<String> vect_s = new Vector<String>();

		Vector<Integer> vect_idx_a = new Vector<Integer>();
		Vector<Integer> vect_idx_s = new Vector<Integer>();
		double threshold = 0.9;
		
		
		
		
		//alignment of statements
		for(int i=0;i<a_num;i++){
			for(int j=0;j<s_num;j++){
				if(a_list.get(i) !=null && s_list.get(j) !=null){

					StringMetric metric = StringMetrics.jaroWinkler();
					if(a_list.get(i).type== IfStatement.IF_STATEMENT && s_list.get(j).type == SwiftParser.RULE_if_statement
							&& metric.compare(a_list.get(i).name, s_list.get(j).name) >threshold)
					{
						System.out.println("android_stmts_list.get(i)"+android_stmts_list.get(i));
						MappingSingleStatement.compute(android_stmts_list.get(i), a_list.get(i).type, swift_stmts_list.get(j), s_list.get(j).type);
						vect_a.add((android_stmts_list.get(i).toString().replace("\n","")));
						vect_s.add(swift_stmts_list.get(j).getText());
						vect_idx_a.add(i);
						vect_idx_s.add(j);
					}
					if(a_list.get(i).type== ReturnStatement.RETURN_STATEMENT && s_list.get(j).type == SwiftParser.RULE_return_statement
							&& metric.compare(a_list.get(i).name, s_list.get(j).name) >threshold)
					{
						MappingSingleStatement.compute(android_stmts_list.get(i), a_list.get(i).type, swift_stmts_list.get(j), s_list.get(j).type);
						vect_a.add((android_stmts_list.get(i).toString().replace("\n","")));
						vect_s.add(swift_stmts_list.get(j).getText());
						vect_idx_a.add(i);
						vect_idx_s.add(j);

					}

					if(a_list.get(i).type== VariableDeclaration.VARIABLE_DECLARATION_STATEMENT && s_list.get(j).type == SwiftParser.RULE_constant_declaration
							&& metric.compare(a_list.get(i).name, s_list.get(j).name) >threshold)
					{
						//						System.out.println("VARIABLE_DECLARATION_STATEMENT");
						//						System.out.println(android_stmts_list.get(i).toString());
						//						System.out.println(swift_stmts_list.get(j).getText());
						MappingSingleStatement.compute(android_stmts_list.get(i), a_list.get(i).type, swift_stmts_list.get(j), s_list.get(j).type);
						vect_a.add((android_stmts_list.get(i).toString().replace("\n","")));
						vect_s.add(swift_stmts_list.get(j).getText());
						vect_idx_a.add(i);
						vect_idx_s.add(j);

					}

					if(a_list.get(i).type== ExpressionStatement.EXPRESSION_STATEMENT && s_list.get(j).type == SwiftParser.RULE_expression
							&& metric.compare(a_list.get(i).name, s_list.get(j).name) >threshold)
					{

						System.out.println("MExpressionStatement.EXPRESSION_STATEMENT"+android_stmts_list.get(i));
						//						System.out.println(android_stmts_list.get(i).toString());
						//						System.out.println(swift_stmts_list.get(j).getText());
						MappingSingleStatement.compute(android_stmts_list.get(i), a_list.get(i).type, swift_stmts_list.get(j), s_list.get(j).type);
						vect_a.add((android_stmts_list.get(i).toString().replace("\n","")));
						vect_s.add(swift_stmts_list.get(j).getText());
						vect_idx_a.add(i);
						vect_idx_s.add(j);
					}

					if(a_list.get(i).type== ExpressionStatement.EXPRESSION_STATEMENT && s_list.get(j).type == SwiftParser.RULE_expression
							&& metric.compare(a_list.get(i).name, s_list.get(j).name) >threshold)
					{

						System.out.println("MExpressionStatement.EXPRESSION_STATEMENT"+android_stmts_list.get(i));
						//						System.out.println(android_stmts_list.get(i).toString());
						//						System.out.println(swift_stmts_list.get(j).getText());
						MappingSingleStatement.compute(android_stmts_list.get(i), a_list.get(i).type, swift_stmts_list.get(j), s_list.get(j).type);
						vect_a.add((android_stmts_list.get(i).toString().replace("\n","")));
						vect_s.add(swift_stmts_list.get(j).getText());
						vect_idx_a.add(i);
						vect_idx_s.add(j);
					}
				}
			}
		}
		//		System.out.println(vect_a);
		//		System.out.println(vect_s);

		String ss ="", aa="";
		for(SwiftParser.StatementContext s:swift_stmts_list_copy){
			//			System.out.println(s.getText());
			ss +=s.getText().toString();
			ss+=" ";
		}
		for(org.eclipse.jdt.core.dom.Statement s:android_stmts_list_copy){
			//			System.out.println(s.getText());
			aa +=s.toString().replace("\n", "");
			aa+=" ";
		}
		//		vect_idx_a = new Vector<Integer>();
		//		vect_idx_s = new Vector<Integer>();


		System.out.println("vect_idx_a"+vect_idx_a+a_list);
		System.out.println("vect_idx_s"+vect_idx_s+s_list);
		for(int ain:vect_idx_a){
			android_stmts_list_copy.remove(android_stmts_list.get(ain));
		}
		for(int sin:vect_idx_s){
			swift_stmts_list_copy.remove(swift_stmts_list.get(sin));
		}

		Connection c = null;
		java.sql.Statement sql_stmt = null;
		Class.forName("org.sqlite.JDBC");
		c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		c.setAutoCommit(false);
		sql_stmt = c.createStatement();

		String ss1="", aa1="";
		for(SwiftParser.StatementContext s:swift_stmts_list_copy){
//			System.out.println("unmatched s"+s.getText().replaceAll("\"", "").replaceAll("\'", ""));
			String expr = s.getText().replaceAll("\"", "").replaceAll("\'", "");
			String gexpr= MappingSingleStatement.makeTemplateSwift(expr);
			String sql= ProtoSQLiteJDBC.insertTableUnmatchedExprSwiftV(umatched_s_id, expr, gexpr, stmts_id, "");
			sql_stmt.executeUpdate(sql);
			c.commit();
			umatched_s_id++;
		}
		for(org.eclipse.jdt.core.dom.Statement s:android_stmts_list_copy){
//			System.out.println("unmatched a"+s);
			//			String sql= ProtoSQLiteJDBC.insertTableUnmatchedExprSwiftV(umatched_a_id, s.toString(), s.toString(), stmts_id, "");
			//			sql_stmt.executeUpdate(sql);
			//			c.commit();
			//			MappingSingleStatement.computeAndroid(s);
			if(s instanceof ReturnStatement){

			}
			if(s instanceof IfStatement){

			}

		}

		ss= ss.replaceAll("\"", "").replaceAll("\'", "");
		aa= aa.replaceAll("\"", "").replaceAll("\'", "");
//		aa=aa.replaceAll("\"", "\\\"");
		if(aa!="" ||ss!=""){
			String e12 = SQLiteJDBC.insertStmtsAndroidTable(stmts_id, aa, this.classnameAndroid+"."+this.methodnameAndroid);
			String e11 = SQLiteJDBC.insertStmtsSwiftTable(stmts_id, ss, this.classnameSwift+"."+this.methodnameSwift,stmts_id);
			stmts_id++;
//			System.out.println("e12"+e12);
			sql_stmt.executeUpdate(e12);	
		
			sql_stmt.executeUpdate(e11);

			c.commit();
		}

	}


}
