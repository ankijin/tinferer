package migration;

import java.io.FileNotFoundException;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import org.antlr.v4.runtime.ParserRuleContext;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;

import mapping.MappingElementExpr;
import mapping.MappingMethods;
//import recording.SwiftParser;
import sqldb.SQLiteJDBC;

public class MappingDeclaration2{
	FieldDeclaration android;
	SwiftParser.Variable_declarationContext swift;
	public static Map<String, String> localtypeMapAndroid 		= new HashMap<String, String>();
	public static Map<String, String> localtypeMapSwift 		= new HashMap<String, String>();
	public static Map<MappingElement, MappingElement> mappingElements1 = new HashMap<MappingElement, MappingElement>();
	public static Map<String, ASTNode> varDeclAndroid 			= new HashMap<String, ASTNode>();
	public static Map<String, ParserRuleContext> varDeclSwift 	= new HashMap<String, ParserRuleContext>();
	public static String methodnameAndroid;
	public static String methodnameSwift;
	
//	public static MappingStatements  map_stmts = new MappingStatements();
	public static int sid;

	public String getType(FieldDeclaration f, String name){
		RecordVarDeclAndroid recordAndroid = new RecordVarDeclAndroid();
		f.accept(recordAndroid);
		if(name.equals(recordAndroid.record.get(2).label)){
			return recordAndroid.record.get(1).label;
		}
		return null;
	}

	public String getTypeSwift(SwiftParser.Variable_declarationContext swift, String name){

		RecordVarDeclSwift recordSwift = new RecordVarDeclSwift<>();
		swift.accept(recordSwift);

		if(name.equals(((MappingElement)recordSwift.record.get(2)).label.toString())){
			return ((MappingElement)recordSwift.record.get(3)).label;
		}
		return null;
	}

	public MappingDeclaration2(FieldDeclaration a, SwiftParser.Variable_declarationContext s){
		android = a;
		swift 	= s;
	}

	Map<Integer, Integer> map_types	= new HashMap<Integer, Integer>();
	public MappingDeclaration2(){
		map_types.put(25, 18);
		map_types.put(21, 76);
		map_types.put(41, 36);
	}

	String params_android="";
	String stms_android ="";
	String name_android ="";
	String modifiers_android="";
	String type_android="";

	String params_swift="";
	String stms_swift ="";
	String name_swift ="";
	String modifiers_swift="";
	String type_swift="";
	String return_android="NULL";

	List<org.eclipse.jdt.core.dom.Statement> android_stmts_list =new LinkedList<org.eclipse.jdt.core.dom.Statement>();

	public void setElementMethodDecl(ASTNode a, ParserRuleContext s) throws IOException, SQLException, ClassNotFoundException, InterruptedException{
		Map<String, String> recordAndroidParams	= new HashMap<String, String>();
		Map<String, String> recordSwiftParams	= new HashMap<String, String>();
		LinkedList<MappingElement> recordStatement = new LinkedList<MappingElement> ();

		LinkedList<MappingElement> record = new LinkedList<MappingElement> ();
		recordAndroidParams.clear();
		recordSwiftParams.clear();
		a.accept(new ASTVisitor() {
			
			@Override
			public boolean visit(MethodDeclaration node) {
				// TODO Auto-generated method stub
				name_android 		= node.getName().toString();
				
			
				
				List<SingleVariableDeclaration> params = node.parameters();
				for(SingleVariableDeclaration varr:params){		
					recordAndroidParams.put(varr.getName().toString(), varr.getType().toString());
				}
				if(node.getReturnType2()!=null)
					type_android 		= node.getReturnType2().toString();
				modifiers_android 	= node.modifiers().toString();
				if(!node.parameters().isEmpty()){
					params_android 	= node.parameters().toString().replace("\n", " ");
				}
				if( node.getBody()!=null && node.getBody().statements()!=null){
					stms_android 	= node.getBody().statements().toString().replace("\n", "");
					android_stmts_list = node.getBody().statements();
				}
				if(node.getReturnType2()!=null){
					return_android =node.getReturnType2().toString(); 
				}
				return super.visit(node);
			}
		});//record android method


		Connection c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		Statement stmt = c.createStatement();
		
		c.setAutoCommit(true);

		//		System.out.println("recordAndroidParams\n"+recordAndroidParams);

		//		Iterator<String> pa_iter = recordAndroidParams.keySet().iterator();
		RecordMethodSwift rrr = new RecordMethodSwift<>();
		s.accept(rrr);

		name_swift = RecordMethodSwift.name_swift;


		Iterator<String> ps_iter = rrr.recordSwiftParams.keySet().iterator();
		MappingStatements map_stmts = new MappingStatements();
		//		android_stmts_list, rrr.stmt_list
//		map_stmts.localtypeMapAndroid = recordAndroidParams;
//		map_stmts.localtypeMapSwift = rrr.recordSwiftParams;
		map_stmts.swift_stmts_list = rrr.stmt_list;
		map_stmts.android_stmts_list =android_stmts_list;
//		map_stmts.returnAndroid = return_android;
//		map_stmts.returnSwift = rrr.returnType;
//		map_stmts.classnameAndroid = Main.androidClassName;
//		map_stmts.classnameSwift = Main.swiftClassName;

		methodnameAndroid = name_android;
		methodnameSwift = name_swift;

		map_stmts.mapping();
		Map<String, String> mm_va = new HashMap<String, String>();
//		System.out.println(mm_va+"::::"+recordAndroidParams+",,"+rrr.recordSwiftParams);
		if(recordAndroidParams.size()==rrr.recordSwiftParams.size()){
			mm_va = MappingMethods.mapping(recordAndroidParams, rrr.recordSwiftParams);
//			System.out.println(map_stmts.localtypeMapAndroid+":::"+map_stmts.localtypeMapSwift);
			System.out.println(mm_va+"+::::+"+recordAndroidParams+",,"+rrr.recordSwiftParams);
			Iterator<String> iter = mm_va.keySet().iterator();
			if(iter.hasNext()){
				String a_param=iter.next();
				String t_param= recordAndroidParams.get(a_param);
				String s_param =(String) rrr.recordSwiftParams.get(mm_va.get(a_param));
				
//				System.out.println(t_param+"####"+s_param);
				
				String i3 = SQLiteJDBC.insertTableAndroid("var_android", id,t_param,name_android,"","","");
				String i4 = SQLiteJDBC.insertTableSwift("var_swift", id, s_param,name_swift,rrr.func_decl,"","",id);
				
				stmt.executeUpdate(i3);
				stmt.executeUpdate(i4);	
				id++;
				
			}
		}else{
			
		}

		Iterator<String> dddd = mm_va.keySet().iterator();
		int aaaaa = rrr.recordStatement.size();
		int bbbbb = recordStatement.size();
		String i3="";
		String i4="";
		mid++;		
		if(return_android=="NULL" || rrr.returnType=="NULL"){
			i3 = SQLiteJDBC.insertTableAndroidM("var_android", id,Main.androidClassName,name_android,"");
			i4 = SQLiteJDBC.insertTableSwiftM("var_swift", id, Main.swiftClassName,name_swift,"",id);
		}else{
			//			insertTableAndroidM
			i3 = SQLiteJDBC.insertTableAndroid("var_android", id,Main.androidClassName,name_android,"",return_android,"");
			i4 = SQLiteJDBC.insertTableSwift("var_swift", id, Main.swiftClassName,name_swift,"",rrr.returnType,"",id);
		}
		stmt.executeUpdate(i3);
		stmt.executeUpdate(i4);	
		id++;
	}

	static LinkedList<VarDeclElement> alist = new LinkedList<VarDeclElement>();
	static LinkedList<VarDeclElement> slist = new LinkedList<VarDeclElement>();

	static Map<String, VarDeclElement> amapping 	= new HashMap<String, VarDeclElement>();
	static Map<String, VarDeclElement> smapping 	= new HashMap<String, VarDeclElement>();
	static Map<String, String> mappingKeysVarDecl 	= new HashMap<String, String>();
	static int id 	= 1;
	static int mid 	= 1;
	public <T> void mappingVarDecl() throws FileNotFoundException, IOException, ClassNotFoundException, SQLException{
		
		
		
		RecordVarDeclSwift recordSwift = new RecordVarDeclSwift<>();
		swift.accept(recordSwift);

		RecordVarDeclAndroid recordAndroid = new RecordVarDeclAndroid();
		android.accept(recordAndroid);
		
		int na = recordAndroid.record.size();
		int ns = recordSwift.record.size();

		//		Class.forName("org.sqlite.JDBC");

		Connection connection = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		Statement stmt = connection.createStatement();

//		System.out.println();
		
		if(na==ns && na==4){

			String a1 = recordAndroid.recordStr.get(0);
			String a2 = recordAndroid.recordStr.get(1);
			String a3 = recordAndroid.recordStr.get(2);
			String a4 = recordAndroid.recordStr.get(3);
			alist.add(new VarDeclElement(a1, a2, a3, a4));
			amapping.put(a3, new VarDeclElement(a1, a2, a3, a4));
			String s1 = ((MappingElement) recordSwift.record.get(0)).label;
			String s2 = ((MappingElement) recordSwift.record.get(1)).label;
			String s3 = ((MappingElement) recordSwift.record.get(2)).label;
			String s4 = ((MappingElement) recordSwift.record.get(3)).label;
			slist.add(new VarDeclElement(s1, s2, s3, s4));

			smapping.put(s3, new VarDeclElement(s1, s2, s3, s4));

			mappingKeysVarDecl.put(a3, s3);
			String i3 = SQLiteJDBC.insertTableAndroid("var_android", id,Main.androidClassName,a3,a1,a2,a4);
			String i4 = SQLiteJDBC.insertTableSwift("var_swift", id, Main.swiftClassName,s3,s1,s2,s4,id);
			stmt.executeUpdate(i3);
			stmt.executeUpdate(i4);	
			id++;
		}
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "[android:"+android.toString()+", swift:"+swift.getText()+"]";
	}
}
