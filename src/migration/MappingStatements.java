package migration;


import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.antlr.v4.runtime.ParserRuleContext;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionMethodReference;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.VariableDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;
import org.simmetrics.StringMetric;
import org.simmetrics.StringMetrics;
import org.stringtemplate.v4.ST;

//import SwiftParser.StatementContext;
//import SwiftParser.StatementContext;
//import SwiftParser.If_statementContext;
import javassist.compiler.ast.Visitor;
//import SwiftParser.Expression_elementContext;
//import SwiftParser.Expression_element_listContext;
import net.vivin.GenericTree;
import net.vivin.GenericTreeNode;
import net.vivin.GenericTreeTraversalOrderEnum;
import recording.SwiftParser.Superclass_expressionContext;
import sqldb.DBTablePrinter;
import sqldb.ProtoSQLiteJDBC;
import sqldb.SQLiteJDBC;

//import SwiftParser.Function_call_expressionContext;

//import SwiftParser.StatementContext;

//import recording.SwiftParser;
//import recording.SwiftParser.Return_statementContext;
//import recording.SwiftParser.StatementContext;

public class MappingStatements {
	//	public Map<String, String> localtypeMapAndroid 		= new HashMap<String, String>();
	//	public Map<String, String> localtypeMapSwift 		= new HashMap<String, String>();
	static int stmts_id =1;
	static int umatched_s_id =1;
	static int umatched_a_id =1;
	//	public String returnAndroid="";
	//	public String returnSwift="";
	//	static public String classnameAndroid="";
	//	static public String classnameSwift="";

	//	static public String methodnameAndroid="";
	//	static public String methodnameSwift="";

	class FeatureStatement{
		Integer type = -1;
		String 	name ="name";

		public FeatureStatement(Integer t){
			type = t;	
			name ="name";
		}
		public FeatureStatement(String n){
			name = n;
		}
		public FeatureStatement(Integer t, String n){
			type = t;
			name = n;
		}
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return type+":"+name;
		}

	}
	static int expr_id=1;
	public static Map<Integer, Object> map_a= new HashMap<Integer, Object>();
	public List<SwiftParser.StatementContext> swift_stmts_list;
	public List<org.eclipse.jdt.core.dom.Statement>  android_stmts_list;
	Map<String, String> s_mapping = new HashMap<String, String>();
	public void MappingStatements(List<org.eclipse.jdt.core.dom.Statement>  android, List<SwiftParser.StatementContext> swift){

		android_stmts_list 	= android;
		swift_stmts_list 	= swift;
		try {
			new MappingSingleStatement();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//		map_a.put(ReturnStatement.RETURN_STATEMENT, ReturnStatement);
	}

	public void mapping() throws ClassNotFoundException, SQLException, InterruptedException{

		List<org.eclipse.jdt.core.dom.Statement> android_stmts_list_copy = new LinkedList<org.eclipse.jdt.core.dom.Statement>();

		List<SwiftParser.StatementContext>	swift_stmts_list_copy 	= new LinkedList<SwiftParser.StatementContext>();
//				System.out.println("mapping"+android_stmts_list);
//				System.err.println("mapping"+swift_stmts_list);



//		int a_num = 0;
//		int s_num = 0;
//		try {
//			a_num = android_stmts_list.size();
//			s_num = swift_stmts_list.size();
//		} catch (NullPointerException e){
			//			System.out.println("android_stmts_list size"+android_stmts_list==null);
//		}
		//		Vector<Integer> a_list = new Vector<Integer>();
		//		Vector<Integer> s_list = new Vector<Integer>();
		StringBuffer a_body = new StringBuffer();
		Map<Integer,FeatureStatement> a_list = new HashMap<Integer, FeatureStatement>();
		Map<Integer,FeatureStatement> s_list = new HashMap<Integer, FeatureStatement>();


		Map<Integer,List<org.eclipse.jdt.core.dom.Statement>> a_cluster = new HashMap<Integer, List<org.eclipse.jdt.core.dom.Statement>>();
		Map<Integer,List<SwiftParser.StatementContext>> s_cluster = new HashMap<Integer, List<SwiftParser.StatementContext>>();
		if(android_stmts_list==null || swift_stmts_list==null)  return;
		for(int i=0;i<android_stmts_list.size();i++){
			Statement a_stmt = android_stmts_list.get(i);
			android_stmts_list_copy.add(i, a_stmt);
			a_body.append(a_stmt);
			//			a_stmt.getNodeType();
			List<Statement> list = a_cluster.get(a_stmt.getNodeType());
			if(list==null){
				list = new LinkedList<Statement>();
			}
			list.add(a_stmt);
			a_cluster.put(a_stmt.getNodeType(),list);


			if(a_stmt instanceof ForStatement){
				//				System.out.println("ForStatement"+a_stmt);
			}
			if(a_stmt instanceof ReturnStatement){
				//				a_list.add(i,  ReturnStatement.RETURN_STATEMENT);
				a_list.put(i, new FeatureStatement(ReturnStatement.RETURN_STATEMENT));
			}
			if(a_stmt instanceof ExpressionStatement){
				//				a_list.add(i, ExpressionStatement.EXPRESSION_STATEMENT);
				ExpressionStatement aaa = (ExpressionStatement)a_stmt;

				a_list.put(i, new FeatureStatement(ExpressionStatement.EXPRESSION_STATEMENT, aaa.getExpression().toString()));
			}
			if( a_stmt instanceof IfStatement){
				//				a_list.add(i, IfStatement.IF_STATEMENT);
				a_list.put(i, new FeatureStatement(IfStatement.IF_STATEMENT));
			}
			if( a_stmt instanceof VariableDeclarationStatement){
				VariableDeclarationStatement var_decl_stmt = (VariableDeclarationStatement) a_stmt; 
				VariableDeclarationFragment frag = (VariableDeclarationFragment)var_decl_stmt.fragments().get(0);
				a_list.put(i, new FeatureStatement(VariableDeclaration.VARIABLE_DECLARATION_STATEMENT, frag.getName().toString()));
			}
			if( a_stmt instanceof ForStatement){
				ForStatement for_stmt = (ForStatement) a_stmt; 
				//				VariableDeclarationFragment frag = (VariableDeclarationFragment)var_decl_stmt.
				a_list.put(i, new FeatureStatement(ForStatement.FOR_STATEMENT, for_stmt.toString()));
			}

			if( a_stmt instanceof WhileStatement){
				WhileStatement while_stmt = (WhileStatement) a_stmt; 
				//				VariableDeclarationFragment frag = (VariableDeclarationFragment)var_decl_stmt.
				a_list.put(i, new FeatureStatement(WhileStatement.WHILE_STATEMENT, while_stmt.toString()));
			}
		}

		//		System.out.println(a_num+"!STATEMENT"+a_list);

		int swift_stmt_type=0;
		for(int j=0;j<swift_stmts_list.size();j++){
			SwiftParser.StatementContext stmt = swift_stmts_list.get(j);
			swift_stmts_list_copy.add(j, stmt);


			//classify stmts type
			if(stmt.expression()!=null){
				s_list.put(j, new FeatureStatement(SwiftParser.RULE_expression, stmt.expression().getText()));
				//				System.err.println("RULE_expression"+SwiftParser.RULE_expression+stmt.expression().getText());
				swift_stmt_type = SwiftParser.RULE_expression;
			}
			else if(stmt.branch_statement() !=null && stmt.branch_statement().if_statement()!=null){
				s_list.put(j, new FeatureStatement(SwiftParser.RULE_if_statement));
				swift_stmt_type = SwiftParser.RULE_if_statement;


			}
			else if(stmt.control_transfer_statement() !=null && stmt.control_transfer_statement().return_statement()!=null){
				s_list.put(j, new FeatureStatement(SwiftParser.RULE_return_statement));
				swift_stmt_type = SwiftParser.RULE_return_statement;


			}
			else if(stmt.declaration() !=null && stmt.declaration().constant_declaration()!=null){
				String name = stmt.declaration().constant_declaration().pattern_initializer_list().pattern_initializer(0).pattern().getText();
				s_list.put(j, new FeatureStatement(SwiftParser.RULE_constant_declaration, name));
				swift_stmt_type = SwiftParser.RULE_constant_declaration;
//				System.err.println("RULE_constant_declaration"+SwiftParser.RULE_constant_declaration+stmt.getText());
			}
			else if(stmt.declaration() !=null && stmt.declaration().variable_declaration()!=null){
				s_list.put(j, new FeatureStatement(SwiftParser.RULE_variable_declaration));
				swift_stmt_type = SwiftParser.RULE_variable_declaration;

				//				System.err.println("RULE_variable_declaration"+SwiftParser.RULE_variable_declaration);
			}

			else if(stmt.loop_statement() !=null && stmt.loop_statement()!=null && stmt.loop_statement().for_in_statement()!=null){
				//		/						stmt.declaration().
				s_list.put(j, new FeatureStatement(SwiftParser.RULE_for_in_statement));
				swift_stmt_type = SwiftParser.RULE_for_in_statement;
//				System.err.println("RULE_for_in_statement"+stmt.loop_statement().getText());
			}

			else if(stmt.loop_statement() !=null && stmt.loop_statement()!=null && stmt.loop_statement().for_statement()!=null){
				//				stmt.declaration().
				s_list.put(j, new FeatureStatement(SwiftParser.RULE_for_statement));
				swift_stmt_type = SwiftParser.RULE_for_statement;
				//				System.err.println("RULE_variable_declaration"+SwiftParser.RULE_variable_declaration);
			}



			//			if(swift_stmt_type!=-1){
			List<SwiftParser.StatementContext> sslist = s_cluster.get(swift_stmt_type);
			if(sslist==null){
				sslist = new LinkedList<SwiftParser.StatementContext>();
				sslist.add(stmt);
			}else{
				sslist.add(stmt);
			}
			s_cluster.put(swift_stmt_type,sslist);
//			System.out.println("before"+s_cluster);
			//			}

		}

		Vector<String> vect_a = new Vector<String>();
		Vector<String> vect_s = new Vector<String>();

		Vector<Integer> vect_idx_a = new Vector<Integer>();
		Vector<Integer> vect_idx_s = new Vector<Integer>();
		double threshold = 0.9;

		//		Integer [] a_cluster_keys = null, s_cluster_keys = null;
		//		a_cluster.keySet().toArray(a_cluster_keys);
		//		s_cluster.keySet().toArray(s_cluster_keys);
		//		a_cluster.keySet().l
		Object[] a_cluster_keys = a_cluster.keySet().toArray();
		Object [] s_cluster_keys = s_cluster.keySet().toArray();

//		System.out.println("a_cluster"+a_cluster);
//		System.out.println("s_cluster"+s_cluster);
		for(int a=0; a< a_cluster_keys.length; a++){
			for(int s=0; s< s_cluster_keys.length; s++){
//				System.out.println("a_cluster_keys"+(Integer)a_cluster_keys[a]);
//				System.out.println("s_cluster_keys"+(Integer)s_cluster_keys[s]);

				if(((Integer)a_cluster_keys[a]).equals(ReturnStatement.RETURN_STATEMENT)
						&& 	((Integer)s_cluster_keys[s]).equals(SwiftParser.RULE_return_statement))
				{

//					System.out.println("ReturnStatement.RETURN_STATEMENT");

					List<Statement> alist = a_cluster.get((Integer)a_cluster_keys[a]);
					List<SwiftParser.StatementContext> slist = s_cluster.get((Integer)s_cluster_keys[s]);
//					AssignmentList assign = new AssignmentList();
					Map<Integer, Integer> returnmap = AssignmentList.assignmentStatement(alist, slist);
					Iterator<Integer> iter = returnmap.keySet().iterator();
					while(iter.hasNext()){
						Integer ka = iter.next();
						Integer ks = returnmap.get(ka);
//						System.out.println(alist.get(ka));
//						System.out.println(slist.get(ks).getText());
						MappingSingleStatement.compute(alist.get(ka), ReturnStatement.RETURN_STATEMENT,
								slist.get(ks), SwiftParser.RULE_return_statement);	 
					}
					//					MappingSingleStatement.compute(android_stmts_list.get(i), a_list.get(i).type, swift_stmts_list.get(j), s_list.get(j).type);

				}//returnStatement Mapping



				else if(((Integer)a_cluster_keys[a]).equals(IfStatement.IF_STATEMENT)
						&& 	((Integer)s_cluster_keys[s]).equals(SwiftParser.RULE_if_statement))
				{



					List<Statement> alist = a_cluster.get((Integer)a_cluster_keys[a]);
					List<SwiftParser.StatementContext> slist = s_cluster.get((Integer)s_cluster_keys[s]);
					//					AssignmentList assign = new AssignmentList();
//					System.out.println("IfStatement.IF_STATEMENT"+alist);
//					int pp=0;
//					for(SwiftParser.StatementContext sss:slist){
//						pp++;
//						System.out.println(pp+"SwiftParser.RULE_if_statement"+sss.getText()); 
//					}
					Map<Integer, Integer> returnmap = AssignmentList.assignmentStatement(alist, slist);
					Iterator<Integer> iter = returnmap.keySet().iterator();
					while(iter.hasNext()){
						Integer ka = iter.next();
						Integer ks = returnmap.get(ka);
//						System.err.println();
//						System.err.println(alist.get(ka)+"%%%%%"+slist.get(ks).getText()+"****");
//						System.err.println();
						MappingSingleStatement.compute(alist.get(ka), IfStatement.IF_STATEMENT,
								slist.get(ks), SwiftParser.RULE_if_statement);	 
					}
					//					MappingSingleStatement.compute(android_stmts_list.get(i), a_list.get(i).type, swift_stmts_list.get(j), s_list.get(j).type);

				}//IfStatement Mapping

				//				ExpressionStatement.EXPRESSION_STATEMENT && s_list.get(j).type == SwiftParser.RULE_expression
				else if(((Integer)a_cluster_keys[a]).equals(ExpressionStatement.EXPRESSION_STATEMENT)
						&& 	((Integer)s_cluster_keys[s]).equals(SwiftParser.RULE_expression))
				{



					List<Statement> alist = a_cluster.get((Integer)a_cluster_keys[a]);
					List<SwiftParser.StatementContext> slist = s_cluster.get((Integer)s_cluster_keys[s]);
					AssignmentList assign = new AssignmentList();
					Map<Integer, Integer> returnmap = AssignmentList.assignmentStatement(alist, slist);
					Iterator<Integer> iter = returnmap.keySet().iterator();
					while(iter.hasNext()){
						Integer ka = iter.next();
						Integer ks = returnmap.get(ka);
//						System.out.println("ExpressionStatement.EXPRESSION_STATEMENT");
//						System.out.println(alist.get(ka));
//						System.err.println(slist.get(ks).getText());
						MappingSingleStatement.compute(alist.get(ka), ExpressionStatement.EXPRESSION_STATEMENT,
								slist.get(ks), SwiftParser.RULE_expression);	 
					}
					//					MappingSingleStatement.compute(android_stmts_list.get(i), a_list.get(i).type, swift_stmts_list.get(j), s_list.get(j).type);

				}//ExpressionStatement Mapping

				//					if(a_list.get(i).type== VariableDeclaration.VARIABLE_DECLARATION_STATEMENT && s_list.get(j).type == SwiftParser.RULE_constant_declaration

				else if(((Integer)a_cluster_keys[a]).equals(VariableDeclaration.VARIABLE_DECLARATION_STATEMENT)
						&& 	((Integer)s_cluster_keys[s]).equals(SwiftParser.RULE_constant_declaration))
				{
					List<Statement> alist = a_cluster.get((Integer)a_cluster_keys[a]);
					List<SwiftParser.StatementContext> slist = s_cluster.get(SwiftParser.RULE_constant_declaration);
					Map<Integer, Integer> returnmap1 = AssignmentList.assignmentStatement(alist, slist);
					Iterator<Integer> iter = returnmap1.keySet().iterator();
					while(iter.hasNext()){
						Integer ka = iter.next();
						Integer ks = returnmap1.get(ka);
						MappingSingleStatement.compute(alist.get(ka), VariableDeclaration.VARIABLE_DECLARATION_STATEMENT,
								slist.get(ks), SwiftParser.RULE_constant_declaration);	 
					}
				}//ExpressionStatement Mapping


				else if(((Integer)a_cluster_keys[a]).equals(ForStatement.FOR_STATEMENT)
						&& 	((Integer)s_cluster_keys[s]).equals(SwiftParser.RULE_for_in_statement))
				{
					
//					System.out.println("ForStatement.FOR_STATEMENT");
					List<Statement> alist = a_cluster.get((Integer)a_cluster_keys[a]);
					List<SwiftParser.StatementContext> slist = s_cluster.get(SwiftParser.RULE_for_in_statement);
					Map<Integer, Integer> returnmap1 = AssignmentList.assignmentStatement(alist, slist);
					Iterator<Integer> iter = returnmap1.keySet().iterator();
					while(iter.hasNext()){
						Integer ka = iter.next();
						Integer ks = returnmap1.get(ka);
						MappingSingleStatement.compute(alist.get(ka), ForStatement.FOR_STATEMENT,
								slist.get(ks), SwiftParser.RULE_for_in_statement);	 
					}
				}//ExpressionStatement Mapping


			}
		}

		//		for()

		/*
		//alignment of statements
		for(int i=0;i<a_num;i++){
			for(int j=0;j<s_num;j++){
				if(a_list.get(i) !=null && s_list.get(j) !=null){

					StringMetric metric = StringMetrics.jaroWinkler();
					if(a_list.get(i).type== IfStatement.IF_STATEMENT && s_list.get(j).type == SwiftParser.RULE_if_statement
							&& metric.compare(a_list.get(i).name, s_list.get(j).name) >threshold)
					{
						//						System.out.println("android_stmts_list.get(i)"+android_stmts_list.get(i));
						//						MappingSingleStatement compute = new MappingSingleStatement(android_stmts_list.get(i), a_list.get(i).type, swift_stmts_list.get(j), s_list.get(j).type);
						MappingSingleStatement.compute(android_stmts_list.get(i), a_list.get(i).type, swift_stmts_list.get(j), s_list.get(j).type);
						vect_a.add((android_stmts_list.get(i).toString().replace("\n","")));
						vect_s.add(swift_stmts_list.get(j).getText());
						vect_idx_a.add(i);
						vect_idx_s.add(j);
					}
					if(a_list.get(i).type== ReturnStatement.RETURN_STATEMENT && s_list.get(j).type == SwiftParser.RULE_return_statement
							&& metric.compare(a_list.get(i).name, s_list.get(j).name) >threshold)
					{
						//						MappingSingleStatement compute = new MappingSingleStatement(android_stmts_list.get(i), a_list.get(i).type, swift_stmts_list.get(j), s_list.get(j).type);
						//						
						MappingSingleStatement.compute(android_stmts_list.get(i), a_list.get(i).type, swift_stmts_list.get(j), s_list.get(j).type);
						vect_a.add((android_stmts_list.get(i).toString().replace("\n","")));
						vect_s.add(swift_stmts_list.get(j).getText());
						vect_idx_a.add(i);
						vect_idx_s.add(j);

					}

					if(a_list.get(i).type== VariableDeclaration.VARIABLE_DECLARATION_STATEMENT && s_list.get(j).type == SwiftParser.RULE_constant_declaration
							&& metric.compare(a_list.get(i).name, s_list.get(j).name) >threshold)
					{
						//						System.out.println("VARIABLE_DECLARATION_STATEMENT");
						//						System.out.println(android_stmts_list.get(i).toString());
						//						System.out.println(swift_stmts_list.get(j).getText());
						//						MappingSingleStatement compute = new MappingSingleStatement(android_stmts_list.get(i), a_list.get(i).type, swift_stmts_list.get(j), s_list.get(j).type);
						//						if(swift_stmts_list.get(j) instanceof SwiftParser.Constant_declarationContext){
						//						if(swift_stmts_list.get(j).declaration().constant_declaration() !=null){
						MappingSingleStatement.compute(android_stmts_list.get(i), a_list.get(i).type, swift_stmts_list.get(j).declaration().constant_declaration() , s_list.get(j).type);
						vect_a.add((android_stmts_list.get(i).toString().replace("\n","")));
						vect_s.add(swift_stmts_list.get(j).getText());
						vect_idx_a.add(i);
						vect_idx_s.add(j);
						//						}
						//					}

					}

					if(a_list.get(i).type== ExpressionStatement.EXPRESSION_STATEMENT && s_list.get(j).type == SwiftParser.RULE_expression
							&& metric.compare(a_list.get(i).name, s_list.get(j).name) >0)
					{

						//					System.out.println("MExpressionStatement.EXPRESSION_STATEMENT"+android_stmts_list.get(i)+swift_stmts_list.get(j).getText());
						//						System.out.println(android_stmts_list.get(i).toString());
						//						System.out.println(swift_stmts_list.get(j).getText());
						//						MappingSingleStatement compute = new MappingSingleStatement(android_stmts_list.get(i), a_list.get(i).type, swift_stmts_list.get(j), s_list.get(j).type);
						//						
						MappingSingleStatement.compute(android_stmts_list.get(i), a_list.get(i).type, swift_stmts_list.get(j), s_list.get(j).type);
						vect_a.add((android_stmts_list.get(i).toString().replace("\n","")));
						vect_s.add(swift_stmts_list.get(j).getText());
						vect_idx_a.add(i);
						vect_idx_s.add(j);
					}

					if(a_list.get(i).type== ExpressionStatement.EXPRESSION_STATEMENT && s_list.get(j).type == SwiftParser.RULE_expression
							&& metric.compare(a_list.get(i).name, s_list.get(j).name) >threshold)
					{

						//					System.out.println("MExpressionStatement.EXPRESSION_STATEMENT"+android_stmts_list.get(i));
						//						System.out.println(android_stmts_list.get(i).toString());
						//						System.out.println(swift_stmts_list.get(j).getText());
						//						MappingSingleStatement compute = new MappingSingleStatement(android_stmts_list.get(i), a_list.get(i).type, swift_stmts_list.get(j), s_list.get(j).type);
						//						
						MappingSingleStatement.compute(android_stmts_list.get(i), a_list.get(i).type, swift_stmts_list.get(j), s_list.get(j).type);
						vect_a.add((android_stmts_list.get(i).toString().replace("\n","")));
						vect_s.add(swift_stmts_list.get(j).getText());
						vect_idx_a.add(i);
						vect_idx_s.add(j);
					}
				}
			}
		}


		 */
		//		System.out.println(vect_a);
		//		System.out.println(vect_s);

		/*
		String ss ="", aa="";
		for(SwiftParser.StatementContext s:swift_stmts_list_copy){
			//			System.out.println(s.getText());
			ss +=s.getText().toString();
			ss+=" ";
		}
		for(org.eclipse.jdt.core.dom.Statement s:android_stmts_list_copy){
			//			System.out.println(s.getText());
			aa +=s.toString().replace("\n", "");
			aa+=" ";
		}
		//		vect_idx_a = new Vector<Integer>();
		//		vect_idx_s = new Vector<Integer>();


		//	System.out.println("vect_idx_a"+vect_idx_a+a_list);
		//	System.out.println("vect_idx_s"+vect_idx_s+s_list);
		for(int ain:vect_idx_a){
			android_stmts_list_copy.remove(android_stmts_list.get(ain));
		}
		for(int sin:vect_idx_s){
			swift_stmts_list_copy.remove(swift_stmts_list.get(sin));
		}

		Connection c = null;
		java.sql.Statement sql_stmt = null;
		Class.forName("org.sqlite.JDBC");
		c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		c.setAutoCommit(true);
		sql_stmt = c.createStatement();

		String ss1="", aa1="";
		for(SwiftParser.StatementContext s:swift_stmts_list_copy){
			//			System.out.println("unmatched s"+s.getText().replaceAll("\"", "").replaceAll("\'", ""));
			String expr = s.getText().replaceAll("\"", "").replaceAll("\'", "");
			String gexpr= MappingSingleStatement.makeTemplateSwift(expr);
			String sql= ProtoSQLiteJDBC.insertTableUnmatchedExprSwiftV(umatched_s_id, expr, gexpr, stmts_id, "");

			//		c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
			//		c.setAutoCommit(true);
			sql_stmt = c.createStatement();

			sql_stmt.executeUpdate(sql);
			//		c.commit();
			umatched_s_id++;
			//		c.close();
		}
		for(org.eclipse.jdt.core.dom.Statement s:android_stmts_list_copy){
			//			System.out.println("unmatched a"+s);
			//			String sql= ProtoSQLiteJDBC.insertTableUnmatchedExprSwiftV(umatched_a_id, s.toString(), s.toString(), stmts_id, "");
			//			sql_stmt.executeUpdate(sql);
			//			c.commit();
			//			MappingSingleStatement.computeAndroid(s);
			if(s instanceof ReturnStatement){

			}
			if(s instanceof IfStatement){

			}

		}

		ss= ss.replaceAll("\"", "").replaceAll("\'", "");
		aa= aa.replaceAll("\"", "").replaceAll("\'", "");
		//		aa=aa.replaceAll("\"", "\\\"");
		if(aa!="" ||ss!=""){
			String e12 = SQLiteJDBC.insertStmtsAndroidTable(stmts_id, aa, Main.androidClassName);
			String e11 = SQLiteJDBC.insertStmtsSwiftTable(stmts_id, ss, Main.swiftClassName,stmts_id);
			stmts_id++;
			//			System.out.println("e12"+e12);
			//		c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
			//		c.setAutoCommit(true);
			java.sql.Statement sql_stmt11 = c.createStatement();
			sql_stmt11.executeUpdate(e12);	
			sql_stmt11.executeUpdate(e11);
			//		c.close();
			//	c.commit();
		}
		 */
	}


}
