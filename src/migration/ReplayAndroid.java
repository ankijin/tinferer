package migration;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;

public class ReplayAndroid extends ASTVisitor{
	String buffer="";
	
	@Override
	public void endVisit(MethodDeclaration node) {
		System.out.println("endVisit"+buffer);
		// TODO Auto-generated method stub
		super.endVisit(node);
	}
	@Override
	public boolean visit(MethodDeclaration node) {
		// TODO Auto-generated method stub
		
		ArrayList<Integer> list = new ArrayList<Integer>();
		if(node.getBody()==null || node.getBody().statements()==null) return false;
		List sts = node.getBody().statements();
		for(int i=0; i<sts.size();i++){
			if( sts.get(i) instanceof ReturnStatement){
//				System.err.println("ReturnStatement"+ReturnStatement.RETURN_STATEMENT);
				list.add(ReturnStatement.RETURN_STATEMENT);
			}
			if( sts.get(i) instanceof SingleVariableDeclaration){
//				System.err.println("SingleVariableDeclaration");
				list.add(SingleVariableDeclaration.SINGLE_VARIABLE_DECLARATION);
			}
			if( sts.get(i) instanceof ExpressionStatement){
				System.err.println("ExpressionStatement"+ExpressionStatement.EXPRESSION_STATEMENT);
				list.add(ExpressionStatement.EXPRESSION_STATEMENT);
			}
			if( sts.get(i) instanceof IfStatement){
				System.err.println("IfStatement");
				list.add(IfStatement.IF_STATEMENT);
				buffer+=sts.get(i).toString();
			}
			if( sts.get(i) instanceof VariableDeclarationStatement){
//				System.err.println("IfStatement");
//				System.err.println("VariableDeclarationStatement"+VariableDeclarationStatement.VARIABLE_DECLARATION_STATEMENT);
				
				list.add(VariableDeclaration.VARIABLE_DECLARATION_STATEMENT);
			}
			if( sts.get(i) instanceof VariableDeclarationExpression){
//				System.err.println("IfStatement");
				list.add(VariableDeclaration.VARIABLE_DECLARATION_EXPRESSION);
			}
		}
		
		return super.visit(node);
	}
	
	public static String readFileToString(String filePath) throws IOException {
		StringBuilder fileData = new StringBuilder(1000);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));

		char[] buf = new char[10];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
			buf = new char[1024];
		}
		reader.close();
		return  fileData.toString();	
	}
	

	public static void main(String[] args) throws FileNotFoundException, IOException{
		ASTParser parser = ASTParser.newParser(AST.JLS8);
		String path = "/Users/kijin/Documents/workspace_mars/GumTreeTester/charts/test_set/ViewPortHandler3.java";
		String str	= readFileToString(path);
		parser.setSource(str.toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		Map<String, ASTNode> nodeAndroid = new HashMap<String, ASTNode>();
		final CompilationUnit cu = (CompilationUnit) parser.createAST(null);
		cu.accept(new ReplayAndroid());
	}
}
