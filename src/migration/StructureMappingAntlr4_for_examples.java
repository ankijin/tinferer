package migration;

import static org.simmetrics.builders.StringMetricBuilder.with;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.DefaultErrorStrategy;
import org.antlr.v4.runtime.InputMismatchException;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStreamRewriter;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.misc.IntervalSet;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.compiler.ITerminalSymbols;
import org.eclipse.jdt.core.compiler.InvalidInputException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.internal.compiler.parser.Scanner;
import org.simmetrics.StringMetric;
import org.simmetrics.StringMetrics;
import org.simmetrics.metrics.CosineSimilarity;
import org.simmetrics.metrics.JaccardSimilarity;
import org.simmetrics.metrics.JaroWinkler;
import org.simmetrics.simplifiers.Simplifiers;

import com.google.code.javakbest.Murty;
import com.google.common.collect.Sets;

import alignment.common.CommonOperators;
import alignment.common.CommonSeqToClustering;
import alignment.common.HungarianAlgorithm;
import alignment.common.JavaWithoutContext;
import alignment.common.LineCharIndex;
import alignment.common.OpProperty;
import alignment.common.SwiftListenerWithoutContext;
import antlr_parsers.javaparser.JavaBaseListener;
import antlr_parsers.javaparser.JavaBaseVisitor;
import antlr_parsers.javaparser.JavaLexer;
import antlr_parsers.javaparser.JavaParser;
import j2swift.Java8Lexer;
import j2swift.Java8Parser;
import recording.java.JDTVisitorWithoutContext;
import recording.java.JavaListenerWithoutContext;
import sqldb.ProtoSQLiteJDBC;
import sqldb.SQLiteJDBC;
import templatechecker.DSLErrorListener;
//import templatechecker.DSLErrorListener;
//import templatechecker.ExceptionErrorStrategy;
import templatechecker.TemplateCBaseVisitor;
import templatechecker.TemplateCLexer;
import templatechecker.TemplateCParser;
import templatechecker.TemplateCParser.ArgContext;

public class StructureMappingAntlr4_for_examples {
	public boolean 					templateSolver 						= false;
	public Map<Integer, Integer> 	android_to_swift 					= new HashMap<Integer, Integer>();
	public Map<Pair, Double> 		android_to_swift_point 				= new HashMap<Pair, Double>();

	public CommonTokenStream 		swiftCommonStream					= null;
	public CommonTokenStream 		androidCommonStream					= null;

	public Map<Interval, Interval> 	constraint 							= new HashMap<Interval, Interval>();
	public Map<Interval, List<Interval>> constraintRefAndroid 			= new HashMap<Interval, List<Interval>>();

	public String afilename, sfilename;

	public String example_sql="", template_sql="", binding_sql="";

	//ssmap, aamap, nextMapping, template_a, template_s

	LinkedHashMap<OpProperty, OpProperty> nextMapping = new LinkedHashMap<OpProperty, OpProperty>();

	public static String[] java_keywords={
			"abstract",	"continue",	"for",	"new",	"switch",
			"assert", "default", "package", "synchronized",
			//			"boolean",	
			"do",	"if",	"private",	"this",
			"break",
			//			"double",	
			"implements",	"protected",	"throw",
			"byte",	"else",	"import",	"public",	"throws",
			"case",	"enum", 	"instanceof",	"return",	"transient",
			"catch",	"extends",	
			//			"int",	
			"short",	"try",
			//			"char",	
			"final", 
			"interface", "static",	
			"void",
			"class", "finally",	
			//			"long",	
			"strictfp",	"volatile",
			//			"float",	
			"native",	"super",	"while",
			"@Override","Override","@", "true", "false"
	};

	public static String[] common_keywords={
			"return", "if","else","class","for","while"
	};


	boolean toskip = false;

	public static List<String> java_keywords_list=Arrays.asList(java_keywords);

	public static String[] java_punctuations={"(",")", "{","}", 
			"[","]", 
			";","=",">","<","&&","&",":","==",",","-","+","/","*","?",".",">=","!=",".","<=","++","+=","()", "!", "==", "||","-="};

	public static List<String> java_punctuations_list=Arrays.asList(java_punctuations);

	public static String[] swift_keywords={"associatedtype", "class", "deinit", "enum", "to",
			"extension", "fileprivate", "func", "import", 
			"init", "inout", "internal", "let", "open",
			"operator", "private", "protocol", "self",
			"public", "static", "struct", "subscript", "typealias", 
			"var", "break", "case", "continue", "default", "defer", 
			"do", "else", "fallthrough", "for", "guard", "if", "in", 
			"repeat", "return", "switch", "where", "while", "override", "public override func", "public override","private func", "internal override", "internal override func","as","public func","private var","public var","stride", "through", "by","private weak var","weak","private weak"};
	//	"private var","public var","public func"};

	public static List<String> swift_keywords_list=Arrays.asList(swift_keywords); 
	public static String[] swift_punctuations={"(",")", "{","}","[","]", ";","=","->",":","<",">","&&","&","-","..<","..",",","!","?",">=","!=", "& &",".","+","??","==","||", "-=", "+="};
	public static List<String> swift_punctuations_list=Arrays.asList(swift_punctuations);

	//	public static String[] common_operators={"+","-","/","*",".","<",">", "= =","=="};
	public static String[] common_operators={"+","-","/","*",".",","};
	//	public static String[] common_operators={"+","-","/","*",".",",","?",":","!",">","<","(",")"};
	public static List<String> common_operators_list = Arrays.asList(common_operators);

	public static int testnum = 0;
	public static int grammar_id=0;
	public static int ggrammar_id=0;
	ParserRuleContext android;
	ParserRuleContext swift;
	public SwiftListenerWithoutContext swift_listner;
	//	public JavaListenerWithoutContext wocontext;
	public JavaWithoutContext android_listener;

	Map<Interval, String> amap_ct 	= new HashMap<Interval, String>();
	Map<Interval, String> smap_ct 	= new HashMap<Interval, String>();

	private boolean stopped 	= false;
	public TokenStreamRewriter rewriter;
	public CommonTokenStream tokens;
	public StructureMappingAntlr4_for_examples(ParserRuleContext android, ParserRuleContext swift) throws FileNotFoundException, IOException{

		this.android 	= android;
		this.swift 		= swift;

		/**
		 * get main constraints, fed back to the .. more levels
		 * skipping common sequence operators in {...}
		 * checking common start end for better mapping 
		 */





	}
	double cost = 0;
	double prev_cost = 0;
	int prev_id =0;
	public void waking(){




		cost = 0;
		android_to_swift_point.clear();
		android_to_swift.clear();

		swift_listner.termsMap.clear();

		android_listener.termsMap.clear();
		swift_listner.termsMapOnlys.clear();
		android_listener.termsMapOnlys.clear();

		android_listener.blockIndex = null;
		swift_listner.blockIndex = null;
		//		android_listener.leftIndex = null;
		//		swift_listner.leftIndex = null;

		amap_ct.clear();
		smap_ct.clear();

		example_sql = "";
		template_sql=""; 
		binding_sql="";

		nextMapping.clear();
		android_listener.scope_constraint.clear();
		swift_listner.scope_constraint.clear();


		ParseTreeWalker walker_swift = new ParseTreeWalker();
		walker_swift.walk(swift_listner, swift);
		ParseTreeWalker walker_android = new ParseTreeWalker();
		walker_android.walk(android_listener, android);

		constraint.clear();
		Map<Interval, String> terms = android_listener.termsMap;

		Map<Interval, String> terms_swft = swift_listner.termsMap;

	}


	public String compute(Statement stmt) throws FileNotFoundException, IOException{
		swift_listner 			= new SwiftListenerWithoutContext(swift);
		android_listener 		= new JavaWithoutContext(android);
		
		waking();

		int az = swift.start.getStartIndex(), bz = swift.stop.getStopIndex();
		Interval swift_interval = new Interval(az,bz);

		int az1 = android.start.getStartIndex(), bz1 = android.stop.getStopIndex();
		Interval android_interval = new Interval(az1,bz1);

		//				System.out.println("android_listener.a_nodes"+android_listener.a_nodes);
		//				System.err.println("swift_listner.s_node"+swift_listner.s_nodes);
		//children mappings
		//resolution {} blocks, = assignments and expressions
		if(android_listener.a_nodes.size()>0 && swift_listner.s_nodes.size()>0){

			//matching and save them
			//a_nodes or s_nodes stores statements
			double[][] aecostMatrix = new double[android_listener.a_nodes.size()][swift_listner.s_nodes.size()];

			StringMetric emetric =
					with(new JaroWinkler())
					.build();

			for(int i=0;i<android_listener.a_nodes.size();i++){
				for(int j=0;j<swift_listner.s_nodes.size();j++){
					if( android_listener.a_nodes.get(i).property.equals(swift_listner.s_nodes.get(j).property)) {
						//					if( android_listener.a_nodes.get(i).property.equals(swift_listner.s_nodes.get(j).property) && toCommon(android_listener.a_nodes.get(i).strASTNode,swift_listner.s_nodes.get(j).strASTNode)>=0.0) {
						aecostMatrix[i][j]	= Math.min(0.999, 1- emetric.compare(android_listener.a_nodes.get(i).strASTNode, swift_listner.s_nodes.get(j).strASTNode));
					}else{
						aecostMatrix[i][j] 	= 1;
					}


				}
			}

			HungarianAlgorithm ehung = null;
			int[] eresult = new int[0];
			//			int[] eresult_copy = new int[0];
			if(android_listener.a_nodes.size() > 0 && swift_listner.s_nodes.size() >0){
				ehung 	= new HungarianAlgorithm(aecostMatrix);
				eresult = ehung.execute();

				//				eresult_copy = ehung.execute();
			}


			for(int p=0; p<eresult.length;p++){
				if(eresult[p]!=-1 && eresult[p]<1){
					//				if(1==1){

					//					String a_exam = android_listener.a_nodes.get(p).strASTNode;
					//					String s_exam = swift_listner.s_nodes.get(eresult[p]).strASTNode;
					if(
							android_listener.a_nodes.get(p).node!=null 
							&& android_listener.a_nodes.get(p).node instanceof ParserRuleContext 
							&& swift_listner.s_nodes.get(eresult[p]).node!=null 
							&& swift_listner.s_nodes.get(eresult[p]).node instanceof ParserRuleContext
							)
					{
						StructureMappingAntlr4_for_examples mapping = new StructureMappingAntlr4_for_examples((ParserRuleContext) android_listener.a_nodes.get(p).node, (ParserRuleContext)swift_listner.s_nodes.get(eresult[p]).node);
						mapping.afilename = afilename;
						mapping.sfilename = sfilename;
						mapping.androidCommonStream = this.androidCommonStream;
						mapping.swiftCommonStream = this.swiftCommonStream;
												System.out.println("examples"+mapping.android.getText()+" "+mapping.swift.getText());

						//						mapping.waking();
						mapping.compute(stmt);
					}
				}
			}

		} /**enclosed's children mappings using by {{...}}, = **/





		example_sql = SQLiteJDBC.updateTableGrammarSwift2(grammar_id, android.start.getInputStream().getText(android_interval), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval), 
				afilename, LineCharIndex.contextToLineIndex(android).toString(),
				sfilename, LineCharIndex.contextToLineIndex(swift).toString());


		try {
			stmt.executeUpdate(example_sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//		queries.addElement();

		//		doupdate();

		return example_sql;

	}

	public static List<String> getMatchingStrings(List<String> list, String regex) {

		ArrayList<String> matches = new ArrayList<String>();

		Pattern p = Pattern.compile(regex);

		for (String s:list) {
			if (p.matcher(s).matches()) {
				matches.add(s);
			}
		}

		return matches;
	}

	public static Set<Character> toSet(String s) {
		Set<Character> ss = new HashSet<Character>(s.length());
		for (char c : s.toCharArray())
			ss.add(Character.valueOf(c));
		return ss;
	}


	public static Set<String> toSet(List<String> s) {
		Set<String> ss = new HashSet<String>(s.size());
		for (String c : s)
			ss.add(c);
		return ss;
	}


	public static double toCommon(List<String> and, List<String> swft) {
		Set<String> aset = toSet(and);
		aset.retainAll(toSet(swft));

		return (double)aset.size()/(double)and.size();
	}

	public static double toCommon(String andS, String swftS) {
		List<String> lst = matchingList(andS);
		List<String> cplst = matchingList(andS);
		List<String> swlst = matchingList(swftS);
		List<String> cpswlst = matchingList(swftS);
		//		System.err.println(lst);
		//		System.err.println(swlst);
		int s = lst.size();
		int ss = swlst.size();
		//		Set<String> aset = toSet(lst);
		//		aset.retainAll(toSet(matchingList(swftS)));
		lst.retainAll(matchingList(swftS));
		//		System.out.println(lst);
		swlst.retainAll(cplst);
		//		System.out.println(swlst);
		if(s==0 && ss==0){
			return 1.0;
		}
		//		else if((double)lst.size()==0){
		//			return 0.0;
		//		}
		double s1 = s==0? 0:(double)lst.size()/(double)s;
		double s2 = ss==0? 0:(double)swlst.size()/(double)ss;
		//		System.out.println(s1);
		//		System.err.println(s2);
		return Math.min(s1, s2);
	}

	public static List<String> matchingList(String str){

		Pattern p = Pattern.compile("\\d+");
		List<String> numbers = new ArrayList<String>();
		Matcher m = p.matcher(str);
		while (m.find()) {  
			//			numbers.add(m.group());
		}   




		try {
			Pattern regex = Pattern.compile("[+*/&><={}]|(?<=\\s)-|if|else|while");
			Matcher regexMatcher = regex.matcher(str);
			while (regexMatcher.find()) {
				numbers.add(regexMatcher.group());
			} 
		} catch (PatternSyntaxException ex) {
			// Syntax error in the regular expression
		}


		//		System.err.println(str+": num"+numbers);
		return numbers;
	}

	public void doupdate(){

		Connection c;
		try {
			//			if(!isEqualTemplate){
			//					if(!listner_swift.hasErrors() && !listner.hasErrors() && cost!=prev_cost){

			//			c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
			c = DriverManager.getConnection("jdbc:sqlite:var_0309.db");

			java.sql.Statement sql_stmt = c.createStatement();
			c.setAutoCommit(true);
			//			sql_stmt.executeUpdate(e12);	
			sql_stmt.executeUpdate(example_sql);
			sql_stmt.executeUpdate(template_sql);
			sql_stmt.executeUpdate(binding_sql);
			//			System.out.println("cost:"+cost+"\n"+StringUtils.join(aaaa.toArray()," ").replaceAll("\\s+", " ")+"\n"+StringUtils.join(ssss.toArray()," "));
			prev_cost = cost;
			grammar_id++;

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			System.out.println("err in sql"+example_sql);
			System.out.println("err in sql"+template_sql);
			System.out.println("err in sql"+binding_sql);
			//			System.out.println(e11);
			//			System.out.println(e122);
			e1.printStackTrace();
		}

	}

	public static void main(String args []){
		/*
		boolean aa = true&false;
		System.out.println(aa+"main"+(StringUtils.countMatches("_chartWidth - _contentRect.size.width - _contentRect.origin.x", "-")==StringUtils.countMatches("_chartWidth - _contentRect.size.width", "-")));
		System.out.println(NumberUtils.isNumber("0"));
		//		NumberUtils.isNumber(arg0)
		System.out.println(NumberUtils.isNumber("0.0")+" "+ (NumberUtils.createDouble("0.0").equals(NumberUtils.createDouble("0"))));

		Pattern p = Pattern.compile("-?\\d+(,\\d+)*?\\.?\\d+?");
		List<String> numbers = new ArrayList<String>();
		Matcher m = p.matcher("Government has distributed 4.8 million textbooks to 2000 schools22");
		while (m.find()) {  
			numbers.add(m.group());
		}   
		System.err.println(numbers);


		List<String> matchList = new ArrayList<String>();
		try {
//			Pattern regex = Pattern.compile("[+/&><=]|(?<=\\s)-|if");
			Matcher regexMatcher = regex.matcher("if -test + aaa * aaaa/a22");
			while (regexMatcher.find()) {
				numbers.add(regexMatcher.group());
			} 
		} catch (PatternSyntaxException ex) {
			// Syntax error in the regular expression
		}
		System.out.println(numbers);

		String s1 = "Seychelles";
		String s2 = "sydney";
		Set<Character> ss1 = toSet(s1);
		ss1.retainAll(toSet(s2));

		 */

		System.out.println("\nSwiftParser.ruleNames[180]"+SwiftParser.ruleNames[214]+"  "+JavaParser.ruleNames[70]);
		System.out.println(toCommon("0f","CGFloat(0.0)"));


		/*
		System.out.println(matchingList("text != null && longest.length() < text.length();"));
		System.out.println(matchingList("text = null > !longest.length() && text.length();"));

		String t1="if ( yScale   == 0.f   )			{            yScale = Float.MAX_VALUE;        }";
		String t2="if ( newValue == 0.0 )         	{            newValue = CGFloat.max           }";
		String t3="if newValue   == 0.0             {            newValue = CGFloat.max           }";


		ANTLRInputStream stream1 = new ANTLRInputStream(t1);
		Lexer lexer1 = new JavaLexer((CharStream)stream1);
		CommonTokenStream androidcommon = new CommonTokenStream(lexer1);
		JavaParser parser = new JavaParser(new CommonTokenStream(lexer1));

		System.out.println(parser.getTokenStream().getTokenSource());
		String test= "let <arg0> = -width * (_scaleX - 1.0)";
		//		let <arg0> = -width * (_scaleX - 1.0) \-width \* \(_scaleX \- 1\.0\)
		//		test=test.replaceFirst("\\b"+"\\-width \\* \\(_scaleX \\- 1\\.0\\)"+"\\b", "<arg>");
		test=test.replaceFirst("\\-width \\* \\(_scaleX \\- 1\\.0\\)", "<arg>");
		//		<(.*?)>
		System.out.println(test);

		String in = "var <arg1>: <arg0> = <arg2>";

		Pattern pp = Pattern.compile("\\<(.*?)\\>");
		Matcher mm = pp.matcher(in);

		while(mm.find()) {
			System.out.println(mm.group(1));
			//		    System.out.println(mm);
		}
		 */
		StringBuffer buf = new StringBuffer("123456789");

		int start = 3;
		int end = 6;
		buf.replace(start, end, "foobar"); 
		System.out.println(buf);
	} 

	public class Pair{
		public int a,b;
		public Interval a_intval, b_intval;

		public Pair(int a, int b){
			this.a=a;
			this.b=b;
		}
		public Pair(int a, int b, Interval ai, Interval bi){
			this.a=a;
			this.b=b;
			this.a_intval = ai;
			this.b_intval = bi;
		}

		@Override
		public boolean equals(Object obj) {
			// TODO Auto-generated method stub
			Pair p=(Pair)obj;
			return a==p.a && b==p.b;
		}

		@Override
		public int hashCode() {
			// TODO Auto-generated method stub
			return a*100+b*1;
		}
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return a+":"+b;
		}
	}
}
