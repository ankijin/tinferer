package migration;

import static org.simmetrics.builders.StringMetricBuilder.with;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.eclipse.jdt.core.compiler.InvalidInputException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.BooleanLiteral;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jdt.core.dom.TextElement;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.internal.compiler.parser.Scanner;
import org.eclipse.jdt.internal.compiler.parser.TerminalTokens;
import org.simmetrics.StringMetric;
import org.simmetrics.metrics.JaroWinkler;

import alignment.common.HungarianAlgorithm;
//import SwiftParser.ExpressionContext;
//import SwiftParser.Variable_declarationContext;
import android.database.CharArrayBuffer;
import mapping.MappingElementExpr;
import mapping.MappingMethods;
//import recording.SwiftParser;
import sqldb.SQLiteJDBC;

public class MappingDeclaration3{
	FieldDeclaration android;
	SwiftParser.Variable_declarationContext swift;
	public static Map<String, String> localtypeMapAndroid 		= new HashMap<String, String>();
	public static Map<String, String> localtypeMapSwift 		= new HashMap<String, String>();
	public static Map<MappingElement, MappingElement> mappingElements1 = new HashMap<MappingElement, MappingElement>();
	public static Map<String, ASTNode> varDeclAndroid 			= new HashMap<String, ASTNode>();
	public static Map<String, ParserRuleContext> varDeclSwift 	= new HashMap<String, ParserRuleContext>();
	public static String methodnameAndroid;
	public static String methodnameSwift;

	//	public static MappingStatements  map_stmts = new MappingStatements();
	public static int sid;

	public String getType(FieldDeclaration f, String name){
		RecordVarDeclAndroid recordAndroid = new RecordVarDeclAndroid();
		f.accept(recordAndroid);
		if(name.equals(recordAndroid.record.get(2).label)){
			return recordAndroid.record.get(1).label;
		}
		return null;
	}

	public String getTypeSwift(SwiftParser.Variable_declarationContext swift, String name){

		RecordVarDeclSwift recordSwift = new RecordVarDeclSwift<>();
		swift.accept(recordSwift);

		if(name.equals(((MappingElement)recordSwift.record.get(2)).label.toString())){
			return ((MappingElement)recordSwift.record.get(3)).label;
		}
		return null;
	}

	public MappingDeclaration3(FieldDeclaration a, SwiftParser.Variable_declarationContext s){
		android = a;
		swift 	= s;
	}

	Map<Integer, Integer> map_types	= new HashMap<Integer, Integer>();
	public MappingDeclaration3(){
		map_types.put(25, 18);
		map_types.put(21, 76);
		map_types.put(41, 36);
	}

	String params_android="";
	String stms_android ="";
	String name_android ="";
	String modifiers_android="";
	String type_android="";

	String params_swift="";
	String stms_swift ="";
	String name_swift ="";
	String modifiers_swift="";
	String type_swift="";
	String return_android="NULL";

	List<org.eclipse.jdt.core.dom.Statement> android_stmts_list =new LinkedList<org.eclipse.jdt.core.dom.Statement>();

	public void setElementMethodDecl(ASTNode a, ParserRuleContext s) throws IOException, SQLException, ClassNotFoundException, InterruptedException{
		Map<String, String> recordAndroidParams	= new HashMap<String, String>();
		Map<String, String> recordSwiftParams	= new HashMap<String, String>();
		LinkedList<MappingElement> recordStatement = new LinkedList<MappingElement> ();

		LinkedList<MappingElement> record = new LinkedList<MappingElement> ();
		recordAndroidParams.clear();
		recordSwiftParams.clear();
		a.accept(new ASTVisitor() {

			@Override
			public boolean visit(MethodDeclaration node) {
				// TODO Auto-generated method stub
				name_android 		= node.getName().toString();



				List<SingleVariableDeclaration> params = node.parameters();
				for(SingleVariableDeclaration varr:params){		
					recordAndroidParams.put(varr.getName().toString(), varr.getType().toString());
				}
				if(node.getReturnType2()!=null)
					type_android 		= node.getReturnType2().toString();
				modifiers_android 	= node.modifiers().toString();
				if(!node.parameters().isEmpty()){
					params_android 	= node.parameters().toString().replace("\n", " ");
				}
				if( node.getBody()!=null && node.getBody().statements()!=null){
					stms_android 	= node.getBody().statements().toString().replace("\n", "");
					android_stmts_list = node.getBody().statements();
				}
				if(node.getReturnType2()!=null){
					return_android =node.getReturnType2().toString(); 
				}
				return super.visit(node);
			}
		});//record android method


		Connection c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		Statement stmt = c.createStatement();

		c.setAutoCommit(true);

		//		System.out.println("recordAndroidParams\n"+recordAndroidParams);

		//		Iterator<String> pa_iter = recordAndroidParams.keySet().iterator();
		RecordMethodSwift rrr = new RecordMethodSwift<>();
		s.accept(rrr);

		name_swift = RecordMethodSwift.name_swift;


		Iterator<String> ps_iter = rrr.recordSwiftParams.keySet().iterator();
		MappingStatements map_stmts = new MappingStatements();
		//		android_stmts_list, rrr.stmt_list
		//		map_stmts.localtypeMapAndroid = recordAndroidParams;
		//		map_stmts.localtypeMapSwift = rrr.recordSwiftParams;
		map_stmts.swift_stmts_list = rrr.stmt_list;
		map_stmts.android_stmts_list =android_stmts_list;
		//		map_stmts.returnAndroid = return_android;
		//		map_stmts.returnSwift = rrr.returnType;
		//		map_stmts.classnameAndroid = Main.androidClassName;
		//		map_stmts.classnameSwift = Main.swiftClassName;

		methodnameAndroid = name_android;
		methodnameSwift = name_swift;


		map_stmts.mapping();
		Map<String, String> mm_va = new HashMap<String, String>();
		//		System.out.println(mm_va+"::::"+recordAndroidParams+",,"+rrr.recordSwiftParams);
		if(recordAndroidParams.size()==rrr.recordSwiftParams.size()){
			mm_va = MappingMethods.mapping(recordAndroidParams, rrr.recordSwiftParams);
			//			System.out.println(map_stmts.localtypeMapAndroid+":::"+map_stmts.localtypeMapSwift);
			System.out.println(mm_va+"+::::+"+recordAndroidParams+",,"+rrr.recordSwiftParams);
			Iterator<String> iter = mm_va.keySet().iterator();
			if(iter.hasNext()){
				String a_param=iter.next();
				String t_param= recordAndroidParams.get(a_param);
				String s_param =(String) rrr.recordSwiftParams.get(mm_va.get(a_param));

				//				System.out.println(t_param+"####"+s_param);

				String i3 = SQLiteJDBC.insertTableAndroid("var_android", id,t_param,name_android,"","","");
				String i4 = SQLiteJDBC.insertTableSwift("var_swift", id, s_param,name_swift,rrr.func_decl,"","",id);

				stmt.executeUpdate(i3);
				stmt.executeUpdate(i4);	
				id++;

			}
		}else{

		}

		Iterator<String> dddd = mm_va.keySet().iterator();
		int aaaaa = rrr.recordStatement.size();
		int bbbbb = recordStatement.size();
		String i3="";
		String i4="";
		mid++;		
		if(return_android=="NULL" || rrr.returnType=="NULL"){
			i3 = SQLiteJDBC.insertTableAndroidM("var_android", id,Main.androidClassName,name_android,"");
			i4 = SQLiteJDBC.insertTableSwiftM("var_swift", id, Main.swiftClassName,name_swift,"",id);
		}else{
			//			insertTableAndroidM
			i3 = SQLiteJDBC.insertTableAndroid("var_android", id,Main.androidClassName,name_android,"",return_android,"");
			i4 = SQLiteJDBC.insertTableSwift("var_swift", id, Main.swiftClassName,name_swift,"",rrr.returnType,"",id);
		}
		stmt.executeUpdate(i3);
		stmt.executeUpdate(i4);	
		id++;
	}

	static LinkedList<VarDeclElement> alist = new LinkedList<VarDeclElement>();
	static LinkedList<VarDeclElement> slist = new LinkedList<VarDeclElement>();

	static Map<String, VarDeclElement> amapping 	= new HashMap<String, VarDeclElement>();
	static Map<String, VarDeclElement> smapping 	= new HashMap<String, VarDeclElement>();
	static Map<String, String> mappingKeysVarDecl 	= new HashMap<String, String>();
	static int id 	= 1;
	static int mid 	= 1;
	public <T> void mappingVarDecl() throws FileNotFoundException, IOException, ClassNotFoundException, SQLException{
		RecordVarDeclSwift recordSwift = new RecordVarDeclSwift<>();
		swift.accept(recordSwift);

		RecordVarDeclAndroid recordAndroid = new RecordVarDeclAndroid();
		android.accept(recordAndroid);

		int na = recordAndroid.record.size();
		int ns = recordSwift.record.size();

		//		Class.forName("org.sqlite.JDBC");

		Connection connection = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		Statement stmt = connection.createStatement();

		//		System.out.println();

		List<String> 		anodes 		= new LinkedList<String>();
		Map<Integer, String> aamap = new HashMap<Integer, String>();
		Map<Integer, String> ssmap = new HashMap<Integer, String>();
		List<String> 		aanodes 	= new LinkedList<String>();
		List<ASTNode> 		taanodes 	= new LinkedList<ASTNode>();


		List<String> 	ssnodes 					= new LinkedList<String>();
		List<ParserRuleContext> 	tssnodes 		= new LinkedList<ParserRuleContext>();
		Scanner scanner = new Scanner();
		scanner.recordLineSeparator = true;
		char[] source = android.toString().toCharArray();

		//		StringBuffer b = new StringBuffer();
		scanner.setSource(source);
		//				scanner.getCurrentTokenString()
		SwiftBaseListener lis = new SwiftBaseListener(){
			@Override
			public void enterEveryRule(ParserRuleContext ctx) {
				// TODO Auto-generated method stub
				if(!ssnodes.contains(ctx.getText()) && !(ctx instanceof SwiftParser.Variable_declarationContext)){
					ssnodes.add(ctx.getText());
					tssnodes.add(ctx);

				}
				super.enterEveryRule(ctx);
			}
			
			@Override
			public void enterExpression(SwiftParser.ExpressionContext ctx) {
				// TODO Auto-generated method stub
				
				super.enterExpression(ctx);
			}
		};
		ParseTreeWalker walker = new ParseTreeWalker();
		walker.walk(lis, swift);
/*
		
		SwiftBaseVisitor3 vvvv = new SwiftBaseVisitor3(){
			
			@Override
			public Object visit(ParseTree tree) {
				// TODO Auto-generated method stub
				ssnodes.add(tree.getText());
//				tssnodes.add(tree);
				System.err.println("ParseTree"+tree.getText());
				return super.visit(tree);
			}
			
		};
		
		swift.accept(vvvv);

*/

		int token = 0;
		while (token != TerminalTokens.TokenNameEOF) {
			try {
				if(scanner.getCurrentTokenStartPosition()>=0 && scanner.getCurrentTokenEndPosition()<=source.length){
					anodes.add(scanner.getCurrentTokenString());
					aamap.put(scanner.getCurrentTokenStartPosition(), scanner.getCurrentTokenString());
					//					taanodes.add
				}
				token = scanner.getNextToken();

			} catch (InvalidInputException e) {

			}
		}




		android.accept(new ASTVisitor() {
			@Override
			public void preVisit(ASTNode n) {
				if(!aanodes.contains(n.toString())&& !(n instanceof Javadoc)&& !(n instanceof FieldDeclaration)){
					//					aanodes.add(n.toString());
					//					taanodes.add(n);
				}
				// TODO Auto-generated method stub
				/*
				if (n instanceof Name) if(!aanodes.contains(n.toString()))aanodes.add(n.toString());
				if (n instanceof Type) if(!aanodes.contains(n.toString()))aanodes.add(n.toString());
				if (n instanceof Modifier) if(!aanodes.contains(n.toString()))aanodes.add(n.toString());
				if (n instanceof StringLiteral) if(!aanodes.contains(n.toString()))aanodes.add(n.toString());
				if (n instanceof NumberLiteral) if(!aanodes.contains(n.toString()))aanodes.add(n.toString());
				if (n instanceof CharacterLiteral) if(!aanodes.contains(n.toString()))aanodes.add(n.toString());
				if (n instanceof BooleanLiteral) if(!aanodes.contains(n.toString()))aanodes.add(n.toString());
				//		        if (n instanceof InfixExpression) aanodes.add(n.toString());
				//		        if (n instanceof PrefixExpression) aanodes.add(n.toString());
				//		        if (n instanceof PostfixExpression) aanodes.add(n.toString());
				//		        if (n instanceof Assignment) if(!aanodes.contains(n.toString()))aanodes.add(n.toString());
				if (n instanceof TextElement) if(!aanodes.contains(n.toString()))aanodes.add(n.toString());
				if (n instanceof TagElement) if(!aanodes.contains(n.toString()))aanodes.add(n.toString());
				if(n instanceof Expression) if(!aanodes.contains(n.toString()))aanodes.add(n.toString());
				//				if(n instanceof TerminalTokens) anodes.add(n);
				 * 
				 */
				super.preVisit(n);
			}

			@Override
			public boolean visit(FieldDeclaration node) {
				// TODO Auto-generated method stub
				if(node.getType()!=null){
					taanodes.add(node.getType());
					aanodes.add(node.getType().toString());
				}
				if(node.modifiers()!=null){
					List modis = node.modifiers();
					for(int j=0; j<modis.size();j++){
						taanodes.add((ASTNode)modis.get(j));
						aanodes.add(modis.get(j).toString());
					}
				}
				List frgs = node.fragments();
				for(int i=0;i<frgs.size();i++){
					VariableDeclarationFragment fg = (VariableDeclarationFragment)frgs.get(i);
					if(fg.getInitializer()!=null){
						taanodes.add(fg.getInitializer());
						aanodes.add(fg.getInitializer().toString());
					}
					if(fg.getName()!=null){
						taanodes.add(fg.getName());
						aanodes.add(fg.getName().toString());
					}
				}

				return super.visit(node);
			}

		});
		//		System.out.println("VarTEST"+android.toString()+"	"+swift.getText()+"    "+ssnodes.toString()+"   "+anodes+"		"+aanodes+ssnodes);


		double[][] ecostMatrix = new double[aanodes.size()][ssnodes.size()];


		StringMetric emetric =
				with(new JaroWinkler())
				.build();

		for(int i=0;i<aanodes.size();i++){
			for(int j=0;j<ssnodes.size();j++){
				ecostMatrix[i][j]= 1- emetric.compare(aanodes.get(i), ssnodes.get(j));
			}
		}
		HungarianAlgorithm ehung = null;
		int[] eresult = new int[0];
		if(aanodes.size() > 0 && ssnodes.size() >0){
			ehung = new HungarianAlgorithm(ecostMatrix);
			eresult = ehung.execute();
		}

		Map<String, String> amap = new HashMap<String, String>();
		//		Map<String, String> aamap = new HashMap<String, String>();
		Map<String, String> smap = new HashMap<String, String>();
		int argnum = 0;
		for(int p=0; p<eresult.length;p++){
			if(eresult[p]!=-1){
				//				String aifexpr= aanodes.get(p).toString().replace(" ", "");	
				//				String sifexpr=ssnodes.get(eresult[p]).replace(" ", "");	
				//				System.out.println(aanodes.size()+aifexpr+" @@@@***** "+ssnodes.size()+sifexpr);
				amap.put(aanodes.get(p), "<arg"+argnum+">");
				//				aamap.put(aifexpr, "arg"+argnum);
				smap.put(ssnodes.get(eresult[p]), "<arg"+argnum+">");
				argnum++;
			}
		}

		int az = swift.start.getStartIndex();
		int bz = swift.stop.getStopIndex();
		Interval interval = new Interval(az,bz);

		String swift_str = swift.start.getInputStream().getText(interval);
		//		System.err.println("amap"+amap+",, "+smap+swift.start.getInputStream().getText(interval));
		Iterator<String> ait = amap.keySet().iterator();
		Iterator<String> sit = smap.keySet().iterator();

		swift.start.getInputStream();
		android.setJavadoc(null);
		String android_str = android.toString();

		int aaa=0;
		while(ait.hasNext()){
			String key = ait.next();

			String expr = key;
			String arg = amap.get(key);
			//			System.err.println(astnode+" "+arg);
			expr=expr.replaceAll("\\(", "\\\\(");
			expr=expr.replaceAll("\\)", "\\\\)");
			expr=expr.replaceAll("\\.", "\\\\.");
			//			expr=expr.replaceAll("\\+", "\\\\+");
			//			expr=expr.replaceAll("\\-", "\\\\-");
			expr=expr.replaceAll("\\*", "\\\\*");
			expr=expr.replaceAll("\\[", "\\\\[");
			expr=expr.replaceAll("\\]", "\\\\]");
			expr=expr.replaceAll("\\}", "\\\\}");
			expr=expr.replaceAll("\\{", "\\\\{");
			expr=expr.replaceAll("/", "\\\\/");
			android_str=android_str.replaceAll("\\b"+expr+"\\b", arg);
			aaa++;
		}

		int sss=0;
		while(sit.hasNext()){
			String key = sit.next();
			String expr = key;
			String arg = smap.get(key);

			expr=expr.replaceAll("\\(", "\\\\(");
			expr=expr.replaceAll("\\)", "\\\\)");
			expr=expr.replaceAll("\\.", "\\\\.");
			//			expr=expr.replaceAll("\\+", "\\\\+");
			//			expr=expr.replaceAll("\\-", "\\\\-");
			expr=expr.replaceAll("\\*", "\\\\*");
			expr=expr.replaceAll("\\[", "\\\\[");
			expr=expr.replaceAll("\\]", "\\\\]");
			expr=expr.replaceAll("\\}", "\\\\}");
			expr=expr.replaceAll("\\{", "\\\\{");
			expr=expr.replaceAll("/", "\\\\/");

			//			System.err.println(astnode+" "+arg);
			swift_str=swift_str.replaceAll("\\b"+expr+"\\b", arg);
			sss++;
		}




		//		System.out.println("new anodes"+android_str+"   "+swift_str);



		if(aaa==sss){
			BufferedWriter out = new BufferedWriter(new FileWriter("sample2.txt", true));
			//		out.write(android.toString()+"	"+swift.getText());
			out.write(android_str.toString()+"\n");
			out.write(" "+swift_str.toString()+"\n");
			//		out.write(sbuf.toString()+"\n");
			out.close();
		}
		if(na==ns && na==4){

			String a1 = recordAndroid.recordStr.get(0);
			String a2 = recordAndroid.recordStr.get(1);
			String a3 = recordAndroid.recordStr.get(2);
			String a4 = recordAndroid.recordStr.get(3);
			alist.add(new VarDeclElement(a1, a2, a3, a4));
			amapping.put(a3, new VarDeclElement(a1, a2, a3, a4));
			String s1 = ((MappingElement) recordSwift.record.get(0)).label;
			String s2 = ((MappingElement) recordSwift.record.get(1)).label;
			String s3 = ((MappingElement) recordSwift.record.get(2)).label;
			String s4 = ((MappingElement) recordSwift.record.get(3)).label;
			slist.add(new VarDeclElement(s1, s2, s3, s4));

			smapping.put(s3, new VarDeclElement(s1, s2, s3, s4));

			mappingKeysVarDecl.put(a3, s3);
			String i3 = SQLiteJDBC.insertTableAndroid("var_android", id,Main.androidClassName,a3,a1,a2,a4);
			String i4 = SQLiteJDBC.insertTableSwift("var_swift", id, Main.swiftClassName,s3,s1,s2,s4,id);
			stmt.executeUpdate(i3);
			stmt.executeUpdate(i4);	
			id++;
		}
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "[android:"+android.toString()+", swift:"+swift.getText()+"]";
	}


}
