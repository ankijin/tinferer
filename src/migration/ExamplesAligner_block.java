package migration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.TokenStreamRewriter;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import org.simmetrics.StringMetric;
import org.simmetrics.StringMetrics;
import org.simmetrics.metrics.CosineSimilarity;

import org.simmetrics.tokenizers.Tokenizers;

import alignment.common.HungarianAlgorithm;
import alignment.common.LineCharIndex;
import alignment.common.OpProperty;
import alignment.ruleinfer.QueryExamples;
import antlr_parsers.javaparser.JavaBaseListener;
import antlr_parsers.javaparser.JavaLexer;
import antlr_parsers.javaparser.JavaParser;
import antlr_parsers.javaparser.JavaParser.ClassBodyDeclarationContext;
import antlr_parsers.pcommon.PCommonBaseListener;
import antlr_parsers.pcommon.PCommonLexer;
import antlr_parsers.pcommon.PCommonParser;
import antlr_parsers.pcommon.PCommonParser.ExpressionContext;
import sqldb.DBTablePrinter;
import sqldb.SQLiteJDBC;

public class ExamplesAligner_block {
	public static CommonTokenStream swiftcommon=null;
	public static CommonTokenStream androidcommon=null;
	public static String androidpath=null;

	static String readFile(String path, Charset encoding) 
			throws IOException 
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}


	public static <T> Map<String, ParserRuleContext> getMethodDeclAndroid(String path) throws FileNotFoundException, IOException, IndexOutOfBoundsException{
		Map<String, ParserRuleContext> nodeAndroid = new HashMap<String, ParserRuleContext>();

		/*
		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new j2swift.Java8Lexer((CharStream)stream1);
		androidcommon= new CommonTokenStream(lexer1);
		j2swift.Java8Parser parser = new j2swift.Java8Parser((new CommonTokenStream(lexer1)));
		j2swift.Java8Parser.CompilationUnitContext com = parser.compilationUnit();
		ParseTreeWalker walker = new ParseTreeWalker();
		 */


		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new JavaLexer((CharStream)stream1);
		androidcommon= new CommonTokenStream(lexer1);
		JavaParser parser = new JavaParser(new CommonTokenStream(lexer1));
		JavaParser.CompilationUnitContext com = parser.compilationUnit();
		ParseTreeWalker walker = new ParseTreeWalker();

		JavaBaseListener listener = new JavaBaseListener(){
			//			j2swift.Java8Parser.FieldDeclarationContext field; 
			ParserRuleContext cntx;
			@Override
			public void enterClassBodyDeclaration(ClassBodyDeclarationContext ctx) {
				// TODO Auto-generated method stub
				cntx = ctx;
				super.enterClassBodyDeclaration(ctx);
			}
			@Override
			public void enterMethodDeclaration(antlr_parsers.javaparser.JavaParser.MethodDeclarationContext ctx) {
				// TODO Auto-generated method stub
				//				ctx.m
				//				;
				if(cntx!=null)
					nodeAndroid.put(ctx.Identifier().getText(), cntx);
				super.enterMethodDeclaration(ctx);
			}
			/*
			@Override
			public void enterMethodDeclaration(MethodDeclarationContext ctx) {
				// TODO Auto-generated method stub
				nodeAndroid.put(ctx.methodHeader().methodDeclarator().Identifier().getText(), ctx);

				super.enterMethodDeclaration(ctx);
			}
			 */
		};
		walker.walk(listener, com);

		return nodeAndroid;
	}


	public static Map<String, String> mappingVarDec(Map<String, ParserRuleContext> nodeAndroid, Map<String, ParserRuleContext> nodeSwift, boolean toPrint, String pp){

		//		alignment of VariableDeclartion
		Object[] keyA =  nodeAndroid.keySet().toArray();
		Object[]  keyS = nodeSwift.keySet().toArray();

		Map<String, String> hmapping = new HashMap<String, String>();
		StringMetric metric = StringMetrics.jaroWinkler();
		//		StringMetrics.jaroWinkler();
		float max_score =-1;
		//		System.out.println(pp);
		double[][] costMatrix = new double[keyA.length][keyS.length];
		//		if(keyA.length>=keyS.length){

		for(int i=0; i< keyA.length;i++){
			for(int j=0; j< keyS.length;j++){
				float score = metric.compare(keyA[i].toString(), keyS[j].toString());
				costMatrix[i][j]=1-score; //same: 1-1
			}
		}

		int[] result =  new int[0];
		if(keyA.length >0 &&  keyS.length>0){
			HungarianAlgorithm hung = new HungarianAlgorithm(costMatrix);
			result = hung.execute();
		}
		for(int p=0; p<result.length;p++){
			if(result[p]!=-1){
				if(costMatrix[p][result[p]] < 0.3){
					//					System.err.println(android_files.get(p).getName()+", "+swift_files.get(result[p]).getName());
					hmapping.put(keyA[p].toString(), keyS[result[p]].toString());
				}
			}
			//			System.out.println(p+",, "+result[p]);
			//			System.out.println(android_files.get(result[p])+", "+swift_files.get(p));
		}

		System.out.println("hmapping"+hmapping);


		//		}
		return hmapping;
	}

	public static <T> Map<String, ParserRuleContext> getMethodDeclSwift(String path) throws FileNotFoundException, IOException{

		Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();

		SwiftParser.Top_levelContext root1;
		try{

			ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
			Lexer lexer1 = new SwiftLexer((CharStream)stream1);
			SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
			root1 = parser1.top_level();
		} catch (Exception e){
			return nodeSwift;
		}

		VariableDeclarion<T> visitor = new VariableDeclarion<T>(nodeSwift){
			@Override
			public T visitFunction_name(SwiftParser.Function_nameContext ctx) {
				nodeSwift.put(ctx.getText(), ctx.getParent());
				return super.visitFunction_name(ctx);
			}

			@Override
			public T visitVariable_name(SwiftParser.Variable_nameContext ctx) {
				// TODO Auto-generated method stub

				nodeSwift.put(ctx.getText(), ctx.getParent());
				return super.visitVariable_name(ctx);
			}	
		};
		root1.accept(visitor);
		return nodeSwift;
	}

	public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {
		// TODO Auto-generated method stub


		//						recording.java.Filewalker fw = new recording.java.Filewalker("testsrc", "mcharts/Charts-master");
		//		recording.java.Filewalker fw = new recording.java.Filewalker("wordpress/WordPress-Android-develop", "wordpress/WordPress-iOS-develop");
		//		recording.java.Filewalker fw = new recording.java.Filewalker("reactivex/RXJava-1.x", "reactivex/RXSwift-master");
		//		recording.java.Filewalker fw = new recording.java.Filewalker("wire/wire-android-master", "wire/wire-ios-develop");
		//		recording.java.Filewalker fw = new recording.java.Filewalker("wechat/TSWeChat-master", "wechat/wechat-master");


		alignment.common.Filewalker 	fw1 	= new alignment.common.Filewalker();
		LinkedList<File> 			afiles 	= new LinkedList<File>(), sfiles = new LinkedList<File>();

		//		fw1.walkforjava("geometry-api-java-master", afiles );
		//		fw1.walkforswift("geometry-api-java-master", sfiles );
		//		reimbursement-platform-for-meals-master
		//		fw1.walkforjava("reimbursement-platform-for-meals-master", afiles );
		//		fw1.walkforswift("reimbursement-platform-for-meals-master", sfiles );

		//		fw1.walkforjava("Sturtevant-Auto-Tools-master", afiles );
		//		fw1.walkforswift("SAS-Tools-master", sfiles );
		//		recording.java.Filewalker fw = new recording.java.Filewalker("Sturtevant-Auto-Tools-master", "SAS-Tools-master");

		//		fw1.walkforjava("SocialQ-IOS-Android-Mobile-App-master", afiles );
		//		fw1.walkforswift("SocialQ-IOS-Android-Mobile-App-master", sfiles );
		//		Refactoring-StringCalculator-Kata-master
		//		SocialQ-IOS-Android-Mobile-App-master

		//good! 100
		//				fw1.walkforjava("roundandsplit-master", afiles );
		//				fw1.walkforswift("roundandsplit-master", sfiles );


		//				String jnames[] = {"Refactoring-StringCalculator-Kata-master"};
		//				String snames[] = {"Refactoring-StringCalculator-Kata-master"};

		//				lib-xpush-react-native-master
		//				String jnames[] = {"/Users/kijin/Downloads/lib-xpush-android-master"};
		//				String snames[] = {"/Users/kijin/Downloads/lib-xpush-react-native-master"};
		//45
		//		fw1.walkforjava("Refactoring-StringCalculator-Kata-master", afiles );
		//		fw1.walkforswift("Refactoring-StringCalculator-Kata-master", sfiles );

		//688
		//		fw1.walkforjava("cardboard-java-master", afiles );
		//		fw1.walkforswift("cardboard-swift-master", sfiles );	
		//		repos.put("antlr4.java", "/Users/kijin/Downloads/antlr4-4.6/runtime/Swift");
		//		recording.java.Filewalker fw = new recording.java.Filewalker("/Users/kijin/Documents/RESEARCH/repos/cardboard-java-master", "/Users/kijin/Documents/RESEARCH/repos/cardboard-swift-master");
		//				recording.java.Filewalker fw = new recording.java.Filewalker("testsrc", "mcharts/Charts-master");
		//	recording.java.Filewalker.walk_folder("Refactoring-StringCalculator-Kata-master", sfiles);
		//		recording.java.Filewalker fw = new recording.java.Filewalker("Refactoring-StringCalculator-Kata-master", "Refactoring-StringCalculator-Kata-master");
		//				repos.put("geometry-api-java-master", "geometry-api-swift-master");	
		//				String jnames[] = {"testsrc", "/Users/kijin/Documents/RESEARCH/repos/cardboard-java-master", "antlr4.java", "geometry-api-java-master", "Refactoring-StringCalculator-Kata-master"};
		//		String snames[] = {"mcharts/Charts-master", "/Users/kijin/Documents/RESEARCH/repos/cardboard-swift-master", "/Users/kijin/Downloads/antlr4-4.6/runtime/Swift", "geometry-api-swift-master", "Refactoring-StringCalculator-Kata-master"};

		//		String jnames[] = {"/Users/kijin/Documents/RESEARCH/repos/cardboard-java-master"}; //testsrc
		//				, "/Users/kijin/Documents/RESEARCH/repos/cardboard-java-master", "antlr4.java", "geometry-api-java-master", "Refactoring-StringCalculator-Kata-master"};
		//		String snames[] = {"/Users/kijin/Documents/RESEARCH/repos/cardboard-swift-master"};//mcharts/Charts-master


		String jnames[] = {"testsrc"};
		String snames[] = {"mcharts/Charts-master"};

		//		String jnames[] = {"/Users/kijin/Documents/RESEARCH/repos/cardboard-java-master"};
		//		String snames[] = {"/Users/kijin/Documents/RESEARCH/repos/cardboard-swift-master"};


		//		String snames[] = {"/Users/kijin/Desktop/Charts-master/Charts"};


		//		String jnames[] = {"/Users/kijin/Downloads/mustache.java-master"};
		//		String snames[] = {"/Users/kijin/Downloads/GRMustache.swift-master"};
		//1c not ported
		//		String jnames[] = {"/Users/kijin/Downloads/JavaVerbalExpressions-master"};
		//		String snames[] = {"/Users/kijin/Downloads/SwiftVerbalExpressions-master"};

		//		String jnames[] = {"/Users/kijin/Downloads/clover-android-sdk-master"};
		//		String snames[] = {"/Users/kijin/Downloads/GRMustache.swift-master"};

		//		String snames[] = {"/Users/kijin/Downloads/Siren-master"};
		//		String jnames[] = {"/Users/kijin/Downloads/Siren-develop"};

		//14
		//387 eas
		//				String jnames[] = {"/Users/kijin/Downloads/android-master"};
		//				String snames[] = {"/Users/kijin/Downloads/swipe-master"};


		//		String jnames[] = {"/Users/kijin/Downloads/live-judging-android-master"};
		//		String snames[] = {"/Users/kijin/Downloads/LiveJudge-iOS-master"};




		//				String jnames[] = {"/Users/kijin/Documents/RESEARCH/repos/mt-data-api-sdk-java-master"}; //testsrc
		//				, "/Users/kijin/Documents/RESEARCH/repos/cardboard-java-master", "antlr4.java", "geometry-api-java-master", "Refactoring-StringCalculator-Kata-master"};
		//				String snames[] = {"/Users/kijin/Documents/RESEARCH/repos/mt-data-api-sdk-swift-master"};//mcharts/Charts-master
		//				, "/Users/kijin/Documents/RESEARCH/repos/cardboard-swift-master", "/Users/kijin/Downloads/antlr4-4.6/runtime/Swift", "geometry-api-swift-master", "Refactoring-StringCalculator-Kata-master"};
		//				String jnames[] = {"geometry-api-java-master"};
		//				String snames[] = {"geometry-api-swift-master"};
		//				String jnames[] = {"antlr4.java"};
		//				String snames[] = {"/Users/kijin/Downloads/antlr4-4.6/runtime/Swift"};


		//		LinkedList<OpProperty> stmts_swift = new LinkedList<OpProperty>();
		//		LinkedList<OpProperty> stmts_android = new LinkedList<OpProperty>();





		//4 ported, 100
		//				String jnames[] = {"/Users/kijin/Documents/RESEARCH/repos/EasyVRClassLibrary-java-master"};
		//				String snames[] = {"/Users/kijin/Documents/RESEARCH/repos/EasyVRKit-Swift-master"};


		//2
		//		String jnames[] = {"/Users/kijin/Documents/RESEARCH/repos/android_quiz_game-master"};
		//		String snames[] = {"/Users/kijin/Documents/RESEARCH/repos/swift_quiz_game-master"};	

		//2
		//		String jnames[] = {"/Users/kijin/Documents/RESEARCH/repos/android-oss-master"};
		//		String snames[] = {"/Users/kijin/Documents/RESEARCH/repos/ios-oss-master"};	

		//				String jnames[] = {"/Users/kijin/Documents/RESEARCH/repos/kiwix-android-master"};
		//				String snames[] = {"/Users/kijin/Documents/RESEARCH/repos/iOS-master"};
		// 2c ported but .. <10
		//				String jnames[] = {"/Users/kijin/Documents/RESEARCH/repos/BuzzMovie-master"};
		//				String snames[] = {"/Users/kijin/Documents/RESEARCH/repos/BuzzMovie-iOS-master"};

		//15classess 134
		//						String jnames[] = {"/Users/kijin/Documents/RESEARCH/repos/docopt.java-master"};
		//						String snames[] = {"/Users/kijin/Documents/RESEARCH/repos/docopt.swift-master"};	
		//ported however .. 4
		//				String jnames[] = {"/Users/kijin/Documents/RESEARCH/repos/java-design-patterns-master"};
		//				String snames[] = {"/Users/kijin/Documents/RESEARCH/repos/DesignPattern-Swift-master"};
		//no
		//				String jnames[] = {"/Users/kijin/Documents/RESEARCH/repos/Android-Charity-App-master"};
		//				String snames[] = {"/Users/kijin/Documents/RESEARCH/repos/iOS-Charity-App-master"};
		//no
		//				String jnames[] = {"/Users/kijin/Documents/RESEARCH/repos/mt-data-api-sdk-java-master"};
		//				String snames[] = {"/Users/kijin/Documents/RESEARCH/repos/mt-data-api-sdk-swift-master"};

		//				String snames[] = {"/Users/kijin/Downloads/URLEmbeddedView-master"};
		//				String jnames[] = {"/Users/kijin/Downloads/OpenGraphView-master"};

		/*
		for(File a:sfiles){


			//			String context = jroot.start.getInputStream().getText(interval);
			String content = readFile(a.getAbsolutePath(), StandardCharsets.UTF_8);
			String aaa="";
//			String aaa = content.replaceAll("@Override[\\s|\\t|\\n]* ", "@Override ");
						aaa = content.replaceAll(",[\\s|\\t|\\n]*", ", ");
//						aaa = content.replaceAll("\\([\\s|\\t|\\n]*", "\\(");
			//			System.err.println("TEST\n\n\n"+aaa);


			Pattern p = Pattern.compile(",[\\s|\\t|\\n]*");
			Matcher m = p.matcher(aaa);
			if(m.find()){
				System.out.println(a.getAbsolutePath());
				System.err.println(aaa);
				FileWriter fw = new FileWriter(a);
				fw.write(aaa);
				fw.close();
			}

		}


//*/
		//		repos.put("antlr4.java", "/Users/kijin/Downloads/antlr4-4.6/runtime/Swift");


		//		recording.java.Filewalker fw = new recording.java.Filewalker("Android-App-master", "iOS-App-master");
		//		recording.java.Filewalker fw = new recording.java.Filewalker("antlr4.java", "/Users/kijin/Downloads/antlr4-4.6/runtime/Swift");

		Connection c;
		//					if(!listner_swift.hasErrors() && !listner.hasErrors() && cost!=prev_cost){

		c = DriverManager.getConnection("jdbc:sqlite:ttttt1110721.db");

		java.sql.Statement stmt = c.createStatement();
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_binding_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("constant_stat"));

		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroidStat());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwiftStat());
		stmt.executeUpdate(SQLiteJDBC.createBindingStat());
		stmt.executeUpdate(SQLiteJDBC.createConstantStat());

		c.setAutoCommit(true);


		for(int b=0;b<jnames.length;b++){
			System.err.println(jnames[b]+"\n"+snames[b]);
			alignment.common.Filewalker fw = new alignment.common.Filewalker(jnames[b], snames[b]);

			Map<File, File> classmap = fw.getClassMapping();

			Iterator<File> afileset = classmap.keySet().iterator();
			System.out.println(classmap.size());
			//			sql_stmt.executeUpdate(e12);	

			//counting types of examples
			int cA=0, cE=0, cB=0;
			while(afileset.hasNext()){
				File afile=afileset.next();
				File sfile=classmap.get(afile);

				//				System.out.println(afile.getName()+"::: "+classmap.size());
				//				if(1==1){
				if(afile.getName().toString().equals("ViewPortHandler.java")){
					//					System.err.println(sfile.getAbsolutePath());
					//					System.out.println(afile.getAbsolutePath());
					String a_exam = "", s_exam="";
					ANTLRInputStream sstream 					= new ANTLRInputStream(new FileReader(sfile));
					Lexer plexer 								= new PCommonLexer((CharStream)sstream);
					antlr_parsers.pcommon.PCommonParser pparser				= new antlr_parsers.pcommon.PCommonParser(new CommonTokenStream(plexer));
					antlr_parsers.pcommon.PCommonParser.ParseContext proot 	= pparser.parse();
					ParseTreeWalker pwalker 					= new ParseTreeWalker();
					LinkedList<OpProperty> sub_stmts_swift 		= new LinkedList<OpProperty>();

//					 = new Integer(0);

					PCommonBaseListener plistener = new PCommonBaseListener(){
						@Override
						public void enterAssignment(PCommonParser.AssignmentContext ctx) {

							//					if(ctx.depth()==9){
//							int max_depth_src = Math.max(max_depth_src, ctx.depth());
							int a = ctx.start.getStartIndex();
							int b = ctx.stop.getStopIndex();
							Interval interval = new Interval(a,b);

							String context 	= ctx.start.getInputStream().getText(interval);
							OpProperty op 	= new OpProperty(null, context,"AssignmentContext",ctx.depth());
							op.toCompareStr = ctx.leftAssign().getText();
							op.lineindex 	= LineCharIndex.contextToLineIndex(ctx);
							sub_stmts_swift.add(op);
							//					}
							super.enterAssignment(ctx);
						}


						@Override
						public void enterEnclosed(PCommonParser.EnclosedContext ctx) {


							//					if(ctx.depth()==6){

							int a = ctx.start.getStartIndex();
							int b = ctx.stop.getStopIndex();
							Interval interval = new Interval(a,b);

							//						String context = proot.start.getInputStream().getText(interval);		
							String context = ctx.start.getInputStream().getText(interval);
							OpProperty op = new OpProperty(null, context,"EnclosedContext",ctx.depth());

							a = ctx.outer().start.getStartIndex();
							b = ctx.outer().stop.getStopIndex();
							op.toCompareStr = ctx.start.getInputStream().getText(new Interval(a,b));
							op.lineindex = LineCharIndex.contextToLineIndex(ctx);

							sub_stmts_swift.add(op);
							super.enterEnclosed(ctx);
						}

						@Override
						public void enterExpression(ExpressionContext ctx) {
							// TODO Auto-generated method stub
							if(!ctx.getText().equals("///\n") || !ctx.getText().equals("//\n")){
								int a = ctx.start.getStartIndex();
								int b = ctx.stop.getStopIndex();
								Interval interval = new Interval(a,b);		
								String context = ctx.start.getInputStream().getText(interval);
								OpProperty op = new OpProperty(null, context,"ExpressionContext",ctx.depth());
								op.toCompareStr = context;
								op.lineindex = LineCharIndex.contextToLineIndex(ctx);
								sub_stmts_swift.add(op);
							}
							super.enterExpression(ctx);
						}
					};

					try{
						pwalker.walk(plistener, proot);
					}catch (Exception e){

					}

					//					System.out.println("^^\n"+sub_stmts_swift);

					sstream = new ANTLRInputStream(new FileReader(sfile));


					Map<LineCharIndex, ParserRuleContext> swiftLineMap = new HashMap<LineCharIndex, ParserRuleContext>();

					Lexer slexer = new antlr_parsers.swiftparser.SwiftLexer((CharStream)sstream);
					antlr_parsers.swiftparser.SwiftParser sparser						= new antlr_parsers.swiftparser.SwiftParser(new CommonTokenStream(slexer));
					antlr_parsers.swiftparser.SwiftParser.Top_levelContext sroot 	 = null;	
					try{
						sroot = sparser.top_level();
					}catch(Exception e){
						System.out.println("parsing error \n"+sfile);
					}
					antlr_parsers.swiftparser.SwiftBaseListener slistner 				= new antlr_parsers.swiftparser.SwiftBaseListener(){

						@Override
						public void enterEveryRule(ParserRuleContext ctx) {
							// TODO Auto-generated method stub
							LineCharIndex lineIndex=LineCharIndex.contextToLineIndex(ctx);


							int a = ctx.start.getStartIndex();
							int b = ctx.stop.getStopIndex();
							Interval interval = new Interval(a,b);
							//						String context = sroot.start.getInputStream().getText(interval);	
							//					System.out.println("lineindex "+lineIndex+" \n"+context);
							for(OpProperty sop:sub_stmts_swift){
								if(sop.lineindex!=null){
									if(sop.lineindex.equals(lineIndex)){
										sop.node = ctx;	
										sop.type = ctx.getRuleIndex();
										swiftLineMap.put(lineIndex, ctx);
									}
								}
							}
							super.enterEveryRule(ctx);
						}

					};
					try{
						if(sroot!=null){
							pwalker.walk(slistner, sroot);
						}
					}catch(Exception e){
						System.out.println("ERRRRR	"+sfile+"  "+afile);
						e.printStackTrace();
					}
					//swift statement alignment
					System.out.println("&&&"+sub_stmts_swift);

					slistner= new antlr_parsers.swiftparser.SwiftBaseListener(){

						@Override
						public void enterEveryRule(ParserRuleContext ctx) {
							// TODO Auto-generated method stub
							LineCharIndex lineIndex=LineCharIndex.contextToLineIndex(ctx);
							//							System.out.println(lineIndex+"*!*"+ctx.getText());
							for(int k=0; k< sub_stmts_swift.size();k++){
								OpProperty sop = sub_stmts_swift.get(k);
								if(sop.type==0 && !swiftLineMap.containsKey(lineIndex)){
									//								if(sop.type==0 && !swiftLineMap.containsKey(lineIndex) && !sop.lineindex.toString().equals("91:8~94:2707")){
									//									System.out.println("sop.lineindexsop.lineindex"+sop.strASTNode+" "+sop.lineindex);
									//parent of
									//									if(lineIndex.isIncluded(sop.lineindex)){
									if(sop.lineindex.isIncluded(lineIndex)){
										//										if(lineIndex.startLine ==87){
										sop.node = ctx;	
										sop.type = ctx.getRuleIndex();
										//										sop.height = 9;
										int a = ctx.start.getStartIndex();
										int b = ctx.stop.getStopIndex();
										Interval interval = new Interval(a,b);
										swiftLineMap.put(lineIndex, ctx);
										//							String context = aproot.start.getInputStream().getText(interval);	
										String context = ctx.start.getInputStream().getText(interval);
										sop.strASTNode = context;
										sop.lineindex = lineIndex;
										//										sop.property = "EnclosedContext";
										//										sub_stmts_swift.set(k, sop);
										//										System.out.println("11sop.lineindexsop.lineindex	"+sop+"     "+ctx.getText());
									}
								}

							}
							/*
							for(OpProperty sop:sub_stmts_swift){
								if(sop.type==0){
//									System.out.println("sop.lineindexsop.lineindex"+sop.strASTNode+" "+sop.lineindex);
									//parent of
//									if(lineIndex.isIncluded(sop.lineindex)){
										if(lineIndex.startLine ==87){
										sop.node = ctx;	
										sop.type = ctx.getRuleIndex();

										int a = ctx.start.getStartIndex();
										int b = ctx.stop.getStopIndex();
										Interval interval = new Interval(a,b);

										//							String context = aproot.start.getInputStream().getText(interval);	
										String context = ctx.start.getInputStream().getText(interval);
										sop.strASTNode = context;
										System.out.println("11sop.lineindexsop.lineindex"+ctx.getText());
									}
								}
							}
							 */
							super.enterEveryRule(ctx);
						}

					};

					try{
						if(sroot!=null){
							pwalker.walk(slistner, sroot);
						}
					}catch(Exception e){
						System.out.println("ERRRRR	"+sfile+"  "+afile);
						e.printStackTrace();
					}


					LinkedList<OpProperty> 	sub_stmts_android 		= new LinkedList<OpProperty>();
					ANTLRInputStream 	   	astream 				= new ANTLRInputStream(new FileReader(afile));
					Lexer 					aplexer 				= new PCommonLexer((CharStream)astream);
					antlr_parsers.pcommon.PCommonParser 	apparser				= new antlr_parsers.pcommon.PCommonParser(new CommonTokenStream(aplexer));
					antlr_parsers.pcommon.PCommonParser.ParseContext aproot 		= apparser.parse();
					ParseTreeWalker 		apwalker 				= new ParseTreeWalker();

					PCommonBaseListener aplistener = new PCommonBaseListener(){
						//				int depth = 1000;

						@Override
						public void enterAssignment(PCommonParser.AssignmentContext ctx) {
							int a = ctx.start.getStartIndex();
							int b = ctx.stop.getStopIndex();
							Interval interval = new Interval(a,b);

							//						String context = aproot.start.getInputStream().getText(interval);		
							String context = ctx.start.getInputStream().getText(interval);
							OpProperty op = new OpProperty(null, context,"AssignmentContext",ctx.depth());
							op.toCompareStr = ctx.leftAssign().getText();
							op.lineindex = LineCharIndex.contextToLineIndex(ctx);
							sub_stmts_android.add(op);

							//					}
							super.enterAssignment(ctx);
						}


						@Override
						public void enterEnclosed(PCommonParser.EnclosedContext ctx) {
							//					if(ctx.depth()==6){
							int a = ctx.start.getStartIndex();
							int bb = ctx.stop.getStopIndex();
							Interval interval = new Interval(a,bb);

							//						String context = aproot.start.getInputStream().getText(interval);
							String context = ctx.start.getInputStream().getText(interval);

							OpProperty op = new OpProperty(null, context,"EnclosedContext",ctx.depth());
							op.lineindex = LineCharIndex.contextToLineIndex(ctx);
							a = ctx.outer().start.getStartIndex();
							bb = ctx.outer().stop.getStopIndex();
							try{
								op.toCompareStr = ctx.start.getInputStream().getText(new Interval(a,bb));}
							catch (java.lang.StringIndexOutOfBoundsException e){
								System.out.println("errorr	"+e.getMessage()+" "+sfile);

							}
							//						System.out.println("op.toCompareStr\n"+op.toCompareStr);
							sub_stmts_android.add(op);
							//					}


							super.enterEnclosed(ctx);
						}



						@Override
						public void enterExpression(ExpressionContext ctx) {

							if(!ctx.getText().equals("//")){
								// TODO Auto-generated method stub
								int a = ctx.start.getStartIndex();
								int b = ctx.stop.getStopIndex();
								Interval interval = new Interval(a,b);

								//							String context = aproot.start.getInputStream().getText(interval);	
								String context = ctx.start.getInputStream().getText(interval);
								OpProperty op = new OpProperty(null, context,"ExpressionContext",ctx.depth());
								op.lineindex = LineCharIndex.contextToLineIndex(ctx);
								op.toCompareStr = ctx.getText();
								sub_stmts_android.add(op);
							}
							super.enterExpression(ctx);
						}

					};

					//							walker.walk(listener, root);

					apwalker.walk(aplistener, aproot);


					astream = new ANTLRInputStream(new FileReader(afile));


					//				System.err.println("\nsub_stmts_android.size()"+sub_stmts_android);
					//				System.out.println("\nsub_stmts_swift.size()"+sub_stmts_swift);
					Lexer jlexer = new antlr_parsers.javaparser.JavaLexer((CharStream)astream);
					CommonTokenStream commonj = new CommonTokenStream(jlexer);
					antlr_parsers.javaparser.JavaParser jparser						= new antlr_parsers.javaparser.JavaParser(new CommonTokenStream(jlexer));
					TokenStreamRewriter rewriter = new TokenStreamRewriter(commonj);

					antlr_parsers.javaparser.JavaParser.CompilationUnitContext jroot 	= jparser.compilationUnit();
					antlr_parsers.javaparser.JavaBaseListener jlistner 				= new antlr_parsers.javaparser.JavaBaseListener(){

						@Override
						public void enterEveryRule(ParserRuleContext ctx) {
							// TODO Auto-generated method stub
							LineCharIndex lineIndex=LineCharIndex.contextToLineIndex(ctx);


							//							int a = ctx.start.getStartIndex();
							//							int b = ctx.stop.getStopIndex();

							for(OpProperty aop:sub_stmts_android){
								if(aop.lineindex.equals(lineIndex)){
									aop.node = ctx;	
									aop.type = ctx.getRuleIndex();

								}
								//							}
							}
							//						if(ctx.getRuleIndex() == 11) System.out.println("ENM"+context);
							super.enterEveryRule(ctx);
						}

					};

					pwalker.walk(jlistner, jroot);



					jlistner= new antlr_parsers.javaparser.JavaBaseListener(){

						@Override
						public void enterEveryRule(ParserRuleContext ctx) {
							// TODO Auto-generated method stub
							LineCharIndex lineIndex=LineCharIndex.contextToLineIndex(ctx);
							/*
							for(OpProperty aop:sub_stmts_android){
								if(aop.type==0){
									if(lineIndex.isIncluded(aop.lineindex)){

										int a = ctx.start.getStartIndex();
										int b = ctx.stop.getStopIndex();
										Interval interval = new Interval(a,b);

										//							String context = aproot.start.getInputStream().getText(interval);	
										String context = ctx.start.getInputStream().getText(interval);
										aop.node = ctx;	
										aop.type = ctx.getRuleIndex();
										aop.strASTNode = context;
										System.err.println("aop.lineindexaop.lineindex"+ctx.getText());
									}
								}
							}
							 */

							for(int k=0; k<sub_stmts_android.size();k++ ){
								OpProperty aop = sub_stmts_android.get(k);
								if(aop.type==0 && !ctx.getText().startsWith("{")){
									if(lineIndex.isIncluded(aop.lineindex)){

										int a = ctx.start.getStartIndex();
										int b = ctx.stop.getStopIndex();
										Interval interval = new Interval(a,b);

										//							String context = aproot.start.getInputStream().getText(interval);	
										String context = ctx.start.getInputStream().getText(interval);
										aop.node = ctx;	
										aop.type = ctx.getRuleIndex();
										aop.strASTNode = context;
										sub_stmts_android.set(k, aop);
										System.err.println("aop.lineindexaop.lineindex"+ctx.getText());
									}
								}
							}

							super.enterEveryRule(ctx);
						}

					};


					pwalker.walk(jlistner, jroot);

					/*
					StringMetric emetric =
							with(new CosineSimilarity<String>())

							//				.simplify()
							.tokenize(Tokenizers.whitespace())
							.build();
					 */
					StringMetric emetric = StringMetrics.jaroWinkler();

					double[][] ecostMatrix = new double[sub_stmts_android.size()][sub_stmts_swift.size()];
					//		double[] ecostM = new double[stmts_android.size()];
					//			for(int i=0; i< 100;i++){
					System.err.println("sub_stmts_android\n"+sub_stmts_android);
					System.out.println("sub_stmts_swift\n"+sub_stmts_swift);
					//					for(int k=0; k<sub_stmts_android.size();k++ ){
					//						sub_stmts_android.get(k).lineindex.toString().equals("sub_stmts_android");
					//					}
					//										107:8~109:2496

					for(int i=0; i< sub_stmts_android.size();i++){
						OpProperty android = sub_stmts_android.get(i);
						for(int j=0; j< sub_stmts_swift.size();j++){
							OpProperty swift = sub_stmts_swift.get(j);
							//						if( android.property.equals(swift.property) && Math.abs(android.height-swift.height)<=0) {
							//							if( android.property.equals(swift.property)) {
							//							if(swift.lineindex.toString().equals("87:8~90:2649") || swift.lineindex.toString().equals("91:8~94:2707")){
							//								swift.type =0;
							//							}

							if(android.property.toString().equals(swift.property.toString()) && ((android.height==swift.height)&& android.height< 5) && (android.type!=0 && swift.type!=0)) {
								ecostMatrix[i][j]	= Math.min(0.9999, 1- emetric.compare(android.strASTNode, swift.strASTNode));
							}else{
								ecostMatrix[i][j]	=1;
							}



						}
						//					if(i%100==0)System.out.println(". "+i);
					}


					HungarianAlgorithm ehung = new HungarianAlgorithm(ecostMatrix);
					int[] eresult = ehung.execute();
					//int[] eresult_copy = new int[0];
					//		if(wocontext.a_nodes.size() > 0 && swictx.s_nodes.size() >0){
					//				ehung = 
					//				eresult = 

					int k=0;
					//				System.err.println("\neresult.length\n"+eresult.length+"\n"+eresult);
					for(int pp=0; pp<eresult.length;pp++){

						//					if(eresult[pp]!=-1 && ecostMatrix[pp][eresult[pp]] !=1 ){
						//						System.out.println("ppp"+pp+" -> "+eresult[pp]);

						if(eresult[pp]!=-1 && ecostMatrix[pp][eresult[pp]] !=1 && ecostMatrix[pp][eresult[pp]] <=0.3 ){
							a_exam = sub_stmts_android.get(pp).strASTNode;
							s_exam = sub_stmts_swift.get(eresult[pp]).strASTNode;
							k++;

							/*
							if(sub_stmts_android.get(pp).type==0 && sub_stmts_swift.get(eresult[pp]).type==0){
								if(sub_stmts_android.get(pp).property.equals("EnclosedContext")){

//									RuleContext aparent = ((ParserRuleContext)sub_stmts_android.get(pp).node).parent;
//									RuleContext sparent = ((ParserRuleContext)sub_stmts_swift.get(eresult[pp]).node).parent;
									System.err.println("$$\n"+sub_stmts_android.get(pp).strASTNode+"\n  "+sub_stmts_swift.get(eresult[pp]).strASTNode);
								}
							}
							 */
							//						if( sub_stmts_swift.get(eresult[p]).type==7|sub_stmts_swift.get(eresult[p]).type==110)
							//						System.out.println(k+" ->"+a_exam+":::"+s_exam);
							String example_sql="";
							try{
								example_sql 	= SQLiteJDBC.updateTableGrammarSwift2(QueryExamples.idd, sub_stmts_android.get(pp).strASTNode.replaceAll("\\'", ""), sub_stmts_android.get(pp).type, sub_stmts_swift.get(eresult[pp]).type, sub_stmts_swift.get(eresult[pp]).strASTNode.replaceAll("\\'", ""), 
										afile.getName(), sub_stmts_android.get(pp).lineindex.toString(),
										sfile.getName(), sub_stmts_swift.get(eresult[pp]).lineindex.toString());
								//						sub_stmts_android.get(p).property.equals(")
								//								System.out.println("example_sql\n"+example_sql);
								stmt.executeUpdate(example_sql);
							} catch (Exception e){
								e.printStackTrace();
								System.out.println("err in\n"+example_sql);
							}
							if(sub_stmts_android.get(pp).type!=0 && sub_stmts_android.get(pp).type!=0){


								if(sub_stmts_android.get(pp).property.equals("ExpressionContext") && 
										sub_stmts_swift.get(eresult[pp]).property.equals("ExpressionContext")){
									cE++;
								}

								if(sub_stmts_android.get(pp).property.equals("AssignmentContext") && 
										sub_stmts_swift.get(eresult[pp]).property.equals("AssignmentContext")){
									cA++;
								}
								if(sub_stmts_android.get(pp).property.equals("EnclosedContext") && 
										sub_stmts_swift.get(eresult[pp]).property.equals("EnclosedContext")){
									cB++;
								}

								//							ExpressionContextEnclosedContextAssignmentContext
								//							System.err.println(sub_stmts_android.get(p).strASTNode);
							}


						}
					}
					//				System.out.println("total :"+k);
					sub_stmts_android.clear();
					sub_stmts_swift.clear();


					//		} temp if
					DBTablePrinter.printTable(c, "grammar_swift_stat");	
					//				System.out.println("B "+cB+" "+"A "+cA+" E "+cE);
				}
			}//for file-mappings


		}
		/*
		for(File sfile:sfiles){
			try{
			LinkedList<OpProperty> sub_stmts_swift = new LinkedList<OpProperty>();
			ANTLRInputStream sstream = new ANTLRInputStream(new FileReader(sfile));
			//			ANTLRInputStream stream2 = new ANTLRInputStream(context);
			System.out.println("sfile"+sfile);
			Lexer plexer = new PCommonLexer((CharStream)sstream);
			pcommon.PCommonParser pparser	= new pcommon.PCommonParser(new CommonTokenStream(plexer));
			pcommon.PCommonParser.ParseContext proot = pparser.parse();
			ParseTreeWalker pwalker = new ParseTreeWalker();
			//		int rootd = proot.depth();
			//		Interval interval1 = new Interval(proot.start.getStartIndex(),proot.stop.getStopIndex());
			//		String context1 = proot.start.getInputStream().getText(interval1);
			//		System.out.println("proot	"+rootd+" "+context1);

			PCommonBaseListener plistener = new PCommonBaseListener(){
				//				int depth = 1000;


				@Override
				public void enterAssignment(PCommonParser.AssignmentContext ctx) {

//					if(ctx.depth()==9){

						int a = ctx.start.getStartIndex();
						int b = ctx.stop.getStopIndex();
						Interval interval = new Interval(a,b);

						String context = proot.start.getInputStream().getText(interval);		
						OpProperty op = new OpProperty(null, context,"AssignmentContext",ctx.depth());
						op.lineindex = LineCharIndex.contextToLineIndex(ctx);
						sub_stmts_swift.add(op);
//					}


					super.enterAssignment(ctx);
				}


				@Override
				public void enterEnclosed(PCommonParser.EnclosedContext ctx) {


//					if(ctx.depth()==6){

						int a = ctx.start.getStartIndex();
						int b = ctx.stop.getStopIndex();
						Interval interval = new Interval(a,b);

						String context = proot.start.getInputStream().getText(interval);		
						OpProperty op = new OpProperty(null, context,"EnclosedContext",ctx.depth());
						op.lineindex = LineCharIndex.contextToLineIndex(ctx);
						sub_stmts_swift.add(op);
//					}

					super.enterEnclosed(ctx);
				}


				@Override
				public void enterExpression(ExpressionContext ctx) {
					// TODO Auto-generated method stub
					int a = ctx.start.getStartIndex();
					int b = ctx.stop.getStopIndex();
					Interval interval = new Interval(a,b);

					String context = proot.start.getInputStream().getText(interval);		
					OpProperty op = new OpProperty(null, context,"ExpressionContext",ctx.depth());

					op.lineindex = LineCharIndex.contextToLineIndex(ctx);

					sub_stmts_swift.add(op);
					super.enterExpression(ctx);
				}

			};

			//		walker.walk(listener, root);

			pwalker.walk(plistener, proot);


			sstream = new ANTLRInputStream(new FileReader(sfile));

			Lexer slexer = new swiftparser.SwiftLexer((CharStream)sstream);
			swiftparser.SwiftParser sparser						= new swiftparser.SwiftParser(new CommonTokenStream(slexer));
			swiftparser.SwiftParser.Top_levelContext sroot 		= sparser.top_level();
			swiftparser.SwiftBaseListener slistner 				= new swiftparser.SwiftBaseListener(){

				@Override
				public void enterEveryRule(ParserRuleContext ctx) {
					// TODO Auto-generated method stub
					LineCharIndex lineIndex=LineCharIndex.contextToLineIndex(ctx);


					int a = ctx.start.getStartIndex();
					int b = ctx.stop.getStopIndex();
					Interval interval = new Interval(a,b);
//					String context = jroot.start.getInputStream().getText(interval);	
//					System.out.println("lineindex "+lineIndex+" \n"+context);
					for(OpProperty sop:sub_stmts_swift){
						if(sop.lineindex!=null){

//							if(aop.strASTNode.equals(context)){
							if(sop.lineindex.equals(lineIndex)){
//								System.out.println("sss testing line index"+ctx.getText());
								sop.node = ctx;	
							}
						}
					}
					super.enterEveryRule(ctx);
				}

			};

			pwalker.walk(slistner, sroot);

			stmts_swift.addAll(sub_stmts_swift);
			} catch (Exception e){
				System.out.println("error in"+sfile);
			}

		}





		for(File afile:afiles){
			LinkedList<OpProperty> sub_stmts_android = new LinkedList<OpProperty>();
			ANTLRInputStream astream = new ANTLRInputStream(new FileReader(afile));
			System.out.println("afile"+afile);
			//			ANTLRInputStream stream2 = new ANTLRInputStream(context);
			Lexer plexer = new PCommonLexer((CharStream)astream);
			pcommon.PCommonParser pparser	= new pcommon.PCommonParser(new CommonTokenStream(plexer));

			pcommon.PCommonParser.ParseContext proot = pparser.parse();
			ParseTreeWalker pwalker = new ParseTreeWalker();
			//		int rootd = proot.depth();
			//		Interval interval1 = new Interval(proot.start.getStartIndex(),proot.stop.getStopIndex());
			//		String context1 = proot.start.getInputStream().getText(interval1);
			//		System.out.println("proot	"+rootd+" "+context1);





			PCommonBaseListener plistener = new PCommonBaseListener(){
				//				int depth = 1000;

				@Override
				public void enterAssignment(PCommonParser.AssignmentContext ctx) {


//					if(ctx.depth()==9){

						int a = ctx.start.getStartIndex();
						int b = ctx.stop.getStopIndex();
						Interval interval = new Interval(a,b);

						String context = proot.start.getInputStream().getText(interval);		
						OpProperty op = new OpProperty(null, context,"AssignmentContext",ctx.depth());
						op.lineindex = LineCharIndex.contextToLineIndex(ctx);
						sub_stmts_android.add(op);

//					}
					super.enterAssignment(ctx);
				}


				@Override
				public void enterEnclosed(PCommonParser.EnclosedContext ctx) {



//					if(ctx.depth()==6){
						int a = ctx.start.getStartIndex();
						int b = ctx.stop.getStopIndex();
						Interval interval = new Interval(a,b);

						String context = proot.start.getInputStream().getText(interval);		
						OpProperty op = new OpProperty(null, context,"EnclosedContext",ctx.depth());
						op.lineindex = LineCharIndex.contextToLineIndex(ctx);
						sub_stmts_android.add(op);
//					}


					super.enterEnclosed(ctx);
				}



				@Override
				public void enterExpression(ExpressionContext ctx) {
					// TODO Auto-generated method stub
					int a = ctx.start.getStartIndex();
					int b = ctx.stop.getStopIndex();
					Interval interval = new Interval(a,b);

					String context = proot.start.getInputStream().getText(interval);		
					OpProperty op = new OpProperty(null, context,"ExpressionContext",ctx.depth());
					op.lineindex = LineCharIndex.contextToLineIndex(ctx);
					sub_stmts_android.add(op);
					super.enterExpression(ctx);
				}

			};

			//		walker.walk(listener, root);

			pwalker.walk(plistener, proot);


			astream = new ANTLRInputStream(new FileReader(afile));

			Lexer jlexer = new javaparser.JavaLexer((CharStream)astream);
			javaparser.JavaParser jparser						= new javaparser.JavaParser(new CommonTokenStream(jlexer));
			javaparser.JavaParser.CompilationUnitContext jroot 	= jparser.compilationUnit();
			javaparser.JavaBaseListener jlistner 				= new javaparser.JavaBaseListener(){

				@Override
				public void enterEveryRule(ParserRuleContext ctx) {
					// TODO Auto-generated method stub
					LineCharIndex lineIndex=LineCharIndex.contextToLineIndex(ctx);


					int a = ctx.start.getStartIndex();
					int b = ctx.stop.getStopIndex();
					Interval interval = new Interval(a,b);
//					String context = jroot.start.getInputStream().getText(interval);	
//					System.out.println("lineindex "+lineIndex+" \n"+context);
					for(OpProperty aop:sub_stmts_android){
						if(aop.lineindex!=null){

//							if(aop.strASTNode.equals(context)){
							if(aop.lineindex.equals(lineIndex)){
//								System.out.println("testing line index"+ctx.getText());
								aop.node = ctx;	
							}
						}
					}
					super.enterEveryRule(ctx);
				}

			};

			pwalker.walk(jlistner, jroot);

			stmts_android.addAll(sub_stmts_android);

//			sub_stmts_android


		}
		 */
		/*
		System.out.println("stmts_android.size()"+stmts_android.size());
		System.out.println(stmts_swift.size());


		StringMetric emetric =
				with(new JaroWinkler())

				//				.simplify()
				.build();

		double[][] ecostMatrix = new double[stmts_android.size()][stmts_swift.size()];
		//		double[] ecostM = new double[stmts_android.size()];
//		for(int i=0; i< 100;i++){

		for(int i=0; i< stmts_android.size();i++){
			OpProperty android = stmts_android.get(i);
			for(int j=0; j< stmts_swift.size();j++){
				OpProperty swift = stmts_swift.get(j);
				if( android.property.equals(swift.property) && Math.abs(android.height-swift.height)==0) {
//					if( android.property.equals(swift.property) && android.height==swift.height && StructureMappingAntlr4.toCommon(android.strASTNode, swift.strASTNode)>0.9) {
					ecostMatrix[i][j]	= Math.min(0.999, 1- emetric.compare(android.strASTNode, swift.strASTNode));
//										System.out.println(android+"::"+swift);
				}else{
					ecostMatrix[i][j]	=1;
				}
			}
			if(i%100==0)System.out.println(". "+i);
		}


		HungarianAlgorithm ehung = null;
		int[] eresult = new int[0];
		int[] eresult_copy = new int[0];
		//		if(wocontext.a_nodes.size() > 0 && swictx.s_nodes.size() >0){
		ehung = new HungarianAlgorithm(ecostMatrix);
		eresult = ehung.execute();

		int k=0;
		System.err.println(eresult.length);
		for(int p=0; p<eresult.length;p++){

			if(eresult[p]!=-1 && ecostMatrix[p][eresult[p]] !=1 && ecostMatrix[p][eresult[p]] <0.2){
				String a_exam = stmts_android.get(p).strASTNode;
				String s_exam = stmts_swift.get(eresult[p]).strASTNode;
				k++;
//				System.out.println(k+" ->"+a_exam+":::"+s_exam);
			}
		}
		System.out.println("total :"+k);

		//		}



		//		pcommon.PCommonParser.ParseContext proot = pparser.parse();



		/*
		Class.forName("org.sqlite.JDBC");
		Connection connection = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		connection.setAutoCommit(true);
		Statement stmt = connection.createStatement();
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift"));

		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift_stat"));

		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroidStat());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwiftStat());


		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroid());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwift());

//		recording.java.Filewalker fw = new recording.java.Filewalker("testsrc", "mcharts/Charts-master");
//		recording.java.Filewalker fw = new recording.java.Filewalker("wordpress/WordPress-Android-develop", "wordpress/WordPress-iOS-develop");
		recording.java.Filewalker fw = new recording.java.Filewalker("reactivex/RXJava-1.x", "reactivex/RXSwift-master");
//		recording.java.Filewalker fw = new recording.java.Filewalker("wire/wire-android-master", "wire/wire-ios-develop");
//		recording.java.Filewalker fw = new recording.java.Filewalker("wechat/TSWeChat-master", "wechat/wechat-master");

		Map<File, File> classmap = fw.getClassMapping();
		recording.java.Filewalker.staticPrintMapping(classmap);

		Map<String, ParserRuleContext> varDeclSwift 		= null;
		Map<String, ParserRuleContext> varDeclAndroid 		= null;
		Map<String, ParserRuleContext> methodDecliOS 		= null;
		Map<String, ParserRuleContext> methodDeclAndroid 	= null;

		//get variable and method declarations class by class
		Iterator<File> a_classes = classmap.keySet().iterator();
		int b =0;
		while(a_classes.hasNext() ){ 
			//			Math.max(a, b);

			//						if(b==5) break;
//anchor has
			//return, remaining has syntax
			//for a in aaa {}

			File aclass_path = a_classes.next();
			File sclass_path = classmap.get(aclass_path);
			androidpath = aclass_path.getAbsolutePath();
//			if(aclass_path.getName().equals("ViewPortHandler.java")){
			if(1==1){
				//			get variable declarations of aligned class by brief visit of ASTs
				varDeclSwift   = getVarDeclSwift(sclass_path.getAbsolutePath());
				varDeclAndroid = getVarDeclAndroid(aclass_path.getAbsolutePath());
				methodDecliOS 	= getMethodDeclSwift(sclass_path.getAbsolutePath());
				methodDeclAndroid = getMethodDeclAndroid(aclass_path.getAbsolutePath());

				Map<String, String> varmap = mappingVarDec(varDeclAndroid, varDeclSwift, true, "");
				Map<String, String> methodmap = mappingVarDec(methodDeclAndroid, methodDecliOS, true, "");
				System.err.println(aclass_path.getName()+":"+sclass_path.getName());
				System.out.println("varmap"+varmap);
				System.out.println("methodmap"+methodmap);
				Iterator<String> iterator_varDec = varmap.keySet().iterator();
				while(iterator_varDec.hasNext()){
					String key_a=iterator_varDec.next();
					//					varDeclAndroid.get(key_a);
					//					varDeclSwift.get(varmap.get(key_a));
//					StructureMappingAntlr4 structure = new StructureMappingAntlr4(varDeclAndroid.get(key_a), varDeclSwift.get(varmap.get(key_a)));
//					structure.compute();
//					DBTablePrinter.printTable(connection, "grammar_android");
//					DBTablePrinter.printTable(connection, "grammar_swift");
				}//iteration of aligned variable declarations


				Iterator<String> iterator_methodDec = methodmap.keySet().iterator();
				while(iterator_methodDec.hasNext()){
					String key_a=iterator_methodDec.next();
					//					varDeclAndroid.get(key_a);
					//					varDeclSwift.get(varmap.get(key_a));
					StructureMappingAntlr4 structure = new StructureMappingAntlr4(methodDeclAndroid.get(key_a), methodDecliOS.get(methodmap.get(key_a)));
					structure.compute();

//					DBTablePrinter.printTable(connection, "grammar_android_stat");
//					DBTablePrinter.printTable(connection, "grammar_swift_stat");

//					DBTablePrinter.printTable(connection, "grammar_android");
//					DBTablePrinter.printTable(connection, "grammar_swift");
//					b++;
				}//iteration of aligned variable declarations`



//				StructureMappingAntlr4 structure1 = new StructureMappingAntlr4();


//				DBTablePrinter.printTable(connection, "grammar_android");
//				DBTablePrinter.printTable(connection, "grammar_swift");


				DBTablePrinter.printTable(connection, "grammar_android_stat");
				DBTablePrinter.printTable(connection, "grammar_swift_stat");

			}

			//				StructureMappingAntlr4 structure = new StructureMappingAntlr4();


		}
		//		}
		//				System.err.println(varDeclSwift);
		//				System.out.println(varDeclAndroid);
		//				System.err.println(methodDecliOS);
		//				System.err.println(methodDeclAndroid);
		connection.close();
		 */
	}



}
