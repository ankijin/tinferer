package j2swift;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.TestRig;
import org.antlr.v4.runtime.tree.*;
//import org.antlr.v4.runtime.tree.gui.TreeViewer;
import org.stringtemplate.v4.ST;

import alignment.common.Filewalker;
import antlr_parsers.javaparser.JavaLexer;
import antlr_parsers.javaparser.JavaParser;
import j2swift.J2SwiftListener_c.RewriteUnit;
import migration.SwiftLexer;
import migration.SwiftParser;
import migration.SwiftParser.For_in_statementContext;
import migration.SwiftParser.Top_levelContext;
//import recording.java.Android2Swift.Migration;
import templates.MatchingTemplate;
import java.lang.reflect.Method;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Very basic Java to Swift syntax converter.
 * See test/Test.java and test/Test.swift for an idea of what this produces.
 * This is a work in progress...
 * @author Pat Niemeyer (pat@pat.net)
 */
public class J2Swift_c
{
	public static String dbName="test_record_others.db";
	public static Map<String, String> item_map = new HashMap<String, String>();
	
	public static void aaa(){

	}
	//Template and variable Matching algorithms
	public static void getMigrationExpr(String android, String log) throws NoSuchMethodException, SecurityException, RecognitionException{
		try {
			Connection connection = DriverManager.getConnection("jdbc:sqlite:"+dbName);
			connection.setAutoCommit(true);
			Statement stmt = connection.createStatement();

//			String sql="select expr_swift.EXPR_TEMP, expr_android.VARS, expr_swift.VARS_S,count(expr_swift.EXPR_TEMP)" 
//					+" from expr_android, expr_swift where expr_swift.ANDROID=expr_android.ID"+ 
//					" and expr_android.EXPR=\'"+android+"\' order by count(expr_swift.EXPR_TEMP) desc";
			
			
			int type=6;
			String sql="select template, example from grammar_swift_stat where parent_type ="+type;
			System.out.println("rule"+SwiftParser.ruleNames[type]);
			ResultSet resultSet = stmt.executeQuery(sql);
//			ResultSet resultSet3 = stmt.executeQuery(sql);
//			Map<String, String> item_map = new HashMap<String, String>();
			
			Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+"test_record.db");
			connection2.setAutoCommit(true);
			Statement stmt2 = connection2.createStatement();
			String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
			ResultSet resultSet2 = stmt2.executeQuery(sql2);
			while(resultSet2.next()){
				String aaa = resultSet2.getString("template");
				String bbb = resultSet2.getString("example");
				aaa = aaa.replaceAll("\\.\\.\\.", "\\<arg10\\>");
				bbb = bbb.replaceAll("\\.\\.\\.", "\\<arg10\\>");
				System.out.println(bbb);
				item_map.put(aaa, bbb);
			}
			System.out.println(item_map);
//			item_map.put("for (<arg2> <arg0>:<arg1>) {<arg3>}", "for <arg0> in <arg1> \n{\n/** \n <arg3> \n**/ \n}");
//			item_map.put("for (<arg2> <arg0>:<arg1>) {<arg3>}", "for <arg0> in <arg1> \n{\n <arg3> }");
			//			item_map.put("for (final <arg1> <arg2> : <arg0>) {<arg3>}", "for <arg2> in <arg0> {<arg3>}");
//for (final <arg1> <arg2> : <arg0>) {...}
			String expr_temp=null, android_vars =null, swift_vars=null;
			while(resultSet.next()){
				//				init = resultSet.getString("INIT");
				//				System.out.println(resultSet.getString("EXPR_TEMP")+"::::"+resultSet.getString("VARS")+"_"+resultSet.getString("VARS_S"));
				expr_temp = resultSet.getString("template");
				android_vars = resultSet.getString("example");
//				swift_vars = resultSet.getString("PARENT_TYPE");
//				System.out.println("sql:"+expr_temp+" "+android_vars+" "+swift_vars);
				
								
				
				Iterator<String> iter = item_map.keySet().iterator();
				while(iter.hasNext()){
					String kkey = iter.next();
					Map<String, String> argmap = MatchingTemplate.getAttributesMap(kkey, expr_temp);
					if(!argmap.isEmpty()){
						
//						System.err.println("argmap"+argmap);
						ST st_android = new ST(item_map.get(kkey));
						Iterator<String> iterator = argmap.keySet().iterator();
						int count =	0;
						while(iterator.hasNext()){
							String key=iterator.next();
							st_android.add(key, argmap.get(key));
							//				st_android2.add(key, argmap.get(key));
//							System.err.println("key"+key);
							count++;
						}
						if(count >0){
//							System.err.println("example:"+expr_temp);
//							System.out.println("groundOf:"+android_vars);
							
							PrintWriter out = new PrintWriter("migration.swift");
							out.write(st_android.render());
							out.close();
							System.out.println("example of android:\n"+expr_temp);
							System.err.println("\nmigration to swift:\n"+st_android.render());
							System.out.println("ground of swift: \n"+android_vars);
							
							ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader("migration.swift"));
							Lexer lexer1 = new SwiftLexer((CharStream)stream1);
							CommonTokenStream swiftcommon = new CommonTokenStream(lexer1);
							SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
							parser1.setTokenStream(swiftcommon);
							parser1.setTrace(true);
							Class<? extends Parser> parserClass;
//							Method startRule = parserClass.getMethod("for_in_statement");
//							parser1.addErrorListener(listener);
//							 ParserRuleContext root1 = parser1.getContext();
//							System.out.println(root1.getRuleContext());
							String arg [] ={"Swift", SwiftParser.ruleNames[type],"migration.swift"};
//							try{
							TestRig aa;
							try {
//								aa = new TestRig(arg);
//								aa.
//								aa.process();
//								System.err.println("migrations:"+st_android.render());
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
//								System.out.println("Exception"+e.getMessage());
							}
							
//							} catch (){
//								
//							}
							System.out.println("\n");
//							replace( ctx, st_android.render());
						}
					}
				}
				
				
//				break;
			}//examples
			

//			List<String> vars_android = MatchingTemplate.getTokenValuesList(getGeneral(android), android);
//			log+=" "+vars_android.toString();
			//			String sss=printResultSet(resultSet3);
			//			log+=" "+sss;
			//[Name1, Name2,...,..]
			//			List<String> vals = MatchingTemplate.getTokenValuesList(getGeneral(android),android);


			//		Iterator<String> android_vars = vars_android.keySet().iterator();
			if(expr_temp!=null){
				//			if(swift_vars !=null && android_vars!=null && expr_temp!=null){


				//arg0, arg1,...
				log+="\n//@template "+expr_temp;

				//[Name1:Template1,...]
				//				Map<String,String> wwww = MatchingTemplate.getTokenValues(expr_temp,expr_new);
				//				log+="\n//@assitive Variables should be declared as: ";
				//				temp1.add("expr", expr_new);
				//	temp.add("log", log);

//				return new Migration(expr_temp, log);

			}else{
				/*
				String gg=getGeneral(android);
				gg = gg.replaceAll("\\(.*\\)", "(\\%)");
				//				gg.replaceAll("", replacement)
				String sql1 = "select stmts_swift.TEMPLATE from stmts_android, stmts_swift "
						+ "where stmts_android.id=stmts_swift.android and stmts_android.statements like \'%"+gg+"%\'" ;
				//				log +=sql;
				ResultSet resultSet1 = stmt.executeQuery(sql1);
				String expr_temp1=null, android_vars1 =null, swift_vars1=null;
				//				while(resultSet1.next()){
				//					System.out.println(resultSet.getString("EXPR_TEMP")+"::::"+resultSet.getString("VARS")+"_"+resultSet.getString("VARS_S"));
				//					expr_temp1= resultSet1.getString("TEMPLATE");
				//					android_vars1 = resultSet1.getString("VARS");
				//					swift_vars1 = resultSet.getString("VARS_S");
				//				}
				 */
//				return new Migration(android, "@warning: no mapping");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
//		return null;
 catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e1) {
	// TODO Auto-generated catch block
	e1.printStackTrace();
}

	}
	
	public static void main( String [] args ) throws Exception
	{

		
		//query of for-statements
		//find templates of for-stmts in android with type
		
		
		getMigrationExpr("","");
		
		
		/*
		//migration syntax with .. 
		Filewalker fw = new Filewalker("mcharts/MPAndroidChart-master", "mcharts/Charts-master");
		Map<File, File> maps = fw.getClassMapping();
		Set<File> androidcodes = maps.keySet();
		Iterator<File> iter = androidcodes.iterator();
		InputStream is = null;


		String protocol = "";
		//		String javaFile = "/Users/kijin/Documents/workspace_mars/GumTreeTester"
		//				+ "/testingFileName/grammarTest/Test_J2Swift2.java";
		//		String javaFile = "/Users/kijin/Documents/workspace_mars/GumTreeTester"
		//				+ "/testingFileName/android/TestingM1.java";
		String javaFile = "test1_0.java";
		if ( javaFile!=null ) { is = new FileInputStream(javaFile); }
		ANTLRInputStream input = new ANTLRInputStream(is);
		JavaLexer lexer = new JavaLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		JavaParser parser = new JavaParser(tokens);
		ParseTree tree = parser.compilationUnit();
		ParseTreeWalker walker = new ParseTreeWalker();
		J2SwiftListener swiftListener = new J2SwiftListener(tokens);
		parser.type();
//		System.out.println(parser.getContext().getRuleContext()+"before\n"+swiftListener.rewriter.getText() );

		//      System.err.println( swiftListener.rewriter2.getText() );
		walker.walk(swiftListener, tree);
		//        walker.walk(arg0, arg1);
		//        walker.
		System.out.println(org.antlr.v4.runtime.tree.Trees.getNodeText(tree, parser));
//		TreeViewer view = new TreeViewer(Arrays.asList(parser.ruleNames), tree);
//		view.open();"grun Java compilationUnit"
		String arg [] ={"Java", "compilationUnit","public class ViewPortHandler {}}"};
		TestRig aa = new TestRig(arg);
		aa.process();
		try{

			while ( !swiftListener.stack.empty() )
			{
				RewriteUnit rr = swiftListener.stack.pop();
				//				swiftListener.stack.
				System.out.println("dd.stack.size()"+swiftListener.stack.size()+" "+rr.context.getText()+"  "+rr.replace);
								swiftListener.replace2(rr.context, rr.replace);
				//				swiftListener.rewriter.replace(rr.context.start, rr.context.stop, rr.replace);
				//				System.out.println("rreplace"+rr.context.getText()+" "+rr.replace);
				//                System.out.print ( ',' );
			}

			System.out.println("final\n"+swiftListener.rewriter.getText() );
		} catch (Exception e){

		}
*/
	}
	
	//struct storing and returing log data and migrationExpr

	public class Migration{
		String migratedExpr;
		String log;
		ResultSet resultSet;

		public Migration(){
			migratedExpr="";
			log="";
			//			resultSet = new ResultSet();
		}
		public Migration(String m, String l){
			migratedExpr = m;
			log = l;
		}
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return migratedExpr;
		}
	}
}
