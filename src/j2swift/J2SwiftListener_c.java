package j2swift;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.stringtemplate.v4.ST;

import antlr_parsers.javaparser.JavaBaseListener;
import j2swift.Java8Parser.ExpressionContext;
import j2swift.Java8Parser.MethodDeclarationContext;
import templates.MatchingTemplate;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;
import java.util.Vector;

/**
 * Very basic Java to Swift syntax converter.
 * See test/Test.java and test/Test.swift for an idea of what this produces.
 * This is a work in progress...
 *
 * TODO:
 *  Class (static) method invocations should be qualified.
 *  Method and fields with the same name should be disambiguated.
 *  Final vars should become "let" vars.
 *
 * @author Pat Niemeyer (pat@pat.net)
 */
public class J2SwiftListener_c extends JavaBaseListener
{
	int seq = 1;
	CommonTokenStream tokens;
	TokenStreamRewriter rewriter;
	public TokenStreamRewriter rewriter2;

	boolean inConstructor;
	int formalParameterPosition;
	public Stack<RewriteUnit> stack= new Stack<RewriteUnit>();
	public Vector<RewriteUnit> queue= new Vector<RewriteUnit>();
	// Some basic type mappings
	static Map<String,String> typeMap = new HashMap<>();
	static {
		/*
        typeMap.put("float", "Float");
        typeMap.put("Float", "Float");
        typeMap.put("int", "Int");
        typeMap.put("Integer", "Int");
        typeMap.put("long", "Int64");
        typeMap.put("Long", "Int64");
        typeMap.put("boolean", "Bool");
        typeMap.put("Boolean", "Bool");
        typeMap.put("Map", "Dictionary");
        typeMap.put("HashSet", "Set");
        typeMap.put("HashMap", "Dictionary");
        typeMap.put("List", "Array");
        typeMap.put("ArrayList", "Array");
		 */
	}

	// Some basic modifier mappings (others in context)
	static Map<String,String> modifierMap = new HashMap<>();
	static {

		//modifierMap.put("protected", "internalinternalinternal");
		//modifierMap.put("volatile", "/*volatile*/");"

	}

	public J2SwiftListener_c( CommonTokenStream tokens )
	{
		this.tokens = tokens;
		this.rewriter = new TokenStreamRewriter( tokens );
		this.rewriter2 = new TokenStreamRewriter( tokens );
	}





	//	public void exitEveryRule(ParserRuleContext ctx) {


	@Override
	public void enterEveryRule(ParserRuleContext ctx) {
		//	public void exitEveryRule(ParserRuleContext ctx) {
		// TODO Auto-generated method stub
		int az = ctx.start.getStartIndex();
		int bz = ctx.stop.getStopIndex();
		Interval interval = new Interval(az,bz);
		String context = ctx.start.getInputStream().getText(interval);

		System.err.println(ctx.getRuleIndex()+":"+context);
		if(ctx.getRuleIndex()==38){
			if(context.equals("boolean")){
				replace( ctx, "Bool");
				System.out.println(seq+"::replace( ctx, Bool);");
				seq++;
				System.out.println(rewriter.getText());
			}
		}
		/*	
		if(ctx.getRuleIndex()==19){
			System.out.println("ctx.getRuleIndex()==19\n"+context);
			//public <arg0> <arg1>() {...}

			System.out.println("ctx.getRuleIndex()==70\n"+context);
			//    		replace(ctx, "testing");
			context=context.replaceAll("\n","");
			Map<String, String> argmap = MatchingTemplate.getAttributesMap("public <arg0> <arg1>() {<arg2>}", context);
			System.out.println("argmap"+argmap);
			Iterator<String> iterator = argmap.keySet().iterator();
			ST st_android = new ST("public var <arg1>: <arg0>    {<arg2>}");
			//			ST st_android2 = new ST("<arg0> var <arg2>: <arg1> = <arg3>");

			while(iterator.hasNext()){
				String key=iterator.next();
				st_android.add(key, argmap.get(key));
				//				st_android2.add(key, argmap.get(key));
				System.err.println("key"+key);
			}

//			System.out.println("st_android"+st_android.render());
			//
			//			System.out.println(MatchingTemplate.getAttributesMap("<arg0> <arg1> <arg2>=<arg3>;", context)+" "+context.indexOf("0f")+" "+context.indexOf("new RectF()"));
			//<arg0> var <arg2> = <arg3> 
			replace( ctx, st_android.render());
			System.out.println(seq+"::replace( ctx, st_android.render());");
			seq++;
			System.out.println(rewriter.getText());
		}
		 */
		Vector<String> items = new Vector<String>();
		Map<String, String> item_map = new HashMap<String, String>();
		item_map.put("for (<arg2> <arg0>:<arg1>){<arg3>}", "for <arg0> in <arg1> {<arg3>}");
//		item_map.put("if (<arg0>) {<arg1>}", "if <arg0>        {<arg1>}");
//		item_map.put("for (<arg2> <arg0>:<arg1>){<arg3>}", "//test_for");
//		item_map.put("if (<arg0>) {<arg1>}", "//test_if");
		//		items.add("for (<arg2> <arg0>:<arg1>){<arg3>}");
		//		items.add("if (<arg0>) {<arg1>}");

		if(ctx.getRuleIndex()==70){
			context=context.replaceAll("\n","");
			System.out.println("ctx.getRuleIndex()==70\n"+context);
			Iterator<String> iter = item_map.keySet().iterator();
			while(iter.hasNext()){
				String kkey = iter.next();
				Map<String, String> argmap = MatchingTemplate.getAttributesMap(kkey, context);
				if(!argmap.isEmpty()){
					System.out.println("argmap"+argmap);
					ST st_android = new ST(item_map.get(kkey));
					Iterator<String> iterator = argmap.keySet().iterator();
					int count =	0;
					while(iterator.hasNext()){
						String key=iterator.next();
						st_android.add(key, argmap.get(key));
						//				st_android2.add(key, argmap.get(key));
						System.err.println("key"+key);
						count++;
					}
					if(count >0){
						System.out.println("st_android"+st_android.render());
						replace( ctx, st_android.render());
					}
				}
			}
			
			/*
			System.out.println("ctx.getRuleIndex()==70\n"+context);
			//    		replace(ctx, "testing");
			context=context.replaceAll("\n","");
			//			if (<arg0>) {<arg1>}
			Map<String, String> argmap = MatchingTemplate.getAttributesMap("for (<arg2> <arg0>:<arg1>){<arg3>}", context);
			//			Map<String, String> argmap = MatchingTemplate.getAttributesMap("if (<arg0>) {<arg1>}", context);
			System.out.println("argmap"+argmap);
			Iterator<String> iterator = argmap.keySet().iterator();
			ST st_android = new ST("for <arg0> in <arg1> {<arg3>}");
			//			ST st_android = new ST("if <arg0>        {<arg1>}");
			//			if <arg0>        {...}
			//			ST st_android2 = new ST("<arg0> var <arg2>: <arg1> = <arg3>");
			int count =	0;
			while(iterator.hasNext()){
				String key=iterator.next();
				st_android.add(key, argmap.get(key));
				//				st_android2.add(key, argmap.get(key));
				System.err.println("key"+key);
				count++;
			}

			System.out.println("st_android"+st_android.render());
			//
			//			System.out.println(MatchingTemplate.getAttributesMap("<arg0> <arg1> <arg2>=<arg3>;", context)+" "+context.indexOf("0f")+" "+context.indexOf("new RectF()"));
			//<arg0> var <arg2> = <arg3>
			if(count >0)
				replace2( ctx, st_android.render());
			 */
			//			argmap = MatchingTemplate.getAttributesMap("for (<arg2> <arg0>:<arg1>){<arg3>}", context);
		}

		//    	<arg0> var <arg2>: <arg1> = <arg3>
		super.exitEveryRule(ctx);
	}


	// Get possibly rewritten text
	private String getText( ParserRuleContext ctx ) {
		if ( ctx == null ) { return ""; }
		return rewriter.getText( new Interval( ctx.start.getTokenIndex(), ctx.stop.getTokenIndex() ) );
	}

	protected void replace( ParserRuleContext ctx, String s ) {
		stack.push(new RewriteUnit(ctx, s));

		//				rewriter.replace( ctx.start, ctx.stop, s );
	}
	protected void replace2( ParserRuleContext ctx, String s ) {
		//		rewriter2.replace( ctx.start, ctx.stop, s );
//		rewriter.replace( ctx.start, ctx.stop, s );
//		rewriter.insertAfter(index, text);
		//		stack.push(new RewriteUnit(ctx, s));

	}

	// remove context and hidden tokens to right
	private void removeRight( ParserRuleContext ctx )
	{
		rewriter.delete( ctx.start, ctx.stop );
		List<Token> htr = tokens.getHiddenTokensToRight( ctx.stop.getTokenIndex() );
		for (Token token : htr) { rewriter.delete( token ); }
	}

	public String mapType( ParserRuleContext ctx )
	{
		//if ( ctx instanceof Java8Parser.UnannArrayTypeContext ) { }
		//String text = ctx.getText();
		String text = getText(ctx);
		return mapType( text );
	}
	public String mapType( String text )
	{
		String mapText = typeMap.get( text );
		return mapText == null ? text : mapText;
	}

	public String mapModifier( ParserRuleContext ctx )
	{
		//if ( ctx instanceof Java8Parser.UnannArrayTypeContext ) { }
		//String text = ctx.getText();
		String text = getText( ctx );
		return mapModifier( text );
	}
	public String mapModifier( String text )
	{
		String mapText = modifierMap.get( text );
		return mapText == null ? text : mapText;
	}

	class RewriteUnit{
		ParserRuleContext context;
		String replace;
		public RewriteUnit(ParserRuleContext c, String r){
			context=c;
			replace = r;
		}
	}
}

