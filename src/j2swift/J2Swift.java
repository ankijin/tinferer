package j2swift;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

import antlr_parsers.javaparser.JavaParser;

import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Very basic Java to Swift syntax converter.
 * See test/Test.java and test/Test.swift for an idea of what this produces.
 * This is a work in progress...
 * @author Pat Niemeyer (pat@pat.net)
 */
public class J2Swift{

	public static Map<Integer, Integer> Java2Java8map = new HashMap<Integer, Integer>();
	public static List<Integer> misexamples;
	
	  public static void init(){
		  Integer [] aaa={
		  99,
		  71,
	      68,
		  95,
		  98,
		  116,
		  122,
		  129,
		  359,
		  366,
		  373,
		  375,
		  376,
		  377,
		  387,
		  393,
		  395,
		  396,
		  398,
		  399,
		  533,
		  592,
		  593,
		  599,
		  637,
		  638,
		  639,
		  649,
		  651,
		  789,
		  790,
		  791,
		  802,
		  803,
		  804,
		  805,
		  827,
		  828,
		  829,
		  956,
		  1007,
		  1009,
		  1034,
		  1148,
		  1188,
		  1278,
		  1291,
		  1331,
		  1335,
		  1338,
		  1339,
		  1940,
		  1941,
		  1942,
		  1944,
		  1945,
		  1946,
		  1948,
		  1949,
		  1951,
		  1952,
		  1954,
		  1955,
		  2043,
		  2044,
		  2045,
		  2058,
		  2063,
		  2077,
		  2086,
		  2109,
		  2110,
		  2111,
		  2113,
		  2114,
		  2116,
		  2143,
		  2147,
		  2150,
		  2151,
		  2175,
		  2177,
		  2180,
		  2184,
		  2186,
		  2210,
		  2211,
		  2212,
		  2217,
		  2218,
		  2219,
		  2312,
		  2421,
		  2424,
		  2432,
		  2437,
		  2462,
		  2480,
		  2482,
		  2483,
		  2494,
		  2500,
		  2501,
		  2631,
		  3737,
		  3738,
		  3740,
		  96,
		  1015,
		  1020};
//		  misexamples
		  misexamples = Arrays.asList(aaa);
//		  Arrays.asList(aaa);
//		misexamples.add(e);
		
	}
	
	public static String getResult(String example, int type, int id) throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		
		if(type==-1){
//			String testing="public class Testing{ public void Dummy(){		"+example+"				}}";
			String testing="public class Testing{ 		"+example+"				}";
				
			
			ANTLRInputStream input = new ANTLRInputStream(testing);
			Java8Lexer lexer = new Java8Lexer(input);
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			Java8Parser parser = new Java8Parser(tokens);
			parser.removeErrorListeners();
//			parser.removeParseListeners();
			/*
			Class<? extends Parser> parserClass = null;
			ClassLoader cl = Thread.currentThread().getContextClassLoader();
			parserClass = cl.loadClass("j2swift.Java8Parser").asSubclass(Parser.class);
			int iaaa = Java2Java8map.get(type);
*/
//			Method startRule = parserClass.getMethod(Java8Parser.ruleNames[iaaa]);
//			ParseTree tree = (ParseTree)startRule.invoke(parser, (Object[])null);
			
			ParseTree tree = parser.compilationUnit();
			ParseTreeWalker walker = new ParseTreeWalker();
			J2SwiftListener swiftListener = new J2SwiftListener(tokens);
			try{
			walker.walk(swiftListener, tree);
			}catch (java.lang.IllegalArgumentException e){
				System.out.println("exception in\n"+example);
			}
//			System.out.println( swiftListener.rewriter.getText() );
			return swiftListener.rewriter.getText();
		}
		Java2Java8map.put(3, 40);
		Java2Java8map.put(19, 87);
//		Java2Java8map.put(19, 52);
		Java2Java8map.put(68, 129);
//		System.out.println("JJJJ"+id);
		//87	
		
	
		ANTLRInputStream input = new ANTLRInputStream(example);
		Java8Lexer lexer = new Java8Lexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		Java8Parser parser = new Java8Parser(tokens);
		
		
		Class<? extends Parser> parserClass = null;
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		parserClass = cl.loadClass("j2swift.Java8Parser").asSubclass(Parser.class);
		int iaaa = Java2Java8map.get(type);
		if(id==141 ||id==798||id==2429){
			iaaa = 87;
		}
		Method startRule = parserClass.getMethod(Java8Parser.ruleNames[iaaa]);
		ParseTree tree = (ParseTree)startRule.invoke(parser, (Object[])null);
		
//		ParseTree tree = parser.compilationUnit();
		ParseTreeWalker walker = new ParseTreeWalker();
		J2SwiftListener swiftListener = new J2SwiftListener(tokens);
		try{
		walker.walk(swiftListener, tree);
		}catch (java.lang.IllegalArgumentException e){
			System.out.println("exception in\n"+example);
		}
//		System.out.println( swiftListener.rewriter.getText() );
		return swiftListener.rewriter.getText();
	}

	public static void main( String [] args ) throws Exception
	{
		String test = getResult("public class CandleData extends BarLineScatterCandleBubbleData<ICandleDataSet> implements AA {...}", 3,1);
		System.out.println("\n"+test);
		
		J2Swift.init();
		System.out.println(J2Swift.misexamples);
		
		
		// This boilerplate largely from the ANTLR example
		String inputFile = null;
		if ( args.length>0 ) inputFile = args[0];
		InputStream is = System.in;
		if ( inputFile!=null ) { is = new FileInputStream(inputFile); }
		ANTLRInputStream input = new ANTLRInputStream(is);
		Java8Lexer lexer = new Java8Lexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParseTree tree = parser.compilationUnit();
		ParseTreeWalker walker = new ParseTreeWalker();
		J2SwiftListener swiftListener = new J2SwiftListener(tokens);
		walker.walk(swiftListener, tree);
		System.out.println( swiftListener.rewriter.getText() );
		
	
		
	}
}

