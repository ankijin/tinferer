package recording;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;

import mapping.MethodStructAndroid;

public class GetGlobalVarDecl {
	
	
	public static Map<String, MethodStructAndroid> getAndroid(String str) {
		
		ASTParser parser = ASTParser.newParser(AST.JLS8);
		parser.setSource(str.toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		Map<String, MethodStructAndroid> nodeAndroid = new HashMap<String, MethodStructAndroid>();
		final CompilationUnit cu = (CompilationUnit) parser.createAST(null);
		
		cu.accept(new ASTVisitor() {

			@Override
			public boolean visit(MethodDeclaration node) {
				// TODO Auto-generated method stub
//				list.clear();
				ArrayList<Integer> list = new ArrayList<Integer>();
				if(node.getBody()==null || node.getBody().statements()==null) return false;
				List sts = node.getBody().statements();
				for(int i=0; i<sts.size();i++){
					if( sts.get(i) instanceof ReturnStatement){
//						System.err.println("ReturnStatement"+ReturnStatement.RETURN_STATEMENT);
						list.add(ReturnStatement.RETURN_STATEMENT);
					}
					if( sts.get(i) instanceof SingleVariableDeclaration){
//						System.err.println("SingleVariableDeclaration");
						list.add(SingleVariableDeclaration.SINGLE_VARIABLE_DECLARATION);
					}
					if( sts.get(i) instanceof ExpressionStatement){
//						System.err.println("ExpressionStatement"+ExpressionStatement.EXPRESSION_STATEMENT);
						list.add(ExpressionStatement.EXPRESSION_STATEMENT);
					}
					if( sts.get(i) instanceof IfStatement){
//						System.err.println("IfStatement");
						list.add(IfStatement.IF_STATEMENT);
					}
					if( sts.get(i) instanceof VariableDeclarationStatement){
//						System.err.println("IfStatement");
//						System.err.println("VariableDeclarationStatement"+VariableDeclarationStatement.VARIABLE_DECLARATION_STATEMENT);
						
						list.add(VariableDeclaration.VARIABLE_DECLARATION_STATEMENT);
					}
					if( sts.get(i) instanceof VariableDeclarationExpression){
//						System.err.println("IfStatement");
						list.add(VariableDeclaration.VARIABLE_DECLARATION_EXPRESSION);
					}
				}
				nodeAndroid.put(node.getName().toString(), new MethodStructAndroid(node, list));
				return false;
			}
		});
		return nodeAndroid;
	}
}
