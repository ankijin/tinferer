package recording;

public class MappingElement {
	public String label;
	int type = 0;
	
	public MappingElement(String label, int type){
		this.label = label;
		this.type = type;
		
	}
	
	public MappingElement(String label){
		this.label = label;
	}
	public int getType(){
		return type;
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
//		return super.equals(obj);
		
//		return this.label.equals(((MappingElement) obj).label)
//				&& this.type == ((MappingElement) obj).type;
		return this.label.equals(((MappingElement) obj).label);

	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
//		return "label:"+label+" type:"+type;
		return label;
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
//		return type*300;
		return label.length();
	}
	
	public static void main(String arg []){
		MappingElement p1 = new MappingElement("test", 1);
		MappingElement p2 = new MappingElement("test", 1);
		
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p1.equals(p2));
		
	}
}
