package recording;

import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;
import org.eclipse.jdt.core.dom.ASTNode;

public class RecordMethod {
	Map<String, String> mapper_methodDec 			= null;
	Map<String, ParserRuleContext> methodDecliOS 	= null;
	Map<String, ASTNode> methodDeclA 				= null;

	public RecordMethod(Map<String, String> map, Map<String, ASTNode> android, Map<String, ParserRuleContext> swift){
		mapper_methodDec 	= map;
		methodDeclA 		= android;
		methodDecliOS 		= swift;
	}
	
	public void record(){
		
	}
	
	public class MethodUnit{
		public String modifiers;
		public String types;
		public String name;
		public String parameters;
		public String statements;
	}
}
