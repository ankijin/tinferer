package recording.java;

import static org.simmetrics.builders.StringMetricBuilder.with;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.eclipse.jdt.core.dom.Statement;
import org.simmetrics.StringMetric;
import org.simmetrics.metrics.JaroWinkler;
import org.simmetrics.simplifiers.Simplifiers;

import alignment.common.HungarianAlgorithm;

//import recordSwiftParser;

public class AssignmentList {
	List<String> list1;
	List<String> list2;

	public AssignmentList(List<String> l1, List<String> l2){
		list1= l1;
		list2= l2;
	}

	public Map<String, String> assignment(){

		StringMetric metric =
				with(new JaroWinkler())
				.simplify(Simplifiers.toLowerCase(Locale.ENGLISH))
				//	            .simplify(Simplifiers.removeAll("Chart"))
				//	            .simplify(Simplifiers.removeAll("Activity"))
				.build();
		double[][] costMatrix = new double[list1.size()][list2.size()];
		for(int i=0;i<list1.size();i++){
			for(int k=0;k<list2.size();k++){
				double score = metric.compare(list1.get(i), list2.get(k));
				costMatrix[i][k] = 1- score;
			}
		}

		Map<String, String> hclassmapping = new HashMap<String, String>();
		if(list1.size()>0 && list2.size()>0){
			HungarianAlgorithm hung = new HungarianAlgorithm(costMatrix);
			int[] result = hung.execute();
			for(int p=0; p<result.length;p++){
				if(result[p]!=-1){
					if(costMatrix[p][result[p]] < 0.9){
						//					System.err.println(android_files.get(p).getName()+", "+swift_files.get(result[p]).getName());
						hclassmapping.put(list1.get(p), list2.get(result[p]));
					}
				}
				//			System.out.println(p+",, "+result[p]);
				//			System.out.println(android_files.get(result[p])+", "+swift_files.get(p));
			}

		}
		return hclassmapping;
	}
}
