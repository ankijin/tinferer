package recording.java;
//import static org.hamcrest.CoreMatchers.instanceOf;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
//import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
import java.util.regex.Pattern;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStreamRewriter;
//import org.antlr.v4.runtime.atn.SemanticContext.Operator;
import org.apache.commons.lang3.StringUtils;
//import org.eclipse.jdt.core.dom.AST;

//import javax.print.attribute.standard.MediaSize.Other;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
//import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
//import org.eclipse.jdt.core.dom.ArrayCreation;
import org.eclipse.jdt.core.dom.ArrayType;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Block;
//import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
//import org.eclipse.jdt.core.dom.EnumConstantDeclaration;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.InfixExpression;
//import org.eclipse.jdt.core.dom.Initializer;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.PostfixExpression;
//import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.QualifiedName;
//import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SimpleName;
//import org.eclipse.jdt.core.dom.SimpleName;
//import org.eclipse.jdt.core.dom.SimpleType;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;
//import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
//import org.springframework.beans.factory.support.ReplaceOverride;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import antlr_parsers.java8.Java8Lexer;

//import com.github.mikephil.charting.utils.Utils;

import net.vivin.GenericTree;
import net.vivin.GenericTreeNode;
import net.vivin.GenericTreeTraversalOrderEnum;
import recording.MappingElement;
import templates.MatchingTemplate;

public class Android2Swift3 extends ASTVisitor{
	CommonTokenStream tokens;
	TokenStreamRewriter rewriter;

	public  Android2Swift3(CommonTokenStream tokens) {
		// TODO Auto-generated constructor stub
		this.tokens = tokens;
		this.rewriter = new TokenStreamRewriter( tokens );
	}
	@Override
	public boolean visit(FieldDeclaration node) {

		// TODO Auto-generated method stub
		//		System.out.println("FieldDeclaration"+node);
		return false;
	}



	public static void main(String[] args) throws IOException{
		String inputFile = "test1_0.java";
		InputStream is = null;
		if ( inputFile!=null ) { is = new FileInputStream(inputFile); }
		ANTLRInputStream input = new ANTLRInputStream(is);
		Java8Lexer lexer = new Java8Lexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		
		Android2Swift3 visitor = new Android2Swift3(tokens);
		
		
		
		String name = "Utils.convertDpToPixel(4f)";
		//		str = str.replaceAll("(*)", "X");
		name = name.replaceAll("\\(.*\\)", "(\\%)");
		System.out.println(name);
		
		
		 Document document = new Document("import java.util.List;\nclass X {}\n");
		
		 ASTParser parser = ASTParser.newParser(AST.JLS3);
		
		parser.setSource(document.get().toCharArray());
		 CompilationUnit cu = (CompilationUnit) parser.createAST(null);
		 AST ast = cu.getAST();
		 ImportDeclaration id = ast.newImportDeclaration();
		 id.setName(ast.newName(new String[] {"java", "util", "Set"}));
		 ASTRewrite rewriter = ASTRewrite.create(ast);
		 TypeDeclaration td = (TypeDeclaration) cu.types().get(0);
		 ITrackedNodePosition tdLocation = rewriter.track(td);
		 ListRewrite lrw = rewriter.getListRewrite(cu, CompilationUnit.IMPORTS_PROPERTY);
		 lrw.insertLast(id, null);
		 TextEdit edits = rewriter.rewriteAST(document, null);
		 UndoEdit undo = null;
		 try {
		     undo = edits.apply(document);
		 } catch(MalformedTreeException e) {
		     e.printStackTrace();
		 } catch(BadLocationException e) {
		     e.printStackTrace();
		 }
		 assert "import java.util.List;\nimport java.util.Set;\nclass X {}\n".equals(document.get());
		 // tdLocation.getStartPosition() and tdLocation.getLength()
		 // are new source range for "class X {}" in document.get()
		
		
		
		
		
		
	}

}
