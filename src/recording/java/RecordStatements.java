package recording.java;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import net.vivin.GenericTree;
import net.vivin.GenericTreeNode;
import recording.MappingElement;

public class RecordStatements extends ASTVisitor implements TemplateIfStatement{
	STGroup group = new STGroupFile("/Users/kijin/Documents/workspace_mars/GumTreeTester/cross_templates/if_statement.stg");

	public LinkedList<MappingElement> record = new LinkedList<MappingElement> ();
	public LinkedList<String> recordStr = new LinkedList<String> ();
	StringBuffer buffer;
	List<ST> template;
	GenericTreeNode<ST> tree;
	public RecordStatements(StringBuffer buffer, List<ST> template){
		this.buffer = buffer;
		this.template = template;
	}

	public RecordStatements(GenericTreeNode<ST> tree){
		this.tree = tree;
	}


	@Override
	public boolean visit(IfStatement node) {
		//		node.accept(new RecordIfStatement(buffer));

		ST if_statement = group.getInstanceOf("if_statement");
		//ST if_statement = template;
		ST expression = group.getInstanceOf("condition_clause");
		expression.add("condition_clause", "hasNext()");
		ST statement = group.getInstanceOf("statements");
		ST else_statement = group.getInstanceOf("else_clause");

		//		buffer.append(node.getExpression()+"\n");
		//if_statement(condition_clause, statements, else_clause)
		if_statement.add("condition_clause", node.getExpression());
		//sSystem.out.println("Then"+node.getThenStatement());
		if_statement.add("statements",node.getThenStatement());
		//		template.add(if_statement);
		if(node.getElseStatement()!=null){

		}
		System.out.println("Else"+node.getElseStatement());
		//		if_statement.add("else_clause",node.getElseStatement());
		//		node.getElseStatement().accept(new RecordStatements(buffer, template));
		//		node.getThenStatement().accept(new RecordStatements(buffer, template));
		System.err.println("if_statement template\n"+if_statement.render());
		return false;
	}
	public boolean visit(ExpressionStatement node) {
		System.out.println("ExpressionStatement"+node);
		if (node.getParent() instanceof IfStatement){
			IfStatement if_stmt= (IfStatement) node.getParent();
			//			if(if_stmt.getThenStatement().toString().equals(node.toString())){
			System.out.println("instanceof getThenStatement"+node.getParent());
			//			}
			if_stmt.getElseStatement();

		}
		/*
		Expression expr = node.getExpression();
		if (expr instanceof Assignment){
			Assignment e = (Assignment) expr;
			buffer.append(e);
		}
		 */
		//		buffer.append("\t");
		//		buffer.append(node);
		//		ST else_clause = template.in
		//		ST else_clause = template.add("else_clause", node);

		//		template.add("statements", node.getExpression());
		//		node.accept(new RecordIfStatement(buffer));
		return super.visit(node);
	}

	//	public boolean visit(ExpressionStatement node) {
	//		buffer.append("\t");
	//		buffer.append(node);

	//		node.accept(new RecordIfStatement(buffer));
	//		return super.visit(node);
	//	}

	/*
	@Override
	public boolean visit(Block node) {
//		System.err.println(node);
//		buffer.append("{");
//		System.err.println("{");
		List sts = node.statements();
		for(int i=0; i<sts.size();i++){
			if( sts.get(i) instanceof ReturnStatement){
				System.err.println("ReturnStatement");
//				list.add(ReturnStatement.RETURN_STATEMENT);
			}
			else if( sts.get(i) instanceof SingleVariableDeclaration){
				System.err.println("SingleVariableDeclaration");
//				list.add(SingleVariableDeclaration.SINGLE_VARIABLE_DECLARATION);
			}
			else if( sts.get(i) instanceof ExpressionStatement){
				System.err.println("ExpressionStatement"+sts.get(i));
				buffer.append(sts.get(i));
//				list.add(ExpressionStatement.EXPRESSION_STATEMENT);
			}
			else if( sts.get(i) instanceof IfStatement){
//				System.err.println("IfStatement");
				((IfStatement)sts.get(i)).accept(new RecordIfStatement(buffer)); 
//				buffer.append("IfStatement");
//				list.add(IfStatement.IF_STATEMENT);
			}
			else if( sts.get(i) instanceof VariableDeclarationStatement){
				System.err.println("VariableDeclarationStatement"+sts.get(i));
//				System.err.println("VariableDeclarationStatement"+VariableDeclarationStatement.VARIABLE_DECLARATION_STATEMENT);

//				list.add(VariableDeclaration.VARIABLE_DECLARATION_STATEMENT);
			}
			else if( sts.get(i) instanceof VariableDeclarationExpression){
				System.err.println("VariableDeclarationExpression");
//				list.add(VariableDeclaration.VARIABLE_DECLARATION_EXPRESSION);
			}
			else{
				System.err.println("others"+sts.get(i) );
			}

		}
//		System.err.println("}");
//		buffer.append("}");
		return super.visit(node);
	}
	 */

}
