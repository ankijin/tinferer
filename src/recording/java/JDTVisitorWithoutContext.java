package recording.java;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.eclipse.jdt.core.ToolFactory;
import org.eclipse.jdt.core.compiler.IScanner;
import org.eclipse.jdt.core.compiler.ITerminalSymbols;
import org.eclipse.jdt.core.compiler.InvalidInputException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.internal.compiler.parser.Scanner;
import org.eclipse.jdt.internal.formatter.Token;

import alignment.common.OpProperty;
import antlr_parsers.common_old.CommonBaseListener;
import antlr_parsers.common_old.CommonLexer;
import antlr_parsers.common_old.CommonParser;
import antlr_parsers.common_old.CommonParser.AssignContext;
import antlr_parsers.common_old.CommonParser.Ast_nodeContext;
import antlr_parsers.common_old.CommonParser.EqualContext;
import antlr_parsers.common_old.CommonParser.ExprContext;
import javassist.compiler.ast.MethodDecl;
import migration.Main;



//import StructureMapping.OpProperty;

public class JDTVisitorWithoutContext extends ASTVisitor{
	List<String> 					aanodes;
	public List<OpProperty> 		op_a_nodes;
	public List<OpProperty> 		op_a_precoding;
	ASTNode android;
	String left="", right="";
	int depth = 0;
//	String property
	String property="none";
	int property_start=0;
	int property_end=0;
	Map<Interval, String> tokens;
	public String type="none";
	public String opsequence ="";
	
	public static final String[] keywords={"final","return"};
	
	public static final String[] OPERATORS={"=",">","||","."};
	List<TokenInfo> 		token_and 	= new LinkedList<TokenInfo>();
	Vector<TokenInfo> 		token_comma 	= new Vector<TokenInfo>();
	
	public JDTVisitorWithoutContext(ASTNode node){
		String orgin= Main.javaSource;
		System.out.println("orgin.substring"+orgin.substring(node.getStartPosition(), node.getStartPosition()+node.getLength()));
//		node.getRoot()
		aanodes 	= new LinkedList<String>();
		op_a_nodes 	= new LinkedList<OpProperty>();
		op_a_precoding = new LinkedList<OpProperty>(); 
		tokens = new HashMap<Interval, String>();
		node.getStartPosition();
		android = node;
		if(android.toString().contains("=")){
			String[] spt = android.toString().split("=");
			if(spt.length==2){
				left 	= spt[0];
				right 	= spt[1];
			}
		}
		char[] source = node.toString().toCharArray();
//		new ByteArrayInputStream(node.toString().getBytes());
		ANTLRInputStream stream1 = new ANTLRInputStream(orgin.substring(node.getStartPosition(), node.getStartPosition()+node.getLength()));
		Lexer lexer1  = new CommonLexer((CharStream)stream1);
		CommonParser parser1 = new CommonParser(new CommonTokenStream(lexer1));
		Ast_nodeContext root = parser1.ast_node();
		
		ParseTreeWalker walker = new ParseTreeWalker();
	
		CommonBaseListener listener = new CommonBaseListener(){
			int w=0;
			@Override
			public void enterAssign(AssignContext ctx) {
				
				// TODO Auto-generated method stub
				ctx.equal().getSourceInterval();
				ctx.equal().start.getStartIndex();
				ctx.equal().stop.getStopIndex();
				property = "AssignContext";
				type= "AssignContext";
				property_start = ctx.equal().start.getStartIndex()+node.getStartPosition();
				property_end = ctx.equal().stop.getStopIndex()+node.getStartPosition();
				
				System.out.println("property  "+property+"  "+property_start+" "+property_end);
				super.enterAssign(ctx);
			}
			
			@Override
			public void exitExpr(ExprContext ctx) {
				// TODO Auto-generated method stub
				int az = ctx.start.getStartIndex();
				int bz = ctx.stop.getStopIndex();
				Interval interval = new Interval(az,bz);
				String context = root.start.getInputStream().getText(interval);
				type= "ExprContext";
			
				System.out.println("exitExprExprContext"+w+" "+ctx.getText()+"  "+context);
				op_a_precoding.add(new OpProperty(ctx, context, "ExprContext"+w));
				w++;
				
				if(ctx.mul()!=null){
					opsequence +="*";
				}
				if(ctx.div()!=null){
					opsequence +="/";
				}
				if(ctx.plus()!=null){
					opsequence +="+";
				}
				if(ctx.minus()!=null){
					opsequence +="-";
				}

				
				super.exitExpr(ctx);
			}
			/*
			@Override
			public void enterExpr(ExprContext ctx) {
				if(ctx.minus()!=null){
					
				}
				
				int az = ctx.start.getStartIndex();
				int bz = ctx.stop.getStopIndex();
				Interval interval = new Interval(az,bz);
				String context = root.start.getInputStream().getText(interval);
				
				System.out.println("ExprContext"+w+" "+ctx.getText()+"  "+context);
				op_a_precoding.add(new OpProperty(ctx, context, "ExprContext"+w));
				w++;
				// TODO Auto-generated method stub
				super.enterExpr(ctx);
			}
			*/
		};
		
		walker.walk(listener, root);
		
		
//		Lexer lexer1 = new SwiftLexer((CharStream)stream1);
//		SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
//		SwiftParser.Top_levelContext root1 = parser1.top_level();
		
		IScanner scanner = ToolFactory.createScanner(false, true, false, true);
		source = orgin.substring(node.getStartPosition(), node.getStartPosition()+node.getLength()).toCharArray();
		scanner.setSource(source);
	
		
		for (int i = -1; (i = node.toString().indexOf(",", i + 1)) != -1; ) {
			int k =i+node.getStartPosition();
		    System.out.println(node+"	commatest "+  k);
		} // prints "4", "13", "22"

		
		int WS=0;
		try {
			while (true) {
				int t = scanner.getNextToken();
				if(t == ITerminalSymbols.TokenNameEOF){
//					ITerminalSymbols.TokenNameLBRACE
					break;
				}

				int e=scanner.getCurrentTokenEndPosition()+node.getStartPosition();
				int s=scanner.getCurrentTokenStartPosition()+node.getStartPosition();
				int length = e-s+1;
				char[] next = scanner.getCurrentTokenSource();
				String tokenName = new String(next);
//				token_and.add(new TokenInfo(tokenName, s, e, length));
//				token_comma.add(new TokenInfo(tokenName, s, e, length));
				
				if(t == ITerminalSymbols.TokenNameWHITESPACE){
//					System.out.println("TokenNameWHITESPACE");
					WS++;
				}
				else{
					token_and.add(new TokenInfo(tokenName, s-WS, e, length));
					if(t== ITerminalSymbols.TokenNameCOMMA){
						tokens.put(new Interval(s-WS, e), ",");
					}
					WS=0;
				}
				

			
//				if(t== ITerminalSymbols.TokenNameCOMMA){
//					tokens.put(new Interval(s-WS, e), ",");
//					token_comma.add(new TokenInfo(",", ));
//					WS=0;
//				}
//				tokens
			}

		} catch (InvalidInputException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("token_and"+token_and);
		System.out.println("tokens"+tokens);

	}

	/**
	 * protected float mChartHeight=0f;
	 * ASTNodes: protected float, protected, float, mChartHeight=0f, 
	 * Tokens: {protected, float, mChartHeight, =, 0f, ;}
	 * detect '=', position, a, b
	 * tagging: if x<a:leftof= else x>b, rightof = ... 
	 * tagging: adj.. 
	 */
	
	
	
	@Override
	public void preVisit(ASTNode node) {
		/**
		 * preprocessing
		 */
//		System.out.println("(op_a_precoding.indexOf"+op_a_precoding.indexOf(new OpProperty(node, node.toString())));
		System.out.println("preVisit"+node);
		int index=op_a_precoding.indexOf(new OpProperty(node, node.toString()));
		if(index !=-1){
			System.out.println("op_a_precoding____"+op_a_precoding.get(index).property);
			op_a_nodes.add(new OpProperty(node, node.toString(), op_a_precoding.get(index).property));
			return;
		}
		
		//		System.out.println("ASTNode"+node);
		if(node instanceof FieldDeclaration){
			((FieldDeclaration)node).setJavadoc(null);
		}

		if(node instanceof MethodDeclaration){
			((MethodDeclaration)node).setJavadoc(null);
		}

/*
		if(node instanceof MethodInvo resolveMethodBinding();
			aanodes.add(node.toString());
			return;
		}

*/
		
//		op_a_nodes;

		
		
		if(node.toString().equals(android.toString())) return;
				
		//disjoint smartly.. 
		// TODO Auto-generated method stub
		for(OpProperty t:op_a_nodes){
			//					node.d
			
			if(t.strASTNode.contains(node.toString())) {
				System.out.println("android contains"+t.strASTNode);
				return;
			}
		}

		
		
		node.getStartPosition();
		Interval srcItv = new Interval(node.getStartPosition(), node.getStartPosition()+node.getLength()-1);
		System.out.println("srcItv"+srcItv+"  "+node+"    "+tokens);
		TokenInfo info = new TokenInfo(node.toString(), srcItv.a, srcItv.b, srcItv.b-srcItv.a);
		int i =token_and.indexOf(info);
		Iterator<Interval> it = tokens.keySet().iterator();

		while(it.hasNext()){
			Interval iii=it.next();
			if(srcItv.adjacent(iii)){
				op_a_nodes.add(new OpProperty(node, node.toString(), "seperated_comma"));
				System.out.println("adjacent , op_a_nodes "+op_a_nodes);
				return;
			}
		}
		
		
		
		String aaaa = node.toString().replace("\n", "");
		//= left, right, assignment
		if(!node.toString().contains("final")&&!node.toString().contains("=")){
			//			System.out.println("testing"+node.toString());
			//	if(!node.toString().contains("final")&&!node.toString().contains(";")&&!node.toString().contains("=")&&!aanodes.contains(node.toString()) && ! node.toString().equals(android.toString())){
			//				if(!node.toString().equals(android)){
			aanodes.add(node.toString());
			//			if(lleft.contains(node.toString()))
			if(left.contains(node.toString())){
				op_a_nodes.add(new OpProperty(node, node.toString(), "left_assign"));
			}
			else if(right.contains(node.toString())){
				op_a_nodes.add(new OpProperty(node, node.toString(), "right_assign"));
			}
/*
			if(property=="AssignContext"){
				if(node.getStartPosition() < property_start){
					op_a_nodes.add(new OpProperty(node, node.toString(), "left_assign"));
				}
				//end position of astnode
				if(node.getStartPosition()+node.getLength()-1 > property_end){
					op_a_nodes.add(new OpProperty(node, node.toString(), "right_assign"));
				}
				
				
			}
		*/
//			node.getStartPosition();
//			node.getLength();

			
			
			
			else if(node.toString().startsWith("{") && node.toString().endsWith("{")){
//				System.out.println("@@@@"+node.toString()+"  "+aaaa.charAt(aaaa.length()-1));
				op_a_nodes.add(new OpProperty(node, node.toString(), "enclosed{}"));
			}

			else{
				op_a_nodes.add(new OpProperty(node, node.toString()));
			}
		}

		System.out.println("op_a_nodes "+op_a_nodes);
		
		depth++;
		super.preVisit(node);

	}
	
	public class TokenInfo{
		public String token;
//		static final String types[]={"="};
		public int start;
		public int end;
		public int length;
		
		public TokenInfo(String t, int s, int e, int l){
			token =t;
			start=s;
			end=e;
			length=l;
		}
		
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return token+" "+start+":"+end;
		}
		@Override
		public boolean equals(Object obj) {
			// TODO Auto-generated method stub
			TokenInfo target=(TokenInfo)obj;
			return this.start==target.start && this.end == target.end;
//			return super.equals(obj);
		}
	}

	
	public class ASTNodeInfo{
		public ASTNode node;
//		static final String types[]={"="};
		public String nodeProterty;
		
		public ASTNodeInfo(ASTNode n, String p){
			node=n;
			nodeProterty =p;
		}
		
	}
	
}
