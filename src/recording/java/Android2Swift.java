package recording.java;
//import static org.hamcrest.CoreMatchers.instanceOf;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
//import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
import java.util.regex.Pattern;

//import org.antlr.v4.runtime.atn.SemanticContext.Operator;
import org.apache.commons.lang3.StringUtils;
//import org.eclipse.jdt.core.dom.AST;

//import javax.print.attribute.standard.MediaSize.Other;

import org.eclipse.jdt.core.dom.ASTNode;
//import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
//import org.eclipse.jdt.core.dom.ArrayCreation;
import org.eclipse.jdt.core.dom.ArrayType;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.BooleanLiteral;
//import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
//import org.eclipse.jdt.core.dom.EnumConstantDeclaration;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.InfixExpression;
//import org.eclipse.jdt.core.dom.Initializer;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.NullLiteral;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.PostfixExpression;
//import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.QualifiedName;
//import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SimpleName;
//import org.eclipse.jdt.core.dom.SimpleName;
//import org.eclipse.jdt.core.dom.SimpleType;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;
//import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
//import org.springframework.beans.factory.support.ReplaceOverride;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

//import com.github.mikephil.charting.utils.Utils;

import net.vivin.GenericTree;
import net.vivin.GenericTreeNode;
import net.vivin.GenericTreeTraversalOrderEnum;
import recording.MappingElement;
import templates.MatchingTemplate;

public class Android2Swift extends ASTVisitor{

	//	GenericTreeNode<String> parent;
	//	GenericTreeNode<String> astNode;
	STGroup group = new STGroupFile("/Users/kijin/Documents/workspace_mars/GumTreeTester/cross_templates/swift_templates.stg");
	public static String dbName="test_record.db";
	public LinkedList<MappingElement> record = new LinkedList<MappingElement> ();
	private Map<String, String> typeMap = new HashMap<String, String>();
	private Map<String, String> localtypeMap = new HashMap<String, String>();
	public LinkedList<String> recordStr = new LinkedList<String> ();
	public GenericTree<ST> tree = new GenericTree<ST>();
	public Map<Integer,GenericTreeNode<ST>> treeMap = new HashMap<Integer,GenericTreeNode<ST>>();
	public Map<ASTNode,GenericTreeNode<ST>> astMap = new HashMap<ASTNode,GenericTreeNode<ST>>();
	public Android2Swift(){
		typeMap.put("mViewPortHandler", "ViewPortHandler");
		typeMap.put("mTrans", "Transformer");	
	}



	GenericTreeNode<ST> top;

	static String className="";
	@Override
	public boolean visit(TypeDeclaration node) {
		// TODO Auto-generated method stub
		//		System.out.println("TypeDeclaration");
		ST program_t= group.getInstanceOf("program");
		program_t.add("className", node.getName());
		program_t.add("modi", "public");

		className = node.getName().toString();
		top = new GenericTreeNode<ST>(program_t);
		astMap.put(node, top);


		//		program_t.add("log", "@template:"+top+"\n");
		//		Type parent = node.getSuperclassType();
		Migration migration = new Migration();
		//superClass
		String typeA="";
		try {
			if(node.getSuperclassType()!=null){

				migration = getType(node.getSuperclassType().toString());
				ResultSet result= migration.resultSet;
				program_t.add("log", "//"+printResultSet(result));
				migration = getType(node.getSuperclassType().toString());
				result = migration.resultSet;
				while(result.next()){
					typeA = result.getString("classname");
					break;
				}
				program_t.add("superClass", typeA);
				//			System.out.println("node.getSuperclassType()"+node.getSuperclassType());



			}

			List<Type> ifaces = node.superInterfaceTypes();
			for(Type iface :ifaces){
				System.out.println("iface"+iface);
				migration = getType(iface.toString());

				ResultSet result= migration.resultSet;
				//				program_t.add("log", "//"+migration.log);
				program_t.add("log", "//"+printResultSet(result));
				migration = getType(iface.toString());
				result = migration.resultSet;

				while(result.next()){
					typeA = result.getString("classname");
					break;
				}
				System.out.println("iface"+iface+" "+typeA);
				program_t.add("superClass", iface.toString());
			}


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return super.visit(node);
	}
	//float[] floatArray;
	final public static String printResultSet(ResultSet rs) throws SQLException
	{
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		StringBuffer buffer = new StringBuffer();
		while (rs.next()) {
			for (int i = 1; i <= columnsNumber; i++) {
				if (i > 1) {System.out.print(" | "); buffer.append(" | ");}
				System.out.print(rs.getString(i));buffer.append(rs.getString(i));
			}
			System.out.println("");
			buffer.append(", ");
		}
		return buffer.toString();
	}


	public Migration getType(String type) throws SQLException{
		Connection connection;
		Statement stmt;
		ResultSet resultSet = null, resultSet1=null;
		String init = null;
		String modifier = null;
		Migration migration = new Migration();
		//		Map<String, String> result = new Hash
		try {
			connection  = DriverManager.getConnection("jdbc:sqlite:"+dbName);
			connection.setAutoCommit(true);
			stmt 		= connection.createStatement();


			/** resultSet
			 *  CGFloat(1.0) | 4
				Int(-1) | 2
			 */
			String sql = "select var_swift.classname, count(var_swift.classname) from var_android, var_swift "
					+ "where var_swift.android=var_android.id and var_android.classname"+"=\'"+type+"\'"
					+" group by var_swift.classname "+"order by count(var_swift.classname) desc";
			//			var_decls_data.add("log", sql);
			resultSet = stmt.executeQuery(sql);
			migration.resultSet = resultSet;
			stmt 		= connection.createStatement();
			resultSet1 = stmt.executeQuery(sql);
			//			migration.log =printResultSet(resultSet1);
			//			migration.log = sql;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}




		return migration;

	}

	public Migration getReturnType(String type) throws SQLException{
		Connection connection;
		Statement stmt;
		ResultSet resultSet = null, resultSet1=null;
		String init = null;
		String modifier = null;
		Migration migration = new Migration();
		//		Map<String, String> result = new Hash
		try {
			connection  = DriverManager.getConnection("jdbc:sqlite:"+dbName);
			connection.setAutoCommit(true);
			stmt 		= connection.createStatement();


			/** resultSet
			 *  CGFloat(1.0) | 4
				Int(-1) | 2
			 */
			String sql = "select var_swift.type, count(var_swift.type) from var_android, var_swift "
					+ "where var_swift.type is not NULL and var_swift.type is not 'var' and var_swift.android=var_android.id and var_android.type"+"=\'"+type+"\'"
					+"group by var_swift.type "+"order by count(var_swift.type) desc";

			//			var_decls_data.add("log", sql);
			resultSet = stmt.executeQuery(sql);
			migration.resultSet = resultSet;
			stmt 		= connection.createStatement();
			resultSet1 = stmt.executeQuery(sql);
			migration.log =printResultSet(resultSet1);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}




		return migration;

	}


	public Migration getModifier(String initial) throws SQLException{
		Connection connection;
		Statement stmt;
		ResultSet resultSet = null, resultSet1=null;
		String init = null;
		String modifier = null;
		Migration migration = new Migration();
		//		Map<String, String> result = new Hash
		try {
			connection  = DriverManager.getConnection("jdbc:sqlite:"+dbName);
			connection.setAutoCommit(true);
			stmt 		= connection.createStatement();


			/** resultSet
			 *  CGFloat(1.0) | 4
				Int(-1) | 2
			 */
			String sql = "select var_swift.modifier, count(var_swift.modifier) from var_android, var_swift where var_android.id = var_swift.android and "
					+ "var_android.modifier=\'"+initial
					+ "\' and var_swift.modifier is not \'\'"
					+"group by var_swift.modifier order by count(var_swift.modifier) desc";

			//			var_decls_data.add("log", sql);
			System.out.println(sql);
			resultSet = stmt.executeQuery(sql);
			migration.resultSet = resultSet;
			stmt 		= connection.createStatement();
			resultSet1 = stmt.executeQuery(sql);
			migration.log =printResultSet(resultSet1);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}




		return migration;

	}


	public Migration getInit(String initial) throws SQLException{
		Connection connection;
		Statement stmt;
		ResultSet resultSet = null, resultSet1=null;
		String init = null;
		String modifier = null;
		Migration migration = new Migration();
		//		Map<String, String> result = new Hash
		try {
			connection  = DriverManager.getConnection("jdbc:sqlite:"+dbName);
			connection.setAutoCommit(true);
			stmt 		= connection.createStatement();

			/** resultSet
			 *  CGFloat(1.0) | 4
				Int(-1) | 2
			 */
			String sql = "select var_swift.init, count(var_swift.init) from var_android, var_swift "
					+ "where var_swift.android=var_android.id and var_android.init"+"=\'"+initial+"\'"
					+"group by var_swift.init "+"order by count(var_swift.init) desc";

			//			var_decls_data.add("log", sql);
			resultSet = stmt.executeQuery(sql);
			migration.resultSet = resultSet;
			stmt 		= connection.createStatement();
			resultSet1 = stmt.executeQuery(sql);
			migration.log =printResultSet(resultSet1);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}




		return migration;

	}

	@Override
	public boolean visit(FieldDeclaration node) {
		// TODO Auto-generated method stub
		//		System.out.println("FieldDeclaration"+node);

		GenericTreeNode<ST> var_decls = new GenericTreeNode<ST>(group.getInstanceOf("var_decls"));
		ST var_decls_data = group.getInstanceOf("var_decls");	
		//		var_decls_data.add("log", node.toString());
		Vector<GenericTreeNode<ST>> patterns = new Vector<GenericTreeNode<ST>>();
		String modifier="";


		Migration migration1 = new Migration();
		//superClass
		String typeAA="";
		if(node.getType()!=null){
			try {
				migration1 = getType(node.getType().toString());
				ResultSet result= migration1.resultSet;
				var_decls_data.add("log", migration1.log);
				System.err.println("migration1.log"+migration1.log);
				//				log += migration1.log;
				while(result.next()){
					typeAA = result.getString("classname");
					break;
				}


				if(typeAA.equals("")){

					migration1 = getReturnType(node.getType().toString());
					result= migration1.resultSet;

					while(result.next()){
						typeAA = result.getString("type");
						break;
					}

				}


			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


		}



		if(node.modifiers().size()>0){
			Migration migration = new Migration();
			try {
				migration = getModifier(node.modifiers().get(0).toString());
				while(migration.resultSet.next()){
					modifier = migration.resultSet.getString("modifier");
					break;
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			var_decls_data.add("modifiers", modifier);
		}

		List<VariableDeclarationFragment> fragments = node.fragments();
		for(VariableDeclarationFragment varr:fragments){

			IVariableBinding binding = varr.resolveBinding();

			//			System.out.println("binding variable declaration: " +binding.getVariableDeclaration());
			//			System.out.println("binding: " +binding);

			typeMap.put(varr.getName().toString(), node.getType().toString());
			GenericTreeNode<ST> pattern = new GenericTreeNode<ST>(group.getInstanceOf("pattern"));
			ST patternData = pattern.getData();
			patternData.add("name", varr.getName());

			//			if(!typeAA.equals("")){
			//				patternData.add("type", typeAA);
			//			}

			//			if(varr.getInitializer() instanceof NumberLiteral){
			//				patternData.add("expression", varr.getInitializer().toString().replaceAll("f", ""));
			//			}
			//			else 
			if(varr.getInitializer()!=null){
				Migration migration = new Migration();
				try {
					migration = getInit(varr.getInitializer().toString());
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				ResultSet result = migration.resultSet;
				//			String r = printResultSet(result);
				var_decls_data.add("log", migration.log);
				//				int i=0;
				String initVar ="";
				try {
					while(result.next()){
						initVar = result.getString("init");
						//						System.out.println("resultSet "+i+" "+result.getString("init"));
						//						i++;
						break;
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if(!initVar.equals(""))
					patternData.add("expression", initVar);
			}
			if(node.getType().isArrayType()){
				ArrayType arr= (ArrayType)node.getType();
				arr.getElementType();
				patternData.add("type", "["+typeAA+"]");
			}else{
				//				patternData.add("type", node.getType());
			}
			pattern.setData(patternData);
			patterns.add(pattern);
		}




		GenericTreeNode<ST> parent = astMap.get(node.getParent());
		var_decls.setData(var_decls_data);
		for(GenericTreeNode<ST>p:patterns){
			var_decls.addChild(p);
		}
		parent.addChild(var_decls);

		astMap.put(node, var_decls);
		//		treeMap.put(ASTNode.FIELD_DECLARATION, childB);

		return false;
	}


	@Override
	public boolean visit(MethodInvocation node) {
		// TODO Auto-generated method stub
		//		Expression
		return super.visit(node);
	}



	@Override
	public boolean visit(MethodDeclaration node) {

		//		if(node instanceof Constructor)
		GenericTreeNode<ST> func_decls_t = new GenericTreeNode<ST>(group.getInstanceOf("func_decls"));
		ST func_t = func_decls_t.getData();

		//modifier
		if(node.modifiers().size()>0){
			String modifier ="";
			Migration migration = new Migration();
			try {
				migration = getModifier(node.modifiers().get(0).toString());
				while(migration.resultSet.next()){
					modifier = migration.resultSet.getString("modifier");
					break;
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if(!modifier.equals(""))
				func_t.add("modifiers", modifier);
		}


		//return type: Array or 
		if(node.getReturnType2()!=null){
			if(node.getReturnType2().toString().equals("void")){

			}
			else{
				try {
					Migration migration = new Migration();
					if(node.getReturnType2().isArrayType()){
						ArrayType arr= (ArrayType)node.getReturnType2();
						arr.getElementType();
						migration = getReturnType(arr.getElementType().toString());
						ResultSet result= migration.resultSet;
						//				program_t.add("log", "//"+migration.log);
						int i=0;
						String typeA="";
						while(result.next()){
							typeA = result.getString("type");
							break;
						} //select first ranked

						if(typeA.equals("")){
							migration = getType(arr.getElementType().toString());
							result= migration.resultSet;
							//				program_t.add("log", "//"+migration.log);
							while(result.next()){
								typeA = result.getString("classname");
								break;
							} //select first ranked

						}
						if(typeA.equals("")){
							func_t.add("type", "["+arr.getElementType().toString()+"]");
						}else{
							func_t.add("type", "["+typeA+"]");
						}
					}else{


						migration = getReturnType(node.getReturnType2().toString());
						ResultSet result= migration.resultSet;
						//				program_t.add("log", "//"+migration.log);
						int i=0;
						String typeA="";
						while(result.next()){
							typeA = result.getString("type");
							break;
						} 

						if(typeA.equals("")){
							migration = getType(node.getReturnType2().toString());
							result= migration.resultSet;
							//				program_t.add("log", "//"+migration.log);
							while(result.next()){
								typeA = result.getString("classname");
								break;
							} //select first ranked

						}
						if(!typeA.equals(""))
							func_t.add("type", typeA);
					}



				} catch (SQLException e) {e.printStackTrace();}

			}





		}

		if(className.equals(node.getName().toString())){
			func_t.add("name", "init");
		}else{
			func_t.add("name", node.getName());
			func_t.add("isfunc", "isfunc");
		}
		func_decls_t.setData(func_t);

		//parameterlist, var_decl
		Vector<GenericTreeNode<ST>> params_v = new Vector<GenericTreeNode<ST>>();
		List<SingleVariableDeclaration> params = node.parameters();
		for(SingleVariableDeclaration varr:params){		
			localtypeMap.put(varr.getName().toString(), varr.getType().toString());
			GenericTreeNode<ST> params_t = new GenericTreeNode<ST>(group.getInstanceOf("params"));
			ST pr = params_t.getData();
			pr.add("name", varr.getName());
			String typeA="";
			if(varr.getType().isArrayType()){
				ArrayType arr= (ArrayType)varr.getType();
				arr.getElementType();
				pr.add("type", "["+arr.getElementType()+"]");
			}else{

				Migration migration;
				try {
					migration = getReturnType(varr.getType().toString());
					ResultSet result= migration.resultSet;
					//				program_t.add("log", "//"+migration.log);
					//					int i=0;

					while(result.next()){
						typeA = result.getString("type");
						break;
					}

					if(typeA.equals("")){
						migration= getType(varr.getType().toString());

						result= migration.resultSet;
						//				program_t.add("log", "//"+migration.log);
						//					int i=0;

						while(result.next()){
							typeA = result.getString("classname");
							break;
						}
						if(typeA.equals("")){
							pr.add("type", varr.getType());
						} else{
							pr.add("type", typeA);
							func_t.add("log",varr.getType()+" to "+typeA+" ");
						}

					}else{
						func_t.add("log",varr.getType()+" to "+typeA+" ");
						pr.add("type", typeA);
					}


				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				localtypeMap.put( varr.getName().toString(), varr.getType().toString());
			}
			params_t.setData(pr);
			params_v.add(params_t);
		}

		for(GenericTreeNode<ST> st:params_v){
			func_decls_t.addChild(st);
		}
		/**
		 * parent: programs
		 */
		GenericTreeNode<ST> parent = astMap.get(node.getParent());
		parent.addChild(func_decls_t);
		astMap.put(node, func_decls_t);


		return super.visit(node);
	}

	//	@Override
	//	public boolean visit(Initializer node) {
	// TODO Auto-generated method stub
	//		System.out.println("Initializer"+node);
	//		return false;
	//	}
	@Override
	public boolean visit(Block node) {
		// TODO Auto-generated method stub
		//		if(node.getParent() instanceof EnumDeclaration ==false){
		//		System.out.println("BBlock"+node+",,"+astMap.get(node.getParent())+node.getParent().getNodeType());
		GenericTreeNode<ST> block_t = new GenericTreeNode<ST>(group.getInstanceOf("block"));
		GenericTreeNode<ST> stmts_t = new GenericTreeNode<ST>(group.getInstanceOf("statements"));
		block_t.addChild(stmts_t);
		GenericTreeNode<ST> parent = astMap.get(node.getParent());
		if(parent !=null)
			parent.addChild(block_t);
		astMap.put(node, block_t);
		//		}
		return super.visit(node);
	}

	@Override
	public boolean visit(VariableDeclarationExpression node) {
		// TODO Auto-generated method stub
		//		System.out.println("VariableDeclarationExpression"+node.getParent());
		//		for(int i=0, j=0; j<10;j++){
		//
		//		}
		//		= node.fragments();
		return super.visit(node);
	}

	@Override
	public boolean visit(ForStatement node) {
		// TODO Auto-generated method stub

		GenericTreeNode<ST> statements = new GenericTreeNode<ST>(group.getInstanceOf("statements"));
		GenericTreeNode<ST> for_statement = new GenericTreeNode<ST>(group.getInstanceOf("for_statement"));
		statements.addChild(for_statement);
		ST temp = for_statement.getData();


		System.err.println(node.initializers());
		System.err.println(node.updaters());
		System.err.println(node.getExpression());
		List<VariableDeclarationExpression> inits = node.initializers();
		Vector<GenericTreeNode<ST>> vv = new Vector<GenericTreeNode<ST>>();
		for(VariableDeclarationExpression e:inits){
			e.getType();
			List<VariableDeclarationFragment> frags = e.fragments();
			for(VariableDeclarationFragment f:frags){
				f.getName();
				f.getInitializer();
				GenericTreeNode<ST> pattern = new GenericTreeNode<ST>(group.getInstanceOf("pattern"));

				ST data = pattern.getData();
				data.add("name", f.getName());
				data.add("type", e.getType());
				data.add("expression", f.getInitializer());
				pattern.setData(data);
				vv.add(pattern);

			}

		}
		//		List<PostfixExpression> up = node.updaters();
		//i++
		//		for(PostfixExpression p:up){
		//			temp.add("updater",p.toString()); 
		//		}

		List<Expression> up = node.updaters();
		//		i++
		for(Expression p:up){
			temp.add("updater",p.toString()); 
		}
		temp.add("expression", node.getExpression());

		//		temp.add("log", "testing for");
		//		temp.add("expression", newexpr);
		for_statement.setData(temp);

		for(GenericTreeNode<ST>p:vv){
			for_statement.addChild(p);
		}
		GenericTreeNode<ST> parent = astMap.get(node.getParent());
		parent.addChild(statements);

		astMap.put(node, for_statement);

		return super.visit(node);
	}


	public Migration getMigrationFromExpression(Expression expr){

		//		ASTVisitor exprvisitor = new ASTVisitor();


		//		expr.accept(visitor);

		Migration migration = new Migration();
		String typeA="";
		if(expr instanceof SimpleName){
			try {
				migration = getType(expr.resolveTypeBinding().toString());
				//				node.getExpression().resolveTypeBinding();
				//				System.out.println("SimpleName	"+expr+migration.migratedExpr+"..."+getGeneral(expr.toString())+"   "+node.getExpression().resolveTypeBinding());


				migration = getType(expr.resolveTypeBinding().toString());
				ResultSet result= migration.resultSet;
				//				program_t.add("log", "//"+migration.log);
				String log1 = printResultSet(result);
				migration = getType(expr.resolveTypeBinding().toString());
				migration.log +=" "+log1; 
				result = migration.resultSet;
				while(result.next()){
					typeA = result.getString("classname");
					break;
				}

				migration.migratedExpr = expr.toString();
				migration.log = typeA+" "+migration.migratedExpr;

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			migration = getMigrationExpr(expr.toString(), "");
		}
		return migration;
	}


	@Override
	public boolean visit(ReturnStatement node) {
		// TODO Auto-generated method stub
		//		System.out.println("~~~~ReturnStatement"+node);

		GenericTreeNode<ST> statements = new GenericTreeNode<ST>(group.getInstanceOf("statements"));
		GenericTreeNode<ST> return_stmt_t = new GenericTreeNode<ST>(group.getInstanceOf("return_stmt"));
		statements.addChild(return_stmt_t);
		ST temp = return_stmt_t.getData();


		//		System.err.println("ReturnStatement\n"+expr);
		String log="@sql ";
		String expr ="";
		if (node.getExpression() !=null){
			expr = node.getExpression().toString();
			Migration migration = getMigrationFrom(node.getExpression(), log);

			String newexpr 		= migration.migratedExpr;

			if(migration!=null){
				temp.add("log", migration.log);
				temp.add("expression", newexpr);
			}
		}

		/*
		Migration migration = new Migration();
		String typeA="";

		if(node.getExpression() instanceof SimpleName){
			try {
				migration = getType(node.getExpression().resolveTypeBinding().toString());
				node.getExpression().resolveTypeBinding();
				System.out.println("SimpleName	"+expr+migration.migratedExpr+"..."+getGeneral(expr.toString())+"   "+node.getExpression().resolveTypeBinding());


				migration = getType(node.getExpression().resolveTypeBinding().toString());
				ResultSet result= migration.resultSet;
				//				program_t.add("log", "//"+migration.log);
				temp.add("log", "//"+printResultSet(result));
				migration = getType(node.getExpression().resolveTypeBinding().toString());
				result = migration.resultSet;
				while(result.next()){
					typeA = result.getString("classname");
					break;
				}

				migration.migratedExpr = node.getExpression().toString();
				migration.log = typeA+" "+migration.migratedExpr;

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			migration = getMigrationExpr(expr.toString(), log);
		}
		 */

		return_stmt_t.setData(temp);

		GenericTreeNode<ST> parent = astMap.get(node.getParent());
		parent.addChild(statements);

		astMap.put(node, return_stmt_t);

		return super.visit(node);
	}

	public String getGeneral(String expr){
		Set<String> varnames = typeMap.keySet();
		Iterator<String> it = varnames.iterator();
		//		System.out.println("typeMap"+typeMap);
		while(it.hasNext()){
			String var_name = it.next();
			//			expr= expr.replaceAll("\\b("+var_name+")\\b","\\${"+typeMap.get(var_name)+"}");
			expr= expr.replaceAll("\\b("+var_name+")\\b",typeMap.get(var_name));

			//			expr=expr.replace(var_name, "${"+typeMap.get(var_name)+"}");
		}

		Set<String> localvarnames = localtypeMap.keySet();
		Iterator<String> it_local = localvarnames.iterator();

		while(it_local.hasNext()){
			String local_var_name = it_local.next();
			//			expr = "+"+expr;

			//			expr= expr.replaceAll("\\b("+Pattern.quote(local_var_name) +")\\b","\\${"+localtypeMap.get(local_var_name)+"}");

			expr= expr.replaceAll("\\b("+Pattern.quote(local_var_name) +")\\b",localtypeMap.get(local_var_name));
			//			expr= expr.replaceAll("\\b("+local_var_name+")\\b","\\${"+localtypeMap.get(local_var_name)+"}");
			//			expr=expr.replace(local_var_name, "${"+localtypeMap.get(local_var_name)+"}");
			//			System.out.println("localtypeMap"+localtypeMap+expr+".."+localtypeMap.get(local_var_name)+".."+expr);
			//			break;
		}

		return expr;
	}
	/*
	static String makeTemplateAndroid(String expr){
		Iterator<String> androidVars = Main.typeMapAndroid.keySet().iterator();
		while(androidVars.hasNext()){
			String varname 	= androidVars.next();
			String type 	= Main.typeMapAndroid.get(varname);
			if(type!=null && expr!=null){
				expr= expr.replaceAll("\\b("+varname+")\\b","\\${"+type+"}");
			}


		}

		Iterator<String> androidVarsLocal = stmtsMap.localtypeMapAndroid.keySet().iterator();

		while(androidVarsLocal.hasNext() && expr!=null){
			expr = expr.replace("this", Main.androidClassName);
			String varname 	= androidVarsLocal.next();
			System.out.println("localvar"+varname);
			String type 	= stmtsMap.localtypeMapAndroid.get(varname);
			if(type!=null)
				expr= expr.replaceAll("\\b("+varname+")\\b","\\${"+type+"}");
			//			if(expr.contains(varname)){
			//			if(expr.contains(varname) && (expr.contains("."+varname) || expr.contains(varname+"."))){
			//				expr = expr.replace(varname, "${"+type+"}");
			//			}

		}
		return expr;
	}
	 */

	@Override
	public boolean visit(WhileStatement node) {
		// TODO Auto-generated method stub

		GenericTreeNode<ST> statements = new GenericTreeNode<ST>(group.getInstanceOf("statements"));
		GenericTreeNode<ST> while_statement = new GenericTreeNode<ST>(group.getInstanceOf("while_statement"));
		statements.addChild(while_statement);
		ST while_t = while_statement.getData();
		while_t.add("expression", node.getExpression().toString());		


		GenericTreeNode<ST> parent = astMap.get(node.getParent());

		parent.addChild(statements);

		astMap.put(node, while_statement);

		//		node.getExpression();
		//		node.getBody();

		return super.visit(node);
	}


	@Override
	public boolean visit(IfStatement node) {
		// TODO Auto-generated method stub
		GenericTreeNode<ST> parent = astMap.get(node.getParent());
		System.out.println("IfStatement"+node.getParent()+", "+parent);

		GenericTreeNode<ST> statements = new GenericTreeNode<ST>(group.getInstanceOf("statements"));
		GenericTreeNode<ST> if_stmt = new GenericTreeNode<ST>(group.getInstanceOf("if_statement"));

		ST if_stmt_data = if_stmt.getData();

		String expr = node.getExpression().toString();
		expr = expr.replaceAll(" ", "");
		String log="@sql ";

		//		Migration migration = getMigrationExpr2(expr.toString().replace(" ", ""), log);
		Migration migration = getMigrationFrom(node.getExpression(), "");
		System.out.println("migration"+migration.migratedExpr+" :::"+expr+"	"+migration.log);
		//		if_stmt_data.add("log", migration.log);
		//		if_stmt_data.add("expression", expr.toString());

		if_stmt_data.add("expression", migration.migratedExpr);
		if_stmt.setData(if_stmt_data);
		statements.addChild(if_stmt);

		if(node.getParent() instanceof IfStatement){
			parent.addChild(if_stmt);
		}else{
			parent.addChild(statements);
		}
		astMap.put(node, if_stmt);

		if(node.getThenStatement()!=null){
			//goto visit(Block)
			node.getThenStatement().accept(this);
		}
		if(node.getElseStatement()!=null){
			//goto visit(Block)
			GenericTreeNode<ST> else_clause = new GenericTreeNode<ST>(group.getInstanceOf("else_clause"));
			if_stmt.addChild(else_clause);
			astMap.put(node, else_clause);
			node.getElseStatement().accept(this);
		}
		return false;
	}

	//post order evaluation
	@Override
	public void endVisit(CompilationUnit node) {
		// TODO Auto-generated method stub
		tree.setRoot(top);
		for(GenericTreeNode<ST> tt1:tree.build(GenericTreeTraversalOrderEnum.POST_ORDER)){
			System.err.println(tt1);

			ST data = tt1.getData();
			GenericTreeNode<ST> p= tt1.getParent();

			//post-order traversal and evaluate property value
			if(p!=null){
				ST pdata = p.getData();
				Map<String, Object> att = pdata.getAttributes();
				System.out.println("\n"+att+"->"+data.getName().replace("/", "")+","+data.render());
				//		System.err.println(data.getName().replace("/", "")+","+data.render());
				if(att.containsKey(data.getName().replace("/", ""))){
					pdata.add(data.getName().replace("/", ""), data.render());
				}
			}
		}

		//final migration result
		String result = top.getData().render();
		System.out.println("finaloutput\n"+result);
		try {
			//			PrintWriter out = new PrintWriter("filename.txt");
			FileWriter fw = new FileWriter("migrated_"+RecordingTestMain.classfileaName+".swift");
			fw.write(result);
			fw.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		super.endVisit(node);
	}
	//	visit


	@Override
	public boolean visit(ExpressionStatement node) {
		// TODO Auto-generated method stub
		Connection connection;
		Statement stmt;
		ResultSet resultSet = null;

		Migration result = null;
		String expr_temp = null, android_vars = null, swift_vars = null;
		String log ="@sql ";


		Expression expr = node.getExpression();


		if(expr instanceof Assignment){
			GenericTreeNode<ST> assignment_stmt_t = new GenericTreeNode<ST>(group.getInstanceOf("assign"));

			ST temp = assignment_stmt_t.getData();

			Assignment assign = (Assignment)expr;
			System.out.println("Assignment"+assign);
//			String name = assign.getLeftHandSide().toString();
			Migration mleft= getMigrationFrom(assign.getLeftHandSide(),"");
			Migration mright= getMigrationFrom(assign.getRightHandSide(),"");
//			String leftT= assign.getLeftHandSide().resolveTypeBinding().toString();
			//			Migration migration = getMigrationExpr(assign.getRightHandSide().toString(), log);

			temp.add("log", mleft.log+mright.log);
			temp.add("left", mleft.migratedExpr);	

			temp.add("right", mright.migratedExpr);
			assignment_stmt_t.setData(temp);

			GenericTreeNode<ST> parent = astMap.get(node.getParent());

			GenericTreeNode<ST> statements 			= new GenericTreeNode<ST>(group.getInstanceOf("statements"));
			GenericTreeNode<ST> expression_stmt_t 	= new GenericTreeNode<ST>(group.getInstanceOf("expression_stmt"));
			GenericTreeNode<ST> expression_t 		= new GenericTreeNode<ST>(group.getInstanceOf("expression"));

			statements.addChild(expression_stmt_t);
			expression_stmt_t.addChild(expression_t);
			expression_t.addChild(assignment_stmt_t);
			parent.addChild(statements);
			astMap.put(node, assignment_stmt_t);

		} //Assignment Expression left=right

		else{
			/*
			String mbinding="";
			MethodInvocation mmmm = (MethodInvocation)expr;

			IMethodBinding m11=null;
			if(mmmm.resolveMethodBinding()!=null)
				m11= mmmm.resolveMethodBinding().getMethodDeclaration();

			if(m11!=null){
				//					System.out.println("aaaandroidExpr"+mmmm+"binding"+m11);
				mbinding=m11.getDeclaringClass().getName()+".";
				mbinding+=m11.getName()+"(";
				ITypeBinding[] pt = m11.getParameterTypes();
				StringUtils.join(pt, ", ");
				ArrayList<String> types = new ArrayList<String>();
				for(ITypeBinding b:pt){
					types.add(b.getName());
				}
				mbinding+= StringUtils.join(types, ", ");
				mbinding+=")";
				System.out.println("mbinding"+mbinding);
			 */

			GenericTreeNode<ST> statements 			= new GenericTreeNode<ST>(group.getInstanceOf("statements"));
			GenericTreeNode<ST> expression_stmt_t 	= new GenericTreeNode<ST>(group.getInstanceOf("expression_stmt"));
			//			GenericTreeNode<ST> expression_t 		= new GenericTreeNode<ST>(group.getInstanceOf("expression"));
			ST temp1 = expression_stmt_t.getData();

			String logg = "//@was "+expr.toString();

			Migration migration = getMigrationFrom(expr, logg);
			//			System.out.println("migratedExpr"+migration.migratedExpr);
			temp1.add("expression", migration.migratedExpr);
			temp1.add("log", "//@migration");

			expression_stmt_t.setData(temp1);	
			statements.addChild(expression_stmt_t);
			//			expression_stmt_t.addChild(expression_t);

			GenericTreeNode<ST> parent = astMap.get(node.getParent());
			parent.addChild(statements);
			astMap.put(node, expression_stmt_t);


		}


		return super.visit(node);
	}

	/*
	@Override
	public boolean visit(InfixExpression node) {
		// TODO Auto-generated method stub
		System.out.println("InfixExpression"+node.getParent());
		GenericTreeNode<ST> infix_t = new GenericTreeNode<ST>(group.getInstanceOf("expression"));
		ST data = infix_t.getData();
		data.add("expr", "InfixTest");

		infix_t.setData(data);

		GenericTreeNode<ST> parent = astMap.get(node.getParent());
		//		var_decls.setData(var_decls_data);
		//		for(GenericTreeNode<ST>p:patterns){
		//			var_decls.addChild(p);
		//		}
		parent.addChild(infix_t);
		astMap.put(node, infix_t);

		return super.visit(node);
	}
	 */

	@Override
	public boolean visit(EnumDeclaration node) {
		// TODO Auto-generated method stub
		System.out.println("EnumDeclaration"+node);
		GenericTreeNode<ST> enum_decls = new GenericTreeNode<ST>(group.getInstanceOf("enum_decls"));
		ST enum_decls_data = group.getInstanceOf("enum_decls");	
		enum_decls_data.add("body", node.toString());
		enum_decls.setData(enum_decls_data);
		GenericTreeNode<ST> parent = astMap.get(node.getParent());
		parent.addChild(enum_decls);
		astMap.put(node, enum_decls);

		return super.visit(node);
	}

	//	VariableDeclaration
	@Override
	public boolean visit(VariableDeclarationStatement node) {
		// TODO Auto-generated method stub
		//		System.out.println("VariableDeclarationStatement"+node.toString());
		GenericTreeNode<ST> const_decl_stmt_t = new GenericTreeNode<ST>(group.getInstanceOf("const_decl_stmt"));
		GenericTreeNode<ST> statements = new GenericTreeNode<ST>(group.getInstanceOf("statements"));
		statements.addChild(const_decl_stmt_t);

		ST temp = const_decl_stmt_t.getData();
		// TODO multiple var declaration:float a = 1f, b=2f;
		VariableDeclarationFragment frag = (VariableDeclarationFragment) node.fragments().get(0);	


		List<VariableDeclarationFragment> frags = node.fragments();
		for(VariableDeclarationFragment f:frags){

			//			f.getName();
			//			node.getType().toString();
			localtypeMap.put(f.getName().toString(), node.getType().toString());

		}


		String expr="";
		if(frag.getInitializer()!=null){
			temp.add("name", frag.getName());
			System.out.println("nameresolveBinding"+frag.getName().resolveBinding());
			if(frag.getInitializer() instanceof NumberLiteral){
				temp.add("expression", frag.getInitializer().toString().replaceAll("f", ""));

			}else{
				expr = frag.getInitializer().toString();

				String log="@sql ";
				Migration migration = getMigrationExpr(expr.toString(), log);
				temp.add("log", migration.log);

				if(!expr.equals("")){
					temp.add("expression", migration.migratedExpr);
				}else{
					temp.add("expression", expr.toString());
				}
			}

		}else{
			//			temp.add("expression", expr.toString());
			temp.add("name", frag.getName());
		}

		const_decl_stmt_t.setData(temp);
		GenericTreeNode<ST> parent = astMap.get(node.getParent());
		parent.addChild(statements);

		return false;
	}


	//struct storing and returing log data and migrationExpr
	class Migration{
		String migratedExpr;
		String log;
		ResultSet resultSet;

		public Migration(){
			migratedExpr="";
			log="";
			//			resultSet = new ResultSet();
		}
		public Migration(String m, String l){
			migratedExpr = m;
			log = l;
		}
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return migratedExpr;
		}
	}



	Migration getEExpr(Expression android, String log){


		GenericTree<String> tree = new GenericTree<String>();
		//		GenericTree<String> tree1 = new GenericTree<String>();

		final GenericTreeNode<String>  rootA = new GenericTreeNode<String>("");
		Map<ASTNode, GenericTreeNode<String>> map = new HashMap<ASTNode, GenericTreeNode<String>>();


		//		map.put(android, rootNode);

		System.out.println("NODETYPE"+android.getNodeType());
		//27
		System.out.println(Expression.INFIX_EXPRESSION);

		//32
		System.out.println(Expression.METHOD_INVOCATION);
		//38
		System.out.println(Expression.PREFIX_EXPRESSION);

		//42
		System.out.println(Expression.SIMPLE_NAME);
		//		System.out.println(Exp);
		//		GenericTreeNode<String> rootNodea;
		Map<String, String> mapp = new HashMap<String, String>();
		mapp.put("mViewPortHandler.contentWidth() > 10 && !mViewPortHandler.isFullyZoomedOutX()", 
				"MMMMmViewPortHandler.contentWidth() > 10 && !mViewPortHandler.isFullyZoomedOutX()");

		mapp.put("mViewPortHandler.contentWidth()", 
				"MMMMmViewPortHandler.contentWidth()");
		//		new ASvi

		Migration migration = new Migration();
		if(android instanceof SimpleName){
			migration.migratedExpr = android.toString();
			System.err.println("android.resolveTypeBinding()"+android.resolveTypeBinding());
		}  //copy name and provide type of swift
		else if(android instanceof MethodInvocation){
			MethodInvocation m	= (MethodInvocation) android;
			m.arguments();
		} //replace method name;
		/*
		android.accept(new ASTVisitor() {
			@Override
			public boolean preVisit2(ASTNode node) {
				// TODO Auto-generated method stub
				if(node instanceof SimpleName){

				}else if(node instanceof MethodInvocation){

				}


				return super.preVisit2(node);
			}
		});
		 */

		//		System.out.println("After"+tree.build(GenericTreeTraversalOrderEnum.POST_ORDER));
		System.out.println("After"+android);
		return new Migration();
	}



	Migration getExpr(Expression android, String log){
		GenericTree<String> tree = new GenericTree<String>();
		//		GenericTree<String> tree1 = new GenericTree<String>();

		final GenericTreeNode<String>  rootA = new GenericTreeNode<String>("");
		Map<ASTNode, GenericTreeNode<String>> map = new HashMap<ASTNode, GenericTreeNode<String>>();


		//		map.put(android, rootNode);

		System.out.println("NODETYPE"+android.getNodeType());
		//27
		System.out.println(Expression.INFIX_EXPRESSION);

		//32
		System.out.println(Expression.METHOD_INVOCATION);
		//38
		System.out.println(Expression.PREFIX_EXPRESSION);

		//42
		System.out.println(Expression.SIMPLE_NAME);
		//		System.out.println(Exp);
		//		GenericTreeNode<String> rootNodea;
		Map<String, String> mapp = new HashMap<String, String>();
		mapp.put("mViewPortHandler.contentWidth() > 10 && !mViewPortHandler.isFullyZoomedOutX()", 
				"MMMMmViewPortHandler.contentWidth() > 10 && !mViewPortHandler.isFullyZoomedOutX()");

		mapp.put("mViewPortHandler.contentWidth()", 
				"MMMMmViewPortHandler.contentWidth()");
		//		new ASvi
		android.accept(new ASTVisitor() {
			GenericTreeNode<String> rootNode;


			@Override
			public boolean visit(MethodInvocation node) {
				// TODO Auto-generated method stub
				node.getName();
				node.getExpression();
				//				node.getAST().set
				//				Expression e = (Expression) node;

				//				rootA = rootNode;
				System.out.println(node.getName()+"####"+node.getExpression());
				//				node.setOperand(node.getAST().newSimpleName("MIGRATITION"));

				GenericTreeNode<String> exprnode = new GenericTreeNode<String>(node.toString());
				//this node becomes root
				if (node.getParent()!=null && node.getParent() instanceof Expression ==false){
					rootNode = exprnode;
					tree.setRoot(exprnode);
					System.out.println("properties"+node.getParent().properties());
				} else{
					GenericTreeNode<String> p = map.get(node.getParent());
					p.addChild(exprnode);
				}

				map.put(node, exprnode);


				return super.visit(node);
			}

			/*
			@Override
			public boolean visit(SimpleName node) {
				// TODO Auto-generated method stub

				GenericTreeNode<String> exprnode = new GenericTreeNode<String>(node.toString());
				//this node becomes root
				if (node.getParent()!=null && node.getParent() instanceof Expression ==false){
					rootNode = exprnode;
					tree.setRoot(exprnode);
				} else{
					GenericTreeNode<String> p = map.get(node.getParent());
					p.addChild(exprnode);
				}

				map.put(node, exprnode);

				return super.visit(node);
			}

			 */


			@Override
			public boolean visit(PrefixExpression node) {
				// TODO Auto-generated method stub

				//				ASTRewrite rewriter = ASTRewrite.create(node.getAST());

				//				SimpleName aa = RecordingTestMain.rewriter.getAST().newSimpleName("MIGRATITION");
				//				node.getOperator();
				//				node.getOperand();

				//				rewriter.gro
				//				rewriter.replace(node, aa);
				//				rewriter.replace(node.getOperand(), node.getOperand() , null);
				node.setOperand(node.getAST().newSimpleName("MIGRATITION"));
				//				PrefixExpression ee = node.getAST().newPrefixExpression();
				//				ee.setOperator(node.getOperator());

				//				node =  node.getAST().newPrefixExpression();
				GenericTreeNode<String> exprnode = new GenericTreeNode<String>(node.toString());
				//this node becomes root
				if (node.getParent()!=null && node.getParent() instanceof Expression ==false){
					rootNode = exprnode;
					tree.setRoot(exprnode);
				} else{
					GenericTreeNode<String> p = map.get(node.getParent());
					p.addChild(exprnode);
				}

				map.put(node, exprnode);

				return false;
			}

			//			visit

			//add operator
			@Override
			public boolean visit(InfixExpression node) {

				String str = mapp.get(node.toString());
				if(str!=null)
					System.out.println("mappp"+mapp.get(node.toString()));


				// TODO Auto-generated method stub
				//				GenericTreeNode<String> left 		= new GenericTreeNode<String>(node.getLeftOperand().toString());
				//				GenericTreeNode<String> operator 	= new GenericTreeNode<String>(node.getOperator().toString());
				//				GenericTreeNode<String> right 		= new GenericTreeNode<String>(node.getRightOperand().toString());
				//				infixnode.addChild(left);
				//				infixnode.addChild(operator);
				//				infixnode.addChild(right);
				//				GenericTreeNode<String> parent = map.get(node.getParent());
				//				parent.addChild(infixnode);

				GenericTreeNode<String> exprnode = new GenericTreeNode<String>(node.getOperator().toString());
				//this node becomes root
				if (node.getParent()!=null && node.getParent() instanceof Expression ==false){
					rootNode = exprnode;
					tree.setRoot(exprnode);
				} else{
					GenericTreeNode<String> p = map.get(node.getParent());
					p.addChild(exprnode);
				}

				map.put(node, exprnode);
				return super.visit(node);
			}
		});


		//		System.out.println("After"+tree.build(GenericTreeTraversalOrderEnum.POST_ORDER));
		System.out.println("After"+android);
		return new Migration();
	}

	
	
	Migration getMigrationFrom(Expression android, String log){

//		GenericTree<String> tree = new GenericTree<String>();
		//		GenericTree<String> tree1 = new GenericTree<String>();

//		final GenericTreeNode<String>  rootA = new GenericTreeNode<String>("");
//		Map<ASTNode, GenericTreeNode<String>> map = new HashMap<ASTNode, GenericTreeNode<String>>();


		Migration migration = new Migration();

		android.accept(new ASTVisitor() {
			
			@Override
			public boolean visit(BooleanLiteral node) {
				// TODO Auto-generated method stub
				migration.migratedExpr += node.toString();
				return super.visit(node);
			}
			@Override
			public boolean visit(StringLiteral node) {
				// TODO Auto-generated method stub
				migration.migratedExpr += node.toString();
				return super.visit(node);
			}
			//			@Override
			public boolean visit(QualifiedName node) {
				// TODO Auto-generated method stub
				migration.migratedExpr += node.toString();
				return super.visit(node);
			}
			@Override
			public boolean visit(NullLiteral node) {
				// TODO Auto-generated method stub
				migration.migratedExpr += "nil";				
				return super.visit(node);
			}

			@Override
			public boolean visit(NumberLiteral node) {
				// TODO Auto-generated method stub
				migration.migratedExpr += node.toString().replace("f", "0");
				return super.visit(node);
			}


			@Override
			public boolean visit(MethodInvocation node) {
				// TODO Auto-generated method stub
				migration.migratedExpr += node.getExpression()+"."+node.getName()+"(";
				List args =node.arguments();
				for(Object a:args){
					migration.migratedExpr += getMigrationFrom((Expression)a, "")+",";
				}
				migration.migratedExpr +=")";
				/*
				String mbinding = "";
				IMethodBinding m11=null;
				if(node.resolveMethodBinding()!=null)
					m11= node.resolveMethodBinding().getMethodDeclaration();

				if(m11!=null){
					mbinding=m11.getDeclaringClass().getName()+".";
					mbinding+=m11.getName()+"(";
					ITypeBinding[] pt = m11.getParameterTypes();
					StringUtils.join(pt, ", ");
					ArrayList<String> types = new ArrayList<String>();
					for(ITypeBinding b:pt){
						types.add(b.getName());
					}
					mbinding+= StringUtils.join(types, ", ");
					mbinding+=")";
					mbinding= mbinding.replaceAll(" ", "");
					System.out.println("mbinding"+mbinding);
					Connection connection;
					try {
						connection = DriverManager.getConnection("jdbc:sqlite:"+dbName);

						connection.setAutoCommit(true);
						Statement stmt = connection.createStatement();
						String sql="select expr_swift.EXPR_TEMP as swift_temp, expr_android.VARS as android_binding, expr_android.EXPR_TEMP as android_temp, count(expr_swift.EXPR_TEMP)" 
								+" from expr_android, expr_swift where expr_swift.ANDROID=expr_android.ID"+ 
								" and expr_android.VARS=\'"+mbinding+"\' order by count(expr_swift.EXPR_TEMP) desc";

						//			System.out.println(sql);
						System.out.println("newsqlsql"+sql);
						//			log +=sql;
						ResultSet resultSet = stmt.executeQuery(sql);
						ResultSet resultSet3 = stmt.executeQuery(sql);

						//			log +=sss;
						//			System.out.println("@@"+sql);
						//			System.out.println("@@"+getGeneral(expr.toString())+",,"+expr.toString());
						String expr_temp=null, android_binding =null, swift_vars=null, android_temp=null;
						while(resultSet.next()){
							expr_temp = resultSet.getString("swift_temp");
							android_binding = resultSet.getString("android_binding");
							android_temp = resultSet.getString("android_temp");
							break;
						}
						//						if(android_temp!=null)
						String sss=printResultSet(resultSet3);
						System.out.println("printResultSet"+android+":   "+sss+" "+expr_temp+"  "+android_binding+"  "+android_temp);
						if(android_temp!=null && expr_temp!=null){
							System.err.println("MatchingTemplate"+MatchingTemplate.getAttributesMap(android_temp, node));
							Map<String, Expression> amap = MatchingTemplate.getAttributesMap(android_temp, node);
							ST st_swift = new ST(expr_temp);
							Iterator<String> iter = amap.keySet().iterator();
							while(iter.hasNext()){
								String key =iter.next();
//								st_swift.add(key, getMigrationFrom(amap.get(key),""));
							}
							System.err.println("st_swift"+st_swift.render());
							migration.migratedExpr = st_swift.render();

						}else{
							migration.migratedExpr = node.toString();
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
				*/
				return false;
			}


			@Override
			public boolean visit(SimpleName node) {
				// TODO Auto-generated method stub
				//	compA.add(node.toString());
				//	compA_ast.add(node);			
				migration.migratedExpr += node.toString();
				return super.visit(node);
			}

			@Override
			public boolean visit(InfixExpression node) {
				
				// TODO Auto-generated method stub
				//				compA.add(node.getOperator().toString());
				//					Operator o = node.getOperator();
				//				compA_ast.add(node.getOperator());
				//	opA.add(node.getOperator().toString());
				//				migration.migratedExpr = node.toString();

//				migration.migratedExpr += node.getOperator().toString();
				System.err.println("InfixExpression	"+migration.migratedExpr);
				migration.migratedExpr += getMigrationFrom(node.getLeftOperand(), "")+node.getOperator().toString()+getMigrationFrom(node.getRightOperand(),"");
				return false;
			}

			@Override
			public boolean visit(PrefixExpression node) {
				
				
				// TODO Auto-generated method stub
				//				compA.add(node.getOperator().toString());
				//				compA_ast.add(node.getOperator());
				//	opA.add(node.getOperator().toString());	
//				migration.migratedExpr += node.getOperator().toString();
				return super.visit(node);
			}

			@Override
			public boolean visit(PostfixExpression node) {				
				// TODO Auto-generated method stub
				//				compA.add(node.getOperator().toString());
				//				compA_ast.add(node.getOperator());
				//	opA.add(node.getOperator().toString());
				
			
//				migration.migratedExpr += node.getOperator().toString();
				return super.visit(node);
			}
		});


//		tree.setRoot(rootA);
		System.out.println("After"+tree.build(GenericTreeTraversalOrderEnum.PRE_ORDER));
		System.out.println("getExpr"+android+"    "+migration);

		//		migration.migratedExpr = android.toString();
		return migration;
	}
	
	/*
	
	
	Migration getMigrationFrom(Expression android, String log){

		GenericTree<String> tree = new GenericTree<String>();
		//		GenericTree<String> tree1 = new GenericTree<String>();

		final GenericTreeNode<String>  rootA = new GenericTreeNode<String>("");
		Map<ASTNode, GenericTreeNode<String>> map = new HashMap<ASTNode, GenericTreeNode<String>>();


		Migration migration = new Migration();

		android.accept(new ASTVisitor() {
			
			@Override
			public boolean visit(BooleanLiteral node) {
				// TODO Auto-generated method stub
				migration.migratedExpr += node.toString();

				GenericTreeNode<String>  tNode = new GenericTreeNode<String>(node.toString());
				if(node.getParent()!=null && map.get(node.getParent()) !=null){
					GenericTreeNode<String> p_t = map.get(node.getParent());
					p_t.addChild(tNode);
				}else{
					tree.setRoot(tNode);
				}
				
				map.put(node, tNode);
				return super.visit(node);
			}
			@Override
			public boolean visit(StringLiteral node) {
				// TODO Auto-generated method stub
				migration.migratedExpr += node.toString();

				GenericTreeNode<String>  tNode = new GenericTreeNode<String>(node.toString());
				if(node.getParent()!=null && map.get(node.getParent()) !=null){
					GenericTreeNode<String> p_t = map.get(node.getParent());
					p_t.addChild(tNode);
				}else{
					tree.setRoot(tNode);
				}
				
				map.put(node, tNode);
				//				map.put(node, value)
				return super.visit(node);
			}
			//			@Override
			public boolean visit(QualifiedName node) {
				// TODO Auto-generated method stub
				migration.migratedExpr += node.toString();
				
				GenericTreeNode<String>  tNode = new GenericTreeNode<String>(node.toString());
				if(node.getParent()!=null && map.get(node.getParent()) !=null){
					GenericTreeNode<String> p_t = map.get(node.getParent());
					p_t.addChild(tNode);
				}else{
					tree.setRoot(tNode);
				}
				
				map.put(node, tNode);
				
				return super.visit(node);
			}
			@Override
			public boolean visit(NullLiteral node) {
				// TODO Auto-generated method stub
				migration.migratedExpr += "nil";
				
				GenericTreeNode<String>  tNode = new GenericTreeNode<String>(node.toString());
				if(node.getParent()!=null && map.get(node.getParent()) !=null){
					GenericTreeNode<String> p_t = map.get(node.getParent());
					p_t.addChild(tNode);
				}else{
					tree.setRoot(tNode);
				}
				
				map.put(node, tNode);
				
				return super.visit(node);
			}

			@Override
			public boolean visit(NumberLiteral node) {
				// TODO Auto-generated method stub
				migration.migratedExpr += node.toString().replace("f", "0");
				
				GenericTreeNode<String>  tNode = new GenericTreeNode<String>(node.toString());
				if(node.getParent()!=null && map.get(node.getParent()) !=null){
					GenericTreeNode<String> p_t = map.get(node.getParent());
					p_t.addChild(tNode);
				}else{
					tree.setRoot(tNode);
				}
				
				map.put(node, tNode);
				
				return super.visit(node);
			}


			@Override
			public boolean visit(MethodInvocation node) {
				// TODO Auto-generated method stub
				
				
				GenericTreeNode<String>  tNode = new GenericTreeNode<String>(node.toString());
				if(node.getParent()!=null && map.get(node.getParent()) !=null){
					GenericTreeNode<String> p_t = map.get(node.getParent());
					p_t.addChild(tNode);
				}else{
					tree.setRoot(tNode);
				}
				
				map.put(node, tNode);
				
				
				String mbinding = "";
				IMethodBinding m11=null;
				if(node.resolveMethodBinding()!=null)
					m11= node.resolveMethodBinding().getMethodDeclaration();

				if(m11!=null){
					mbinding=m11.getDeclaringClass().getName()+".";
					mbinding+=m11.getName()+"(";
					ITypeBinding[] pt = m11.getParameterTypes();
					StringUtils.join(pt, ", ");
					ArrayList<String> types = new ArrayList<String>();
					for(ITypeBinding b:pt){
						types.add(b.getName());
					}
					mbinding+= StringUtils.join(types, ", ");
					mbinding+=")";
					mbinding= mbinding.replaceAll(" ", "");
					System.out.println("mbinding"+mbinding);
					Connection connection;
					try {
						connection = DriverManager.getConnection("jdbc:sqlite:"+dbName);

						connection.setAutoCommit(true);
						Statement stmt = connection.createStatement();
						String sql="select expr_swift.EXPR_TEMP as swift_temp, expr_android.VARS as android_binding, expr_android.EXPR_TEMP as android_temp, count(expr_swift.EXPR_TEMP)" 
								+" from expr_android, expr_swift where expr_swift.ANDROID=expr_android.ID"+ 
								" and expr_android.VARS=\'"+mbinding+"\' order by count(expr_swift.EXPR_TEMP) desc";

						//			System.out.println(sql);
						System.out.println("newsqlsql"+sql);
						//			log +=sql;
						ResultSet resultSet = stmt.executeQuery(sql);
						ResultSet resultSet3 = stmt.executeQuery(sql);

						//			log +=sss;
						//			System.out.println("@@"+sql);
						//			System.out.println("@@"+getGeneral(expr.toString())+",,"+expr.toString());
						String expr_temp=null, android_binding =null, swift_vars=null, android_temp=null;
						while(resultSet.next()){
							expr_temp = resultSet.getString("swift_temp");
							android_binding = resultSet.getString("android_binding");
							android_temp = resultSet.getString("android_temp");
							break;
						}
						//						if(android_temp!=null)
						String sss=printResultSet(resultSet3);
						System.out.println("printResultSet"+android+":   "+sss+" "+expr_temp+"  "+android_binding+"  "+android_temp);
						if(android_temp!=null && expr_temp!=null){
							System.err.println("MatchingTemplate"+MatchingTemplate.getAttributesMap(android_temp, node));
							Map<String, Expression> amap = MatchingTemplate.getAttributesMap(android_temp, node);
							ST st_swift = new ST(expr_temp);
							Iterator<String> iter = amap.keySet().iterator();
							while(iter.hasNext()){
								String key =iter.next();
//								st_swift.add(key, getMigrationFrom(amap.get(key),""));
							}
							System.err.println("st_swift"+st_swift.render());
							migration.migratedExpr = st_swift.render();

						}else{
							migration.migratedExpr = node.toString();
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
				return super.visit(node);
			}


			@Override
			public boolean visit(SimpleName node) {
				// TODO Auto-generated method stub
				//	compA.add(node.toString());
				//	compA_ast.add(node);
				
				GenericTreeNode<String>  tNode = new GenericTreeNode<String>(node.toString());
				if(node.getParent()!=null && map.get(node.getParent()) !=null){
					GenericTreeNode<String> p_t = map.get(node.getParent());
					p_t.addChild(tNode);
				}else{
					tree.setRoot(tNode);
				}
				
				map.put(node, tNode);
				
				
				
				migration.migratedExpr += node.toString();
				return super.visit(node);
			}

			@Override
			public boolean visit(InfixExpression node) {
				
				GenericTreeNode<String>  tNode = new GenericTreeNode<String>(node.getOperator().toString());
				if(node.getParent()!=null && map.get(node.getParent()) !=null){
					GenericTreeNode<String> p_t = map.get(node.getParent());
					p_t.addChild(tNode);
				}else{
					tree.setRoot(tNode);
				}
				
				map.put(node, tNode);
				
				
				
				// TODO Auto-generated method stub
				//				compA.add(node.getOperator().toString());
				//					Operator o = node.getOperator();
				//				compA_ast.add(node.getOperator());
				//	opA.add(node.getOperator().toString());
				//				migration.migratedExpr = node.toString();

				migration.migratedExpr += node.getOperator().toString();
				System.err.println("InfixExpression	"+migration.migratedExpr);
				migration.migratedExpr += getMigrationFrom(node.getLeftOperand(), "")+node.getOperator().toString()+getMigrationFrom(node.getRightOperand(),"");
				return super.visit(node);
			}

			@Override
			public boolean visit(PrefixExpression node) {
				
				GenericTreeNode<String>  tNode = new GenericTreeNode<String>(node.getOperator().toString());
				if(node.getParent()!=null && map.get(node.getParent()) !=null){
					GenericTreeNode<String> p_t = map.get(node.getParent());
					p_t.addChild(tNode);
				}else{
					tree.setRoot(tNode);
				}
				
				map.put(node, tNode);
				
				
				// TODO Auto-generated method stub
				//				compA.add(node.getOperator().toString());
				//				compA_ast.add(node.getOperator());
				//	opA.add(node.getOperator().toString());	
				migration.migratedExpr += node.getOperator().toString();
				return super.visit(node);
			}

			@Override
			public boolean visit(PostfixExpression node) {
				
				GenericTreeNode<String>  tNode = new GenericTreeNode<String>(node.getOperator().toString());
				if(node.getParent()!=null){
					GenericTreeNode<String> p_t = map.get(node.getParent());
					p_t.addChild(tNode);
				}else{
					tree.setRoot(tNode);
				}
				
				map.put(node, tNode);
				
				// TODO Auto-generated method stub
				//				compA.add(node.getOperator().toString());
				//				compA_ast.add(node.getOperator());
				//	opA.add(node.getOperator().toString());
				
			
				migration.migratedExpr += node.getOperator().toString();
				return super.visit(node);
			}
		});


//		tree.setRoot(rootA);
		System.out.println("After"+tree.build(GenericTreeTraversalOrderEnum.PRE_ORDER));
		System.out.println("getExpr"+android+"    "+migration);

		//		migration.migratedExpr = android.toString();
		return migration;
	}

*/
	Migration getMigrationExpr2(String android, String log){
		try {
			//			log+="\n//@was "+android_vars;
			Connection connection = DriverManager.getConnection("jdbc:sqlite:"+dbName);
			connection.setAutoCommit(true);
			Statement stmt = connection.createStatement();

			String sql="select expr_swift.EXPR_TEMP, expr_android.VARS, expr_swift.VARS_S,count(expr_swift.EXPR_TEMP)" 
					+" from expr_android, expr_swift where expr_swift.ANDROID=expr_android.ID"+ 
					" and expr_android.VARS=\'"+android+"\' order by count(expr_swift.EXPR_TEMP) desc";

			//			System.out.println(sql);
			System.out.println("sqlsql"+sql);
			//			log +=sql;
			ResultSet resultSet = stmt.executeQuery(sql);
			ResultSet resultSet3 = stmt.executeQuery(sql);

			//			log +=sss;
			//			System.out.println("@@"+sql);
			//			System.out.println("@@"+getGeneral(expr.toString())+",,"+expr.toString());
			String expr_temp=null, android_vars =null, swift_vars=null;
			while(resultSet.next()){
				//				init = resultSet.getString("INIT");
				//				System.out.println(resultSet.getString("EXPR_TEMP")+"::::"+resultSet.getString("VARS")+"_"+resultSet.getString("VARS_S"));
				expr_temp = resultSet.getString("EXPR_TEMP");
				android_vars = resultSet.getString("VARS");
				swift_vars = resultSet.getString("VARS_S");
				break;
			}

			String sss=printResultSet(resultSet3);
			//			List<String> vals = MatchingTemplate.getTokenValuesList(getGeneral(android),android);


			//		Iterator<String> android_vars = vars_android.keySet().iterator();
			if(expr_temp!=null){
				//			if(swift_vars !=null && android_vars!=null && expr_temp!=null){


				//arg0, arg1,...
				String[] a_vars = android_vars.split("\\,");
				List<String> a_list = Arrays.asList(a_vars);
				//[arg0:Name1, arg0:Name2,...]
				//				Map<String, String> nameMap = MatchingTemplate.getNameMap(a_list,vals);
				//arg0, arg1,...
				String[] s_vars = swift_vars.split("\\,");
				List<String> s_vars_list = Arrays.asList(s_vars);

				//				String expr_new= MatchingTemplate.findMatching(expr_temp, s_vars_list, nameMap);

				log+="\n//@template "+android_vars;

				//[Name1:Template1,...]
				//				Map<String,String> wwww = MatchingTemplate.getTokenValues(expr_temp,expr_new);
				//				log+="\n//@assitive Variables should be declared as: ";


				return new Migration(expr_temp, log);

			}else{
				/*				
				String gg=getGeneral(android);
				gg = gg.replaceAll("\\(.*\\)", "(\\%)");
				//				gg.replaceAll("", replacement)
				String sql1 = "select stmts_swift.TEMPLATE from stmts_android, stmts_swift "
						+ "where stmts_android.id=stmts_swift.android and stmts_android.statements like \'%"+gg+"%\'" ;
				//				log +=sql;
				ResultSet resultSet1 = stmt.executeQuery(sql1);
				String expr_temp1=null, android_vars1 =null, swift_vars1=null;
				//				while(resultSet1.next()){
				//					System.out.println(resultSet.getString("EXPR_TEMP")+"::::"+resultSet.getString("VARS")+"_"+resultSet.getString("VARS_S"));
				//					expr_temp1= resultSet1.getString("TEMPLATE");
				//					android_vars1 = resultSet1.getString("VARS");
				//					swift_vars1 = resultSet.getString("VARS_S");
				//				}
				 */
				return new Migration(android, "@warning: no mapping");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}




	//Template and variable Matching algorithms
	Migration getMigrationExpr(String android, String log){
		try {
			Connection connection = DriverManager.getConnection("jdbc:sqlite:"+dbName);
			connection.setAutoCommit(true);
			Statement stmt = connection.createStatement();

			String sql="select expr_swift.EXPR_TEMP, expr_android.VARS, expr_swift.VARS_S,count(expr_swift.EXPR_TEMP)" 
					+" from expr_android, expr_swift where expr_swift.ANDROID=expr_android.ID"+ 
					" and expr_android.EXPR=\'"+android+"\' order by count(expr_swift.EXPR_TEMP) desc";

			ResultSet resultSet = stmt.executeQuery(sql);
			ResultSet resultSet3 = stmt.executeQuery(sql);

			String expr_temp=null, android_vars =null, swift_vars=null;
			while(resultSet.next()){
				//				init = resultSet.getString("INIT");
				//				System.out.println(resultSet.getString("EXPR_TEMP")+"::::"+resultSet.getString("VARS")+"_"+resultSet.getString("VARS_S"));
				expr_temp = resultSet.getString("EXPR_TEMP");
				android_vars = resultSet.getString("VARS");
				swift_vars = resultSet.getString("VARS_S");
				break;
			}

			List<String> vars_android = MatchingTemplate.getTokenValuesList(getGeneral(android), android);
			log+=" "+vars_android.toString();
			//			String sss=printResultSet(resultSet3);
			//			log+=" "+sss;
			//[Name1, Name2,...,..]
			//			List<String> vals = MatchingTemplate.getTokenValuesList(getGeneral(android),android);


			//		Iterator<String> android_vars = vars_android.keySet().iterator();
			if(expr_temp!=null){
				//			if(swift_vars !=null && android_vars!=null && expr_temp!=null){


				//arg0, arg1,...
				log+="\n//@template "+expr_temp;

				//[Name1:Template1,...]
				//				Map<String,String> wwww = MatchingTemplate.getTokenValues(expr_temp,expr_new);
				//				log+="\n//@assitive Variables should be declared as: ";
				//				temp1.add("expr", expr_new);
				//	temp.add("log", log);

				return new Migration(expr_temp, log);

			}else{
				/*
				String gg=getGeneral(android);
				gg = gg.replaceAll("\\(.*\\)", "(\\%)");
				//				gg.replaceAll("", replacement)
				String sql1 = "select stmts_swift.TEMPLATE from stmts_android, stmts_swift "
						+ "where stmts_android.id=stmts_swift.android and stmts_android.statements like \'%"+gg+"%\'" ;
				//				log +=sql;
				ResultSet resultSet1 = stmt.executeQuery(sql1);
				String expr_temp1=null, android_vars1 =null, swift_vars1=null;
				//				while(resultSet1.next()){
				//					System.out.println(resultSet.getString("EXPR_TEMP")+"::::"+resultSet.getString("VARS")+"_"+resultSet.getString("VARS_S"));
				//					expr_temp1= resultSet1.getString("TEMPLATE");
				//					android_vars1 = resultSet1.getString("VARS");
				//					swift_vars1 = resultSet.getString("VARS_S");
				//				}
				 */
				return new Migration(android, "@warning: no mapping");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	//Template and variable Matching algorithms
	Migration getMigrationExpE(Expression android, String log){



		try {
			Connection connection = DriverManager.getConnection("jdbc:sqlite:"+dbName);
			connection.setAutoCommit(true);
			Statement stmt = connection.createStatement();


			//select expr_swift.EXPR_TEMP, expr_android.VARS, expr_swift.VARS_S from expr_android, expr_swift where expr_swift.ANDROID=expr_android.ID and expr_android.EXPR_TEMP
			String general = getGeneral(android.toString());
			general = general.replaceAll("\'", "");

			System.out.println("general"+general);

			//			String sql = "select expr_swift.EXPR_TEMP, expr_android.VARS, expr_swift.VARS_S from expr_android, expr_swift "
			//					+ "where expr_swift.ANDROID=expr_android.ID and expr_android.EXPR_TEMP=\'"+general+"\'";
			//			data.add("log", sql);
			String sql="select expr_swift.EXPR_TEMP, expr_android.VARS, expr_swift.VARS_S,count(expr_swift.EXPR_TEMP)" 
					+" from expr_android, expr_swift where expr_swift.ANDROID=expr_android.ID"+ 
					" and expr_android.EXPR_TEMP=\'"+general+"\' order by count(expr_swift.EXPR_TEMP) desc";

			//			System.out.println(sql);

			log +=sql;
			ResultSet resultSet = stmt.executeQuery(sql);
			ResultSet resultSet3 = stmt.executeQuery(sql);

			//			log +=sss;
			//			System.out.println("@@"+sql);
			//			System.out.println("@@"+getGeneral(expr.toString())+",,"+expr.toString());
			String expr_temp=null, android_vars =null, swift_vars=null;
			while(resultSet.next()){
				//				init = resultSet.getString("INIT");
				System.out.println(resultSet.getString("EXPR_TEMP")+"::::"+resultSet.getString("VARS")+"_"+resultSet.getString("VARS_S"));
				expr_temp = resultSet.getString("EXPR_TEMP");
				android_vars = resultSet.getString("VARS");
				swift_vars = resultSet.getString("VARS_S");
				break;
			}

			List<String> vars_android = MatchingTemplate.getTokenValuesList(getGeneral(android), android);
			log+=" "+vars_android.toString();
			String sss=printResultSet(resultSet3);
			//			log+=" "+sss;
			//[Name1, Name2,...,..]
			List<String> vals = MatchingTemplate.getTokenValuesList(getGeneral(android),android);


			//		Iterator<String> android_vars = vars_android.keySet().iterator();
			if(swift_vars !=null && android_vars!=null && expr_temp!=null){


				//arg0, arg1,...
				String[] a_vars = android_vars.split("\\,");
				List<String> a_list = Arrays.asList(a_vars);
				//[arg0:Name1, arg0:Name2,...]
				Map<String, String> nameMap = MatchingTemplate.getNameMap(a_list,vals);
				//arg0, arg1,...
				String[] s_vars = swift_vars.split("\\,");
				List<String> s_vars_list = Arrays.asList(s_vars);

				String expr_new= MatchingTemplate.findMatching(expr_temp, s_vars_list, nameMap);
				log+="\n//@template "+expr_temp;

				//[Name1:Template1,...]
				Map<String,String> wwww = MatchingTemplate.getTokenValues(expr_temp,expr_new);
				log+="\n//@assitive Variables should be declared as: "+wwww;
				//				temp1.add("expr", expr_new);
				//	temp.add("log", log);

				return new Migration(expr_new, log);

			}else{
				String sql1 = "select un_expr_swift.EXPR_TEMP from un_expr_android, un_expr_swift "
						+ "where un_expr_android.EXPR=\'"+android+"\' and un_expr_android.PARENT_ID=un_expr_swift.PARENT_ID" ;
				log +=sql;
				ResultSet resultSet1 = stmt.executeQuery(sql);
				String expr_temp1=null, android_vars1 =null, swift_vars1=null;
				while(resultSet1.next()){
					System.out.println(resultSet.getString("EXPR_TEMP")+"::::"+resultSet.getString("VARS")+"_"+resultSet.getString("VARS_S"));
					expr_temp1= resultSet1.getString("EXPR_TEMP");
					android_vars1 = resultSet1.getString("VARS");
					swift_vars1 = resultSet.getString("VARS_S");
				}
				return new Migration(android, "@warning: no mapping "+getGeneral(android+expr_temp1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	 */
	public static void main(String[] args){
		String name = "Utils.convertDpToPixel(4f)";
		//		str = str.replaceAll("(*)", "X");
		name = name.replaceAll("\\(.*\\)", "(\\%)");
		System.out.println(name);

		StringBuffer buffer = new StringBuffer();

	}

}
