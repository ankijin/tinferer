package recording.java;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ArrayCreation;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SimpleType;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import net.vivin.GenericTree;
import net.vivin.GenericTreeNode;
import net.vivin.GenericTreeTraversalOrderEnum;
import recording.MappingElement;
import templates.MatchingTemplate;

public class RecordFieldDeclaration3 extends ASTVisitor{

	//	GenericTreeNode<String> parent;
	//	GenericTreeNode<String> astNode;
	STGroup group = new STGroupFile("/Users/kijin/Documents/workspace_mars/GumTreeTester/cross_templates/if_statement.stg");

	public LinkedList<MappingElement> record = new LinkedList<MappingElement> ();
	private Map<String, String> typeMap = new HashMap<String, String>();
	public LinkedList<String> recordStr = new LinkedList<String> ();
	public GenericTree<ST> tree = new GenericTree<ST>();
	public Map<Integer,GenericTreeNode<ST>> treeMap = new HashMap<Integer,GenericTreeNode<ST>>();
	public Map<ASTNode,GenericTreeNode<ST>> astMap = new HashMap<ASTNode,GenericTreeNode<ST>>();
	public RecordFieldDeclaration3(){

		//		GenericTreeNode<Integer> comp 			= new GenericTreeNode<Integer>(ASTNode.COMPILATION_UNIT);
		//		GenericTreeNode<Integer> field_decl 	= new GenericTreeNode<Integer>(ASTNode.FIELD_DECLARATION);
		//		GenericTreeNode<Integer> method_decl 	= new GenericTreeNode<Integer>(ASTNode.METHOD_DECLARATION);
		//		GenericTreeNode<Integer> return_stmt 	= new GenericTreeNode<Integer>(ASTNode.RETURN_STATEMENT);
		//		GenericTreeNode<Integer> if_stmt 		= new GenericTreeNode<Integer>(ASTNode.IF_STATEMENT);
		//		GenericTreeNode<Integer> block 			= new GenericTreeNode<Integer>(ASTNode.BLOCK);

		/*
		comp.addChild(field_decl);
		comp.addChild(method_decl);
		method_decl.addChild(return_stmt);
		method_decl.addChild(if_stmt);
		if_stmt.addChild(return_stmt);
		if_stmt.addChild(if_stmt);
		 */
	}
	GenericTreeNode<ST> top;


	@Override
	public boolean visit(TypeDeclaration node) {
		// TODO Auto-generated method stub
		System.out.println("TypeDeclaration");
		ST data= group.getInstanceOf("program");
		data.add("className", node.getName());
		top = new GenericTreeNode<ST>(data);
		astMap.put(node, top);
		treeMap.put(ASTNode.COMPILATION_UNIT, top);
		return super.visit(node);
	}
	/*
	@Override
	public boolean visit(CompilationUnit node) {
		// TODO Auto-generated method stub
		System.out.println("CompilationUnit");
		ST data= group.getInstanceOf("program");
		data.add("className", "TestClass");
		top = new GenericTreeNode<ST>(data);

		treeMap.put(ASTNode.COMPILATION_UNIT, top);

		//		parent = childB;

		return super.visit(node);
	}
	 */
	@Override
	public boolean visit(FieldDeclaration node) {
		// TODO Auto-generated method stub
		node.getType();
		//		node.modifiers().get(0);
		node.fragments();
		//		node.

		System.out.println("FieldDeclaration"+node.getParent().getNodeType()+","+ASTNode.COMPILATION_UNIT);
		GenericTreeNode<ST> childB = new GenericTreeNode<ST>(group.getInstanceOf("var_decls"));
		//		GenericTreeNode<ST> childC = new GenericTreeNode<ST>(group.getInstanceOf("var_declaration"));
		ST data = childB.getData();
		VariableDeclarationFragment var_decl = (VariableDeclarationFragment)node.fragments().get(0);


		Connection connection;
		Statement stmt;
		ResultSet resultSet = null;
		String init = null;
		String modifier = null;
		try {
			connection  = DriverManager.getConnection("jdbc:sqlite:test_record.db");
			connection.setAutoCommit(false);
			stmt 		= connection.createStatement();

			String sql = "SELECT DISTINCT var_swift.MODIFIER, var_swift.INIT FROM var_android, var_swift "
					+ "WHERE var_swift.ANDROID=var_android.ID and var_android.INIT"+"=\'"+var_decl.getInitializer()+"\'";
			data.add("log", sql);
			resultSet = stmt.executeQuery(sql);
			while(resultSet.next()){
				init = resultSet.getString("INIT");
				modifier  = resultSet.getString("MODIFIER");
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		typeMap.put(var_decl.getName().toString(), node.getType().toString());

		//		data.add("modifiers", node.modifiers().get(0).toString());
		data.add("name", var_decl.getName());
		if(init==null){
			data.add("init", var_decl.getInitializer());
		}else{
			data.add("init", init);
		}

		if(modifier==null){
			data.add("modifiers", node.modifiers().get(0).toString());
		}else{
			data.add("modifiers", modifier);
		}

		//data.add("tmp", data.getAttributes().toString());
		//		childC.setData(data);
		//		data.add("modifiers", node.modifiers().get(0).toString());
		//		GenericTreeNode<ST> parent = treeMap.get(ASTNode.COMPILATION_UNIT);
		GenericTreeNode<ST> parent = astMap.get(node.getParent());
		//		childB.addChild(childC);
		childB.setData(data);
		parent.addChild(childB);

		astMap.put(node, childB);
		treeMap.put(ASTNode.FIELD_DECLARATION, childB);

		return false;
	}

	@Override
	public boolean visit(MethodDeclaration node) {
		// TODO Auto-generated method stub
		System.out.println("MethodDeclaration"+node.getParent().getNodeType()+","+ASTNode.COMPILATION_UNIT);

		GenericTreeNode<ST> childB = new GenericTreeNode<ST>(group.getInstanceOf("func_decls"));
		//		ST temp = childB.getData();
		//		temp.add("func_declaration", "func test()->float{}");
		//		childB.setData(temp);

		//		GenericTreeNode<ST> childC = new GenericTreeNode<ST>(group.getInstanceOf("func_declaration"));
		ST temp = childB.getData();

		temp.add("modifiers", "public");
		temp.add("type", "CGFloat");
		temp.add("name", "s"+node.getName());
		//		temp.add("block","int a");
		childB.setData(temp);
		//		childB.addChild(childC);
		//		GenericTreeNode<ST> parent = treeMap.get(ASTNode.COMPILATION_UNIT);
		GenericTreeNode<ST> parent = astMap.get(node.getParent());
		parent.addChild(childB);

		//		treeMap.put(ASTNode.METHOD_DECLARATION, childB);
		astMap.put(node, childB);
		System.out.println("astMap"+astMap);
		//		List list = node.getBody().statements();


		return super.visit(node);
	}
	/*
	@Override
	public boolean visit(Block node) {
		// TODO Auto-generated method stub
		GenericTreeNode<Integer> block 	= new GenericTreeNode<Integer>(ASTNode.BLOCK);
		List list = node.statements();
		for(int i=0;i<list.size();i++){
			if (list.get(i) instanceof ReturnStatement){
				GenericTreeNode<Integer> return_stmt 	= new GenericTreeNode<Integer>(ASTNode.RETURN_STATEMENT);
				block.addChild(return_stmt);
			}

			if (list.get(i) instanceof IfStatement){
				GenericTreeNode<Integer> if_stmt 	= new GenericTreeNode<Integer>(ASTNode.IF_STATEMENT);
				block.addChild(if_stmt);
			}
		}
		return super.visit(node);
	}
	 */

	@Override
	public boolean visit(Block node) {
		// TODO Auto-generated method stub
		//		GenericTreeNode<ST> childC = new GenericTreeNode<ST>(group.getInstanceOf("block"));
		//		astMap.put(node, childC);
//		System.out.println("Block"+node.getParent().getNodeType()+", "+ASTNode.METHOD_DECLARATION);
		System.out.println("BBlock"+node+",,"+astMap.get(node.getParent()));
		GenericTreeNode<ST> childB = new GenericTreeNode<ST>(group.getInstanceOf("statements"));
		//		ST temp = childB.getData();
		//		temp.add("func_declaration", "func test()->float{}");
		//		childB.setData(temp);

		//		GenericTreeNode<ST> childC = new GenericTreeNode<ST>(group.getInstanceOf("func_declaration"));
		//		ST temp = childC.getData();
		//		temp.add("modifiers", "public");
		//		temp.add("type", "CGFloat");
		//		temp.add("name", "nameTest");
		//		temp.add("block","int a");
		//		childC.setData(temp);
		//		childB.addChild(childC);
		//		GenericTreeNode<ST> parent = treeMap.get(ASTNode.COMPILATION_UNIT);
		//		System.out.println("astMap"+astMap);
		GenericTreeNode<ST> parent = astMap.get(node.getParent());
		parent.addChild(childB);

		treeMap.put(ASTNode.METHOD_DECLARATION, childB);
		astMap.put(node, childB);

		return super.visit(node);
	}


	@Override
	public boolean visit(ReturnStatement node) {
		// TODO Auto-generated method stub
		System.out.println("ReturnStatement"+node.getParent().getNodeType()+", "+ASTNode.BLOCK);
		GenericTreeNode<ST> childB = new GenericTreeNode<ST>(group.getInstanceOf("return_stmt"));
		ST temp = childB.getData();
		getGeneral(node.getExpression().toString());

		Connection connection;
		Statement stmt;
		ResultSet resultSet = null;

		String result = null;
		String expr_temp = null, android_vars = null, swift_vars = null;
		String log ="@SQL:";
		try {
			connection  = DriverManager.getConnection("jdbc:sqlite:test_record.db");
			connection.setAutoCommit(true);
			stmt 		= connection.createStatement();


			//select expr_swift.EXPR_TEMP, expr_android.VARS, expr_swift.VARS_S from expr_android, expr_swift where expr_swift.ANDROID=expr_android.ID and expr_android.EXPR_TEMP

			String sql = "select expr_swift.EXPR_TEMP, expr_android.VARS, expr_swift.VARS_S from expr_android, expr_swift "
					+ "where expr_swift.ANDROID=expr_android.ID and expr_android.EXPR_TEMP=\'"+getGeneral(node.getExpression().toString())+"\'";
			//			data.add("log", sql);
			log +=sql;
			resultSet = stmt.executeQuery(sql);

			System.out.println(sql);

			while(resultSet.next()){
				//				init = resultSet.getString("INIT");
				System.out.println(resultSet.getString("EXPR_TEMP")+"::::"+resultSet.getString("VARS")+resultSet.getString("VARS_S"));
				expr_temp = resultSet.getString("EXPR_TEMP");
				android_vars = resultSet.getString("VARS");
				swift_vars = resultSet.getString("VARS_S");
			}


			System.out.println(sql+"SQL Result"+result);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		List<String> vars_android = MatchingTemplate.getTokenValuesList(getGeneral(node.getExpression().toString()), node.getExpression().toString());
		log+=" "+vars_android.toString();

		//[Name1, Name2,...,..]
		List<String> vals = MatchingTemplate.getTokenValuesList(getGeneral(node.getExpression().toString()),node.getExpression().toString());


		//		Iterator<String> android_vars = vars_android.keySet().iterator();
		if(swift_vars !=null && android_vars!=null && expr_temp!=null){


			//arg0, arg1,...
			String[] a_vars = android_vars.split("\\,");
			List<String> a_list = Arrays.asList(a_vars);
			//[arg0:Name1, arg0:Name2,...]
			Map<String, String> nameMap = MatchingTemplate.getNameMap(a_list,vals);
			//arg0, arg1,...
			String[] s_vars = swift_vars.split("\\,");
			List<String> s_vars_list = Arrays.asList(s_vars);

			String expr_new= MatchingTemplate.findMatching(expr_temp, s_vars_list, nameMap);
			log+="\n//@template "+expr_temp;

			//[Name1:Template1,...]
			Map<String,String> wwww = MatchingTemplate.getTokenValues(expr_temp,expr_new);
			log+="\n//@warning Variables should be declared as: "+wwww;
			temp.add("expression", expr_new);
			//	temp.add("log", log);

		}else{
			temp.add("expression", node.getExpression().toString());

		}




		/*
		List<String> vals = MatchingTemplate.getTokenValuesList(
				getGeneral(node.getExpression().toString()),node.getExpression().toString());

		Iterator<String> android_vars = vars_android.keySet().iterator();
		if(result!=null){
			Pattern dollarRgx = Pattern.compile("\\$\\{([a-zA-Z0-9]+?)\\}");
			Matcher dollarMatcher = dollarRgx.matcher(result);
			int lastEnd =0;
			while(dollarMatcher.find() && android_vars.hasNext()){
				//			String before = result.substring(lastEnd, dollarMatcher.start());
				//			dollarMatcher.group(0);
				String name="s"+android_vars.next();
				System.err.println(result+" pattern"+dollarMatcher.group(lastEnd)+result.contains(dollarMatcher.group(lastEnd))+name);
				log+="\n//@warning:\""+name+"\" should be declarared as "+dollarMatcher.group(lastEnd).replace("${", "").replace("}", "");
				//			result.replace(target, replacement);
				result =result.replace(dollarMatcher.group(lastEnd), name);
				//			lastEnd = dollarMatcher.end();
			}
			temp.add("expression", result);
		}else{
			temp.add("expression", node.getExpression().toString());

		}
		 */

		temp.add("log",log);


		childB.setData(temp);

		GenericTreeNode<ST> parent = astMap.get(node.getParent());
		parent.addChild(childB);

		astMap.put(node, childB);

		return super.visit(node);
	}

	public String getGeneral(String expr){
		Set<String> varnames = typeMap.keySet();
		Iterator<String> it = varnames.iterator();
		while(it.hasNext()){
			String var_name = it.next();
			expr=expr.replace(var_name, "${"+typeMap.get(var_name)+"}");
		}
		return expr;
	}

	@Override
	public boolean visit(IfStatement node) {
		// TODO Auto-generated method stub
		System.out.println("IfStatement"+node.getParent().getNodeType()+", "+ASTNode.BLOCK);


		GenericTreeNode<ST> childB = new GenericTreeNode<ST>(group.getInstanceOf("if_statement"));
		ST temp1 = childB.getData();

		String expr = node.getExpression().toString();
		String log1="";
		Migration migration = getMigrationExpr(expr.toString(), log1);
		String newexpr = migration.migratedExpr;

		//			String newexpr = "NEWEXPR";
		temp1.add("log", migration.log);
		temp1.add("expression", newexpr);

		System.out.println("IfStatement"+expr+",,"+newexpr+"... "+log1);

		childB.setData(temp1);
		//		node.getThenStatement().

		GenericTreeNode<ST> parent = astMap.get(node.getParent());
		parent.addChild(childB);
		astMap.put(node, childB);

		if(node.getThenStatement()!=null){
			node.getThenStatement().accept(this);
		}
		
		if(node.getElseStatement()!=null){
			GenericTreeNode<ST> childC = new GenericTreeNode<ST>(group.getInstanceOf("else_clause"));
			System.out.println("node.getElseStatement()");
			if(node.getElseStatement() instanceof IfStatement){
				System.out.println("instanceof IfStatement");
			}else{
			
				System.out.println("instanceof elseIfStatement");
				

			}
			
		
			GenericTreeNode<ST> childCCC = new GenericTreeNode<ST>(group.getInstanceOf("if_statement"));
		

			GenericTreeNode<ST> childCC = new GenericTreeNode<ST>(group.getInstanceOf("statements"));
			childC.addChild(childCCC);
			childC.addChild(childCC);
			childB.addChild(childC);
//			ST temp11 = childC.getData();
//			parent.addChild(childC);
			astMap.put(node, childC);
//			node.getElseStatement().
			System.out.println("getElseStatement"+astMap);
//			node.getElseStatement();
			node.getElseStatement().accept(this);
			
		}
		
		return false;
	}


	@Override
	public void endVisit(CompilationUnit node) {
		// TODO Auto-generated method stub
		tree.setRoot(top);
		for(GenericTreeNode<ST> tt1:tree.build(GenericTreeTraversalOrderEnum.POST_ORDER)){
			System.err.println(tt1);

			ST data = tt1.getData();
			GenericTreeNode<ST> p= tt1.getParent();

			if(p!=null){
				ST pdata = p.getData();
				Map<String, Object> att = pdata.getAttributes();
				System.out.println("\n"+att+"->"+data.getName().replace("/", "")+","+data.render());
				//		System.err.println(data.getName().replace("/", "")+","+data.render());
				if(att.containsKey(data.getName().replace("/", ""))){
					pdata.add(data.getName().replace("/", ""), data.render());
				}
			}
		}
		System.out.println("final\n"+top.getData().render());
		System.out.println("types\n"+typeMap);

		super.endVisit(node);
	}
	//	visit

	@Override
	public boolean visit(ExpressionStatement node) {
		// TODO Auto-generated method stub

		//		System.out.println(x);
		Connection connection;
		Statement stmt;
		ResultSet resultSet = null;

		String result = null;
		String expr_temp = null, android_vars = null, swift_vars = null;
		String log ="@SQL:";



		Expression expr = node.getExpression();
		if(expr instanceof Assignment){
			GenericTreeNode<ST> childB = new GenericTreeNode<ST>(group.getInstanceOf("const_decl_stmt"));
			ST temp = childB.getData();

			Assignment assign = (Assignment)expr;
			System.out.println("Assignment"+assign);
			String name = assign.getLeftHandSide().toString();


			try {
				connection  = DriverManager.getConnection("jdbc:sqlite:test_record.db");
				connection.setAutoCommit(true);
				stmt 		= connection.createStatement();


				//select expr_swift.EXPR_TEMP, expr_android.VARS, expr_swift.VARS_S from expr_android, expr_swift where expr_swift.ANDROID=expr_android.ID and expr_android.EXPR_TEMP

				String sql = "select expr_swift.EXPR_TEMP, expr_android.VARS, expr_swift.VARS_S from expr_android, expr_swift "
						+ "where expr_swift.ANDROID=expr_android.ID and expr_android.EXPR_TEMP=\'"+getGeneral(assign.getRightHandSide().toString())+"\'";
				//			data.add("log", sql);
				log +=sql;
				resultSet = stmt.executeQuery(sql);

				System.out.println(sql);

				while(resultSet.next()){
					//				init = resultSet.getString("INIT");
					System.out.println(resultSet.getString("EXPR_TEMP")+"::::"+resultSet.getString("VARS")+resultSet.getString("VARS_S"));
					expr_temp = resultSet.getString("EXPR_TEMP");
					android_vars = resultSet.getString("VARS");
					swift_vars = resultSet.getString("VARS_S");
				}

				List<String> vars_android = MatchingTemplate.getTokenValuesList(getGeneral(node.getExpression().toString()), node.getExpression().toString());
				log+=" "+vars_android.toString();

				//[Name1, Name2,...,..]
				List<String> vals = MatchingTemplate.getTokenValuesList(getGeneral(assign.getRightHandSide().toString()),assign.getRightHandSide().toString());


				//		Iterator<String> android_vars = vars_android.keySet().iterator();
				if(swift_vars !=null && android_vars!=null && expr_temp!=null){


					//arg0, arg1,...
					String[] a_vars = android_vars.split("\\,");
					List<String> a_list = Arrays.asList(a_vars);
					//[arg0:Name1, arg0:Name2,...]
					Map<String, String> nameMap = MatchingTemplate.getNameMap(a_list,vals);
					//arg0, arg1,...
					String[] s_vars = swift_vars.split("\\,");
					List<String> s_vars_list = Arrays.asList(s_vars);

					String expr_new= MatchingTemplate.findMatching(expr_temp, s_vars_list, nameMap);
					log+="\n//@template "+expr_temp;

					//[Name1:Template1,...]
					Map<String,String> wwww = MatchingTemplate.getTokenValues(expr_temp,expr_new);
					log+="\n//@warning Variables should be declared as: "+wwww;
					temp.add("expression", expr_new);
					//	temp.add("log", log);

				}else{
					temp.add("expression", assign.getRightHandSide().toString());

				}









				//				System.out.println(sql+"SQL Result"+result);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			temp.add("log", log);
			temp.add("name", "s"+name);

			//			temp.add("expression", assign.getRightHandSide().toString());


			childB.setData(temp);

			GenericTreeNode<ST> parent = astMap.get(node.getParent());
			parent.addChild(childB);
			astMap.put(node, childB);

		} //Assignment Expression left=right



		else{

			//			Assignment assign = (Assignment)expr;
			GenericTreeNode<ST> childB = new GenericTreeNode<ST>(group.getInstanceOf("expression_stmt"));
			ST temp1 = childB.getData();

			String log1="";
			Migration migration = getMigrationExpr(expr.toString(), log1);
			String newexpr = migration.migratedExpr;

			//			String newexpr = "NEWEXPR";
			temp1.add("log", migration.log);
			temp1.add("expression", newexpr);

			System.out.println("EELSE"+expr+",,"+newexpr+"... "+log1);

			childB.setData(temp1);

			GenericTreeNode<ST> parent = astMap.get(node.getParent());
			parent.addChild(childB);
			astMap.put(node, childB);


			//				System.out.println(sql+"SQL Result"+result);







		}


		/*
		System.out.println("VariableDeclarationStatement"+node.toString());
		GenericTreeNode<ST> childB = new GenericTreeNode<ST>(group.getInstanceOf("const_decl_stmt"));
		ST temp = childB.getData();
//		getGeneral(node.getExpression().toString());
//		VariableDeclarationFragment = 
		VariableDeclarationFragment frag = (VariableDeclarationFragment) node.fragments().get(0);
		frag.getInitializer();

		temp.add("name", "testAAA");
		temp.add("expression", frag.getInitializer());

		 */
		return super.visit(node);
	}

	//	VariableDeclaration
	@Override
	public boolean visit(VariableDeclarationStatement node) {
		// TODO Auto-generated method stub
		//		node.fragments();
		System.out.println("VariableDeclarationStatement"+node.toString());
		GenericTreeNode<ST> childB = new GenericTreeNode<ST>(group.getInstanceOf("const_decl_stmt"));
		ST temp = childB.getData();
		//		getGeneral(node.getExpression().toString());
		//		VariableDeclarationFragment = 
		VariableDeclarationFragment frag = (VariableDeclarationFragment) node.fragments().get(0);
		frag.getInitializer();

		temp.add("name", frag.getName());
		temp.add("expression", frag.getInitializer());


		/*
		Connection connection;
		Statement stmt;
		ResultSet resultSet = null;

		String result = null;
		String expr_temp = null, android_vars = null, swift_vars = null;
		String log ="@SQL:";
		try {
			connection  = DriverManager.getConnection("jdbc:sqlite:test_record.db");
			connection.setAutoCommit(true);
			stmt 		= connection.createStatement();


			//select expr_swift.EXPR_TEMP, expr_android.VARS, expr_swift.VARS_S from expr_android, expr_swift where expr_swift.ANDROID=expr_android.ID and expr_android.EXPR_TEMP

			String sql = "select expr_swift.EXPR_TEMP, expr_android.VARS, expr_swift.VARS_S from expr_android, expr_swift "
					+ "where expr_swift.ANDROID=expr_android.ID and expr_android.EXPR_TEMP=\'"+getGeneral(node.getExpression().toString())+"\'";
			//			data.add("log", sql);
			log +=sql;
			resultSet = stmt.executeQuery(sql);

			System.out.println(sql);

			while(resultSet.next()){
				//				init = resultSet.getString("INIT");
				System.out.println(resultSet.getString("EXPR_TEMP")+"::::"+resultSet.getString("VARS")+resultSet.getString("VARS_S"));
				//				result  = resultSet.getString("EXPR_TEMP");
				expr_temp = resultSet.getString("EXPR_TEMP");
				android_vars = resultSet.getString("VARS");
				swift_vars = resultSet.getString("VARS_S");

				//				resultSet.getString("EXPR_TEMP");

			}


			System.out.println(sql+"SQL Result"+result);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		List<String> vars_android = MatchingTemplate.getTokenValuesList(getGeneral(node.getExpression().toString()), node.getExpression().toString());
		log+=" "+vars_android.toString();


		List<String> vals = MatchingTemplate.getTokenValuesList(
				getGeneral(node.getExpression().toString()),node.getExpression().toString());


		//		Iterator<String> android_vars = vars_android.keySet().iterator();
		if(swift_vars !=null && android_vars!=null && expr_temp!=null){

			String[] a_vars = android_vars.split("\\,");
			List<String> a_list = Arrays.asList(a_vars);
			Map<String, String> nameMap = MatchingTemplate.getNameMap(a_list,vals);
			String[] s_vars = swift_vars.split("\\,");
			List<String> s_vars_list = Arrays.asList(s_vars);

			String expr_new= MatchingTemplate.findMatching(expr_temp, s_vars_list, nameMap);
			log+="\n//@template "+expr_temp;

			Map<String,String> wwww = MatchingTemplate.getTokenValues(expr_temp,expr_new);
			log+="\n//@warning Variables should be declared as: "+wwww;
			temp.add("expression", expr_new);
		//	temp.add("log", log);

		}else{
			temp.add("expression", node.getExpression().toString());

		}

		 */


		/*
		List<String> vals = MatchingTemplate.getTokenValuesList(
				getGeneral(node.getExpression().toString()),node.getExpression().toString());

		Iterator<String> android_vars = vars_android.keySet().iterator();
		if(result!=null){
			Pattern dollarRgx = Pattern.compile("\\$\\{([a-zA-Z0-9]+?)\\}");
			Matcher dollarMatcher = dollarRgx.matcher(result);
			int lastEnd =0;
			while(dollarMatcher.find() && android_vars.hasNext()){
				//			String before = result.substring(lastEnd, dollarMatcher.start());
				//			dollarMatcher.group(0);
				String name="s"+android_vars.next();
				System.err.println(result+" pattern"+dollarMatcher.group(lastEnd)+result.contains(dollarMatcher.group(lastEnd))+name);
				log+="\n//@warning:\""+name+"\" should be declarared as "+dollarMatcher.group(lastEnd).replace("${", "").replace("}", "");
				//			result.replace(target, replacement);
				result =result.replace(dollarMatcher.group(lastEnd), name);
				//			lastEnd = dollarMatcher.end();
			}
			temp.add("expression", result);
		}else{
			temp.add("expression", node.getExpression().toString());

		}
		 */

		temp.add("log","test var decl");


		childB.setData(temp);

		GenericTreeNode<ST> parent = astMap.get(node.getParent());
		parent.addChild(childB);

		astMap.put(node, childB);




		return super.visit(node);
	}

	class Migration{
		String migratedExpr;
		String log;

		public Migration(String m, String l){
			migratedExpr = m;
			log = l;
		}
	}

	Migration getMigrationExpr(String android, String log){
		try {
			Connection connection = DriverManager.getConnection("jdbc:sqlite:test_record.db");
			connection.setAutoCommit(true);
			Statement stmt = connection.createStatement();


			//select expr_swift.EXPR_TEMP, expr_android.VARS, expr_swift.VARS_S from expr_android, expr_swift where expr_swift.ANDROID=expr_android.ID and expr_android.EXPR_TEMP

			String sql = "select expr_swift.EXPR_TEMP, expr_android.VARS, expr_swift.VARS_S from expr_android, expr_swift "
					+ "where expr_swift.ANDROID=expr_android.ID and expr_android.EXPR_TEMP=\'"+getGeneral(android)+"\'";
			//			data.add("log", sql);
			log +=sql;
			ResultSet resultSet = stmt.executeQuery(sql);

			//			System.out.println("@@"+sql);
			//			System.out.println("@@"+getGeneral(expr.toString())+",,"+expr.toString());
			String expr_temp=null, android_vars =null, swift_vars=null;
			while(resultSet.next()){
				//				init = resultSet.getString("INIT");
				System.out.println(resultSet.getString("EXPR_TEMP")+"::::"+resultSet.getString("VARS")+"_"+resultSet.getString("VARS_S"));
				expr_temp = resultSet.getString("EXPR_TEMP");
				android_vars = resultSet.getString("VARS");
				swift_vars = resultSet.getString("VARS_S");
			}

			List<String> vars_android = MatchingTemplate.getTokenValuesList(getGeneral(android), android);
			log+=" "+vars_android.toString();

			//[Name1, Name2,...,..]
			List<String> vals = MatchingTemplate.getTokenValuesList(getGeneral(android),android);


			//		Iterator<String> android_vars = vars_android.keySet().iterator();
			if(swift_vars !=null && android_vars!=null && expr_temp!=null){


				//arg0, arg1,...
				String[] a_vars = android_vars.split("\\,");
				List<String> a_list = Arrays.asList(a_vars);
				//[arg0:Name1, arg0:Name2,...]
				Map<String, String> nameMap = MatchingTemplate.getNameMap(a_list,vals);
				//arg0, arg1,...
				String[] s_vars = swift_vars.split("\\,");
				List<String> s_vars_list = Arrays.asList(s_vars);

				String expr_new= MatchingTemplate.findMatching(expr_temp, s_vars_list, nameMap);
				log+="\n//@template "+expr_temp;

				//[Name1:Template1,...]
				Map<String,String> wwww = MatchingTemplate.getTokenValues(expr_temp,expr_new);
				log+="\n//@warning Variables should be declared as: "+wwww;
				//				temp1.add("expr", expr_new);
				//	temp.add("log", log);

				return new Migration(expr_new, log);

			}else{
				return new Migration(android, "@warning: no mapping");

			}



			//			System.out.println(sql+"SQL Result"+result);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}




		return null;
	}

}
