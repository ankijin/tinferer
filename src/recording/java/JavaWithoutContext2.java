package recording.java;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.TokenStreamRewriter;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;

import alignment.DelcarationMapping;
import alignment.common.OpProperty;
import antlr_parsers.common_old.CommonBaseListener;
import antlr_parsers.common_old.CommonLexer;
import antlr_parsers.common_old.CommonParser;
import antlr_parsers.common_old.CommonParser.AssignContext;
import antlr_parsers.common_old.CommonParser.Ast_nodeContext;
import antlr_parsers.common_old.CommonParser.ExprContext;
import antlr_parsers.javaparser.JavaBaseListener;
import antlr_parsers.pcommon.PCommonBaseListener;
import antlr_parsers.pcommon.PCommonLexer;
import antlr_parsers.pcommon.PCommonParser;
import antlr_parsers.pcommon.PCommonParser.ExpressionContext;
import common.CommonParser.AssignmentContext;
import common.CommonParser.EnclosedContext;
import common.CommonParser.Keyword_withContext;
import common.CommonParser.ParseContext;
import migration.StructureMappingAntlr4;
import recording.BaseSwiftRecorder;

public class JavaWithoutContext2 extends JavaBaseListener{
	public ParserRuleContext android;
	public List<String> 	ssnodes 					= new LinkedList<String>();
	public List<OpProperty> 	op_a_nodes 					= new LinkedList<OpProperty>();
	public List<OpProperty> 	a_nodes 					= new LinkedList<OpProperty>();
	public List<OpProperty> op_a_precoding 		= new LinkedList<OpProperty>();
	String left="", right="",rright="";

	
	public String type="none";
	public String opsequence="";
	Map<Interval, String> tokens;

	public List<String> punc = new LinkedList<String>();

	public JavaWithoutContext2(ParserRuleContext android){

		opsequence="";
		op_a_nodes 					= new LinkedList<OpProperty>();
		op_a_precoding = new LinkedList<OpProperty>();
		tokens = new HashMap<Interval, String>();
		this.android = android;

		int az = android.start.getStartIndex();
		int bz = android.stop.getStopIndex();
		Interval interval = new Interval(az,bz);
		String context = android.start.getInputStream().getText(interval);
		//		List<TerminalNode> 	tokens 					= new LinkedList<TerminalNode>();


		ANTLRInputStream stream1 = new ANTLRInputStream(context);
		Lexer lexer1  = new common.CommonLexer((CharStream)stream1);
		common.CommonParser parser1 = new common.CommonParser(new CommonTokenStream(lexer1));
		ParseContext root = parser1.parse();

		ParseTreeWalker walker = new ParseTreeWalker();
		ParseTreeWalker pwalker = new ParseTreeWalker();
		
		ANTLRInputStream stream2 = new ANTLRInputStream(context);
		Lexer plexer = new PCommonLexer((CharStream)stream2);
		antlr_parsers.pcommon.PCommonParser pparser	= new antlr_parsers.pcommon.PCommonParser(new CommonTokenStream(plexer));

		
		antlr_parsers.pcommon.PCommonParser.ParseContext proot = pparser.parse();

		int rootd = proot.depth();
		Interval interval1 = new Interval(proot.start.getStartIndex(),proot.stop.getStopIndex());
		String context1 = proot.start.getInputStream().getText(interval1);
//		System.err.println("proot	"+rootd+" "+context1);
		
		
		
	
		
		
		PCommonBaseListener plistener = new PCommonBaseListener(){
			@Override
			public void enterAssignment(PCommonParser.AssignmentContext ctx) {
				// TODO Auto-generated method stub
				if(ctx.depth()==6) // 6 mean one lower level of paranthesis
					a_nodes.add(new OpProperty(ctx, ctx.getText(),"AssignmentContext",ctx.depth()));
				super.enterAssignment(ctx);
			}

			@Override
			public void enterEnclosed(PCommonParser.EnclosedContext ctx) {
				// TODO Auto-generated method stub
				if(ctx.depth()==6)
					a_nodes.add(new OpProperty(ctx, ctx.getText(),"EnclosedContext",ctx.depth()));
				super.enterEnclosed(ctx);
			}

			@Override
			public void enterExpression(ExpressionContext ctx) {
				// TODO Auto-generated method stub
				if(ctx.depth()==6)
					a_nodes.add(new OpProperty(ctx, ctx.getText(),"ExpressionContext",ctx.depth()));
				
				super.enterExpression(ctx);
			}
		};
		
		
		
		
		
		
		
		
		/*

		CommonBaseListener listener = new CommonBaseListener(){
			int w=0;
			@Override
			public void enterAssign(AssignContext ctx) {
				type="AssignContext";
				// TODO Auto-generated method stub
				ctx.equal().getSourceInterval();
				ctx.equal().start.getStartIndex();
				ctx.equal().stop.getStopIndex();
				//				property = "AssignContext";
				//				property_start = ctx.equal().start.getStartIndex()+node.getStartPosition();
				//				property_end = ctx.equal().stop.getStopIndex()+node.getStartPosition();

				//				System.out.println("property  "+property+"  "+property_start+" "+property_end);
				super.enterAssign(ctx);
			}

			@Override
			public void exitExpr(ExprContext ctx) {
				type="ExprContext";
				// TODO Auto-generated method stub
				int az = ctx.start.getStartIndex();
				int bz = ctx.stop.getStopIndex();
				Interval interval = new Interval(az,bz);
				String context = ctx.start.getInputStream().getText(interval);

				System.out.println("sExprContext"+w+" "+ctx.getText()+"  "+context);
				op_s_precoding.add(new OpProperty(ctx, context, "ExprContext"+w));
				w++;
				if(ctx.mul()!=null){
					opsequence +="*";
				}
				if(ctx.div()!=null){
					opsequence +="/";
				}
				if(ctx.plus()!=null){
					opsequence +="+";
				}
				if(ctx.minus()!=null){
					opsequence +="-";
				}


				super.exitExpr(ctx);
			}

		};
		 */
		common.CommonBaseListener listener = new common.CommonBaseListener(){
			int depth = 1000;

			
			@Override
			public void enterEnclosed(EnclosedContext ctx) {


				// TODO Auto-generated method stub
				if(ctx.depth() < depth){
					type = "EnclosedContext";
					//					System.err.println("enterE"+ctx.getText());
					depth = ctx.depth();
					left = ctx.outer().getText();
					right = ctx.inner().getText();
					//					ctx.outer();
					//					ctx.inner();
					//					System.out.println("javactx.outer()"+ctx.outer().getText());
				}

				super.enterEnclosed(ctx);
			}

			@Override
			public void enterAssignment(AssignmentContext ctx) {

				// TODO Auto-generated method stub
				if(ctx.depth() < depth){
					type = "AssignmentContext";
					//					System.err.println("enterA");
					depth = ctx.depth();
					left = ctx.leftAssign().getText();
					
					rright = ctx.rightAssign().getText().replace(";", "");
					System.out.println("rright"+right);
					super.enterAssignment(ctx);
				}
			}
			
/*
			@Override
			public void enterKeyword_with(Keyword_withContext ctx) {

				// TODO Auto-generated method stub
				if(ctx.depth() < depth){
					type = "Keyword_withContext";
					//					System.err.println("enterK");
					depth = ctx.depth();
					left = ctx.keyword().getText();
					right = ctx.block().getText();
					super.enterKeyword_with(ctx);

				}
			}
			*/
		};

//		walker.walk(listener, root);

		
//		pwalker.walk(plistener, proot);


//		System.out.println("a_nodes"+a_nodes);

		/*

		if(context.contains("=")){

			String[] spt = context.split("=");
			if(spt.length==2){
				left 	= spt[0];
				right 	= spt[1];
			}
		}
		*/

	}

	/**
	 * only identifiers
	 */

	@Override
	public void visitTerminal(TerminalNode node) {
		// TODO Auto-generated method stub
		node.getSourceInterval();
		boolean disj = true;
		boolean notc = true;
		Interval it = null;
		if(StructureMappingAntlr4.java_punctuations_list.contains(node.getText())){
			punc.add(node.getText());
		}
		
		for(OpProperty t:op_a_nodes){
			if(t.node instanceof ParserRuleContext) it = ((ParserRuleContext)t.node).getSourceInterval();
			else if(t.node instanceof TerminalNode) it = ((TerminalNode)t.node).getSourceInterval();
			
			disj = it.disjoint(node.getSourceInterval()) & disj;
			//			System.out.println("swift contains"+t.strASTNode);
			//			t.node
			if(t.strASTNode.equals(node.getText())) {
				//				System.out.println("not overlapped term	"+node.getText());
				notc = notc & false;
				return;
			}
		}
		
//		if(!disj) return;
		
		if(!StructureMappingAntlr4.java_keywords_list.contains(node.getText()) && !StructureMappingAntlr4.java_punctuations_list.contains(node.getText())){

//			System.out.println("not overlapped term	"+node.getText());
			//			op_a_nodes.add(new OpProperty(node, node.getText(),""));

			if(type.equals("EnclosedContext")){
				//				System.out.println("left.contains"+left+" "+ctx.getText());
				if(left.contains(node.getText())){
					
					//					System.out.println("#####"+context);
					op_a_nodes.add(new OpProperty(node, node.getText(),"enclosed_outer"));
				}else if(right.contains(node.getText())){
					op_a_nodes.add(new OpProperty(node, node.getText(),"enclosed_inner"));
				}
			}

			else if(type.equals("AssignmentContext")){
				//				System.out.println("right"+right+"  "+ctx.getText());
				if(left.contains(node.getText())){
					//					System.out.println("AssignmentContext"+context);
					op_a_nodes.add(new OpProperty(node, node.getText(),"assign_left"));
				}else if(rright.equals(node.getText())){
					op_a_nodes.add(new OpProperty(node, node.getText(),"assign_right"));
				}
				else{
					op_a_nodes.add(new OpProperty(node, node.getText(),"none"));
				}
			}
			else{
				op_a_nodes.add(new OpProperty(node, node.getText(),"none"));
			}


			//		}

			super.visitTerminal(node);
		}
	}

	//one-to-one mappings
	/// how to know it is one to one
	/// structure? asfdasf.(dfafasf, asfasf, asdfasf) 
	//many-to-many mappings: binding case, grap
	//separates
	//enter examples
	//condition: keywords, puncs. 
	@Override
	public void enterEveryRule(ParserRuleContext ctx) {
		// TODO Auto-generated method stub
		int az = ctx.start.getStartIndex();
		int bz = ctx.stop.getStopIndex();
		Interval interval = new Interval(az,bz);
		String context="";
		try{
		 context = ctx.start.getInputStream().getText(interval);}catch(Exception e){
			
		}
//		if(context.equals("return false;"))System.out.println("@@"+context);
//		if(op_a_precoding.size()>0)
//			System.out.println("aa_precoding"+context+"  "+op_a_precoding);
		//		if(Character.isUpperCase(ctx.getText().charAt(0))) return;
		//				ctx.depth();
		//				System.out.println();
		//with depth?? sub-tree?

		if(StructureMappingAntlr4.java_keywords_list.contains(ctx.getText())) return;
	/*
		int index=op_a_precoding.indexOf(new OpProperty(ctx, ctx.getText()));
		if(index !=-1){
			System.out.println("op_ss_precoding"+op_a_precoding.get(index).property);
			op_a_nodes.add(new OpProperty(ctx, context, op_a_precoding.get(index).property, ctx.depth()));
			return;
		}
	*/	
		//		System.out.println("same?"+ctx.getText()+" "+android.getText().replaceAll(" ", "")+" "+ctx.getText().replaceAll(" ", "").equals(android.getText().replaceAll(" ", "")));
		if(ctx.getText().replaceAll(" ", "").equals(android.getText().replaceAll(" ", ""))) return;

		for(OpProperty t:op_a_nodes){
			//			System.out.println("swift contains"+t.strASTNode);
//			if(t.strASTNode.equals(context) || t.strASTNode.contains(context)) {
			if(t.strASTNode.equals(context)) {
				return;
			}
		}
		
		
		/**op property write
		 *enclosed{} 
		 */

		if(context.replaceAll(" ", "").startsWith("(") && context.replaceAll(" ", "").endsWith(")") && !context.replaceAll(" ", "").equals("()")){

			//			System.out.println("enclosed()"+context);
			op_a_nodes.add(new OpProperty(ctx, context, "enclosed()", ctx.depth()));
						return;
		}

		if(context.replaceAll(" ", "").startsWith("{") && context.replaceAll(" ", "").endsWith("}")){

			//			System.out.println("enclosed{}"+context);
			op_a_nodes.add(new OpProperty(ctx, context, "enclosed{}", ctx.depth()));
						return;
		}
		/**op property write
		 *AST seperated by ,  
		 */		

		int az1 = ctx.start.getStartIndex();
		int bz1 = ctx.stop.getStopIndex();
		Interval interval1 = new Interval(az1+1,az+1);
		Interval interval2 = new Interval(az1-1,az-1);
		String adj1 = android.start.getInputStream().getText(interval1);
		String adj2 = android.start.getInputStream().getText(interval2);

		Interval srcItv = ctx.getSourceInterval();
		Iterator<Interval> it = tokens.keySet().iterator();
		//	System.out.println("java srcItv    "+ ctx.getText()  +"    "+srcItv+" "+tokens);
		/*
		if(!context.contains("=")){

			ssnodes.add(context);

			if( left.contains(context)){
				op_a_nodes.add(new OpProperty(ctx, context, "left_assign"));
				System.out.println("llll"+type);
			}
			else if(right.contains(context)){
				op_a_nodes.add(new OpProperty(ctx, context, "right_assign"));
			}

			//			else 

			else{
				op_a_nodes.add(new OpProperty(ctx, context));
			}

			//			tssnodes.add(ctx);


		}
		 */
		//		if(ctx.getText().startsWith("float") || ctx.getText().endsWith("var")) return;
		//		if( ctx.getText().startsWith("=")|| ctx.getText().endsWith("=")) return;

		//		if(ctx.getText().startsWith("new") || ctx.getText().startsWith("=")|| ctx.getText().endsWith("=") || ctx.getText().contains("=")) return;

		if(type.equals("EnclosedContext")){
			//			System.out.println("left.contains"+left+" "+ctx.getText());
			if(left.contains(ctx.getText())){
				//				System.out.println("#####"+context);
				op_a_nodes.add(new OpProperty(ctx, context,"enclosed_outer",ctx.depth()));
			}else if(right.contains(ctx.getText())){
				op_a_nodes.add(new OpProperty(ctx, context,"enclosed_inner",ctx.depth()));
			}
			else{
				op_a_nodes.add(new OpProperty(ctx, context,"none",ctx.depth()));
			}
		}

		else if(type.equals("AssignmentContext")){
//						System.out.println("rrright"+rright+"  "+ctx.getText());
			if(left.contains(ctx.getText())){
				//				System.out.println("AssignmentContext"+context);
				op_a_nodes.add(new OpProperty(ctx, context,"assign_left",ctx.depth()));
			}else if(rright.equals(ctx.getText())){
				op_a_nodes.add(new OpProperty(ctx, context,"assign_right",ctx.depth()));
			}
			else if(android.getText().equals(ctx.getText())){
				op_a_nodes.add(new OpProperty(ctx, context,"assign",ctx.depth()));
			}
			else{
				op_a_nodes.add(new OpProperty(ctx, context,"none",ctx.depth()));
			}
		}
		
		else{
			op_a_nodes.add(new OpProperty(ctx, context,"none",ctx.depth()));
		}
		//				}
		super.enterEveryRule(ctx);
	}

}
