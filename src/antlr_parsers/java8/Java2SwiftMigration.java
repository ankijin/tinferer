package antlr_parsers.java8;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.*;

import alignment.common.Filewalker;
import antlr_parsers.javaparser.JavaLexer;
import antlr_parsers.javaparser.JavaParser;
import antlr_parsers.javaparser.JavaParser.CompilationUnitContext;
import j2swift.J2SwiftListener_c;
import j2swift.Java8Lexer;
import j2swift.Java8Parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Very basic Java to Swift syntax converter.
 * See test/Test.java and test/Test.swift for an idea of what this produces.
 * This is a work in progress...
 * @author Pat Niemeyer (pat@pat.net)
 */
public class Java2SwiftMigration
{
	public static void main( String [] args ) throws Exception
	{

		Filewalker fw = new Filewalker("mcharts/MPAndroidChart-master", "mcharts/Charts-master");
		Map<File, File> maps = fw.getClassMapping();
		Set<File> androidcodes = maps.keySet();
		Iterator<File> iter = androidcodes.iterator();
		InputStream is = null;
		/*
		while(iter.hasNext()){
			String javaFile = iter.next().getAbsolutePath();
	        if ( javaFile!=null ) { is = new FileInputStream(javaFile); }
	        ANTLRInputStream input = new ANTLRInputStream(is);
	        Java8Lexer lexer = new Java8Lexer(input);
	        CommonTokenStream tokens = new CommonTokenStream(lexer);
	        Java8Parser parser = new Java8Parser(tokens);
	        ParseTree tree = parser.compilationUnit();
	        ParseTreeWalker walker = new ParseTreeWalker();
	        J2SwiftListener swiftListener = new J2SwiftListener(tokens);
	        walker.walk(swiftListener, tree);
	        System.out.println( swiftListener.rewriter.getText() );
	        break;
		}
		 */

		String protocol = "";
		//		String javaFile = "/Users/kijin/Documents/workspace_mars/GumTreeTester"
		//				+ "/testingFileName/grammarTest/Test_J2Swift2.java";
		//		String javaFile = "/Users/kijin/Documents/workspace_mars/GumTreeTester"
		//				+ "/testingFileName/android/TestingM1.java";
		//		String javaFile = "test1_0.java";
		
		/**
		 * 		String javaFile = "test1_0.java";
        if ( javaFile!=null ) { is = new FileInputStream(javaFile); }
        ANTLRInputStream input = new ANTLRInputStream(is);
        Java8Lexer lexer = new Java8Lexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        Java8Parser parser = new Java8Parser(tokens);
        ParseTree tree = parser.compilationUnit();
        ParseTreeWalker walker = new ParseTreeWalker();
        J2SwiftListener swiftListener = new J2SwiftListener(tokens);
        System.out.println("before"+swiftListener.rewriter.getText() );
        System.out.println( swiftListener.rewriter.getText() );
        System.err.println( swiftListener.rewriter2.getText() );
        walker.walk(swiftListener, tree);
		 * 
		 * 
		 */
		
		String javaFile = "test1_0.java";
        if ( javaFile!=null ) { is = new FileInputStream(javaFile); }
        ANTLRInputStream input = new ANTLRInputStream(is);
        JavaLexer lexer = new JavaLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
//        TokenStreamRewriter
//        tokens.
        JavaParser parser = new JavaParser(tokens);
        
//        JavaParser parser = new JavaParser(tokens);
//        JavaPar
        CompilationUnitContext tree = parser.compilationUnit();
        ParseTreeWalker walker = new ParseTreeWalker();
        tree.getStart();
		int az = tree.start.getStartIndex();
		int bz = tree.stop.getStopIndex();
		Interval interval = new Interval(az,bz);
		String context = tree.start.getInputStream().getText(interval);
//		tokens.getTokens(az, bz);
//        tree.getStart().getTokenSource();
        Java2SwiftMigrationListener swiftListener = new Java2SwiftMigrationListener(tokens);
        System.out.println("before"+context );
        walker.walk(swiftListener, tree);
        System.err.println(  swiftListener.rewriter.getText());
       
//        System.err.println( swiftListener.rewriter2.getText() );
        // This boilerplate largely from the ANTLR example
		// This boilerplate largely from the ANTLR example

	}
}
