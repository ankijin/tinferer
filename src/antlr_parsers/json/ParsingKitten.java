package antlr_parsers.json;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;

import antlr_parsers.json.JSONParser.JsonContext;
import antlr_parsers.json.JSONParser.PairContext;
import migration.SwiftLexer;
import migration.SwiftParser;

public class ParsingKitten {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		// TODO Auto-generated method stub

		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader("/Users/kijin/Documents/workspace_mars/GumTreeTester/src/json/kittenlog.json"));
		Lexer lexer1 = new JSONLexer((CharStream)stream1);
		CommonTokenStream swiftcommon = new CommonTokenStream(lexer1);
		JSONParser parser1 = new JSONParser(new CommonTokenStream(lexer1));
		JsonContext root1 = parser1.json();
		root1.accept(new JSONBaseVisitor(){
			@Override
			public Object visitPair(PairContext ctx) {
				// TODO Auto-generated method stub
				if(ctx.depth()==6){
					String file = ctx.STRING().getText().replaceAll("\\\\", "");
					//					System.out.println(ctx.depth()+" -> "+file);
					System.out.println(new File(file).getAbsolutePath());
				}
				//				ctx.depth();
				//				String tttt = ctx.STRING().getText();
				String tttt = ctx.STRING().getText().replaceAll("\"", "");
				//				System.err.println(tttt);
				//				System.out.println(ctx.STRING().getPayload());
				String type="", offset="", length ="", name="";
				if(tttt.equals("key.typename")){
//					System.out.println(ctx.depth()+":"+ctx.value().getText());
					type = ctx.value().getText();
					type = type.replaceAll("\"", "");
					if(ctx.getParent()!=null){
						 ParserRuleContext parent = ctx.getParent();
						List<ParseTree> childs = parent.children;
						for(ParseTree c:childs){
							if(c instanceof PairContext){
								PairContext pair=(PairContext)c;
								String str=pair.STRING().getText().replaceAll("\"", "");
								if(str.equals("key.nameoffset")){
//									System.err.println(ctx.depth()+":"+pair.value().getText());
									offset = pair.value().getText();
									offset = offset.replaceAll("\"", "");
								}
								if(str.equals("key.length")){
//									System.err.println(ctx.depth()+":"+pair.value().getText());
									length = pair.value().getText();
									length = length.replaceAll("\"", "");
								}
								if(str.equals("key.name")){
//									System.err.println(ctx.depth()+":"+pair.value().getText());
									name = pair.value().getText();
									name = name.replaceAll("\"", "");
								}
							}
						}
					}
					System.out.println("\t"+offset+":"+length+", "+name+", "+type);
				}
				return super.visitPair(ctx);
			}
		});
	}

}
