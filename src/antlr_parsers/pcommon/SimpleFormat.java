package antlr_parsers.pcommon;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;



public class SimpleFormat {
	public static String getSimpleFormat(String example){
		ANTLRInputStream stream2 = new ANTLRInputStream(example);


		Lexer plexer = new PCommonLexer((CharStream)stream2);
		CommonTokenStream tokens = new CommonTokenStream(plexer);
		antlr_parsers.pcommon.PCommonParser pparser	= new antlr_parsers.pcommon.PCommonParser(tokens);

//		pcommon.PCommonParser.ParseContext proot = pparser.parse();
//		ParseTreeWalker pwalker = new ParseTreeWalker();
		//		int rootd = proot.depth();
		//		Interval interval1 = new Interval(proot.start.getStartIndex(),proot.stop.getStopIndex());
		//		String context1 = proot.start.getInputStream().getText(interval1);
		//		System.out.println("proot	"+rootd+" "+context1);

//        Java8Parser parser = new Java8Parser(tokens);
        ParseTree tree = pparser.parse();
        ParseTreeWalker walker = new ParseTreeWalker();
//        J2SwiftListener swiftListener = new J2SwiftListener(tokens);
        SimpleFormatListener listener = new SimpleFormatListener(tokens);
        try{
        walker.walk(listener, tree);
        }catch (java.lang.IllegalArgumentException e){
        	System.out.println(example);
        }
//        System.out.println( listener.rewriter.getText() );

		return listener.rewriter.getText();
	}
	public static void main(String args []){
		System.out.println(getSimpleFormat("public class BubbleData extends BarLineScatterCandleBubbleData<IBubbleDataSet> {asfaf;\n public void A(){aasdfasd;}\n}"));
		System.out.println(getSimpleFormat("public void A(){aa;}"));
		System.out.println(getSimpleFormat("public aa = new Int();"));
		
		
	}
}
