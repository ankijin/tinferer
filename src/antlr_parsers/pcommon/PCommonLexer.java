package antlr_parsers.pcommon;

// Generated from PCommon.g4 by ANTLR 4.6.1
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class PCommonLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.6.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, Identifier=5, WS=6, String=7, COMMENT=8, 
		LINE_COMMENT=9;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "T__3", "Identifier", "WS", "String", "COMMENT", 
		"LINE_COMMENT"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'\n'", "'='", "'{'", "'}'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, "Identifier", "WS", "String", "COMMENT", 
		"LINE_COMMENT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public PCommonLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "PCommon.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

//	@Override
//	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\13h\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\3\2\3\2"+
		"\3\3\3\3\3\4\3\4\3\5\3\5\3\6\7\6\37\n\6\f\6\16\6\"\13\6\3\6\6\6%\n\6\r"+
		"\6\16\6&\3\6\7\6*\n\6\f\6\16\6-\13\6\3\7\6\7\60\n\7\r\7\16\7\61\3\7\3"+
		"\7\3\b\3\b\3\b\3\b\3\b\3\b\7\b<\n\b\f\b\16\b?\13\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\7\bH\n\b\f\b\16\bK\13\b\3\b\5\bN\n\b\3\t\3\t\3\t\3\t\7\tT\n\t"+
		"\f\t\16\tW\13\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\7\nb\n\n\f\n\16\n"+
		"e\13\n\3\n\3\n\3U\2\13\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\3\2\13"+
		"\5\2BB]]__\t\2#%\'AC\\`ac|~~\u0080\u0080\4\2]]__\5\2\13\13\16\17\"\"\3"+
		"\2$$\5\2\f\f\17\17$$\3\2))\5\2\f\f\17\17))\4\2\f\f\17\17t\2\3\3\2\2\2"+
		"\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2"+
		"\2\2\2\21\3\2\2\2\2\23\3\2\2\2\3\25\3\2\2\2\5\27\3\2\2\2\7\31\3\2\2\2"+
		"\t\33\3\2\2\2\13 \3\2\2\2\r/\3\2\2\2\17M\3\2\2\2\21O\3\2\2\2\23]\3\2\2"+
		"\2\25\26\7\f\2\2\26\4\3\2\2\2\27\30\7?\2\2\30\6\3\2\2\2\31\32\7}\2\2\32"+
		"\b\3\2\2\2\33\34\7\177\2\2\34\n\3\2\2\2\35\37\t\2\2\2\36\35\3\2\2\2\37"+
		"\"\3\2\2\2 \36\3\2\2\2 !\3\2\2\2!$\3\2\2\2\" \3\2\2\2#%\t\3\2\2$#\3\2"+
		"\2\2%&\3\2\2\2&$\3\2\2\2&\'\3\2\2\2\'+\3\2\2\2(*\t\4\2\2)(\3\2\2\2*-\3"+
		"\2\2\2+)\3\2\2\2+,\3\2\2\2,\f\3\2\2\2-+\3\2\2\2.\60\t\5\2\2/.\3\2\2\2"+
		"\60\61\3\2\2\2\61/\3\2\2\2\61\62\3\2\2\2\62\63\3\2\2\2\63\64\b\7\2\2\64"+
		"\16\3\2\2\2\65=\t\6\2\2\66<\n\7\2\2\678\7^\2\28<\7^\2\29:\7^\2\2:<\7$"+
		"\2\2;\66\3\2\2\2;\67\3\2\2\2;9\3\2\2\2<?\3\2\2\2=;\3\2\2\2=>\3\2\2\2>"+
		"@\3\2\2\2?=\3\2\2\2@N\t\6\2\2AI\t\b\2\2BH\n\t\2\2CD\7^\2\2DH\7^\2\2EF"+
		"\7^\2\2FH\7)\2\2GB\3\2\2\2GC\3\2\2\2GE\3\2\2\2HK\3\2\2\2IG\3\2\2\2IJ\3"+
		"\2\2\2JL\3\2\2\2KI\3\2\2\2LN\t\b\2\2M\65\3\2\2\2MA\3\2\2\2N\20\3\2\2\2"+
		"OP\7\61\2\2PQ\7,\2\2QU\3\2\2\2RT\13\2\2\2SR\3\2\2\2TW\3\2\2\2UV\3\2\2"+
		"\2US\3\2\2\2VX\3\2\2\2WU\3\2\2\2XY\7,\2\2YZ\7\61\2\2Z[\3\2\2\2[\\\b\t"+
		"\2\2\\\22\3\2\2\2]^\7\61\2\2^_\7\61\2\2_c\3\2\2\2`b\n\n\2\2a`\3\2\2\2"+
		"be\3\2\2\2ca\3\2\2\2cd\3\2\2\2df\3\2\2\2ec\3\2\2\2fg\b\n\2\2g\24\3\2\2"+
		"\2\16\2 &+\61;=GIMUc\3\2\3\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}