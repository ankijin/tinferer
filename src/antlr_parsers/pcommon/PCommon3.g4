grammar PCommon;		
parse : statement* EOF
 ;


statement : 
    ('\n')+|(enclosed |assignment |expression) 
 ;

expression 
 : Identifier+
 ;
assignment
 : leftAssign '=' rightAssign
 ;


leftAssign :
 Identifier+ 
;

rightAssign :
 Identifier+
;

enclosed
 : outer ('\n')* '{' inner '}' enclosed*
 ;


outer : Identifier*
 ;
inner : statement*
 ;
Identifier
 : [@]*[a-zA-Z_0-9()->,:.?&;!%=|"'/^#~]+|('['|']')*
 ;



WS  :  [ \t\r\u000C]+ -> channel(HIDDEN)
    ;


COMMENT
    :   '/*' .*? '*/' -> channel(HIDDEN) 
    ;


LINE_COMMENT
    :   '//' ~[\r\n]* -> channel(HIDDEN)
    ;
Line_comments : '//' .*? ('\n'|EOF)	-> channel(HIDDEN);
 