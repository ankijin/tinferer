grammar Common;		
ast_node
	:  (none | enclosed_p | colon_list |assign |expr) ;

none
	: COMMON_STR + ;

enclosed_p
	:
	'{' COMMON_STR '}' ;
enclosed_rb: '(' colon_list ')' ;
fun_call
	: COMMON_STR'.'enclosed_rb ;

colon_list
	:
	seperted_colon (',' seperted_colon)* ;
seperted_colon :
	COMMON_STR ;
assign: left_assign equal right_assign ;

left_assign: COMMON_STR ; 
right_assign: COMMON_STR ; 


expr:	expr (mul|div) expr
    |	expr (plus|minus) expr
    |	COMMON_STR
    |	'(' expr ')'
    ;


mul : '*'
	;
div : '/'
	;

plus : '+'
;

minus : '-'
;


equal : '=' ;

COMMON_STR
    : [A-Za-z0-9_-';''.''('')''_']+
    ;

COMMON_STR2
    : [A-Za-z0-9_-';''.''('')']+
    ;
IDENTIFIER : [A-Za-z0-9_-';''.''('')']+
;
NEWLINE : [ \r\n] ->skip;