package mapping;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.simmetrics.StringMetric;
import org.simmetrics.StringMetrics;

public class MappingMethods {
	public static Map<String, String> mappingMethod(Map<String, MethodStructAndroid> nodeAndroid, Map<String, MethodStructSwift> nodeSwift){

		//		alignment of VariableDeclartion
		Object[] keyA = nodeAndroid.keySet().toArray();
		Object[] keyS = nodeSwift.keySet().toArray();
		
		Map<String, String> fmapping = new HashMap<String, String>();

		StringMetric metric = StringMetrics.jaroWinkler();
		//		StringMetrics.jaroWinkler();
		float max_score =-1;
		//		System.out.println(pp);
		Vector<String> iii = new Vector<String>();
		String setting ="";
		for(int i=0; i< keyA.length;i++){
			for(int j=0; j< keyS.length;j++){
				float score = metric.compare(keyA[i].toString(), keyS[j].toString());
				
				if (!iii.contains(keyS[j].toString()) && score > max_score && score > 0.55) {
					max_score = score;
					if(!fmapping.containsValue(keyA[i].toString()) && !fmapping.containsValue(keyS[j].toString()))
						fmapping.put(keyA[i].toString(), keyS[j].toString());
					setting = keyS[j].toString();
				}
				
				iii.add(setting);

			}
			max_score = -1;
		}
	
		return fmapping;
	}
	public static Map<String, String> mapping(Map<String, String> nodeAndroid, Map<String, String> nodeSwift){

//		System.out.println("mapping"+nodeAndroid.toString() + nodeSwift.toString());
		//		alignment of VariableDeclartion
		Object[] keyA = nodeAndroid.keySet().toArray();
		Object[] keyS = nodeSwift.keySet().toArray();

		Map<String, String> fmapping = new HashMap<String, String>();

		StringMetric metric = StringMetrics.jaroWinkler();
		//		StringMetrics.jaroWinkler();
		float max_score =-1;
		//		System.out.println(pp);
		Vector<Integer> iii = new Vector<Integer>();
//		float max_score =-1;
		//		System.out.println(pp);

		if(keyA.length>=keyS.length){
			for(int i=0; i< keyA.length;i++){
				//			fmapping.put(keyA[i].toString(), keyS[0].toString());
				int m =0;
				for(int j=0; j< keyS.length;j++){
					float score = metric.compare(keyA[i].toString(), keyS[j].toString());
					if (score > max_score && score > 0.5 && !iii.contains(i)) {
						max_score = score;
						fmapping.put(keyA[i].toString(), keyS[j].toString());
						m=i; //store max index
					}
				}
				iii.add(m);
				//				iii.add(swift_files.indexOf(classmapping.get(android_files.get(i))));
				//prunning
				max_score = 0;
			}
		}else{

//			System.out.println("for(int i=0; i< keyS.length;i++){");
			for(int i=0; i< keyS.length;i++){
				//			fmapping.put(keyA[i].toString(), keyS[0].toString());
				int m =0;
				for(int j=0; j< keyA.length;j++){
					float score = metric.compare(keyS[i].toString(), keyA[j].toString());

					if (score > max_score && score > 0.95 && !iii.contains(i)) {
						max_score = score;
							fmapping.put(keyA[j].toString(), keyS[i].toString());
						m=i; //store max index
					}
				}
				iii.add(m);
				//				iii.add(swift_files.indexOf(classmapping.get(android_files.get(i))));
				//prunning
				max_score = 0;
			}


		}

		return fmapping;
	}
}
