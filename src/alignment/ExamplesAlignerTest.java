package alignment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
//import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.TokenStreamRewriter;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import org.simmetrics.StringMetric;
//import org.simmetrics.StringMetrics;
//import org.simmetrics.metrics.CosineSimilarity;
import org.simmetrics.metrics.StringMetrics;

import alignment.common.HungarianAlgorithm;
import alignment.common.LineCharIndex;
import alignment.common.OpProperty;
import alignment.ruleinfer.QueryExamples;

//import org.simmetrics.tokenizers.Tokenizers;

import antlr_parsers.javaparser.JavaBaseListener;
import antlr_parsers.javaparser.JavaLexer;
import antlr_parsers.javaparser.JavaParser;
import antlr_parsers.javaparser.JavaParser.ClassBodyDeclarationContext;
import antlr_parsers.pcommon.PCommonBaseListener;
import antlr_parsers.pcommon.PCommonLexer;
import antlr_parsers.pcommon.PCommonParser;
import antlr_parsers.pcommon.PCommonParser.ExpressionContext;
import migration.SwiftLexer;
import migration.SwiftParser;
import migration.VariableDeclarion;
import migration.SwiftParser.Function_nameContext;
import migration.SwiftParser.Top_levelContext;
import migration.SwiftParser.Variable_nameContext;
import sqldb.DBTablePrinter;
import sqldb.SQLiteJDBC;

public class ExamplesAlignerTest {
	public static CommonTokenStream swiftcommon=null;
	public static CommonTokenStream androidcommon=null;
	public static String androidpath=null;

	static String readFile(String path, Charset encoding) 
			throws IOException 
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}


	public static <T> Map<String, ParserRuleContext> getMethodDeclAndroid(String path) throws FileNotFoundException, IOException, IndexOutOfBoundsException{
		Map<String, ParserRuleContext> nodeAndroid = new HashMap<String, ParserRuleContext>();

		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new JavaLexer((CharStream)stream1);
		androidcommon= new CommonTokenStream(lexer1);
		JavaParser parser = new JavaParser(new CommonTokenStream(lexer1));
		JavaParser.CompilationUnitContext com = parser.compilationUnit();
		ParseTreeWalker walker = new ParseTreeWalker();

		JavaBaseListener listener = new JavaBaseListener(){
			//			j2swift.Java8Parser.FieldDeclarationContext field; 
			ParserRuleContext cntx;
			@Override
			public void enterClassBodyDeclaration(ClassBodyDeclarationContext ctx) {
				// TODO Auto-generated method stub
				cntx = ctx;
				super.enterClassBodyDeclaration(ctx);
			}
			@Override
			public void enterMethodDeclaration(antlr_parsers.javaparser.JavaParser.MethodDeclarationContext ctx) {
				if(cntx!=null)
					nodeAndroid.put(ctx.Identifier().getText(), cntx);
				super.enterMethodDeclaration(ctx);
			}
		};
		walker.walk(listener, com);

		return nodeAndroid;
	}


	public static Map<String, String> mappingVarDec(Map<String, ParserRuleContext> nodeAndroid, Map<String, ParserRuleContext> nodeSwift, boolean toPrint, String pp){

		//		alignment of VariableDeclartion
		Object[] keyA =  nodeAndroid.keySet().toArray();
		Object[]  keyS = nodeSwift.keySet().toArray();

		Map<String, String> hmapping = new HashMap<String, String>();
		StringMetric metric = StringMetrics.jaroWinkler();
		//		StringMetrics.jaroWinkler();
		float max_score =-1;
		//		System.out.println(pp);
		double[][] costMatrix = new double[keyA.length][keyS.length];
		//		if(keyA.length>=keyS.length){

		for(int i=0; i< keyA.length;i++){
			for(int j=0; j< keyS.length;j++){
				float score = metric.compare(keyA[i].toString(), keyS[j].toString());
				costMatrix[i][j]=1-score; //same: 1-1
			}
		}

		int[] result =  new int[0];
		if(keyA.length >0 &&  keyS.length>0){
			HungarianAlgorithm hung = new HungarianAlgorithm(costMatrix);
			result = hung.execute();
		}
		for(int p=0; p<result.length;p++){
			if(result[p]!=-1){
				if(costMatrix[p][result[p]] < 0.3){
					//					System.err.println(android_files.get(p).getName()+", "+swift_files.get(result[p]).getName());
					hmapping.put(keyA[p].toString(), keyS[result[p]].toString());
				}
			}
			//			System.out.println(p+",, "+result[p]);
			//			System.out.println(android_files.get(result[p])+", "+swift_files.get(p));
		}

		System.out.println("hmapping"+hmapping);


		//		}
		return hmapping;
	}

	public static <T> Map<String, ParserRuleContext> getMethodDeclSwift(String path) throws FileNotFoundException, IOException{

		Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();

		SwiftParser.Top_levelContext root1;
		try{

			ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
			Lexer lexer1 = new SwiftLexer((CharStream)stream1);
			SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
			root1 = parser1.top_level();
		} catch (Exception e){
			return nodeSwift;
		}

		VariableDeclarion<T> visitor = new VariableDeclarion<T>(nodeSwift){
			@Override
			public T visitFunction_name(SwiftParser.Function_nameContext ctx) {
				nodeSwift.put(ctx.getText(), ctx.getParent());
				return super.visitFunction_name(ctx);
			}

			@Override
			public T visitVariable_name(SwiftParser.Variable_nameContext ctx) {
				// TODO Auto-generated method stub

				nodeSwift.put(ctx.getText(), ctx.getParent());
				return super.visitVariable_name(ctx);
			}	
		};
		root1.accept(visitor);
		return nodeSwift;
	}

	public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {
		// TODO Auto-generated method stub				
		String jnames[] = {"examples"};
		String snames[] = {"examples"};

		Connection c;
		c = DriverManager.getConnection("jdbc:sqlite:examples/examples.db");
		java.sql.Statement stmt = c.createStatement();
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_binding_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("constant_stat"));

		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroidStat());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwiftStat());
		stmt.executeUpdate(SQLiteJDBC.createBindingStat());
		stmt.executeUpdate(SQLiteJDBC.createConstantStat());

		c.setAutoCommit(true);
		for(int b=0;b<jnames.length;b++){
			System.err.println(jnames[b]+"\n"+snames[b]);
			alignment.common.Filewalker fw = new alignment.common.Filewalker(jnames[b], snames[b]);

			Map<File, File> classmap = fw.getClassMapping();

			Iterator<File> afileset = classmap.keySet().iterator();
			System.out.println(classmap.size());
			int cA=0, cE=0, cB=0;
			while(afileset.hasNext()){
				File afile=afileset.next();
				File sfile=classmap.get(afile);

				//				System.out.println(afile.getName()+"::: "+classmap.size());
				if(1==1){
					//				if(afile.getName().toString().equals("ViewPortHandler.java")){
					//					System.err.println(sfile.getAbsolutePath());
					//					System.out.println(afile.getAbsolutePath());
					String a_exam = "", s_exam="";
					ANTLRInputStream sstream 					= new ANTLRInputStream(new FileReader(sfile));
					Lexer plexer 								= new PCommonLexer((CharStream)sstream);
					antlr_parsers.pcommon.PCommonParser pparser				= new antlr_parsers.pcommon.PCommonParser(new CommonTokenStream(plexer));
					antlr_parsers.pcommon.PCommonParser.ParseContext proot 	= pparser.parse();
					ParseTreeWalker pwalker 					= new ParseTreeWalker();
					LinkedList<OpProperty> sub_stmts_swift 		= new LinkedList<OpProperty>();


					PCommonBaseListener plistener = new PCommonBaseListener(){
						@Override
						public void enterAssignment(PCommonParser.AssignmentContext ctx) {

							//					if(ctx.depth()==9){

							int a = ctx.start.getStartIndex();
							int b = ctx.stop.getStopIndex();
							Interval interval = new Interval(a,b);

							String context 	= ctx.start.getInputStream().getText(interval);
							OpProperty op 	= new OpProperty(null, context,"AssignmentContext",ctx.depth());
							op.toCompareStr = ctx.leftAssign().getText();
							op.lineindex 	= LineCharIndex.contextToLineIndex(ctx);
							sub_stmts_swift.add(op);
							//					}
							super.enterAssignment(ctx);
						}


						@Override
						public void enterEnclosed(PCommonParser.EnclosedContext ctx) {


							//					if(ctx.depth()==6){

							int a = ctx.start.getStartIndex();
							int b = ctx.stop.getStopIndex();
							Interval interval = new Interval(a,b);

							//						String context = proot.start.getInputStream().getText(interval);		
							String context = ctx.start.getInputStream().getText(interval);
							OpProperty op = new OpProperty(null, context,"EnclosedContext",ctx.depth());

							a = ctx.outer().start.getStartIndex();
							b = ctx.outer().stop.getStopIndex();
							op.toCompareStr = ctx.start.getInputStream().getText(new Interval(a,b));
							op.lineindex = LineCharIndex.contextToLineIndex(ctx);

							sub_stmts_swift.add(op);
							super.enterEnclosed(ctx);
						}

						@Override
						public void enterExpression(ExpressionContext ctx) {
							// TODO Auto-generated method stub
							if(!ctx.getText().equals("///\n") || !ctx.getText().equals("//\n")){
								int a = ctx.start.getStartIndex();
								int b = ctx.stop.getStopIndex();
								Interval interval = new Interval(a,b);		
								String context = ctx.start.getInputStream().getText(interval);
								OpProperty op = new OpProperty(null, context,"ExpressionContext",ctx.depth());
								op.toCompareStr = context;
								op.lineindex = LineCharIndex.contextToLineIndex(ctx);
								sub_stmts_swift.add(op);
							}
							super.enterExpression(ctx);
						}
					};

					try{
						pwalker.walk(plistener, proot);
					}catch (Exception e){

					}

					//					System.out.println("^^\n"+sub_stmts_swift);

					sstream = new ANTLRInputStream(new FileReader(sfile));


					Map<LineCharIndex, ParserRuleContext> swiftLineMap = new HashMap<LineCharIndex, ParserRuleContext>();

					Lexer slexer = new antlr_parsers.swiftparser.SwiftLexer((CharStream)sstream);
					antlr_parsers.swiftparser.SwiftParser sparser						= new antlr_parsers.swiftparser.SwiftParser(new CommonTokenStream(slexer));
					antlr_parsers.swiftparser.SwiftParser.Top_levelContext sroot 	 = null;	
					try{
						sroot = sparser.top_level();
					}catch(Exception e){
						System.out.println("parsing error \n"+sfile);
					}
					antlr_parsers.swiftparser.SwiftBaseListener slistner 				= new antlr_parsers.swiftparser.SwiftBaseListener(){

						@Override
						public void enterEveryRule(ParserRuleContext ctx) {
							// TODO Auto-generated method stub
							LineCharIndex lineIndex=LineCharIndex.contextToLineIndex(ctx);


							int a = ctx.start.getStartIndex();
							int b = ctx.stop.getStopIndex();
							Interval interval = new Interval(a,b);
							//						String context = sroot.start.getInputStream().getText(interval);	
							//					System.out.println("lineindex "+lineIndex+" \n"+context);
							for(OpProperty sop:sub_stmts_swift){
								if(sop.lineindex!=null){
									if(sop.lineindex.equals(lineIndex)){
										sop.node = ctx;	
										sop.type = ctx.getRuleIndex();
										swiftLineMap.put(lineIndex, ctx);
									}
								}
							}
							super.enterEveryRule(ctx);
						}

					};
					try{
						if(sroot!=null){
							pwalker.walk(slistner, sroot);
						}
					}catch(Exception e){
						System.out.println("ERRRRR	"+sfile+"  "+afile);
						e.printStackTrace();
					}
					//swift statement alignment
					System.out.println("&&&"+sub_stmts_swift);

					slistner= new antlr_parsers.swiftparser.SwiftBaseListener(){

						@Override
						public void enterEveryRule(ParserRuleContext ctx) {
							// TODO Auto-generated method stub
							LineCharIndex lineIndex=LineCharIndex.contextToLineIndex(ctx);
							//							System.out.println(lineIndex+"*!*"+ctx.getText());
							for(int k=0; k< sub_stmts_swift.size();k++){
								OpProperty sop = sub_stmts_swift.get(k);
								if(sop.type==0 && !swiftLineMap.containsKey(lineIndex) && !sop.lineindex.toString().equals("91:8~94:2707")){
									//									System.out.println("sop.lineindexsop.lineindex"+sop.strASTNode+" "+sop.lineindex);
									//parent of
									//									if(lineIndex.isIncluded(sop.lineindex)){
									if(sop.lineindex.isIncluded(lineIndex)){
										//										if(lineIndex.startLine ==87){
										sop.node = ctx;	
										sop.type = ctx.getRuleIndex();
										//										sop.height = 9;
										int a = ctx.start.getStartIndex();
										int b = ctx.stop.getStopIndex();
										Interval interval = new Interval(a,b);
										swiftLineMap.put(lineIndex, ctx);
										//							String context = aproot.start.getInputStream().getText(interval);	
										String context = ctx.start.getInputStream().getText(interval);
										sop.strASTNode = context;
										sop.lineindex = lineIndex;
										//										sop.property = "EnclosedContext";
										//										sub_stmts_swift.set(k, sop);
										System.out.println("11sop.lineindexsop.lineindex	"+sop+"     "+ctx.getText());
									}
								}

							}
							super.enterEveryRule(ctx);
						}

					};

					try{
						if(sroot!=null){
							pwalker.walk(slistner, sroot);
						}
					}catch(Exception e){
						System.out.println("ERRRRR	"+sfile+"  "+afile);
						e.printStackTrace();
					}


					LinkedList<OpProperty> 	sub_stmts_android 		= new LinkedList<OpProperty>();
					ANTLRInputStream 	   	astream 				= new ANTLRInputStream(new FileReader(afile));
					Lexer 					aplexer 				= new PCommonLexer((CharStream)astream);
					antlr_parsers.pcommon.PCommonParser 	apparser				= new antlr_parsers.pcommon.PCommonParser(new CommonTokenStream(aplexer));
					antlr_parsers.pcommon.PCommonParser.ParseContext aproot 		= apparser.parse();
					ParseTreeWalker 		apwalker 				= new ParseTreeWalker();

					PCommonBaseListener aplistener = new PCommonBaseListener(){
						//				int depth = 1000;

						@Override
						public void enterAssignment(PCommonParser.AssignmentContext ctx) {
							int a = ctx.start.getStartIndex();
							int b = ctx.stop.getStopIndex();
							Interval interval = new Interval(a,b);

							//						String context = aproot.start.getInputStream().getText(interval);		
							String context = ctx.start.getInputStream().getText(interval);
							OpProperty op = new OpProperty(null, context,"AssignmentContext",ctx.depth());
							op.toCompareStr = ctx.leftAssign().getText();
							op.lineindex = LineCharIndex.contextToLineIndex(ctx);
							sub_stmts_android.add(op);

							//					}
							super.enterAssignment(ctx);
						}


						@Override
						public void enterEnclosed(PCommonParser.EnclosedContext ctx) {
							//					if(ctx.depth()==6){
							int a = ctx.start.getStartIndex();
							int bb = ctx.stop.getStopIndex();
							Interval interval = new Interval(a,bb);

							//						String context = aproot.start.getInputStream().getText(interval);
							String context = ctx.start.getInputStream().getText(interval);

							OpProperty op = new OpProperty(null, context,"EnclosedContext",ctx.depth());
							op.lineindex = LineCharIndex.contextToLineIndex(ctx);
							a = ctx.outer().start.getStartIndex();
							bb = ctx.outer().stop.getStopIndex();
							try{
								op.toCompareStr = ctx.start.getInputStream().getText(new Interval(a,bb));}
							catch (java.lang.StringIndexOutOfBoundsException e){
								System.out.println("errorr	"+e.getMessage()+" "+sfile);

							}
							//						System.out.println("op.toCompareStr\n"+op.toCompareStr);
							sub_stmts_android.add(op);
							//					}


							super.enterEnclosed(ctx);
						}



						@Override
						public void enterExpression(ExpressionContext ctx) {

							if(!ctx.getText().equals("//")){
								// TODO Auto-generated method stub
								int a = ctx.start.getStartIndex();
								int b = ctx.stop.getStopIndex();
								Interval interval = new Interval(a,b);

								//							String context = aproot.start.getInputStream().getText(interval);	
								String context = ctx.start.getInputStream().getText(interval);
								OpProperty op = new OpProperty(null, context,"ExpressionContext",ctx.depth());
								op.lineindex = LineCharIndex.contextToLineIndex(ctx);
								op.toCompareStr = ctx.getText();
								sub_stmts_android.add(op);
							}
							super.enterExpression(ctx);
						}

					};

					//							walker.walk(listener, root);

					apwalker.walk(aplistener, aproot);


					astream = new ANTLRInputStream(new FileReader(afile));


					//				System.err.println("\nsub_stmts_android.size()"+sub_stmts_android);
					//				System.out.println("\nsub_stmts_swift.size()"+sub_stmts_swift);
					Lexer jlexer = new antlr_parsers.javaparser.JavaLexer((CharStream)astream);
					CommonTokenStream commonj = new CommonTokenStream(jlexer);
					antlr_parsers.javaparser.JavaParser jparser						= new antlr_parsers.javaparser.JavaParser(new CommonTokenStream(jlexer));
					TokenStreamRewriter rewriter = new TokenStreamRewriter(commonj);

					antlr_parsers.javaparser.JavaParser.CompilationUnitContext jroot 	= jparser.compilationUnit();
					antlr_parsers.javaparser.JavaBaseListener jlistner 				= new antlr_parsers.javaparser.JavaBaseListener(){

						@Override
						public void enterEveryRule(ParserRuleContext ctx) {
							// TODO Auto-generated method stub
							LineCharIndex lineIndex=LineCharIndex.contextToLineIndex(ctx);


							//							int a = ctx.start.getStartIndex();
							//							int b = ctx.stop.getStopIndex();

							for(OpProperty aop:sub_stmts_android){
								if(aop.lineindex.equals(lineIndex)){
									aop.node = ctx;	
									aop.type = ctx.getRuleIndex();

								}
								//							}
							}
							//						if(ctx.getRuleIndex() == 11) System.out.println("ENM"+context);
							super.enterEveryRule(ctx);
						}

					};

					pwalker.walk(jlistner, jroot);



					jlistner= new antlr_parsers.javaparser.JavaBaseListener(){

						@Override
						public void enterEveryRule(ParserRuleContext ctx) {
							// TODO Auto-generated method stub
							LineCharIndex lineIndex=LineCharIndex.contextToLineIndex(ctx);

							for(int k=0; k<sub_stmts_android.size();k++ ){
								OpProperty aop = sub_stmts_android.get(k);
								if(aop.type==0 && !ctx.getText().startsWith("{")){
									if(lineIndex.isIncluded(aop.lineindex)){

										int a = ctx.start.getStartIndex();
										int b = ctx.stop.getStopIndex();
										Interval interval = new Interval(a,b);

										//							String context = aproot.start.getInputStream().getText(interval);	
										String context = ctx.start.getInputStream().getText(interval);
										aop.node = ctx;	
										aop.type = ctx.getRuleIndex();
										aop.strASTNode = context;
										sub_stmts_android.set(k, aop);
										System.err.println("aop.lineindexaop.lineindex"+ctx.getText());
									}
								}
							}

							super.enterEveryRule(ctx);
						}

					};


					pwalker.walk(jlistner, jroot);
					StringMetric emetric = StringMetrics.jaroWinkler();

					double[][] ecostMatrix = new double[sub_stmts_android.size()][sub_stmts_swift.size()];

					System.err.println("sub_stmts_android\n"+sub_stmts_android);
					System.out.println("sub_stmts_swift\n"+sub_stmts_swift);

					for(int i=0; i< sub_stmts_android.size();i++){
						OpProperty android = sub_stmts_android.get(i);
						for(int j=0; j< sub_stmts_swift.size();j++){
							OpProperty swift = sub_stmts_swift.get(j);

							if(android.property.toString().equals(swift.property.toString()) && android.height==swift.height && (android.type!=0 && swift.type!=0)) {

								ecostMatrix[i][j]	= Math.min(0.9999, 1- emetric.compare(android.strASTNode, swift.strASTNode));

							}else{
								ecostMatrix[i][j]	=1;
							}



						}
					}


					HungarianAlgorithm ehung = new HungarianAlgorithm(ecostMatrix);
					int[] eresult = ehung.execute();
					int k=0;
					for(int pp=0; pp<eresult.length;pp++){
						if(eresult[pp]!=-1 && ecostMatrix[pp][eresult[pp]] !=1 && ecostMatrix[pp][eresult[pp]] <=0.5 ){
							a_exam = sub_stmts_android.get(pp).strASTNode;
							s_exam = sub_stmts_swift.get(eresult[pp]).strASTNode;
							k++;

							String example_sql="";
							try{
								example_sql 	= SQLiteJDBC.updateTableGrammarSwift2(QueryExamples.idd, sub_stmts_android.get(pp).strASTNode.replaceAll("\\'", ""), sub_stmts_android.get(pp).type, sub_stmts_swift.get(eresult[pp]).type, sub_stmts_swift.get(eresult[pp]).strASTNode.replaceAll("\\'", ""), 
										afile.getName(), sub_stmts_android.get(pp).lineindex.toString(),
										sfile.getName(), sub_stmts_swift.get(eresult[pp]).lineindex.toString());
								//						sub_stmts_android.get(p).property.equals(")
								//								System.out.println("example_sql\n"+example_sql);
								stmt.executeUpdate(example_sql);
							} catch (Exception e) {
								e.printStackTrace();
								System.out.println("err in\n"+example_sql);
							}
							if(sub_stmts_android.get(pp).type!=0 && sub_stmts_android.get(pp).type!=0){


								if(sub_stmts_android.get(pp).property.equals("ExpressionContext") && 
										sub_stmts_swift.get(eresult[pp]).property.equals("ExpressionContext")){
									cE++;
								}

								if(sub_stmts_android.get(pp).property.equals("AssignmentContext") && 
										sub_stmts_swift.get(eresult[pp]).property.equals("AssignmentContext")){
									cA++;
								}
								if(sub_stmts_android.get(pp).property.equals("EnclosedContext") && 
										sub_stmts_swift.get(eresult[pp]).property.equals("EnclosedContext")){
									cB++;
								}

								//							ExpressionContextEnclosedContextAssignmentContext
								//							System.err.println(sub_stmts_android.get(p).strASTNode);
							}


						}
					}
					//				System.out.println("total :"+k);
					sub_stmts_android.clear();
					sub_stmts_swift.clear();


					//		} temp if
					DBTablePrinter.printTable(c, "grammar_swift_stat");	
					//				System.out.println("B "+cB+" "+"A "+cA+" E "+cE);
				}
			}//for file-mappings


		}
	}



}
