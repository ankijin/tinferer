package alignment.ruleinfer;

import static org.hamcrest.CoreMatchers.instanceOf;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.misc.TestRig;
import org.apache.commons.collections4.ListUtils;

import com.googlecode.concurrenttrees.radix.node.concrete.DefaultCharSequenceNodeFactory;
import com.googlecode.concurrenttrees.solver.LCSubstringSolver;

import alignment.common.CommonOperators;
import alignment.common.MappingOutput;
import alignment.common.OpProperty;
import antlr_parsers.javaparser.JavaLexer;
import antlr_parsers.javaparser.JavaParser;
import migration.SwiftParser;
import sqldb.DBTablePrinter;
import sqldb.SQLiteJDBC;
//import templatechecker.DSLErrorListener;
//import templatechecker.TemplateCLexer;
//import templatechecker.TemplateCParser;


//import org.apache.commons.collections4.ListUtils;

public class QueryExamples3 {
	private int i2;
	public static int idd;
	private static boolean flip = true;
	//		public static final String db_name = "test_record11.db";
	//	public static final String db_name = "test_record0208.db";

	public static  String db_name = "ttt.db";
	//		public static String db_name = "test_record0210.db";
	//		public static final String db_name = "train_record7.db";
	//	public static final String db_name = "test_record_charts.db";

	private static boolean templateOk = false;
	public static String query(String tableName, int android, int swift){
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, count from "+tableName+ " where parent_type="+swift +" order by count;";
		return query;
	}


	public static ResultSet queryExampleResult(String tableName, int android, int swift) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where ast_type="+android +" and parent_type="+swift +" order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);
		return resultSet;
	}

	public static ResultSet queryExampleResultbyID(String tableName, int id) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where id="+id+ " order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;
	}

	public static ResultSet queryExampleResultbyExample(String tableName, String a_example, String s_example) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, parent_type, count from "+tableName+ " where template=\'"+a_example +"\' and example=\'"+s_example +"\' order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println(query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;

	}

	public static ResultSet queryExampleResultbyExample(String tableName, String a_example) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, parent_type, count from "+tableName+ " where template=\'"+a_example +"\' order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println(query);
		//		test_record11.db
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;

	}

	/*
	public static ResultSet queryExampleResultbyExample(String tableName, String example) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, parent_type, count from "+tableName+ " where template=\'"+example +"\' order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println(query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+"train_record.db");
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;

	}
	 */



	public static String getLongestCommonSubstring(Collection<String> strings) {
		LCSubstringSolver solver = new LCSubstringSolver(new DefaultCharSequenceNodeFactory());

		for (String s: strings) {
			solver.add(s);
		}
		return solver.getLongestCommonSubstring().toString();
	}


	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		// TODO Auto-generated method stub
		//query examples
		//generates template to except for bindings
		//when there are no binding .. just syntax change
		//Goals: removal syntax errors

		//set of training
		//query .. 


		//		db_name = "test_record_for.db";
		Class.forName("org.sqlite.JDBC");
		Connection connection = DriverManager.getConnection("jdbc:sqlite:"+"test_record.db");
		connection.setAutoCommit(true);
		Statement stmt = connection.createStatement();
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift"));

		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_binding_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("constant_stat"));

		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroidStat());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwiftStat());
		stmt.executeUpdate(SQLiteJDBC.createBindingStat());
		stmt.executeUpdate(SQLiteJDBC.createConstantStat());

		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroid());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwift());


		//		String q1= query("grammar_swift_stat", 70, 36);
		//		System.err.println(q1);

		//		int atype 	= 18, stype	= 70;
		//constant
								int atype 	= 68, stype	= 76;
		//epxress		
		//				int atype 	= 70, stype	= 214;

		//var func
		//				int atype 	= 19, stype	= 80;
		//var func
		//				int atype 	= 19, stype	= 97;
		//expession
		//		int atype 	= 19, stype	= 97;
		//return
		//												int atype 	= 70, stype	= 36;
		//		19, 27
		//if..
		//						int atype 	= 70, stype	= 18;
		//for
//		int atype 	= 70, stype	= 6;
		// 70 6
		//76, 68

		db_name = "ttt.db";
		//		db_name = "test_record_charts.db";

		ResultSet resultSet2 =  queryExampleResult("grammar_swift_stat", atype, stype);
//				ResultSet resultSet2 =  queryExampleResultbyID("grammar_swift_stat", 4248);
		//	ResultSet resultSet2 =  queryExampleResultbyExample("grammar_swift_stat", "@Override    protected float[] getMarkerPosition(Highlight high) {        return new float[]{high.getDrawY(), high.getDrawX()};    }");
		int num 	= 0;
		int count 	= 0;
		int cycle 	= 8;

		while(resultSet2.next()){
			templateOk = false;
			flip = true;
			double c1 = 1.0000, c2 = 1.0001;

			num++;
			String aaa = resultSet2.getString("template");
			String bbb = resultSet2.getString("example");

			atype = Integer.parseInt(resultSet2.getString("AST_TYPE"));
			stype = Integer.parseInt(resultSet2.getString("PARENT_TYPE"));
//			aaa = "mAnimator.getPhaseY() "; bbb="dataSet.barBorderWidth"; <arg0> . <arg1> ( )  , <arg1> . <arg0>
//			aaa = "mXBounds.range+mXBounds.min-z"; bbb="_xBounds.range+_xBounds.min-z";
//			aaa = "ranges.length - 1, 0"; bbb="ranges!.count - 1, 0";
//			atype = 35; stype=180;
			idd = Integer.parseInt(resultSet2.getString("ID"));
			int ccc = Integer.parseInt(resultSet2.getString("count"));
			count = count + ccc;

			antlr_parsers.swiftparser.SwiftLexer lexer1 = new antlr_parsers.swiftparser.SwiftLexer((CharStream)new ANTLRInputStream(bbb));
			CommonTokenStream swiftCommonStream = new CommonTokenStream(lexer1);
			antlr_parsers.swiftparser.SwiftParser parser = new antlr_parsers.swiftparser.SwiftParser(swiftCommonStream);


			JavaLexer lexerj = new JavaLexer((CharStream)new ANTLRInputStream(aaa));
			CommonTokenStream javaCommonStream = new CommonTokenStream(lexerj);
			JavaParser parserj = new JavaParser(javaCommonStream);
			ParserRuleContext com = null, comj=null;

			Class<? extends Parser> jparserClass = null, sparserClass = null;
			ClassLoader cl = Thread.currentThread().getContextClassLoader();
			jparserClass = cl.loadClass("javaparser.JavaParser").asSubclass(Parser.class);
			sparserClass = cl.loadClass("swiftparser.SwiftParser").asSubclass(Parser.class);

			try {
				Method jstartRule = null, sstartRule =null;

				/**
				 * get ParserRuleContexts by type ids
				 */
				jstartRule = jparserClass.getMethod(JavaParser.ruleNames[atype]);
				sstartRule = sparserClass.getMethod(SwiftParser.ruleNames[stype]);

				System.out.println("rulej"+JavaParser.ruleNames[atype]);
				System.out.println("rules"+SwiftParser.ruleNames[stype]);
				comj 	= (ParserRuleContext)jstartRule.invoke(parserj, (Object[])null);
				com 	= (ParserRuleContext)sstartRule.invoke(parser, (Object[])null);


			} catch (NoSuchMethodException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			} catch (SecurityException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			StructureMappingAntlr4_for_stmt mapping=null;
			if(comj!=null &&com!=null){
				try {
					mapping = new StructureMappingAntlr4_for_stmt(comj, com);
					mapping.swiftCommonStream = swiftCommonStream;
					mapping.androidCommonStream = javaCommonStream;
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}



				/** rule1
				 * stop when cost equals
				 */
				List<MappingOutput> outputs = new ArrayList<MappingOutput>();
				for(int q=0;q<=cycle && mapping!=null  ;q++){
					//				for(int q=0;q<=cycle && mapping!=null ;q++){
					try {
						System.out.println("test"+q);

						//initialize get next candidates
						//						System.out.println("blcklists"+mapping.swift_listner.blacklist);
						mapping.initialize();
//						System.out.println("op_a_nodes"+mapping.android_listener.op_a_nodes);
//						System.out.println("op_s_nodes"+mapping.swift_listner.op_s_nodes);
						//						mapping.amap_ct
						//mappings
						//						if(mapping.android_listener.op_a_nodes.size()==mapping.swift_listner.op_s_nodes.size())
						c2 =c1;
						MappingOutput output = mapping.compute();
						if(output!=null){
							c1=output.cost;


							//							System.out.println("output>>>>"+output);
							if(!output.hasTemplateError())
								outputs.add(output);
						}
						//						output.doupdate();




						//						System.out.println("c1:"+c1+"c2:"+c2+"min:"+Math.min(c1,c2));
						//						if(c2==c1)

						//						templateOk=mapping.templateSolver;

						//						Iterator<Pair> mapps 		= mapping.android_to_swift_point.keySet().iterator();
						//						Iterator<Interval> aaaaaa 	= mapping.amap_ct.keySet().iterator();
						Iterator<OpProperty> aaaaaa = mapping.android_listener.op_a_nodes.iterator();
						List<Interval> listc 		= new LinkedList<Interval>();
						HashSet<OpProperty> set 			= new HashSet<OpProperty>();
						LinkedHashMap<String, List<Interval>> constraint = mapping.android_listener.scope_constraint;
						Iterator<String> decls = constraint.keySet().iterator();
//						System.out.println("constraint"+constraint);
						while(decls.hasNext()){
							listc = constraint.get(decls.next());
//														System.out.println("listc"+listc);;
//							boolean isMet = true;
							//check if template have same args
							while(aaaaaa.hasNext()){
								OpProperty anode = aaaaaa.next();
								Interval nnnnn = anode.interval;
								for(int i = 0; i< listc.size();i++){

									if(!listc.get(i).equals(nnnnn) && !listc.get(i).disjoint(nnnnn)){
										//										System.out.println("nnnnn"+mapping.amap_ct.get(nnnnn));
										set.add(anode);

									}
								}
							}
						}
						//don't exist

						//							System.out.println(set+" sizeof"+set.size()+"amap_ct"+set);
						//if .. not in the same arguments 
						if(set.size()>=1){
							for(int i = 0; i< listc.size();i++){
								for(OpProperty op: mapping.android_listener.op_a_nodes)
									if(!listc.get(i).equals(op.interval) && !listc.get(i).disjoint(op.interval)){
										mapping.android_listener.blacklist.addAll(set);
										//										System.out.println(set+" size");
									}
							}


							if(mapping.android_to_swift_point.size()>0){
								Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
									public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
										return entry1.getValue().compareTo(entry2.getValue());
									}
								});

								if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(min.getKey().b)))
									mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
								//								System.out.println("black_s"+mapping.swift_listner.blacklist);

							}
						} // un-met .. cases

						else if(mapping.android_listener.op_a_nodes.size() < mapping.swift_listner.op_s_nodes.size()){
							//							if(flip){
							//							System.out.println("android mapping	"+mapping.android_to_swift_point);
							//							System.out.println("larger"+mapping.android_listener.op_a_nodes);
							//							System.err.println("larger"+mapping.swift_listner.op_s_nodes);

							//							System.out.println("mapping	"+mapping.android_to_swift_point);
							if(mapping.android_to_swift_point.size()>0){

								Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
									public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
										return entry1.getValue().compareTo(entry2.getValue());
									}
								});


								//								System.out.println(min.getKey()+"\n"+);
								//								black_a.clear();
								//								System.out.println("mmmmin	"+min+"  "+min.getKey()+"\n"+mapping.swift_listner.op_s_nodes.get(min.getKey().b)+"\n"+mapping.android_listener.op_a_nodes.get(min.getKey().a));
								//								
								//								black_a.add(mapping.android_listener.op_a_nodes.get(min.getKey().a));
								if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(min.getKey().a)))
									mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(min.getKey().a));
								//								System.out.println("black_a"+mapping.android_listener.blacklist);
								//								black_a.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
								//								if(min!=null)
								//									mapping.android_listener.op_a_nodes.remove(min.getKey().a);
							}
							//							black_s.addAll(mapping.swift_listner.op_s_nodes);
							//							black_a.addAll(mapping.android_listener.op_a_nodes);
							flip = false;
						}

						/** return <>; return <>
						 * for ( <arg0> <arg1> : <arg2> ) {...}  to for <arg1> in <arg2> {...}
						 * 없으면 이대로 .. 있으면 .. 계속. best matching
						 */

						else if(mapping.android_listener.op_a_nodes.size() > mapping.swift_listner.op_s_nodes.size()){
							//							System.out.println("swift mapping	"+mapping.android_to_swift_point);
							//							System.out.println(mapping.android_listener.op_a_nodes);
							//							System.out.println(mapping.swift_listner.op_s_nodes);
							if(mapping.android_to_swift_point.size()>0){
								Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
									public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
										return entry1.getValue().compareTo(entry2.getValue());
									}
								});
								//								System.out.println("|a|<|s|"+min.getKey()+"\n"+mapping.swift_listner.op_s_nodes.get(min.getKey().b)+"\n"+mapping.android_listener.op_a_nodes.get(min.getKey().a));
								//								black_s.clear();
								//								black_s.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));



								Iterator<Integer> a_keys = mapping.android_to_swift.keySet().iterator();
								while(a_keys.hasNext()){
									int aa=a_keys.next();
									int ss = mapping.android_to_swift.get(aa);

									if(aa >=0 && ss>=0){
										int decision = CommonOperators.determineGoDownbyNum(mapping.android_listener.op_a_nodes.get(aa).strASTNode, mapping.swift_listner.op_s_nodes.get(ss).strASTNode);
										//										System.out.println("eq_size decision:"+decision+"   "+mapping.android_listener.op_a_nodes.get(aa)+"   "+mapping.swift_listner.op_s_nodes.get(ss));

										if(decision ==2){
											if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
												Iterator<Interval> itttt = mapping.constraint.values().iterator();
												//									   while(itttt.hasNext()){
												//										   Interval constrainttt = itttt.next();
												//										   if(constrainttt.properlyContains(mapping.swift_listner.op_s_nodes.get(ss).interval)&& constrainttt.length()>mapping.swift_listner.op_s_nodes.get(ss).interval.length())
												mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
												//												System.out.println("adding blck"+mapping.swift_listner.op_s_nodes.get(ss));
												//									   }

											}
											//												if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
											//													mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
											//												}
											//												System.out.println("SSSBBBB"+mapping.swift_listner.op_s_nodes.get(ss));
										}
										else if(decision ==1){
											if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
												mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
											}
										}
										else if(decision == 0){
											if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
												mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
											}
											if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
												mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
											}
										}
									}
								}

								/*
								Iterator<Integer> a_keys = mapping.android_to_swift.keySet().iterator();
								while(a_keys.hasNext()){
									int aa=a_keys.next();
									int ss = mapping.android_to_swift.get(aa);
									//									ParserRuleContext adrd = (ParserRuleContext)mapping.android_listener.op_a_nodes.get(aa).node;
									//									ParserRuleContext swft = (ParserRuleContext)mapping.swift_listner.op_s_nodes.get(ss).node;

									//0 both
									//1 aop
									//2 sop
									//									int decision = CommonOperators.determineGoDownbyNum(mapping.android_listener.op_a_nodes.get(aa), mapping.swift_listner.op_s_nodes.get(ss));

									if(aa>=0 && ss>=0){
										int decision = CommonOperators.determineGoDownbyNum(mapping.android_listener.op_a_nodes.get(aa), mapping.swift_listner.op_s_nodes.get(ss));
										//										if(decision!=-1){
										System.out.println("SSBBBB"+decision+"   "+mapping.android_listener.op_a_nodes.get(aa)+ " "+mapping.swift_listner.op_s_nodes.get(ss));
										if(decision ==2){
											if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
												mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
											}
//											if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
//												mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
//											}
											//												System.out.println("SSSBBBB"+mapping.swift_listner.op_s_nodes.get(ss));
										}
										else if(decision ==1){
											if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
												mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
											}
										}
										else if(decision == 0){
											if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
												mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
											}
											if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
												mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
											}
										}
										//										}
									}

								}
								 */
								//								black_s.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
								//								if(min!=null)
								//									mapping.swift_listner.op_s_nodes.remove(min.getKey().b);
							}
							//							black_s.clear();
							//							black_s.addAll(mapping.swift_listner.op_s_nodes);
							//							System.out.println("black_s.addAll(mapping.swift_listner.op_s_nodes)");
							flip = true;
						}
						else{






							//							mapping.swift_listner.op_s_nodes.get
							Iterator<Integer> a_keys = mapping.android_to_swift.keySet().iterator();
							while(a_keys.hasNext()){
								int aa=a_keys.next();
								int ss = mapping.android_to_swift.get(aa);
								int decision = CommonOperators.determineGoDownbyNum(mapping.android_listener.op_a_nodes.get(aa).strASTNode, mapping.swift_listner.op_s_nodes.get(ss).strASTNode);
//								System.out.println("eq_size decision:"+decision+"   "+mapping.android_listener.op_a_nodes.get(aa)+"   "+mapping.swift_listner.op_s_nodes.get(ss));
								if(decision ==2){
									if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
										Iterator<Interval> itttt = mapping.constraint.values().iterator();
										//									   while(itttt.hasNext()){
										//										   Interval constrainttt = itttt.next();
										//										   if(constrainttt.properlyContains(mapping.swift_listner.op_s_nodes.get(ss).interval)&& constrainttt.length()>mapping.swift_listner.op_s_nodes.get(ss).interval.length())
										mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
										//										System.out.println("adding blck"+mapping.swift_listner.op_s_nodes.get(ss));
										//									   }

									}
									//												if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
									//													mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
									//												}
									//												System.out.println("SSSBBBB"+mapping.swift_listner.op_s_nodes.get(ss));
								}
								else if(decision ==1){
									if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
										mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
									}
								}
								else if(decision == 0){
									if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
										mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
									}
									if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
										mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
									}
								}

							}

						}//if same # of
						//					black_a = mapping.android_listener.op_a_nodes;
						//						System.out.println("black_a"+black_a);
						//						System.err.println("black_s"+black_s);

						//					black_s = mapping.swift_listner.op_s_nodes;

					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(outputs.size()>0){
					System.out.println("MappingOutput\n"+outputs);
					MappingOutput minOutput = Collections.min(outputs);
					//										minOutput.doupdate();
					//					outputs.size();

					MappingOutput last =outputs.get(outputs.size()-1);
					last.doupdate();
					LinkedHashMap<OpProperty, OpProperty> nextmapping = last.nextMapping;
					//updating next level
					MappingOutput.updateNextMapping(connection, nextmapping);
					Iterator<OpProperty> akey_iter = nextmapping.keySet().iterator();
					while(akey_iter.hasNext()){
						OpProperty key = akey_iter.next();
						//						System.out.println("nextmapping"+key.getText()+"  "+ nextmapping.get(key).getText());
					}
					//					System.out.println(x);

				}
				DBTablePrinter.printTable(connection, "grammar_android_stat");
				DBTablePrinter.printTable(connection, "grammar_swift_stat");
				DBTablePrinter.printTable(connection, "grammar_binding_stat");
				DBTablePrinter.printTable(connection, "constant_stat");

			}



		}


		System.out.println(num);
		System.out.println(count);


		connection.close();

	}

}
