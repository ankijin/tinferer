package alignment.ruleinfer;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.sqlite.*;

import java.util.Collection;
import java.util.regex.Pattern;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Interval;

//import org.apache.commons.collections4.ListUtils;

import com.googlecode.concurrenttrees.radix.node.concrete.DefaultCharSequenceNodeFactory;
import com.googlecode.concurrenttrees.solver.LCSubstringSolver;

import alignment.common.CommonOperators;
import alignment.common.OpProperty;
import antlr_parsers.javaparser.JavaLexer;
import antlr_parsers.javaparser.JavaParser;
import migration.SwiftParser;
import sqldb.DBTablePrinter;
import sqldb.SQLiteJDBC;
//import templatechecker.DSLErrorListener;
//import templatechecker.TemplateCLexer;
//import templatechecker.TemplateCParser;


//import org.apache.commons.collections4.ListUtils;

public class QueryExamples4 {
	private int i2;
	public static int idd;
	private static boolean flip = true;
	//		public static final String db_name = "test_record11.db";
	//	public static final String db_name = "test_record0208.db";

	public static  String db_name = "ttt.db";
	//		public static String db_name = "test_record0210.db";
	//		public static final String db_name = "train_record7.db";
//		public static final String db_name = "test_record_charts.db";

	private static boolean templateOk = false;
	public static String query(String tableName, int android, int swift){
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, count from "+tableName+ " where parent_type="+swift +" order by count;";
		return query;
	}


	public static ResultSet queryExampleResult() throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select *,sum(count) as c from grammar_android_stat group by example, template order by c DESC;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);
		return resultSet;
	}
	
	public static ResultSet queryExampleResult2(int a, int b) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select *, sum(count) as c from grammar_android_stat where ast_type="+a +" and parent_type="+b +" group by example, template order by c DESC;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);
		return resultSet;
	}

	public static ResultSet queryTemplateByExampleID(int id) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
	
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+"test_record.db");
//		connection2.prepareStatement( "attach 'total_example.db' as tempp").execute();
		
		String query = "SELECT template , example, rform FROM grammar_android_stat where (SELECT template FROM temp1.grammar_swift_stat where id=700) REGEXP rform;";
//		String query = "select ast_type, parent_type, count(*) as c from grammar_swift_stat group by ast_type, parent_type order by c DESC;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);

		
//		connection2.setAutoCommit(true);

		

		
		Statement stmt2 = connection2.createStatement();
		stmt2.execute("ATTACH 'db_result/total_examples.db' as temp1;");
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
	
        Function.create(connection2, "REGEXP", new Function() {
            @Override
            protected void xFunc() throws SQLException {
                String expression = value_text(0);
                String value = value_text(1);
                if (value == null)
                    value = "";

                Pattern pattern=Pattern.compile(expression);
                result(pattern.matcher(value).find() ? 1 : 0);
            }
        });
		
        
		
		ResultSet resultSet = stmt2.executeQuery(query);
		
		return resultSet;
	}
	
	public static ResultSet queryExampleResultbyID(String tableName, int id) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where id="+id+ " order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;
	}

	public static ResultSet queryExampleResultbyExample(String tableName, String a_example, String s_example) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, parent_type, count from "+tableName+ " where template=\'"+a_example +"\' and example=\'"+s_example +"\' order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println(query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;

	}

	public static ResultSet queryExampleResultbyExample(String tableName, String a_example) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, parent_type, count from "+tableName+ " where template=\'"+a_example +"\' order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println(query);
		//		test_record11.db
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;

	}

	/*
	public static ResultSet queryExampleResultbyExample(String tableName, String example) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, parent_type, count from "+tableName+ " where template=\'"+example +"\' order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println(query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+"train_record.db");
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;

	}
	 */



	public static String getLongestCommonSubstring(Collection<String> strings) {
		LCSubstringSolver solver = new LCSubstringSolver(new DefaultCharSequenceNodeFactory());

		for (String s: strings) {
			solver.add(s);
		}
		return solver.getLongestCommonSubstring().toString();
	}


	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		// TODO Auto-generated method stub
		//query examples
		//generates template to except for bindings
		//when there are no binding .. just syntax change
		//Goals: removal syntax errors

		//set of training
		//query .. 


		//		db_name = "test_record_for.db";
		Class.forName("org.sqlite.JDBC");
		
		
		
		
		/*
		Connection connection = DriverManager.getConnection("jdbc:sqlite:"+"test_record.db");
		connection.setAutoCommit(true);
		Statement stmt = connection.createStatement();
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_binding_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("constant_stat"));

		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroidStat());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwiftStat());
		stmt.executeUpdate(SQLiteJDBC.createBindingStat());
		stmt.executeUpdate(SQLiteJDBC.createConstantStat());

		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroid());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwift());
*/

		//		String q1= query("grammar_swift_stat", 70, 36);
		//		System.err.println(q1);

		//		int atype 	= 18, stype	= 70;
		//constant
//								int atype 	= 68, stype	= 76;
		//epxress		
		//				int atype 	= 70, stype	= 214;

		//var func
		//				int atype 	= 19, stype	= 80;
		//var func
		//				int atype 	= 19, stype	= 97;
								//swith
//												int atype 	= 70, stype	= 21;
		//expession
		//		int atype 	= 19, stype	= 97;
		//return
		//												int atype 	= 70, stype	= 36;
		//		19, 27
		//if..
		//						int atype 	= 70, stype	= 18;
		//for
		int atype 	= 70, stype	= 6;
		// 70 6
		//76, 68

//		db_name = "total_examples0421_cards.db";
//		db_name = "ttt.db";
//				db_name = "template_total0504.db";
//		db_name = "temp0509.db";
//		ResultSet resultSet2 =  queryExampleResult2(2, 72);
		ResultSet resultSet2 =  queryExampleResult();
//		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);

		// a distance function using the spherical law of cosines

		
		
//				ResultSet resultSet2 =  queryExampleResultbyID("grammar_swift_stat", 3887);
		//	ResultSet resultSet2 =  queryExampleResultbyExample("grammar_swift_stat", "@Override    protected float[] getMarkerPosition(Highlight high) {        return new float[]{high.getDrawY(), high.getDrawX()};    }");
		int num 	= 0;
		int count 	= 0;
		int cycle 	= 6;

		
		ResultSet resultSet3 = queryTemplateByExampleID(3);
		
		System.out.println("\n\n");
		while(resultSet3.next()){
			templateOk = false;
			flip = true;
			double c1 = 1.0000, c2 = 1.0001;

			num++;
//			String aaa = resultSet2.getString("template");
//			String bbb = resultSet2.getString("example");
			String aaa="", bbb="";
			String t_a = resultSet3.getString("template");
			String t_s  =resultSet3.getString("example");
			System.out.println("t_a\n"+t_a+"\n"+t_s);
		}
		
		while(resultSet2.next()){
			templateOk = false;
			flip = true;
			double c1 = 1.0000, c2 = 1.0001;

			num++;
			String aaa = resultSet2.getString("template");
			String bbb = resultSet2.getString("example");
//			String aaa="", bbb="";
			atype = Integer.parseInt(resultSet2.getString("AST_TYPE"));
			stype = Integer.parseInt(resultSet2.getString("PARENT_TYPE"));
			
//			aaa = "mAnimator.getPhaseY() "; bbb="dataSet.barBorderWidth"; <arg0> . <arg1> ( )  , <arg1> . <arg0>
//			aaa = "mXBounds.range+mXBounds.min-z"; bbb="_xBounds.range+_xBounds.min-z";
//			aaa = "ranges.length - 1, 0"; bbb="ranges!.count - 1, 0";
//			aaa = ""
//			atype = 70; stype=21;
//			aaa ="switch (shape) \n{"+
//            "case SQUARE: return new SquareShapeRenderer();}";
			
//			bbb="switch shape \n{"+
//					"case .Square: return SquareShapeRenderer()"+
//        "};";
//			System.out.println();
//			aaa = "public class ChartXAxisRendererRadarChart extends ChartXAxisRenderer {\nppublic RadarChartView chart;\n}";
//			bbb = "public class ChartXAxisRendererRadarChart: ChartXAxisRenderer {\npublic weak var chart: RadarChartView?\n}";
//			idd = Integer.parseInt(resultSet2.getString("ID"));
			int ccc = Integer.parseInt(resultSet2.getString("c"));
			count = count + ccc;

//			atype=3;
//			stype=130;
			antlr_parsers.swiftparser.SwiftLexer lexer1 = new antlr_parsers.swiftparser.SwiftLexer((CharStream)new ANTLRInputStream(bbb));
			CommonTokenStream swiftCommonStream = new CommonTokenStream(lexer1);
			antlr_parsers.swiftparser.SwiftParser parser = new antlr_parsers.swiftparser.SwiftParser(swiftCommonStream);


			JavaLexer lexerj = new JavaLexer((CharStream)new ANTLRInputStream(aaa));
			CommonTokenStream javaCommonStream = new CommonTokenStream(lexerj);
			JavaParser parserj = new JavaParser(javaCommonStream);
			ParserRuleContext com = null, comj=null;

			Class<? extends Parser> jparserClass = null, sparserClass = null;
			ClassLoader cl = Thread.currentThread().getContextClassLoader();
			jparserClass = cl.loadClass("javaparser.JavaParser").asSubclass(Parser.class);
			sparserClass = cl.loadClass("swiftparser.SwiftParser").asSubclass(Parser.class);

			try {
				Method jstartRule = null, sstartRule =null;

				/**
				 * get ParserRuleContexts by type ids
				 */				
				jstartRule = jparserClass.getMethod(JavaParser.ruleNames[atype]);
				sstartRule = sparserClass.getMethod(SwiftParser.ruleNames[stype]);
				if(atype!=0 && stype!=0)
				System.out.println(ccc+"  "+JavaParser.ruleNames[atype]+" | "+SwiftParser.ruleNames[stype]+" | "+ccc+" | "+atype+" |"+stype+"|"+aaa+"|"+bbb);
//				System.out.println(JavaParser.ruleNames[atype]+" | "+SwiftParser.ruleNames[stype]+" | "+ccc+" | "+atype+" |"+stype);

				//				System.out.println();
			
				

			} catch (NoSuchMethodException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

		}

		System.out.println(num);
		System.out.println(count);

		
//		String str="\\s*(.+?)(\s*(.+?)[] \s*(.+?))\s*(\{(.?|\n)+\})";
		
//		connection.close();S

	}

}
