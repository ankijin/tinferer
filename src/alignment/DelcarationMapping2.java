package alignment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.simmetrics.StringMetric;
//import org.simmetrics.StringMetrics;
import org.simmetrics.metrics.StringMetrics;

import alignment.common.HungarianAlgorithm;
import antlr_parsers.java8.Java8BaseListener;
import antlr_parsers.java8.Java8Parser.FieldDeclarationContext;
import antlr_parsers.java8.Java8Parser.VariableDeclaratorIdContext;
import antlr_parsers.javaparser.JavaBaseListener;
import antlr_parsers.javaparser.JavaLexer;
import antlr_parsers.javaparser.JavaParser;
import antlr_parsers.javaparser.JavaParser.ClassBodyDeclarationContext;
import j2swift.Java8Parser.MethodDeclarationContext;
import j2swift.Java8Parser.VariableDeclaratorContext;
import j2swift.Java8Parser.VariableDeclaratorListContext;
import migration.BaseSwiftRecorder;
import migration.StructureMappingAntlr4;
import migration.SwiftLexer;
import migration.SwiftParser;
import migration.VariableDeclarion;
import migration.SwiftParser.Function_nameContext;
import migration.SwiftParser.Identifier_patternContext;
import migration.SwiftParser.Pattern_initializerContext;
import migration.SwiftParser.Top_levelContext;
import migration.SwiftParser.Variable_declarationContext;
import migration.SwiftParser.Variable_nameContext;
import sqldb.DBTablePrinter;
import sqldb.SQLiteJDBC;

public class DelcarationMapping2 {
	public static CommonTokenStream swiftcommon=null;
	public static CommonTokenStream androidcommon=null;
	public static String androidpath=null;
	
	public static <T> Map<String, ParserRuleContext> getVarDeclSwift(String path) throws FileNotFoundException, IOException, IndexOutOfBoundsException{
		Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();

		SwiftParser.Top_levelContext root1 = null;
		try{

			ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
			Lexer lexer1 = new SwiftLexer((CharStream)stream1);
			swiftcommon= new CommonTokenStream(lexer1);
			SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
			root1 = parser1.top_level();
		} catch (Exception e){
			//			return nodeSwift;
		}
		BaseSwiftRecorder record= new BaseSwiftRecorder(){
			Variable_declarationContext var_decl;
			@Override
			public Object visitVariable_declaration(Variable_declarationContext ctx) {
				// TODO Auto-generated method stub
				var_decl=ctx;


				

				if(ctx.pattern_initializer_list()!=null&& !ctx.pattern_initializer_list().pattern_initializer().isEmpty()){
					List<Pattern_initializerContext> list = ctx.pattern_initializer_list().pattern_initializer();
					nodeSwift.put(list.get(0).pattern().identifier_pattern().getText(), var_decl);
				}
				return super.visitVariable_declaration(ctx);
			}


		};
		if(root1!=null)
			root1.accept(record);


		return nodeSwift;
	}

	public static <T> Map<String, ParserRuleContext> getVarDeclAndroid(String path) throws FileNotFoundException, IOException, IndexOutOfBoundsException{
		Map<String, ParserRuleContext> nodeAndroid = new HashMap<String, ParserRuleContext>();

		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new JavaLexer((CharStream)stream1);
		androidcommon= new CommonTokenStream(lexer1);
		JavaParser parser = new JavaParser(new CommonTokenStream(lexer1));
		JavaParser.CompilationUnitContext com = parser.compilationUnit();
		ParseTreeWalker walker = new ParseTreeWalker();


		//		System.out.println("parser.compilationUnit()"+parser.compilationUnit().getText());
		//		walker.walk(listner, parser.compilationUnit());

		JavaBaseListener listener = new JavaBaseListener(){
			ParserRuleContext field; 
			
			@Override
			public void enterClassBodyDeclaration(ClassBodyDeclarationContext ctx) {
				// TODO Auto-generated method stub
				field = ctx;
				super.enterClassBodyDeclaration(ctx);
			}
			@Override
			public void enterFieldDeclaration(antlr_parsers.javaparser.JavaParser.FieldDeclarationContext ctx) {
				// TODO Auto-generated method stub
				
				List<antlr_parsers.javaparser.JavaParser.VariableDeclaratorContext> list = ctx.variableDeclarators().variableDeclarator();
			
//				VariableDeclaratorListContext list = ctx.variableDeclaratorList();
//				List<VariableDeclaratorContext> ll = list.variableDeclarator();
				if(!list.isEmpty() && field !=null){
					nodeAndroid.put(list.get(0).variableDeclaratorId().getText(), field);
				}
				
				super.enterFieldDeclaration(ctx);
			}
		};
		
		/*
		j2swift.Java8BaseListener listener = new j2swift.Java8BaseListener(){
			ParserRuleContext field; 
			@Override
			public void enterFieldDeclaration(j2swift.Java8Parser.FieldDeclarationContext ctx) {
				field = ctx;

				VariableDeclaratorListContext list = ctx.variableDeclaratorList();
				List<VariableDeclaratorContext> ll = list.variableDeclarator();
				if(!ll.isEmpty()){
					nodeAndroid.put(ll.get(0).variableDeclaratorId().getText(), field);
				}

				// TODO Auto-generated method stub
				super.enterFieldDeclaration(ctx);
			}

		};
		*/
		walker.walk(listener, com);

		return nodeAndroid;
	}


	public static <T> Map<String, ParserRuleContext> getMethodDeclAndroid(String path) throws FileNotFoundException, IOException, IndexOutOfBoundsException{
		Map<String, ParserRuleContext> nodeAndroid = new HashMap<String, ParserRuleContext>();

		/*
		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new j2swift.Java8Lexer((CharStream)stream1);
		androidcommon= new CommonTokenStream(lexer1);
		j2swift.Java8Parser parser = new j2swift.Java8Parser((new CommonTokenStream(lexer1)));
		j2swift.Java8Parser.CompilationUnitContext com = parser.compilationUnit();
		ParseTreeWalker walker = new ParseTreeWalker();
*/
		
		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new JavaLexer((CharStream)stream1);
		androidcommon= new CommonTokenStream(lexer1);
		JavaParser parser = new JavaParser(new CommonTokenStream(lexer1));
		JavaParser.CompilationUnitContext com = parser.compilationUnit();
		ParseTreeWalker walker = new ParseTreeWalker();
	
		JavaBaseListener listener = new JavaBaseListener(){
//			j2swift.Java8Parser.FieldDeclarationContext field; 
			ParserRuleContext cntx;
			@Override
			public void enterClassBodyDeclaration(ClassBodyDeclarationContext ctx) {
				// TODO Auto-generated method stub
				cntx = ctx;
				super.enterClassBodyDeclaration(ctx);
			}
			@Override
			public void enterMethodDeclaration(antlr_parsers.javaparser.JavaParser.MethodDeclarationContext ctx) {
				// TODO Auto-generated method stub
//				ctx.m
//				;
				if(cntx!=null)
				nodeAndroid.put(ctx.Identifier().getText(), cntx);
				super.enterMethodDeclaration(ctx);
			}
			/*
			@Override
			public void enterMethodDeclaration(MethodDeclarationContext ctx) {
				// TODO Auto-generated method stub
				nodeAndroid.put(ctx.methodHeader().methodDeclarator().Identifier().getText(), ctx);

				super.enterMethodDeclaration(ctx);
			}
			*/
		};
		walker.walk(listener, com);

		return nodeAndroid;
	}


	public static Map<String, String> mappingVarDec(Map<String, ParserRuleContext> nodeAndroid, Map<String, ParserRuleContext> nodeSwift, boolean toPrint, String pp){

		//		alignment of VariableDeclartion
		Object[] keyA =  nodeAndroid.keySet().toArray();
		Object[]  keyS = nodeSwift.keySet().toArray();

		Map<String, String> hmapping = new HashMap<String, String>();
		StringMetric metric = StringMetrics.jaroWinkler();
		//		StringMetrics.jaroWinkler();
		float max_score =-1;
		//		System.out.println(pp);
		double[][] costMatrix = new double[keyA.length][keyS.length];
		//		if(keyA.length>=keyS.length){

		for(int i=0; i< keyA.length;i++){
			for(int j=0; j< keyS.length;j++){
				float score = metric.compare(keyA[i].toString(), keyS[j].toString());
				costMatrix[i][j]=1-score; //same: 1-1
			}
		}

		int[] result =  new int[0];
		if(keyA.length >0 &&  keyS.length>0){
			HungarianAlgorithm hung = new HungarianAlgorithm(costMatrix);
			result = hung.execute();
		}
		for(int p=0; p<result.length;p++){
			if(result[p]!=-1){
				if(costMatrix[p][result[p]] < 0.2){
					//					System.err.println(android_files.get(p).getName()+", "+swift_files.get(result[p]).getName());
					hmapping.put(keyA[p].toString(), keyS[result[p]].toString());
				}
			}
			//			System.out.println(p+",, "+result[p]);
			//			System.out.println(android_files.get(result[p])+", "+swift_files.get(p));
		}

		System.out.println("hmapping"+hmapping);


		//		}
		return hmapping;
	}

	public static <T> Map<String, ParserRuleContext> getMethodDeclSwift(String path) throws FileNotFoundException, IOException{

		Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();

		SwiftParser.Top_levelContext root1;
		try{

			ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
			Lexer lexer1 = new SwiftLexer((CharStream)stream1);
			SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
			root1 = parser1.top_level();
		} catch (Exception e){
			return nodeSwift;
		}
		
		VariableDeclarion<T> visitor = new VariableDeclarion<T>(nodeSwift){
			@Override
			public T visitFunction_name(SwiftParser.Function_nameContext ctx) {
				nodeSwift.put(ctx.getText(), ctx.getParent());
				return super.visitFunction_name(ctx);
			}

			@Override
			public T visitVariable_name(SwiftParser.Variable_nameContext ctx) {
				// TODO Auto-generated method stub

				nodeSwift.put(ctx.getText(), ctx.getParent());
				return super.visitVariable_name(ctx);
			}	
		};
		root1.accept(visitor);
		return nodeSwift;
	}

	public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {
		// TODO Auto-generated method stub

		Class.forName("org.sqlite.JDBC");
		Connection connection = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		connection.setAutoCommit(true);
		Statement stmt = connection.createStatement();
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift"));

		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroid());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwift());


		alignment.common.Filewalker fw = new alignment.common.Filewalker("testsrc", "mcharts/Charts-master");
		Map<File, File> classmap = fw.getClassMapping();
		alignment.common.Filewalker.staticPrintMapping(classmap);

		Map<String, ParserRuleContext> varDeclSwift 	= null;
		Map<String, ParserRuleContext> varDeclAndroid 	= null;
		Map<String, ParserRuleContext> methodDecliOS 	= null;
		Map<String, ParserRuleContext> methodDeclAndroid 	= null;

		//get variable and method declarations class by class
		Iterator<File> a_classes = classmap.keySet().iterator();
		int b =0;
		while(a_classes.hasNext()){
			//			Math.max(a, b);
			b++;
			//						if(b==5) break;

			File aclass_path = a_classes.next();
			File sclass_path = classmap.get(aclass_path);
			androidpath = aclass_path.getAbsolutePath();
//			if(aclass_path.getName().equals("ViewPortHandler.java")){
			if(1==1){
				//			get variable declarations of aligned class by brief visit of ASTs
				varDeclSwift   = getVarDeclSwift(sclass_path.getAbsolutePath());
				varDeclAndroid = getVarDeclAndroid(aclass_path.getAbsolutePath());
				methodDecliOS 	= getMethodDeclSwift(sclass_path.getAbsolutePath());
				methodDeclAndroid = getMethodDeclAndroid(aclass_path.getAbsolutePath());

				Map<String, String> varmap = mappingVarDec(varDeclAndroid, varDeclSwift, true, "");
				Map<String, String> methodmap = mappingVarDec(methodDeclAndroid, methodDecliOS, true, "");
				System.out.println("varmap"+varmap);
				System.out.println("methodmap"+methodmap);
				Iterator<String> iterator_varDec = varmap.keySet().iterator();
				while(iterator_varDec.hasNext()){
					String key_a=iterator_varDec.next();
					//					varDeclAndroid.get(key_a);
					//					varDeclSwift.get(varmap.get(key_a));
					StructureMappingAntlr4 structure = new StructureMappingAntlr4(varDeclAndroid.get(key_a), varDeclSwift.get(varmap.get(key_a)));
					structure.compute();
				}//iteration of aligned variable declarations

				
				Iterator<String> iterator_methodDec = methodmap.keySet().iterator();
				while(iterator_methodDec.hasNext()){
					String key_a=iterator_methodDec.next();
					//					varDeclAndroid.get(key_a);
					//					varDeclSwift.get(varmap.get(key_a));
					StructureMappingAntlr4 structure = new StructureMappingAntlr4(methodDeclAndroid.get(key_a), methodDecliOS.get(methodmap.get(key_a)));
					structure.compute();
	
				}//iteration of aligned variable declarations`

				
				
//				StructureMappingAntlr4 structure1 = new StructureMappingAntlr4();
				
				
				DBTablePrinter.printTable(connection, "grammar_android");
				DBTablePrinter.printTable(connection, "grammar_swift");
			
			}

			//				StructureMappingAntlr4 structure = new StructureMappingAntlr4();


		}
		//		}
		//				System.err.println(varDeclSwift);
		//				System.out.println(varDeclAndroid);
		//				System.err.println(methodDecliOS);
		//				System.err.println(methodDeclAndroid);


		connection.close();


	}

}
