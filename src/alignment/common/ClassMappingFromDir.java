package alignment.common;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.simmetrics.StringMetric;
import org.simmetrics.metrics.StringMetrics;
//import org.simmetrics.StringMetrics;

public class ClassMappingFromDir {
	String _folder_a;
	String _folder_s;
	public ClassMappingFromDir(String folder_a, String folder_s){

		_folder_a = folder_a;
		_folder_s = folder_s;

	}

	//loop directory to get file list
	public Map<File, File> getClassMapping() throws IOException{
		File android_dirs = new File(_folder_a);
		String android_dirPath = android_dirs.getCanonicalPath();
		File android_root = new File(android_dirPath);
		File[] android_files = android_root.listFiles ( );
		String android_filePath = null;



		File swift_dirs = new File(_folder_s);
		String swift_dirPath = swift_dirs.getCanonicalPath();
		File swift_root = new File(swift_dirPath);
		File[] swift_files = swift_root.listFiles ( );
		String swift_filePath = null;


		Map<File, File> classmapping = new HashMap<File, File>();

		StringMetric metric = StringMetrics.cosineSimilarity();

//		StringMetrics.diceSimilarity();

		float max_score =-1;




		for (int i=0;i< android_files.length;i++ ) {
			android_filePath = android_files[i].getAbsolutePath();

			if(android_files[i].isFile()){
				//				parse(readFileToString(android_filePath));

				for (int j=0;j< swift_files.length;j++ ) {
					swift_filePath = swift_files[j].getAbsolutePath();

					if(swift_files[j].isFile()){
						//						parseSwift(swift_filePath);
						//						System.out.println(android_files[i].getName().split(".java")[0]);
						//						System.out.println(swift_files[j].getName().split(".swift")[0]);
						String a = android_files[i].getName().split(".java")[0];
						String s = swift_files[j].getName().split(".swift")[0];
						//						String[] bb = "ViewPortHandler.java".split(".java");
						//						System.err.println(bb[0]);

						float score = metric.compare(a, s);

						if (score > max_score && score > 0.5) {
							max_score = score;
							classmapping.put(android_files[i], swift_files[j]);
						}


					}
				}

				max_score = -1;


			}
		}

		return classmapping;
	}

	public static void staticPrintMapping(Map<File, File> fmapping){

		System.out.println("class mapping");

		Iterator<File> classmap = fmapping.keySet().iterator();
		while(classmap.hasNext()){

			File aclass_path = classmap.next();
			File sclass_path =fmapping.get(aclass_path);
			System.out.println(aclass_path.getName()+":"+sclass_path.getName());
		}
	}
	public static void main(String[] args) throws IOException{
		ClassMappingFromDir classmapper = new ClassMappingFromDir("facebook/facebook-android-sdk-master", "facebook/facebook-sdk-swift-master");
		Map<File, File> classmap = classmapper.getClassMapping();
		ClassMappingFromDir.staticPrintMapping(classmap);
	}


}
