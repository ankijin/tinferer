package alignment.common;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.TerminalNode;

import alignment.ruleinfer.StructureMappingAntlr4_for_stmt;
import migration.StructureMappingAntlr4;

public class CommonOperators implements Comparable<CommonOperators>{
	public String text;
	public Interval interval;
	public List<Interval> ops_intervals;
	public Interval opi;
	public int depth;
	//	public int opcount;
	public static Map<String, Integer> op_encoding = new HashMap<String, Integer>();
	
	
	public static void init(){
		
		//mutation && cross-over
		/** Level 1: Function call, scope, array/member access*/
		op_encoding.put("[", 1); op_encoding.put("]", 1);
		op_encoding.put("(", 1); op_encoding.put(")", 1);
		op_encoding.put(".", 1); op_encoding.put("->", 1); op_encoding.put("::", 1);
		
		/** Level 2: most) unary operators, sizeof and type casts (right to left)*/
		op_encoding.put("++", 2); op_encoding.put("--", 2); op_encoding.put("!", 2); op_encoding.put("~", 2);

		/** Level 3: Multiplication, division, modulo*/		
		op_encoding.put("*", 3); op_encoding.put("/", 3);
		
		/** Level 4: Addition and subtraction*/		
		op_encoding.put("+", 4); op_encoding.put("-", 4);
		
		/** Level 6: Comparisons: less-than, ... <   <=   >   >= */		
		op_encoding.put("<", 6); op_encoding.put("<=", 6); op_encoding.put(">", 4); op_encoding.put(">=", 4);
		
		/** Level 7: Comparisons: equal and not equal */		
		op_encoding.put("==", 7); op_encoding.put("!=", 7);
		
		op_encoding.put("&&", 11);
		op_encoding.put("||", 12);
		op_encoding.put("=", 14);
		op_encoding.put(",", 15);
	}
	
	public CommonOperators(){
		text="";
		interval= new Interval(0,0);
	}
	public CommonOperators(String text, Interval interval){
		this.text 		= text;
		this.interval 	= interval;
		ops_intervals 	= new LinkedList<Interval>();
	}
	public CommonOperators(String text, Interval interval, int depth){
		this.text 		= text;
		this.interval 	= interval;
		this.depth 		= depth;
		ops_intervals 	= new LinkedList<Interval>();
	}
	public CommonOperators(String text, Interval interval, Interval opii){
		this.text = text;
		this.interval = interval;
		ops_intervals = new LinkedList<Interval>();
		opi = opii;
	}
	public boolean isNull(){
		return (interval==null && interval.length()==0);
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		//		return text.equals(((CommonOperators)obj).text);
		return text.equals(((CommonOperators)obj).text) && interval.equals(((CommonOperators)obj).interval);
		//		return interval.equals(((CommonOperators)obj).interval);
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return interval.a*interval.b+interval.a+interval.b;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "("+depth+")"+interval+":"+text;
	}
	public int opcount(){

		return text.length();
	}


	public static boolean hasPunc(String context, boolean isAndroid){
		if(context.length()==0) return false;
		String start="", end="";
		try{
			start = context.substring(0, 1);
			end = context.substring(context.length()-1, context.length());
		}catch (Exception e){
			System.err.println("hasPunc"+context);
		}

		if(isAndroid){

			//enclosed by ()
			if(context.startsWith("(") && context.endsWith(")")){
				System.out.println("java ()");;
				return true;
			}
			//			if(StructureMappingAntlr4.java_punctuations_list.contains(start)){
			//				
			//			}
			//method .. 
			else if(context.endsWith(")")){
				return false;
			}
			if(StructureMappingAntlr4_for_stmt.java_punctuations_list.contains(start) || StructureMappingAntlr4_for_stmt.java_punctuations_list.contains(end)){
				//				System.out.println("java_punctuations_list");
				return true;
			}
			else return false;
		}else{


			if(context.startsWith("(") && context.endsWith(")")){
				//				System.out.println("swift ()");
				return true;
			}
			else if(StructureMappingAntlr4_for_stmt.swift_punctuations_list.contains(start) && context.endsWith(")")){
				return true;
			}

			else if(context.endsWith(")")){
				return false;
			}

			if(StructureMappingAntlr4_for_stmt.swift_punctuations_list.contains(start)||StructureMappingAntlr4_for_stmt.swift_punctuations_list.contains(end))
				return true;


			else return false;

		}
	}


	public static boolean hasKeyword(String context, boolean isAndroid){
		if(context.length()==0) return false;
		String start="", end="";
		try{
			start = context.substring(0, 1);
			end = context.substring(context.length()-1, context.length());
		}catch (Exception e){
			System.err.println("hasKeywords"+context);
		}
		if(isAndroid){
			for(String k:StructureMappingAntlr4_for_stmt.java_keywords_list){
				if(context.startsWith(k))return true;
			}
			//			if(StructureMappingAntlr4.java_punctuations_list.contains(start) && StructureMappingAntlr4.java_punctuations_list.contains(end))
			//				return true;
			return false;
		}else{

			for(String k:StructureMappingAntlr4_for_stmt.swift_keywords_list){
				if(context.startsWith(k)){
					//					System.out.println("keywords "+k);
					return true;
				}
				if(context.endsWith(k)){
					//					System.out.println("keywords "+k);
					return true;
				}
			}
			return false;

		}
	}

	@Override
	public int compareTo(CommonOperators o) {
		// TODO Auto-generated method stub
		if(this.depth <o.depth) return -1; 
		else if(this.depth ==o.depth) return 0; 
		else return 1;
	}

	public static String [] t1ops={"[","]"};
	public static String [] t4ops={"+","-"};
	public static List<String> t1ops_list=Arrays.asList(t1ops);
	public static List<String> t4ops_list=Arrays.asList(t4ops);

	
	public int compareOp(String op1, String op2){
		if(t1ops_list.contains(op1) && t1ops_list.contains(op2)){
			return 0;
		}
		if(t4ops_list.contains(op1) && t4ops_list.contains(op2)){
			return 0;
		}
		if(t1ops_list.contains(op1) && t4ops_list.contains(op2)){
			return 1;
		}
		if(t1ops_list.contains(op1) && t4ops_list.contains(op2)){
			return 1;
		}
		return 0;
	}
	//-1
	//0 both
	//1 aop
	//2 sop


	public static int determineGoDownbyNum(OpProperty aop, OpProperty sop){


		String atext ="", stext="";
		if(aop.node instanceof ParserRuleContext){
			atext =	((ParserRuleContext)aop.node).start.getInputStream().getText(((ParserRuleContext)aop.node).getSourceInterval());
			//			atext = ((ParserRuleContext)aop.node).getText();
		}else{
			atext = ((TerminalNode)aop.node).getText();
		}

		if(sop.node instanceof ParserRuleContext){
			//			stext = ((ParserRuleContext)sop.node).getText();
			stext = ((ParserRuleContext)sop.node).start.getInputStream().getText(((ParserRuleContext)sop.node).getSourceInterval());
		}else{
			stext = ((TerminalNode)sop.node).getText();
		}

		//		atext = aop.getxt;
		//		stext = sop.getxt;

		//		if(hasPunc(aop.getxt, true) && hasPunc(sop.getxt, true)) return null;
		if(hasPunc(atext, true) && !hasPunc(stext, false)){
			return 1;
		}
		else if(!hasPunc(atext, true) && hasPunc(stext, false)){
			return 2;
		}
		else if(hasPunc(atext, true) && hasPunc(stext, false)){
			System.out.println("else if(hasPunc(atext, true) && hasPunc(stext, false))");
			return 0;
		}


		else if(!hasPunc(atext, true) && !hasPunc(stext, false)){
			//			System.out.println("else if(hasPunc(atext, true) && hasPunc(stext, false))");
			return -1;
		}


		else if(hasKeyword(atext, true) && !hasKeyword(stext, false)){
			return 1;
		}
		else if(!hasKeyword(atext, true) && hasKeyword(stext, false)){
			return 2;
		}

		else if(hasKeyword(atext, true) && hasKeyword(stext, false)){
			System.out.println("hasKeyword(atext, true) && hasKeyword(stext, false)");
			return 0;
		}
		else if(!hasKeyword(atext, true) && !hasKeyword(stext, false)){
			//			System.out.println("hasKeyword(atext, true) && hasKeyword(stext, false)");
			return -1;
		}

		//		else if(hasPunc(atext, true) && hasPunc(stext, false)){
		//			return 0;
		//		}

		//		else if(atext.contains(" ") && stext.contains(" ")){
		//			return 0;
		//		}

		else return -1;
	}


	public static int determineGoDownbyNum(String aop, String sop){


		String atext ="", stext="";


		atext = aop;
		stext = sop;

		if((atext.startsWith("(") && atext.endsWith(")")) && stext.startsWith("!")){
			return 1;


		}


		//		if(hasPunc(aop.getxt, true) && hasPunc(sop.getxt, true)) return null;
		if(hasPunc(atext, true) && !hasPunc(stext, false)){
			return 1;
		}
		else if(!hasPunc(atext, true) && hasPunc(stext, false)){
			return 2;
		}
		else if(hasPunc(atext, true) && hasPunc(stext, false)){
			System.out.println("rrr");
			return 0;
		}

		else if(hasKeyword(atext, true) && !hasKeyword(stext, false)){
			return 1;
		}
		else if(!hasKeyword(atext, true) && hasKeyword(stext, false)){
			//			System.out.println("swift has keywords");
			return 2;
		}

		else if(hasKeyword(atext, true) && hasKeyword(stext, false)){
			System.out.println("aaaa");
			return 0;
		}

		else if(hasPunc(atext, true) && hasPunc(stext, false)){
			return 0;
		}
		//		else if(atext.contains(" ") && stext.contains(" ")){
		//			return 2;


		//		}

		//		else if(atext.contains(" ") && !stext.contains(" ")){
		//			return 1;


		//		}
		//		else if(!atext.contains(" ") && stext.contains(" ")){
		//			return 0;


		//		}


		//		else if(atext.contains(" ") && stext.contains(" ")){
		//			return 0;
		//		}

		return -1;
	}



	public static OpProperty determineGoDown(OpProperty aop, OpProperty sop){
		String atext ="", stext="";
		if(aop.node instanceof ParserRuleContext){
			atext =	((ParserRuleContext)aop.node).start.getInputStream().getText(((ParserRuleContext)aop.node).getSourceInterval());
			//			atext = ((ParserRuleContext)aop.node).getText();
		}else{
			atext = ((TerminalNode)aop.node).getText();
		}

		if(sop.node instanceof ParserRuleContext){
			//			stext = ((ParserRuleContext)sop.node).getText();
			stext = ((ParserRuleContext)sop.node).start.getInputStream().getText(((ParserRuleContext)sop.node).getSourceInterval());
		}else{
			stext = ((TerminalNode)sop.node).getText();
		}

		//		if(hasPunc(aop.getxt, true) && hasPunc(sop.getxt, true)) return null;
		if(hasPunc(atext, true) && !hasPunc(stext, false)){
			return aop;
		}
		else if(!hasPunc(atext, true) && hasPunc(stext, false)){
			return sop;
		}
		if(hasKeyword(atext, true) && !hasKeyword(stext, false)){
			return aop;
		}
		else if(!hasKeyword(atext, true) && hasKeyword(stext, false)){
			return sop;
		}

		return null;
	}

	//-1
	//0 both
	//1 aop
	//2 sop
	public static void main(String args[]){// 
		System.out.println(CommonOperators.determineGoDownbyNum("(labelCount == 0 || range <= 0)", "(labelCount == 0 || range <= 0)"));
		//		eq_size decision:2   5..9:mXBounds.min + 1   7..13:_xBounds.min + 1
		//		System.out.println("public_xBounds.min".startsWith("public"));
	}


}
