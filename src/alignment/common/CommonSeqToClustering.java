package alignment.common;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.misc.Interval;
//import org.apache.commons.collections4.ListUtils;

public class CommonSeqToClustering {

	
	/**
	 * commonSequence Operators and clustering
	 * @param lefttoright
	 * @param termsMap
	 * @return
	 */
	public static List<CommonOperators> clustering(List<CommonOperators> lefttoright, Map<Interval, String> termsMap){
		
		Interval last = null;
		String ops_android = "";
		int j=0;
		List<CommonOperators> newlefttoright = new LinkedList<CommonOperators>();
		Map<Interval, String> terms = termsMap;
		List<Interval> list = new ArrayList<Interval>(terms.keySet());
		
		for(CommonOperators c:lefttoright){

			int i=list.indexOf(c.interval);
			if(i>0){
				//						if(){

				//						}
				if(last==null) last = list.get(i-1);
				//						System.out.println("last1	"+Math.abs(last.a-list.get(i-1).a));
				if(Math.abs(last.b-list.get(i-1).a) <2){
					//update last
					last=last.union(list.get(i-1));
					//							System.out.println("last1	"+last);
				} else{
					newlefttoright.add(new CommonOperators(ops_android, last));
					last = list.get(i-1);
					ops_android ="";
					//							System.out.println("last1<	"+last);
				}
				//						newlefttoright.add(new CommonOperators(terms.get(list.get(i-1)), list.get(i-1))); //left
				//						last  = list.get(i-1);


				if(Math.abs(last.b-list.get(i).a) <2){
					last=last.union(list.get(i));
					ops_android +=terms.get(list.get(i));
					//						System.out.println("last2	"+last);
				} else{
					newlefttoright.add(new CommonOperators(ops_android, last));
					ops_android ="";
					last = list.get(i);
					//						System.out.println("last2<	"+last);
				}

				if(list.size()>i+1 && Math.abs(last.b-list.get(i+1).a) <2){last=last.union(list.get(i+1));
				} else{
					newlefttoright.add(new CommonOperators(ops_android, last));
					ops_android ="";
					if(list.size()>i+1)
					last = list.get(i+1);
				}

				j++;
				
				if(j==lefttoright.size()){
					newlefttoright.add(new CommonOperators(ops_android, last));
				}
			}
		}
		
		return newlefttoright;
	}
	

	
	public static String opText(List<CommonOperators> list){
		String ops="";
		for(CommonOperators op:list){
			ops+=op.text;
		}
		return ops;
	}
	
	public static void main(String [] args){
		List<CommonOperators> l1 = new LinkedList<CommonOperators>();
		List<CommonOperators> l2 = new LinkedList<CommonOperators>();
		l1.add(new CommonOperators(".", new Interval(1,1)));
		l1.add(new CommonOperators("+", new Interval(2,2)));
		l1.add(new CommonOperators(".", new Interval(3,3)));
		l1.add(new CommonOperators("+", new Interval(4,4)));
		l1.add(new CommonOperators(".", new Interval(5,5)));
		int c = 100;
		l2.add(new CommonOperators(".", new Interval(1+c,1+c)));
		l2.add(new CommonOperators("+", new Interval(2+c,2+c)));
		l2.add(new CommonOperators(".", new Interval(3+c,3+c)));
		l2.add(new CommonOperators("+", new Interval(4+c,4+c)));
		l2.add(new CommonOperators(".", new Interval(5+c,5+c)));
		l2.add(new CommonOperators("+", new Interval(6+c,6+c)));
		l2.add(new CommonOperators(".", new Interval(7+c,7+c)));
		
		
		
//		System.out.println(ListUtils.longestCommonSubsequence("eatsleepnightxyz", "eatQQQQQsleepabcxyz"));
//		System.out.println(ListUtils.longestCommonSubsequence(l1,l2));
//		List<CommonOperators> l3=ListUtils.longestCommonSubsequence(l2,l1);
//		System.out.println(ListUtils.longestCommonSubsequence(l3,l2));
		
	}
}
