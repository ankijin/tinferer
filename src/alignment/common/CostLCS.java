package alignment.common;

import org.antlr.v4.runtime.misc.Interval;

public class CostLCS {
	public double cost;
	public Interval minIntvlA, minItvlS;
	
	public CostLCS(double c, Interval a, Interval s){
		cost =c ;
		minIntvlA = a;
		minItvlS = s;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return minIntvlA.toString()+"->>>"+minItvlS.toString();
	}
}
