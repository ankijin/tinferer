package alignment.common;

//import migration.BuildBindingTableForSwift.NumLine;

public class NumLine{
	public int line;
	public int column;
	public NumLine(int l, int c){
		line 	= l;
		column 	= c;
	}
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		NumLine num=(NumLine) obj;
		return line==num.line && column ==num.column;
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return line+line*column;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return line+":"+column;
	}
}
