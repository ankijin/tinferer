

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
//import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
//import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
//import java.util.List;
import java.util.Map;
//import java.util.Map.Entry;
//import java.util.Set;
import java.util.Set;
import java.util.Vector;

//import javax.lang.model.type.DeclaredType;

//import org.abego.treelayout.NodeExtentProvider;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.simmetrics.StringMetric;
import org.simmetrics.StringMetrics;
import org.stringtemplate.v4.ST;

import com.github.gumtreediff.tree.ITree;

import gumtree_tester.SwiftBaseListener;
import gumtree_tester.SwiftLexer;
import gumtree_tester.SwiftParser;
import mapping.MethodStructAndroid;
import mapping.MethodStructSwift;
import recording.MappingElement;
import recording.RecordVarDeclAndroid;
//import SwiftParser.Variable_declarationContext;
//import SwiftParser.StatementContext;
//import SwiftParser.StatementsContext;
//import SwiftParser.ExpressionContext;
//import SwiftParser.Constant_declarationContext;
//import SwiftParser.Function_declarationContext;
//import SwiftParser.If_statementContext;
//import SwiftParser.Return_statementContext;
//import SwiftParser.Function_declarationContext;
//import SwiftParser.StatementContext;
//import SwiftParser.Constant_declarationContext;
//import SwiftParser.If_statementContext;
//import SwiftParser.Return_statementContext;
//import SwiftParser.Variable_nameContext;
//import SwiftParser.Return_statementContext;
//import SwiftParser.Variable_declarationContext;
//import SwiftParser.Variable_declarationContext;
//import SwiftParser.Return_statementContext;
//import SwiftParser.Variable_declarationContext;
import sqldb.DBTablePrinter;
import sqldb.SQLiteJDBC;
import templates.ConTextTemplate;





public class MainMappingClass {
	static ASTParser parser;
	static Map<String, String> mappingKeysVarDecl = new HashMap<String, String>();
	static Map<MappingElement, MappingElement> mappingElements = new HashMap<MappingElement, MappingElement>();
	static Map<Integer, Integer> mapStatement = new HashMap<Integer, Integer>();
	
	public MainMappingClass(){
		//return
		mapStatement.put(41, 36);
		//expr, constant
		//
		//ExpressionStatement
		/**   
  			Matrix save=new Matrix();
  			save.set(mMatrixTouch);
  			save.setScale(scaleX,scaleY,x,y);
  			return save;
		  **/
		/**  
				var matrix = _touchMatrix
        		matrix.a = 1.0
        		matrix.d = 1.0
        		matrix = CGAffineTransformTranslate(matrix, x, y)
        		matrix = CGAffineTransformScale(matrix, scaleX, scaleY)
        		matrix = CGAffineTransformTranslate(matrix, -x, -y)
		 
		 
		 */
		mapStatement.put(21, 68);
		//21, 41
		//68, 36
		mapStatement.put(25, 18);
		//60, 180
		//VariableDeclarationStatement60
		mapStatement.put(21, 180);
	}
	
	public static boolean hasSameStructure(ArrayList<Integer> android, ArrayList<Integer> swift){
		if(android.size()!=swift.size()) return false;
		ArrayList<Integer> mappedSwift = new ArrayList<Integer>();
		for(int i=0; i< android.size();i++){
			if(mapStatement.containsKey(android.get(i))){
				mappedSwift.add(mapStatement.get(android.get(i)));
			}else{
				mappedSwift.add(-1);
			}
		}
//		System.out.println("hasSameStructure"+android+swift+mappedSwift);
		return mappedSwift.containsAll(swift);
	}
	public static <T> Map<String, ParserRuleContext> getVarDeclSwift(String path) throws FileNotFoundException, IOException{
		Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();
		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new SwiftLexer((CharStream)stream1);
		SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
		SwiftParser.Top_levelContext root1 = parser1.top_level();

		VariableDeclarion<T> visitor = new VariableDeclarion<T>(nodeSwift);
		root1.accept(visitor);
		return nodeSwift;
	}

	public static <T> Map<String, ParserRuleContext> getMethodDeclSwift(String path) throws FileNotFoundException, IOException{
		Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();		
		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new SwiftLexer((CharStream)stream1);
		SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
//		if(parser1.top_level() ==null){return }
		SwiftParser.Top_levelContext root1 = parser1.top_level();

		VariableDeclarion<T> visitor = new VariableDeclarion<T>(nodeSwift){
			@Override
			public T visitPattern(SwiftParser.PatternContext ctx) {
				return visitChildren(ctx);
			}
			@Override
			public T visitFunction_declaration(SwiftParser.Function_declarationContext ctx) {	
				return visitChildren(ctx);
			}
			@Override
			public T visitFunction_name(SwiftParser.Function_nameContext ctx) {
				nodeSwift.put(ctx.getText(), ctx.getParent());
				return super.visitFunction_name(ctx);
			}

			@Override
			public T visitVariable_name(SwiftParser.Variable_nameContext ctx) {
				// TODO Auto-generated method stub

				nodeSwift.put(ctx.getText(), ctx.getParent());


				return super.visitVariable_name(ctx);
			}
		};
		root1.accept(visitor);
		return nodeSwift;
	}

	public static <T> Map<String, ParserRuleContext> getMethodVarDeclSwift2(String path) throws FileNotFoundException, IOException{
		Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();
		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new SwiftLexer((CharStream)stream1);
		SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
		SwiftParser.Top_levelContext root1 = parser1.top_level();

		SwiftBaseListener lst = new SwiftBaseListener(){

			@Override
			public void enterVariable_declaration(SwiftParser.Variable_declarationContext ctx) {
				// TODO Auto-generated method stub
				super.enterVariable_declaration(ctx);
			}

			@Override
			public void exitVariable_declaration(SwiftParser.Variable_declarationContext ctx) {
				// TODO Auto-generated method stub
				super.exitVariable_declaration(ctx);
			}

			@Override
			public void enterReturn_statement(SwiftParser.Return_statementContext ctx) {
				// TODO Auto-generated method stub
				super.enterReturn_statement(ctx);
			}
		};

		return nodeSwift;
	}

	public static <T> Map<String, ParserRuleContext> getMethodVarDeclSwift(String path) throws FileNotFoundException, IOException{
		Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();
		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new SwiftLexer((CharStream)stream1);
		SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
		SwiftParser.Top_levelContext root1 = parser1.top_level();

		VariableDeclarion<T> visitor = new VariableDeclarion<T>(nodeSwift){
			@Override
			public T visitPattern(SwiftParser.PatternContext ctx) {
				return visitChildren(ctx);
			}
			@Override
			public T visitFunction_declaration(SwiftParser.Function_declarationContext ctx) {	
				return visitChildren(ctx);
			}
			@Override
			public T visitFunction_name(SwiftParser.Function_nameContext ctx) {
				nodeSwift.put(ctx.getText(), ctx.getParent());
				return super.visitFunction_name(ctx);
			}

			@Override
			public T visitVariable_declaration(SwiftParser.Variable_declarationContext ctx) {
				// TODO Auto-generated method stub
				return super.visitVariable_declaration(ctx);
			}

			@Override
			public T visitReturn_statement(SwiftParser.Return_statementContext ctx) {
				// TODO Auto-generated method stub

				//				ctx.getParent().getParent().getParent().getParent().getParent().
				//ctx.getParent().getParent().getParent().getParent().getParent().Identifier().toString()
				return super.visitReturn_statement(ctx);
			}
		};
		root1.accept(visitor);
		return nodeSwift;
	}
	
	
	public static <T> Map<String, MethodStructSwift> getMethodVarDeclSwift3(String path) throws FileNotFoundException, IOException{
//		Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();
		Map<String, MethodStructSwift> node = new HashMap<String, MethodStructSwift>();
		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new SwiftLexer((CharStream)stream1);
		SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
		
//		try{
//		 parser1.top_level();
//		}catch(Exception e){
//			return node;
//		}
		
		SwiftParser.Top_levelContext root1 = parser1.top_level();
		
		SwiftBaseListener listener = new SwiftBaseListener(){
			

			@Override
			public void enterFunction_declaration(SwiftParser.Function_declarationContext ctx) {
				// TODO Auto-generated method stub
				ArrayList<Integer> list  = new ArrayList<Integer>();
				if(ctx.function_body()!=null && ctx.function_body().code_block()!=null && ctx.function_body().code_block().statements()!=null){
					 List<SwiftParser.StatementContext> stmts = ctx.function_body().code_block().statements().statement();
					 for(int i=0; i< stmts.size();i++){
						 SwiftParser.StatementContext stmt = stmts.get(i);
							if(stmt.expression()!=null){
								list.add(SwiftParser.RULE_expression);
//								System.err.println("RULE_expression"+SwiftParser.RULE_expression);
							}
							if(stmt.branch_statement() !=null && stmt.branch_statement().if_statement()!=null){
								list.add(SwiftParser.RULE_if_statement);
							}
							if(stmt.control_transfer_statement() !=null && stmt.control_transfer_statement().return_statement()!=null){
								list.add(SwiftParser.RULE_return_statement);
							}
							if(stmt.declaration() !=null && stmt.declaration().constant_declaration()!=null){
								list.add(SwiftParser.RULE_constant_declaration);
//								System.err.println("RULE_constant_declaration"+SwiftParser.RULE_constant_declaration);
							}
							if(stmt.declaration() !=null && stmt.declaration().variable_declaration()!=null){
								list.add(SwiftParser.RULE_variable_declaration);
//								System.err.println("RULE_variable_declaration"+SwiftParser.RULE_variable_declaration);
							}
					 }
				}
				node.put(ctx.function_name().getText(), new MethodStructSwift(ctx, list));
				super.enterFunction_declaration(ctx);
			}
			
			@Override
			public void enterVariable_declaration(SwiftParser.Variable_declarationContext ctx) {
				// TODO Auto-generated method stub
				if(ctx.code_block()!=null && ctx.code_block().statements()!=null){
					ArrayList<Integer> list  = new ArrayList<Integer>();
					List<SwiftParser.StatementContext> stmts = ctx.code_block().statements().statement();
					 for(int i=0; i< stmts.size();i++){
						 SwiftParser.StatementContext stmt = stmts.get(i);
							if(stmt.expression()!=null){
								list.add(SwiftParser.RULE_expression);
							}
							if(stmt.branch_statement() !=null && stmt.branch_statement().if_statement()!=null){
								list.add(SwiftParser.RULE_if_statement);
							}
							if(stmt.control_transfer_statement() !=null && stmt.control_transfer_statement().return_statement()!=null){
								list.add(SwiftParser.RULE_return_statement);
							}
							if(stmt.declaration() !=null && stmt.declaration().constant_declaration()!=null){
								list.add(SwiftParser.RULE_declaration);
							}
					 }
					 
					 node.put(ctx.variable_name().getText(), new MethodStructSwift(ctx, list));
				}
				super.enterVariable_declaration(ctx);
			}
		};
		

		ParseTreeWalker walker = new ParseTreeWalker();
		//			    AntlrDrinkListener listener = new AntlrDrinkListener();
		walker.walk(listener, root1);
		
		return node;
	}
	



	public static Map<String, ASTNode> getVarDeclAndroid(String str) {
		parser = ASTParser.newParser(AST.JLS8);
		parser.setSource(str.toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		Map<String, ASTNode> nodeAndroid = new HashMap<String, ASTNode>();
		final CompilationUnit cu = (CompilationUnit) parser.createAST(null);

		cu.accept(new ASTVisitor() {

			public boolean visit(VariableDeclarationFragment node) {
				if(node.getParent().getParent().getNodeType()==55){
					SimpleName name = node.getName();
					nodeAndroid.put(name.getIdentifier(), node.getParent());
				}
				return false; // do not continue 
			}


		});

		return nodeAndroid;

	}

	public static Map<String, ASTNode> getMethodDeclAndroid(String str) {
		parser = ASTParser.newParser(AST.JLS8);
		parser.setSource(str.toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		Map<String, ASTNode> nodeAndroid = new HashMap<String, ASTNode>();
		final CompilationUnit cu = (CompilationUnit) parser.createAST(null);

		cu.accept(new ASTVisitor() {

			public boolean visit(org.eclipse.jdt.core.dom.MethodDeclaration node) {
				nodeAndroid.put(node.getName().toString(), node);
				return false;
			}

		});
		return nodeAndroid;
	}
	
	public static Map<String, MethodStructAndroid> getMethodDeclAndroid2(String str) {
		parser = ASTParser.newParser(AST.JLS8);
		parser.setSource(str.toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		Map<String, MethodStructAndroid> nodeAndroid = new HashMap<String, MethodStructAndroid>();
		final CompilationUnit cu = (CompilationUnit) parser.createAST(null);
		
		cu.accept(new ASTVisitor() {

			@Override
			public boolean visit(MethodDeclaration node) {
				// TODO Auto-generated method stub
//				list.clear();
				ArrayList<Integer> list = new ArrayList<Integer>();
				if(node.getBody()==null || node.getBody().statements()==null) return false;
				List sts = node.getBody().statements();
				for(int i=0; i<sts.size();i++){
					if( sts.get(i) instanceof ReturnStatement){
//						System.err.println("ReturnStatement"+ReturnStatement.RETURN_STATEMENT);
						list.add(ReturnStatement.RETURN_STATEMENT);
					}
					if( sts.get(i) instanceof SingleVariableDeclaration){
//						System.err.println("SingleVariableDeclaration");
						list.add(SingleVariableDeclaration.SINGLE_VARIABLE_DECLARATION);
					}
					if( sts.get(i) instanceof ExpressionStatement){
//						System.err.println("ExpressionStatement"+ExpressionStatement.EXPRESSION_STATEMENT);
						list.add(ExpressionStatement.EXPRESSION_STATEMENT);
					}
					if( sts.get(i) instanceof IfStatement){
//						System.err.println("IfStatement");
						list.add(IfStatement.IF_STATEMENT);
					}
					if( sts.get(i) instanceof VariableDeclarationStatement){
//						System.err.println("IfStatement");
//						System.err.println("VariableDeclarationStatement"+VariableDeclarationStatement.VARIABLE_DECLARATION_STATEMENT);
						
						list.add(VariableDeclaration.VARIABLE_DECLARATION_STATEMENT);
					}
					if( sts.get(i) instanceof VariableDeclarationExpression){
//						System.err.println("IfStatement");
						list.add(VariableDeclaration.VARIABLE_DECLARATION_EXPRESSION);
					}
				}
				nodeAndroid.put(node.getName().toString(), new MethodStructAndroid(node, list));
				return false;
			}
		});
		return nodeAndroid;
	}

	
	

	//read file content into a string
	public static String readFileToString(String filePath) throws IOException {
		StringBuilder fileData = new StringBuilder(1000);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));

		char[] buf = new char[10];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
			buf = new char[1024];
		}

		reader.close();

		return  fileData.toString();	
	}

	public static Map<String, String> mapping(Map<String, String> nodeAndroid, Map<String, String> nodeSwift){

//		System.out.println("mapping"+nodeAndroid.toString() + nodeSwift.toString());
		//		alignment of VariableDeclartion
		Object[] keyA = nodeAndroid.keySet().toArray();
		Object[] keyS = nodeSwift.keySet().toArray();

		Map<String, String> fmapping = new HashMap<String, String>();

		StringMetric metric = StringMetrics.jaroWinkler();
		//		StringMetrics.jaroWinkler();
		float max_score =-1;
		//		System.out.println(pp);

		for(int i=0; i< keyA.length;i++){
			//			fmapping.put(keyA[i].toString(), keyS[0].toString());
			for(int j=0; j< keyS.length;j++){
				float score = metric.compare(keyA[i].toString(), keyS[j].toString());

				if (score > max_score && score > 0.8) {
					max_score = score;
					if(!fmapping.containsValue(keyA[i].toString()) && !fmapping.containsValue(keyS[j].toString()))
						fmapping.put(keyA[i].toString(), keyS[j].toString());
					//					System.out.println(keyA[i]+":"+keyS[j]+"="+score);
				}

			}
			max_score = -1;
		}
		//		System.out.println("fmapping"+fmapping);
		/*
		if(toPrint){
			if(fmapping.keySet().size()==0) System.out.println("\t->No vardeclaration matching");
			else{ System.out.println("\t->there are matching "+fmapping.keySet().size());}
			Iterator<String> it_f = fmapping.keySet().iterator();
			while(it_f.hasNext()){
				String key_a=it_f.next();
				FieldDeclaration aa = (FieldDeclaration) nodeAndroid.get(key_a);
				SwiftParser.ParserRuleContext ss = (SwiftParser.parse)nodeSwift.get(fmapping.get(key_a));
				//	System.out.println(nodeAndroid.get(key_a)+":"+((SwiftParser.Variable_declarationContext)nodeSwift.get(fmapping.get(key_a))).getText());

				System.out.println("\t"+aa.toString().replace("\n", "\n\t")+":"+ss.getText());

			}//iteration of aligned variable declarations


		}
		 */
		return fmapping;
	}

	
	public static Map<String, String> mappingMethodwo(Map<String, MethodStructAndroid> nodeAndroid, Map<String, MethodStructSwift> nodeSwift){

		//		alignment of VariableDeclartion
		Object[] keyA = nodeAndroid.keySet().toArray();
		Object[] keyS = nodeSwift.keySet().toArray();

		Map<String, String> fmapping = new HashMap<String, String>();

		StringMetric metric = StringMetrics.jaroWinkler();
		//		StringMetrics.jaroWinkler();
		float max_score =-1;
		//		System.out.println(pp);

		Vector<String> iii = new Vector<String>();
		String setting ="";
		for(int i=0; i< keyA.length;i++){
			//			fmapping.put(keyA[i].toString(), keyS[0].toString());
			for(int j=0; j< keyS.length;j++){
				float score = metric.compare(keyA[i].toString(), keyS[j].toString());

				MethodStructAndroid methodA = nodeAndroid.get(keyA[i]);
				MethodStructSwift methodS = nodeSwift.get(keyS[j]);
//				System.out.println("TEST"+hasSameStructure(methodA.statements, methodS.statements));
				
				
				if (!iii.contains(keyS[j].toString()) && score > max_score && score > 0.55 && hasSameStructure(methodA.statements, methodS.statements)) {
				
					max_score = score;
					if(!fmapping.containsValue(keyA[i].toString()) && !fmapping.containsValue(keyS[j].toString()))
						fmapping.put(keyA[i].toString(), keyS[j].toString());
					//					System.out.println(keyA[i]+":"+keyS[j]+"="+score);
					setting = keyS[j].toString();
				}
				
				iii.add(setting);

			}
			max_score = -1;
		}
		
		
		//		System.out.println("fmapping"+fmapping);
		/*
		if(toPrint){
			if(fmapping.keySet().size()==0) System.out.println("\t->No vardeclaration matching");
			else{ System.out.println("\t->there are matching "+fmapping.keySet().size());}
			Iterator<String> it_f = fmapping.keySet().iterator();
			while(it_f.hasNext()){
				String key_a=it_f.next();
				FieldDeclaration aa = (FieldDeclaration) nodeAndroid.get(key_a);
				SwiftParser.ParserRuleContext ss = (SwiftParser.parse)nodeSwift.get(fmapping.get(key_a));
				//	System.out.println(nodeAndroid.get(key_a)+":"+((SwiftParser.Variable_declarationContext)nodeSwift.get(fmapping.get(key_a))).getText());

				System.out.println("\t"+aa.toString().replace("\n", "\n\t")+":"+ss.getText());

			}//iteration of aligned variable declarations


		}
		 */
		return fmapping;
	}
	
	
	public static Map<String, String> mappingMethod(Map<String, MethodStructAndroid> nodeAndroid, Map<String, MethodStructSwift> nodeSwift){

		//		alignment of VariableDeclartion
		Object[] keyA = nodeAndroid.keySet().toArray();
		Object[] keyS = nodeSwift.keySet().toArray();
		
		Map<String, String> fmapping = new HashMap<String, String>();

		StringMetric metric = StringMetrics.jaroWinkler();
		//		StringMetrics.jaroWinkler();
		float max_score =-1;
		//		System.out.println(pp);
		Vector<String> iii = new Vector<String>();
		String setting ="";
		for(int i=0; i< keyA.length;i++){
			//			fmapping.put(keyA[i].toString(), keyS[0].toString());
			for(int j=0; j< keyS.length;j++){
				float score = metric.compare(keyA[i].toString(), keyS[j].toString());

				MethodStructAndroid methodA = nodeAndroid.get(keyA[i]);
				MethodStructSwift methodS = nodeSwift.get(keyS[j]);
//				System.out.println("TEST"+hasSameStructure(methodA.statements, methodS.statements));
				
				
				if (!iii.contains(keyS[j].toString()) && score > max_score && score > 0.55 && hasSameStructure(methodA.statements, methodS.statements)) {
				
					max_score = score;
					if(!fmapping.containsValue(keyA[i].toString()) && !fmapping.containsValue(keyS[j].toString()))
						fmapping.put(keyA[i].toString(), keyS[j].toString());
					//					System.out.println(keyA[i]+":"+keyS[j]+"="+score);
					setting = keyS[j].toString();
				}
				
				iii.add(setting);

			}
			max_score = -1;
		}
	
		return fmapping;
	}

	

	public static Map<String, String> mappingVarDec(Map<String, ASTNode> nodeAndroid, Map<String, ParserRuleContext> nodeSwift, boolean toPrint, String pp){

		//		alignment of VariableDeclartion
		Object[] keyA = nodeAndroid.keySet().toArray();
		Object[] keyS = nodeSwift.keySet().toArray();

		Map<String, String> fmapping = new HashMap<String, String>();

		StringMetric metric = StringMetrics.jaroWinkler();
		//		StringMetrics.jaroWinkler();
		float max_score =-1;
		//		System.out.println(pp);

		for(int i=0; i< keyA.length;i++){
			//			fmapping.put(keyA[i].toString(), keyS[0].toString());
			for(int j=0; j< keyS.length;j++){
				float score = metric.compare(keyA[i].toString(), keyS[j].toString());

				if (score > max_score && score > 0.55) {
					max_score = score;
					if(!fmapping.containsValue(keyA[i].toString()) && !fmapping.containsValue(keyS[j].toString()))
						fmapping.put(keyA[i].toString(), keyS[j].toString());
					//					System.out.println(keyA[i]+":"+keyS[j]+"="+score);
				}

			}
			max_score = -1;
		}
		
		
		//		System.out.println("fmapping"+fmapping);
		/*
		if(toPrint){
			if(fmapping.keySet().size()==0) System.out.println("\t->No vardeclaration matching");
			else{ System.out.println("\t->there are matching "+fmapping.keySet().size());}
			Iterator<String> it_f = fmapping.keySet().iterator();
			while(it_f.hasNext()){
				String key_a=it_f.next();
				FieldDeclaration aa = (FieldDeclaration) nodeAndroid.get(key_a);
				SwiftParser.ParserRuleContext ss = (SwiftParser.parse)nodeSwift.get(fmapping.get(key_a));
				//	System.out.println(nodeAndroid.get(key_a)+":"+((SwiftParser.Variable_declarationContext)nodeSwift.get(fmapping.get(key_a))).getText());

				System.out.println("\t"+aa.toString().replace("\n", "\n\t")+":"+ss.getText());

			}//iteration of aligned variable declarations


		}
		 */
		return fmapping;
	}

	public static void applyVarDeclRules(Map<MappingElement, MappingElement> mappingElements, Collection<ASTNode> statments){

		Iterator<ASTNode> iter = statments.iterator();

		while(iter.hasNext()){
			//			System.out.println(iter.next());
			ASTNode item = iter.next();
			if(item instanceof FieldDeclaration){
				RuleStatement rule1 = new RuleStatement();
				rule1.setMap(mappingElements);
				//				System.err.println(item.toString().replace("\n", ""));
				item.accept(rule1);
				System.err.print(item.toString());
				System.out.println(rule1.swiftstr);
			}
		}
	}

	static LinkedList<VarDeclElement> alist = new LinkedList<VarDeclElement>();
	static LinkedList<VarDeclElement> slist = new LinkedList<VarDeclElement>();

	static Map<String, VarDeclElement> amapping = new HashMap<String, VarDeclElement>();
	static Map<String, VarDeclElement> smapping = new HashMap<String, VarDeclElement>();

	public static void applyVarDeclRules2(Map<MappingElement, MappingElement> mappingElements, Collection<ASTNode> statments) throws ClassNotFoundException, SQLException{

		Iterator<ASTNode> iter = statments.iterator();


		//		Connection c = null;
		Connection connection = null;
		connection = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		connection.setAutoCommit(false);
		//		System.out.println("Opened database successfully");

		Statement stmt = null;
		//		Class.forName("org.sqlite.JDBC");
		//		connection = DriverManager.getConnection("jdbc:sqlite:test2.db");
		//		connection.setAutoCommit(false);
		//		System.out.println("Opened database successfully");

		//      System.out.println(insertTable(1, "_nameX","public","Rect","new Rect()"));

		stmt = connection.createStatement();




		//      System.out.println(insertTable(1, "_nameX","public","Rect","new Rect()"));

		stmt = connection.createStatement();

		stmt.executeUpdate(SQLiteJDBC.dropTable("VarDecl_Android1"));
		System.err.println(SQLiteJDBC.createTable("VarDecl_Android1"));
		stmt.executeUpdate(SQLiteJDBC.createTable("VarDecl_Android1"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("VarDecl_Swift1"));
		stmt.executeUpdate(SQLiteJDBC.createTable("VarDecl_Swift1"));

		//		stmt.executeUpdate(SQLiteJDBC.dropTable("var_swift"));
		//		stmt.executeUpdate(SQLiteJDBC.createExprTablesSwift());
		//		stmt.executeUpdate(SQLiteJDBC.dropTable("var_android"));
		//		stmt.executeUpdate(SQLiteJDBC.createExprTables());

		System.out.println("CHECKING MAPPING ELEMENTS\n"+mappingElements);
		int id = 1;
		while(iter.hasNext()){
			//			stmt = connection.createStatement();
			//			System.out.println(iter.next());
			ASTNode item = iter.next();
			if(item instanceof FieldDeclaration){
				RecordVarDeclAndroid record = new RecordVarDeclAndroid();
				//				rule.setMap(mappingElements);
				item.accept(record);
				//System.out.println(record.recordStr);
				String a1 = record.recordStr.get(0);
				String a2 = record.recordStr.get(1);
				String a3 = record.recordStr.get(2);
				String a4 = record.recordStr.get(3);
				alist.add(new VarDeclElement(a1, a2, a3, a4));
				amapping.put(a3, new VarDeclElement(a1, a2, a3, a4));



				String s1 = mappingElements.get(record.record.get(0)).toString();
				String s2 = mappingElements.get(record.record.get(1))!=null?mappingElements.get(record.record.get(1)).toString():"NOMAPPING";
				String s3 = a3;			
				String s4 = mappingElements.get(record.record.get(3))!=null?
						mappingElements.get(record.record.get(3)).toString():"NOMAPPING";
						slist.add(new VarDeclElement(s1, s2, s3, s4));
						//				String s5 = s1!=null?
						smapping.put(s3, new VarDeclElement(s1, s2, s3, s4));

						String i1 = SQLiteJDBC.insertTable("VarDecl_Android1", id, a3,a1,a2,a4);
						String i2 = SQLiteJDBC.insertTable("VarDecl_Swift1", id, s3,s1,s2,s4);
						//						System.out.println(i1);
						//						System.out.println(i2);
						stmt.executeUpdate(i1);
						stmt.executeUpdate(i2);

						mappingKeysVarDecl.put(a3, s3);

						//				String android = ConTextTemplate.varDeclAndroid(record.recordStr.get(0), record.recordStr.get(1), record.recordStr.get(2), record.recordStr.get(3));
						//				VarDeclElement ea = new VarDeclElement(record.recordStr.get(0));
						System.err.println(ConTextTemplate.varDeclAndroid(a1, a2, a3, a4));
						String q1 = SQLiteJDBC.selectTemplate("TypeDeclaration");
						ResultSet rrr = stmt.executeQuery(q1);

						String template = rrr.getString("TEMPLATE");
						System.err.println("TEMPLATE test"+template);
						ST st = new ST(template);



						//						}



						String se1 = "SELECT DISTINCT var_swift.MODIFIER "+
								" FROM var_android, var_swift "
								+ "where var_swift.ANDROID=var_android.ID and "
								+ "var_android.MODIFIER"+"=\'"+a1+"\'";

						ResultSet rrrr = stmt.executeQuery(se1);
						String modi = "<modi>";
						while(rrrr.next()){
							modi = rrrr.getString("MODIFIER");
							//							System.err.println("SQL"+modi);
						}


						se1 = "SELECT DISTINCT var_swift.INIT "+
								" FROM var_android, var_swift "
								+ "where var_swift.ANDROID=var_android.ID and "
								+ "var_android.INIT"+"=\'"+a4+"\'";


						rrrr = stmt.executeQuery(se1);
						String init = "<init>";
						while(rrrr.next()){
							init = rrrr.getString("INIT");
							//							System.err.println("SQL"+init);
						}


						se1 = "SELECT DISTINCT var_swift.TYPE "+
								" FROM var_android, var_swift "
								+ "where var_swift.ANDROID=var_android.ID and "
								+ "var_android.TYPE"+"=\'"+a2+"\'";


						rrrr = stmt.executeQuery(se1);
						String type = "<type>";
						while(rrrr.next()){
							type = rrrr.getString("TYPE");
							//							System.err.println("SQL"+type);
						}

						//						a22= "mContentRect.left";
						se1 = "SELECT DISTINCT method_swift.RETURN "+
								" FROM method_android, method_swift "
								+ "where method_swift.ANDROID=method_android.ID and "
								+ "method_android.RETURN"+"=\'"+a4+"\'";


						rrrr = stmt.executeQuery(se1);
						//						System.out.println("RETURN\n"+se1);
						while(rrrr.next() && init.equals("<init>")){
							init = rrrr.getString("RETURN");
							//							System.err.println("SQL"+init);
						}


						st.add("modifier", modi);
						st.add("type", type);
						st.add("varname", s3);
						st.add("init", init);
						System.out.println("(@@template db based) "+st.render());





						/*
						String se = "SELECT DISTINCT var_swift.TYPE, var_swift.MODIFIER, var_swift.INIT "+
								" FROM var_android, var_swift "
								+ "where var_swift.ANDROID=var_android.ID and "+
								"var_android.TYPE="+"=\'"+a2+"\'"
								+ " and "
								+ "var_android.MODIFIER="+"=\'"+a1+"\'"+ 
								" and "
								+ "var_android.INIT="+"=\'"+a4+"\'";

						 */
						//String ss = "SELECT DISTINCT var_swift."+name +" FROM var_android, var_swift where var_swift.ANDROID=var_android.ID and var_android."+name+
						//"=\'"+value+"\'";


						//						String ssss= "SELECT DISTINCT"
						//								+ " var_swift.MODIFIER FROM var_android, var_swift "
						//								+ "where var_swift.ANDROID=var_android.ID and var_android.MODIFIER='private'";

						//						ResultSet r1 = stmt.executeQuery(aa1);
						//						ResultSet r2 = stmt.executeQuery(aa2);
						//						ResultSet r4 = stmt.executeQuery(aa4);
						//						ResultSet r1 = stmt.executeQuery(SQLiteJDBC.selectDistinct("TYPE", a2));
						//						ResultSet r = stmt.executeQuery(se);

						//						String[] qr ={"TypeDeclaration","MethodDeclaration" };
						//						String[] qr ={"TypeDeclaration"};



						//						for(int i=0;i<qr.length;i++){
						//						String q1 = SQLiteJDBC.selectTemplate("TypeDeclaration");


						//							st.add("modifier", "public");
						//							st.add("type", "Matrix");
						//							st.add("varname", "mMatrx");
						//							st.add("init", "new Matrix()");
						//							System.out.println("(generated) "+st.render());





						//						System.out.println("MODIFIER	"+r.toString().getString(1));
						//						System.out.println("RRRResultSet"+r.getString("MODIFIER"));
						//						System.out.println("RRRResultSet"+r1.getString(1));
						//						System.out.println("RRRResultSet"+r2.getString(1));
						//						System.out.println("RRRResultSet"+r4.getString(1));
						//						System.out.println("RRRResultSet"+r4.getString("INIT"));
						//						System.out.println(SQLiteJDBC.selectDistinct("MODIFIER", a1)+"#"+r.next());

						//						while(r.next()){
						//
						//							st.add("modifier", r.getString("MODIFIER"));
						//							st.add("type", r.getString("TYPE"));
						//							st.add("varname", s3);
						//							st.add("init", r.getString("INIT"));
						//							System.out.println("(template db based) "+st.render());
						//						}

						id++;
			}
		}

		stmt.close();
		//		c.commit();
		connection.commit();

		//		System.out.println("@@@@@@@@");
		//		DBTablePrinter.printTable(connection, "var_swift");
		//		DBTablePrinter.printTable(connection, "var_android");
		//		DBTablePrinter.printTable(connection, "var_swift");
		//		c.close();
		connection.close();

		//		System.out.println("ALIST"+alist);
		//		System.out.println("SLIST"+slist);
		//		System.out.println(amapping);
		//		System.out.println(smapping);
	}



	public static void applyMethodDeclRules(Map<MappingElement, MappingElement> mappingElements, Collection<ASTNode> statments){

		Iterator<ASTNode> iter = statments.iterator();

		while(iter.hasNext()){
			//			System.out.println(iter.next());
			ASTNode item = iter.next();
			if(item instanceof MethodDeclaration){
				System.err.print(item.toString());
				//				RuleStatement rule1 = new RuleStatement();
				//				rule1.setMap(mappingElements);
				//				System.err.println(item.toString().replace("\n", ""));
				//				item.accept(rule1);
				//				System.err.print(item.toString());
				//				System.out.println(rule1.swiftstr);
				RuleMethodDecl rule = new RuleMethodDecl();
				rule.setMap(mappingElements);
				item.accept(rule);


				System.out.println(rule.swiftstr);

			}
		}
	}

	public static void applyMethodDeclRules2(Map<MappingElement, MappingElement> mappingElements, Collection<ASTNode> statments) throws SQLException, ClassNotFoundException{


		Class.forName("org.sqlite.JDBC");

		Connection connection = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		connection.setAutoCommit(false);
		//		System.out.println("Opened database successfully");

		//      System.out.println(insertTable(1, "_nameX","public","Rect","new Rect()"));

		Statement stmt = connection.createStatement();

		Iterator<ASTNode> iter = statments.iterator();
		while(iter.hasNext()){
			//			System.out.println(iter.next());
			ASTNode item = iter.next();
			if(item instanceof MethodDeclaration){

				RercordMethodDeclAndroid record = new RercordMethodDeclAndroid();
				//				rule.setMap(mappingElements);
				item.accept(record);
				//				System.out.println("applyMethodDeclRules2\n"+record.recordStr);

				System.err.print(item.toString());

				String q1 = SQLiteJDBC.selectTemplate("MethodDeclaration");
				ResultSet rrr = stmt.executeQuery(q1);

				String template = rrr.getString("TEMPLATE");
				System.err.println("TEMPLATE MethodDeclaration"+template);
				ST st = new ST(template);

				//				RuleStatement rule1 = new RuleStatement();
				//				rule1.setMap(mappingElements);
				//				System.err.println(item.toString().replace("\n", ""));
				//				item.accept(rule1);
				//				System.err.print(item.toString());
				//				System.out.println(rule1.swiftstr);
				//				RuleMethodDecl rule = new RuleMethodDecl();
				//				rule.setMap(mappingElements);
				//				item.accept(rule);
				//				System.out.println(rule.swiftstr);
				//				System.err.println("(generated android)\n"+ConTextTemplate.methodDeclAndroid(record.recordStr.get(0), record.recordStr.get(1), record.recordStr.get(2), record.recordStr.get(3)));
				//				System.out.println("(generated swift)\n"+ConTextTemplate.methodDeclSwift
				//						(mappingElements.get(record.record.get(0)).toString(), mappingElements.get(record.record.get(1)).toString(), record.recordStr.get(2), mappingElements.get(record.record.get(3)).toString()));

				//				System.out.println("(generated swift)\n"+ConTextTemplate.methodDeclSwift
				//						(mappingElements.get(record.record.get(0)).toString(), 
				//								mappingElements.get(record.record.get(1)).toString(), 
				//								record.recordStr.get(2), 
				//								applyExpressionRule(record.recordStr.get(3))));
				String expr ="";
				//				System.err.println(mappingElements);
				//				System.out.println("RRR"+record.record.get(3)+"..."+mappingElements.get(record.record.get(3)));
				if(mappingElements.get(record.record.get(3))!=null){
					expr =mappingElements.get(record.record.get(3)).label;
				}
				String a1=record.record.get(0).label;
				String a2=record.record.get(1).label;
				String a4=record.record.get(3).label;



				String se = "SELECT DISTINCT method_swift.TYPE, method_swift.MODIFIER, method_swift.RETURN"+
						" FROM method_android, method_swift where method_swift.ANDROID=method_android.ID and "+
						"method_android.TYPE="+"=\'"+a2+"\'"
						+ " and "
						+ "method_android.MODIFIER="+"=\'"+a1+"\'"+ 
						" and "
						+ "method_android.RETURN="+"=\'"+a4+"\'";				

				//				System.out.println("query\n"+se);




				ResultSet r = stmt.executeQuery(se);
				//						System.out.println("MODIFIER	"+r.toString().getString(1));
				//				System.out.println("RRRResultSet"+r.getString("MODIFIER"));
				//				System.out.println("RRRResultSet"+r1.getString(1));
				//				System.out.println("RRRResultSet"+r2.getString(1));
				//				System.out.println("RRRResultSet"+r4.getString(1));
				//				System.out.println("RRRResultSet"+r4.getString("INIT"));
				//				System.out.println(SQLiteJDBC.selectDistinct("MODIFIER", a1)+"#"+r.next());

				while(r.next()){
					//					System.out.println("SSSSSesultSet	"+r.getString("TYPE"));
					//					System.out.println("SSSSSesultSet	"+r.getString("MODIFIER"));
					//					System.out.println("SSSSSesultSet	"+r.getString("RETURN"));
					//					System.out.println("SSSSSesultSet"+r.getString("MODIFIER"));
					//					System.out.println("RRRResultSet"+r.getString("INIT"));
					//					System.out.println("(generated swift by DB)\n"+ConTextTemplate.varDeclSwift(r.getString("MODIFIER"), r.getString("TYPE"), s3, r.getString("INIT")));
					/*
					System.out.println("(generated swift method by DB)\n"+ConTextTemplate.methodDeclSwift
							(r.getString("MODIFIER"), 
									r.getString("TYPE"), 
									record.recordStr.get(2), 
									r.getString("RETURN")));
					 */


					st.add("modi", r.getString("MODIFIER"));
					st.add("type", r.getString("TYPE"));
					st.add("name", record.recordStr.get(2));
					st.add("expr", r.getString("RETURN"));
					System.out.println("(template db based) "+st.render());


				}
			}
		}
	}




	public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException {
		//recording process
		//1. automatic mapping by using name similarity of cross-platform repository

		new MainMappingClass();


//		fw.walk("facebook/facebook-android-sdk-master", android_files);
//		fw.walk("facebook/facebook-sdk-swift-master", swift_files);
		
//		fw.walk("wechat/wechat-master", android_files);
//		fw.walk("wechat/TSWeChat-master", swift_files);
		
//		fw.walk("wordpress/WordPress-Android-develop", android_files);
//		fw.walk("wordpress/WordPress-iOS-develop", swift_files);
		
//		fw.walk("wire/wire-android-master", android_files);
//		fw.walk("wire/wire-ios-develop", swift_files);

//		fw.walk("reactivex/RxJava-1.x", android_files);
//		fw.walk("reactivex/RxSwift-master", swift_files);
		
//		fw.walk("mcharts/MPAndroidChart-master", android_files);
//		fw.walk("mcharts/Charts-master", swift_files);
		
		//Filewalker fw = new Filewalker("charts/android_test3", "charts/swift_test3");
		Filewalker fw = new Filewalker("charts/android_test", "charts/swift_test");
		
		Map<File, File> classmap = fw.getClassMapping();
		Filewalker.staticPrintMapping(classmap);
		
		//get VariableDeclation of each android and swift
		Iterator<File> a_classes = classmap.keySet().iterator();

		Map<String, ASTNode> varDeclAndroid = null;
		Map<String, ParserRuleContext> varDeclSwift = null;
		Map<String, String> mapper_varDec = null;
		Map<String, String> mapper_methodDec = null;
		Map<String, ParserRuleContext> methodDecliOS = null;
		Map<String, ASTNode> methodDeclA = null;



		File fout = new File("out_chart.txt");
		FileOutputStream fos = new FileOutputStream(fout);
	 
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
	 
	
	 
//		bw.close();
		

		//get variable and method declarations class by class
		while(a_classes.hasNext()){
			File aclass_path = a_classes.next();
			File sclass_path = classmap.get(aclass_path);

			//			get variable declarations of aligned class by brief visit of ASTs
//			varDeclSwift   = getVarDeclSwift(sclass_path.getAbsolutePath());
//			varDeclAndroid = getVarDeclAndroid(readFileToString(aclass_path.getAbsolutePath()));


//			MappingDeclaration.varDeclAndroid = varDeclAndroid;
//			MappingDeclaration.varDeclSwift = varDeclSwift;

			//			get variable declarations of aligned class by brief visit of ASTs
//			methodDecliOS = getMethodDeclSwift(sclass_path.getAbsolutePath());
//			methodDeclA = getMethodDeclAndroid(readFileToString(aclass_path.getAbsolutePath()));
			 Map<String, MethodStructAndroid> mma = getMethodDeclAndroid2(readFileToString(aclass_path.getAbsolutePath()));
			Map<String, MethodStructSwift> mms = getMethodVarDeclSwift3((sclass_path.getAbsolutePath()));
//			 System.out.println("methodDeclSwift\n"+methodDecliOS);
			System.out.println("MethodStructAndroid\n"+mma);
			System.out.println("MethodStructSwift\n"+mms);
			boolean toPrint = true;

			Map<String, String> method_mapping = mappingMethod(mma, mms);
			//aligning of variable declaration by score.(score : name similarities of var_name ..)
			//fmapping: variable name pairs are stored in it.
//			mapper_varDec 		= mappingVarDec(varDeclAndroid, varDeclSwift, toPrint, "var declaration mapping"+aclass_path.getName()+":"+sclass_path.getName());
//			mapper_methodDec 	= mappingVarDec(methodDeclA, methodDecliOS, toPrint, "method declaration mapping"+aclass_path.getName()+":"+sclass_path.getName());
			mapper_methodDec= 	mappingMethodwo(mma, mms);
//			System.out.println("var declartion mapping\n"+mapper_varDec);
			System.err.println("method declartion mapping\n"+mapper_methodDec);
			System.err.println("method declartion mapping with type\n"+method_mapping);
			bw.write(aclass_path.getName()+" : "+sclass_path.getName());
			bw.newLine();
			bw.write(mapper_methodDec.toString());
			bw.newLine();
			bw.write(method_mapping.toString());
			bw.newLine();
			bw.newLine();
			/*
			//record mapping of method declarations 
			Iterator<String> it_f2 = mapper_methodDec.keySet().iterator();
			while(it_f2.hasNext()){
				String key_a=it_f2.next();
				MappingDeclaration mmm = new MappingDeclaration();
				mmm.setElementMethodDecl(
						methodDeclA.get(key_a), 
						methodDecliOS.get(mapper_methodDec.get(key_a))
						);

			}//iteration of aligned variable declarations
			*/
		}//iteration of aligned classes

		bw.close();
	


	}
	
	

}