package templatechecker;

// Generated from TemplateC.g4 by ANTLR 4.6.1
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link TemplateCParser}.
 */
public interface TemplateCListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link TemplateCParser#template}.
	 * @param ctx the parse tree
	 */
	void enterTemplate(TemplateCParser.TemplateContext ctx);
	/**
	 * Exit a parse tree produced by {@link TemplateCParser#template}.
	 * @param ctx the parse tree
	 */
	void exitTemplate(TemplateCParser.TemplateContext ctx);
	/**
	 * Enter a parse tree produced by {@link TemplateCParser#unit}.
	 * @param ctx the parse tree
	 */
	void enterUnit(TemplateCParser.UnitContext ctx);
	/**
	 * Exit a parse tree produced by {@link TemplateCParser#unit}.
	 * @param ctx the parse tree
	 */
	void exitUnit(TemplateCParser.UnitContext ctx);
	/**
	 * Enter a parse tree produced by {@link TemplateCParser#arg}.
	 * @param ctx the parse tree
	 */
	void enterArg(TemplateCParser.ArgContext ctx);
	/**
	 * Exit a parse tree produced by {@link TemplateCParser#arg}.
	 * @param ctx the parse tree
	 */
	void exitArg(TemplateCParser.ArgContext ctx);
	/**
	 * Enter a parse tree produced by {@link TemplateCParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(TemplateCParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link TemplateCParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(TemplateCParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link TemplateCParser#java_keyword}.
	 * @param ctx the parse tree
	 */
	void enterJava_keyword(TemplateCParser.Java_keywordContext ctx);
	/**
	 * Exit a parse tree produced by {@link TemplateCParser#java_keyword}.
	 * @param ctx the parse tree
	 */
	void exitJava_keyword(TemplateCParser.Java_keywordContext ctx);
	/**
	 * Enter a parse tree produced by {@link TemplateCParser#swift_keyword}.
	 * @param ctx the parse tree
	 */
	void enterSwift_keyword(TemplateCParser.Swift_keywordContext ctx);
	/**
	 * Exit a parse tree produced by {@link TemplateCParser#swift_keyword}.
	 * @param ctx the parse tree
	 */
	void exitSwift_keyword(TemplateCParser.Swift_keywordContext ctx);
	/**
	 * Enter a parse tree produced by {@link TemplateCParser#java_punc}.
	 * @param ctx the parse tree
	 */
	void enterJava_punc(TemplateCParser.Java_puncContext ctx);
	/**
	 * Exit a parse tree produced by {@link TemplateCParser#java_punc}.
	 * @param ctx the parse tree
	 */
	void exitJava_punc(TemplateCParser.Java_puncContext ctx);
	/**
	 * Enter a parse tree produced by {@link TemplateCParser#swift_punc}.
	 * @param ctx the parse tree
	 */
	void enterSwift_punc(TemplateCParser.Swift_puncContext ctx);
	/**
	 * Exit a parse tree produced by {@link TemplateCParser#swift_punc}.
	 * @param ctx the parse tree
	 */
	void exitSwift_punc(TemplateCParser.Swift_puncContext ctx);
}