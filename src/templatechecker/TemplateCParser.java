package templatechecker;

// Generated from TemplateC.g4 by ANTLR 4.6.1
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class TemplateCParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.6.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, T__41=42, T__42=43, T__43=44, T__44=45, 
		T__45=46, T__46=47, T__47=48, T__48=49, T__49=50, T__50=51, T__51=52, 
		T__52=53, T__53=54, T__54=55, T__55=56, T__56=57, T__57=58, T__58=59, 
		T__59=60, T__60=61, T__61=62, T__62=63, T__63=64, T__64=65, T__65=66, 
		T__66=67, T__67=68, T__68=69, T__69=70, T__70=71, T__71=72, T__72=73, 
		T__73=74, T__74=75, T__75=76, T__76=77, T__77=78, T__78=79, T__79=80, 
		T__80=81, T__81=82, T__82=83, T__83=84, T__84=85, T__85=86, T__86=87, 
		T__87=88, T__88=89, T__89=90, T__90=91, T__91=92, T__92=93, T__93=94, 
		T__94=95, T__95=96, T__96=97, T__97=98, T__98=99, T__99=100, T__100=101, 
		T__101=102, T__102=103, T__103=104, T__104=105, T__105=106, T__106=107, 
		T__107=108, T__108=109, T__109=110, T__110=111, T__111=112, T__112=113, 
		T__113=114, T__114=115, T__115=116, NUM=117, WS=118;
	public static final int
		RULE_template = 0, RULE_unit = 1, RULE_arg = 2, RULE_block = 3, RULE_java_keyword = 4, 
		RULE_swift_keyword = 5, RULE_java_punc = 6, RULE_swift_punc = 7;
	public static final String[] ruleNames = {
		"template", "unit", "arg", "block", "java_keyword", "swift_keyword", "java_punc", 
		"swift_punc"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'<arg'", "'>'", "'{...}'", "'abstract'", "'continue'", "'for'", 
		"'new'", "'switch'", "'assert'", "'default'", "'package'", "'synchronized'", 
		"'boolean'", "'do'", "'if'", "'private'", "'this'", "'break'", "'double'", 
		"'implements'", "'protected'", "'throw'", "'byte'", "'else'", "'import'", 
		"'public'", "'throws'", "'case'", "'enum'", "'instanceof'", "'return'", 
		"'transient'", "'catch'", "'extends'", "'int'", "'short'", "'try'", "'char'", 
		"'final'", "'interface'", "'static'", "'void'", "'class'", "'finally'", 
		"'long'", "'strictfp'", "'volatile'", "'float'", "'native'", "'super'", 
		"'while'", "'@Override'", "'Override'", "'@'", "'T'", "'associatedtype'", 
		"'deinit'", "'extension'", "'fileprivate'", "'func'", "'init'", "'inout'", 
		"'internal'", "'let'", "'open'", "'operator'", "'protocol'", "'self'", 
		"'struct'", "'subscript'", "'typealias'", "'private(set)'", "'convenience'", 
		"'var'", "'defer'", "'fallthrough'", "'guard'", "'in'", "'repeat'", "'where'", 
		"'weak'", "'override'", "'as'", "'through'", "'by'", "'stride'", "'to'", 
		"'NSObject'", "'NSObjectProtocol'", "'Hashable'", "'('", "')'", "'{'", 
		"'}'", "'['", "']'", "';'", "'='", "'<'", "'&'", "':'", "'=='", "','", 
		"'+'", "'-'", "'*'", "'/'", "'.'", "'|'", "'()'", "'->'", "'..<'", "'..'", 
		"'!'", "'?'", "'_'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, "NUM", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "TemplateC.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public TemplateCParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class TemplateContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(TemplateCParser.EOF, 0); }
		public List<UnitContext> unit() {
			return getRuleContexts(UnitContext.class);
		}
		public UnitContext unit(int i) {
			return getRuleContext(UnitContext.class,i);
		}
		public TemplateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_template; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TemplateCListener ) ((TemplateCListener)listener).enterTemplate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TemplateCListener ) ((TemplateCListener)listener).exitTemplate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TemplateCVisitor ) return ((TemplateCVisitor<? extends T>)visitor).visitTemplate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TemplateContext template() throws RecognitionException {
		TemplateContext _localctx = new TemplateContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_template);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(19);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << T__36) | (1L << T__37) | (1L << T__38) | (1L << T__39) | (1L << T__40) | (1L << T__41) | (1L << T__42) | (1L << T__43) | (1L << T__44) | (1L << T__45) | (1L << T__46) | (1L << T__47) | (1L << T__48) | (1L << T__49) | (1L << T__50) | (1L << T__51) | (1L << T__52) | (1L << T__53) | (1L << T__54) | (1L << T__55) | (1L << T__56) | (1L << T__57) | (1L << T__58) | (1L << T__59) | (1L << T__60) | (1L << T__61) | (1L << T__62))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (T__63 - 64)) | (1L << (T__64 - 64)) | (1L << (T__65 - 64)) | (1L << (T__66 - 64)) | (1L << (T__67 - 64)) | (1L << (T__68 - 64)) | (1L << (T__69 - 64)) | (1L << (T__70 - 64)) | (1L << (T__71 - 64)) | (1L << (T__72 - 64)) | (1L << (T__74 - 64)) | (1L << (T__75 - 64)) | (1L << (T__76 - 64)) | (1L << (T__77 - 64)) | (1L << (T__78 - 64)) | (1L << (T__79 - 64)) | (1L << (T__80 - 64)) | (1L << (T__81 - 64)) | (1L << (T__82 - 64)) | (1L << (T__83 - 64)) | (1L << (T__84 - 64)) | (1L << (T__85 - 64)) | (1L << (T__86 - 64)) | (1L << (T__87 - 64)) | (1L << (T__88 - 64)) | (1L << (T__89 - 64)) | (1L << (T__90 - 64)) | (1L << (T__91 - 64)) | (1L << (T__92 - 64)) | (1L << (T__93 - 64)) | (1L << (T__94 - 64)) | (1L << (T__95 - 64)) | (1L << (T__96 - 64)) | (1L << (T__97 - 64)) | (1L << (T__98 - 64)) | (1L << (T__99 - 64)) | (1L << (T__100 - 64)) | (1L << (T__101 - 64)) | (1L << (T__102 - 64)) | (1L << (T__103 - 64)) | (1L << (T__104 - 64)) | (1L << (T__105 - 64)) | (1L << (T__106 - 64)) | (1L << (T__107 - 64)) | (1L << (T__108 - 64)) | (1L << (T__109 - 64)) | (1L << (T__110 - 64)) | (1L << (T__111 - 64)) | (1L << (T__112 - 64)) | (1L << (T__113 - 64)) | (1L << (T__114 - 64)) | (1L << (T__115 - 64)))) != 0)) {
				{
				{
				setState(16);
				unit();
				}
				}
				setState(21);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(22);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnitContext extends ParserRuleContext {
		public Java_keywordContext java_keyword() {
			return getRuleContext(Java_keywordContext.class,0);
		}
		public Swift_keywordContext swift_keyword() {
			return getRuleContext(Swift_keywordContext.class,0);
		}
		public ArgContext arg() {
			return getRuleContext(ArgContext.class,0);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public Java_puncContext java_punc() {
			return getRuleContext(Java_puncContext.class,0);
		}
		public Swift_puncContext swift_punc() {
			return getRuleContext(Swift_puncContext.class,0);
		}
		public UnitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unit; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TemplateCListener ) ((TemplateCListener)listener).enterUnit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TemplateCListener ) ((TemplateCListener)listener).exitUnit(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TemplateCVisitor ) return ((TemplateCVisitor<? extends T>)visitor).visitUnit(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnitContext unit() throws RecognitionException {
		UnitContext _localctx = new UnitContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_unit);
		try {
			setState(30);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(24);
				java_keyword();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(25);
				swift_keyword();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(26);
				arg();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(27);
				block();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(28);
				java_punc();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(29);
				swift_punc();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgContext extends ParserRuleContext {
		public List<TerminalNode> NUM() { return getTokens(TemplateCParser.NUM); }
		public TerminalNode NUM(int i) {
			return getToken(TemplateCParser.NUM, i);
		}
		public ArgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arg; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TemplateCListener ) ((TemplateCListener)listener).enterArg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TemplateCListener ) ((TemplateCListener)listener).exitArg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TemplateCVisitor ) return ((TemplateCVisitor<? extends T>)visitor).visitArg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgContext arg() throws RecognitionException {
		ArgContext _localctx = new ArgContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_arg);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(32);
			match(T__0);
			setState(36);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NUM) {
				{
				{
				setState(33);
				match(NUM);
				}
				}
				setState(38);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(39);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TemplateCListener ) ((TemplateCListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TemplateCListener ) ((TemplateCListener)listener).exitBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TemplateCVisitor ) return ((TemplateCVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_block);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(41);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Java_keywordContext extends ParserRuleContext {
		public Java_keywordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_java_keyword; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TemplateCListener ) ((TemplateCListener)listener).enterJava_keyword(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TemplateCListener ) ((TemplateCListener)listener).exitJava_keyword(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TemplateCVisitor ) return ((TemplateCVisitor<? extends T>)visitor).visitJava_keyword(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Java_keywordContext java_keyword() throws RecognitionException {
		Java_keywordContext _localctx = new Java_keywordContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_java_keyword);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(43);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << T__36) | (1L << T__37) | (1L << T__38) | (1L << T__39) | (1L << T__40) | (1L << T__41) | (1L << T__42) | (1L << T__43) | (1L << T__44) | (1L << T__45) | (1L << T__46) | (1L << T__47) | (1L << T__48) | (1L << T__49) | (1L << T__50) | (1L << T__51) | (1L << T__52) | (1L << T__53) | (1L << T__54))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Swift_keywordContext extends ParserRuleContext {
		public Swift_keywordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_swift_keyword; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TemplateCListener ) ((TemplateCListener)listener).enterSwift_keyword(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TemplateCListener ) ((TemplateCListener)listener).exitSwift_keyword(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TemplateCVisitor ) return ((TemplateCVisitor<? extends T>)visitor).visitSwift_keyword(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Swift_keywordContext swift_keyword() throws RecognitionException {
		Swift_keywordContext _localctx = new Swift_keywordContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_swift_keyword);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(97);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__55:
				{
				setState(45);
				match(T__55);
				}
				break;
			case T__42:
				{
				setState(46);
				match(T__42);
				}
				break;
			case T__56:
				{
				setState(47);
				match(T__56);
				}
				break;
			case T__28:
				{
				setState(48);
				match(T__28);
				}
				break;
			case T__57:
				{
				setState(49);
				match(T__57);
				}
				break;
			case T__58:
				{
				setState(50);
				match(T__58);
				}
				break;
			case T__59:
				{
				setState(51);
				match(T__59);
				}
				break;
			case T__24:
				{
				setState(52);
				match(T__24);
				}
				break;
			case T__60:
				{
				setState(53);
				match(T__60);
				}
				break;
			case T__61:
				{
				setState(54);
				match(T__61);
				}
				break;
			case T__62:
				{
				setState(55);
				match(T__62);
				}
				break;
			case T__63:
				{
				setState(56);
				match(T__63);
				}
				break;
			case T__64:
				{
				setState(57);
				match(T__64);
				}
				break;
			case T__65:
				{
				setState(58);
				match(T__65);
				}
				break;
			case T__15:
				{
				setState(59);
				match(T__15);
				}
				break;
			case T__66:
				{
				setState(60);
				match(T__66);
				}
				break;
			case T__67:
				{
				setState(61);
				match(T__67);
				}
				break;
			case T__25:
				{
				setState(62);
				match(T__25);
				}
				break;
			case T__40:
				{
				setState(63);
				match(T__40);
				}
				break;
			case T__68:
				{
				setState(64);
				match(T__68);
				}
				break;
			case T__69:
				{
				setState(65);
				match(T__69);
				}
				break;
			case T__70:
				{
				setState(66);
				match(T__70);
				}
				break;
			case T__71:
				{
				setState(67);
				match(T__71);
				}
				break;
			case T__72:
				{
				setState(68);
				match(T__72);
				setState(69);
				match(T__73);
				}
				break;
			case T__17:
				{
				setState(70);
				match(T__17);
				}
				break;
			case T__27:
				{
				setState(71);
				match(T__27);
				}
				break;
			case T__4:
				{
				setState(72);
				match(T__4);
				}
				break;
			case T__9:
				{
				setState(73);
				match(T__9);
				}
				break;
			case T__74:
				{
				setState(74);
				match(T__74);
				}
				break;
			case T__13:
				{
				setState(75);
				match(T__13);
				}
				break;
			case T__23:
				{
				setState(76);
				match(T__23);
				}
				break;
			case T__75:
				{
				setState(77);
				match(T__75);
				}
				break;
			case T__5:
				{
				setState(78);
				match(T__5);
				}
				break;
			case T__76:
				{
				setState(79);
				match(T__76);
				}
				break;
			case T__14:
				{
				setState(80);
				match(T__14);
				}
				break;
			case T__77:
				{
				setState(81);
				match(T__77);
				}
				break;
			case T__78:
				{
				setState(82);
				match(T__78);
				}
				break;
			case T__30:
				{
				setState(83);
				match(T__30);
				}
				break;
			case T__7:
				{
				setState(84);
				match(T__7);
				}
				break;
			case T__79:
				{
				setState(85);
				match(T__79);
				}
				break;
			case T__50:
				{
				setState(86);
				match(T__50);
				}
				break;
			case T__80:
				{
				setState(87);
				match(T__80);
				}
				break;
			case T__81:
				{
				setState(88);
				match(T__81);
				}
				break;
			case T__82:
				{
				setState(89);
				match(T__82);
				}
				break;
			case T__83:
				{
				setState(90);
				match(T__83);
				}
				break;
			case T__84:
				{
				setState(91);
				match(T__84);
				}
				break;
			case T__85:
				{
				setState(92);
				match(T__85);
				}
				break;
			case T__86:
				{
				setState(93);
				match(T__86);
				}
				break;
			case T__87:
				{
				setState(94);
				match(T__87);
				}
				break;
			case T__88:
				{
				setState(95);
				match(T__88);
				}
				break;
			case T__89:
				{
				setState(96);
				match(T__89);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Java_puncContext extends ParserRuleContext {
		public Java_puncContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_java_punc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TemplateCListener ) ((TemplateCListener)listener).enterJava_punc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TemplateCListener ) ((TemplateCListener)listener).exitJava_punc(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TemplateCVisitor ) return ((TemplateCVisitor<? extends T>)visitor).visitJava_punc(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Java_puncContext java_punc() throws RecognitionException {
		Java_puncContext _localctx = new Java_puncContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_java_punc);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(99);
			_la = _input.LA(1);
			if ( !(_la==T__1 || ((((_la - 91)) & ~0x3f) == 0 && ((1L << (_la - 91)) & ((1L << (T__90 - 91)) | (1L << (T__91 - 91)) | (1L << (T__92 - 91)) | (1L << (T__93 - 91)) | (1L << (T__94 - 91)) | (1L << (T__95 - 91)) | (1L << (T__96 - 91)) | (1L << (T__97 - 91)) | (1L << (T__98 - 91)) | (1L << (T__99 - 91)) | (1L << (T__100 - 91)) | (1L << (T__101 - 91)) | (1L << (T__102 - 91)) | (1L << (T__103 - 91)) | (1L << (T__104 - 91)) | (1L << (T__105 - 91)) | (1L << (T__106 - 91)) | (1L << (T__107 - 91)) | (1L << (T__108 - 91)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Swift_puncContext extends ParserRuleContext {
		public Swift_puncContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_swift_punc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TemplateCListener ) ((TemplateCListener)listener).enterSwift_punc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TemplateCListener ) ((TemplateCListener)listener).exitSwift_punc(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TemplateCVisitor ) return ((TemplateCVisitor<? extends T>)visitor).visitSwift_punc(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Swift_puncContext swift_punc() throws RecognitionException {
		Swift_puncContext _localctx = new Swift_puncContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_swift_punc);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(101);
			_la = _input.LA(1);
			if ( !(_la==T__1 || ((((_la - 91)) & ~0x3f) == 0 && ((1L << (_la - 91)) & ((1L << (T__90 - 91)) | (1L << (T__91 - 91)) | (1L << (T__92 - 91)) | (1L << (T__93 - 91)) | (1L << (T__94 - 91)) | (1L << (T__95 - 91)) | (1L << (T__96 - 91)) | (1L << (T__97 - 91)) | (1L << (T__98 - 91)) | (1L << (T__99 - 91)) | (1L << (T__100 - 91)) | (1L << (T__102 - 91)) | (1L << (T__104 - 91)) | (1L << (T__107 - 91)) | (1L << (T__108 - 91)) | (1L << (T__109 - 91)) | (1L << (T__110 - 91)) | (1L << (T__111 - 91)) | (1L << (T__112 - 91)) | (1L << (T__113 - 91)) | (1L << (T__114 - 91)) | (1L << (T__115 - 91)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3xj\4\2\t\2\4\3\t\3"+
		"\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\3\2\7\2\24\n\2\f\2\16"+
		"\2\27\13\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\5\3!\n\3\3\4\3\4\7\4%\n\4\f"+
		"\4\16\4(\13\4\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7d\n\7\3\b\3\b\3\t\3\t\3\t\2\2"+
		"\n\2\4\6\b\n\f\16\20\2\5\3\2\69\4\2\4\4]o\7\2\4\4]giikknv\u009a\2\25\3"+
		"\2\2\2\4 \3\2\2\2\6\"\3\2\2\2\b+\3\2\2\2\n-\3\2\2\2\fc\3\2\2\2\16e\3\2"+
		"\2\2\20g\3\2\2\2\22\24\5\4\3\2\23\22\3\2\2\2\24\27\3\2\2\2\25\23\3\2\2"+
		"\2\25\26\3\2\2\2\26\30\3\2\2\2\27\25\3\2\2\2\30\31\7\2\2\3\31\3\3\2\2"+
		"\2\32!\5\n\6\2\33!\5\f\7\2\34!\5\6\4\2\35!\5\b\5\2\36!\5\16\b\2\37!\5"+
		"\20\t\2 \32\3\2\2\2 \33\3\2\2\2 \34\3\2\2\2 \35\3\2\2\2 \36\3\2\2\2 \37"+
		"\3\2\2\2!\5\3\2\2\2\"&\7\3\2\2#%\7w\2\2$#\3\2\2\2%(\3\2\2\2&$\3\2\2\2"+
		"&\'\3\2\2\2\')\3\2\2\2(&\3\2\2\2)*\7\4\2\2*\7\3\2\2\2+,\7\5\2\2,\t\3\2"+
		"\2\2-.\t\2\2\2.\13\3\2\2\2/d\7:\2\2\60d\7-\2\2\61d\7;\2\2\62d\7\37\2\2"+
		"\63d\7<\2\2\64d\7=\2\2\65d\7>\2\2\66d\7\33\2\2\67d\7?\2\28d\7@\2\29d\7"+
		"A\2\2:d\7B\2\2;d\7C\2\2<d\7D\2\2=d\7\22\2\2>d\7E\2\2?d\7F\2\2@d\7\34\2"+
		"\2Ad\7+\2\2Bd\7G\2\2Cd\7H\2\2Dd\7I\2\2Ed\7J\2\2FG\7K\2\2Gd\7L\2\2Hd\7"+
		"\24\2\2Id\7\36\2\2Jd\7\7\2\2Kd\7\f\2\2Ld\7M\2\2Md\7\20\2\2Nd\7\32\2\2"+
		"Od\7N\2\2Pd\7\b\2\2Qd\7O\2\2Rd\7\21\2\2Sd\7P\2\2Td\7Q\2\2Ud\7!\2\2Vd\7"+
		"\n\2\2Wd\7R\2\2Xd\7\65\2\2Yd\7S\2\2Zd\7T\2\2[d\7U\2\2\\d\7V\2\2]d\7W\2"+
		"\2^d\7X\2\2_d\7Y\2\2`d\7Z\2\2ad\7[\2\2bd\7\\\2\2c/\3\2\2\2c\60\3\2\2\2"+
		"c\61\3\2\2\2c\62\3\2\2\2c\63\3\2\2\2c\64\3\2\2\2c\65\3\2\2\2c\66\3\2\2"+
		"\2c\67\3\2\2\2c8\3\2\2\2c9\3\2\2\2c:\3\2\2\2c;\3\2\2\2c<\3\2\2\2c=\3\2"+
		"\2\2c>\3\2\2\2c?\3\2\2\2c@\3\2\2\2cA\3\2\2\2cB\3\2\2\2cC\3\2\2\2cD\3\2"+
		"\2\2cE\3\2\2\2cF\3\2\2\2cH\3\2\2\2cI\3\2\2\2cJ\3\2\2\2cK\3\2\2\2cL\3\2"+
		"\2\2cM\3\2\2\2cN\3\2\2\2cO\3\2\2\2cP\3\2\2\2cQ\3\2\2\2cR\3\2\2\2cS\3\2"+
		"\2\2cT\3\2\2\2cU\3\2\2\2cV\3\2\2\2cW\3\2\2\2cX\3\2\2\2cY\3\2\2\2cZ\3\2"+
		"\2\2c[\3\2\2\2c\\\3\2\2\2c]\3\2\2\2c^\3\2\2\2c_\3\2\2\2c`\3\2\2\2ca\3"+
		"\2\2\2cb\3\2\2\2d\r\3\2\2\2ef\t\3\2\2f\17\3\2\2\2gh\t\4\2\2h\21\3\2\2"+
		"\2\6\25 &c";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}