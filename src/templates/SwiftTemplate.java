package templates;

import java.io.StringReader;

import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupDir;
import org.stringtemplate.v4.STGroupFile;
import org.stringtemplate.v4.STGroupString;



public class SwiftTemplate {
	static String NL = "\n";
	static STGroup group = new STGroupDir("/Users/kijin/Documents/workspace_mars/GumTreeTester/cross_templates");

	public static String varDeclAndroid(String modifier, String type, String name, String init){

		ST st_android = group.getInstanceOf("vardecl_java");

		st_android.add("modi", modifier);
		st_android.add("type", type);
		st_android.add("name", name);
		st_android.add("value", init);

		return st_android.render();
	}

	public static String enclosing(String str){
		return "\""+str+"\"\n";
	}

	public void testNestedIterationWithArg() throws Exception {
		STGroup group = new STGroup();
		group.defineTemplate("test", "users", "<users:{u | <u.id:{id | <id>=}><u.name>}>!");
		ST st = group.getInstanceOf("test");
		//	        st.add("users", new TestCoreBasics.User(1, "parrt"));
		//	        st.add("users", new TestCoreBasics.User(2, "tombu"));
		//	        st.add("users", new TestCoreBasics.User(3, "sri"));
		String expected = "1=parrt2=tombu3=sri!";
		String result = st.render();
		//	        assertEquals(expected, result);
	}

	public static String varDeclSwift(String modifier, String type, String name, String init){
		ST st_swift = group.getInstanceOf("vardecl_swift");

		st_swift.add("modi", modifier);
		st_swift.add("type", type);
		st_swift.add("name", name);
		st_swift.add("value", init);
		return st_swift.render();
	}

	public static String methodDeclAndroid(String modifier, String type, String name, String init){
		ST st_swift = group.getInstanceOf("methoddecl_java");

		st_swift.add("modi", modifier);
		st_swift.add("type", type);
		st_swift.add("name", name);
		st_swift.add("value", init);
		return st_swift.render();
	}

	public static String methodDeclSwift(String modifier, String type, String name, String init){
		ST st_swift = group.getInstanceOf("methoddecl_swift");

		st_swift.add("modi", modifier);
		st_swift.add("type", type);
		st_swift.add("name", name);
		st_swift.add("value", init);
		return st_swift.render();
	}

	

	public static void main(String[] args){

		STGroup group1 = new STGroupFile("/Users/kijin/Documents/workspace_mars/GumTreeTester/cross_templates/if_statement.stg");
		
//		if_statement(condition_clause, statements, else_clause)
		ST if_statement = group1.getInstanceOf("if_statement");
		ST statements = group1.getInstanceOf("statements");
		System.err.println(statements.getAttributes());
//		if_statement.add("condition_clause", "x>y");
		if_statement.add("statements", "x = y");
//		if_statement.add("else_clause", "else{return null}");
		if_statement.add("statements", if_statement.render());
		System.out.println(if_statement.render());
		
//		st_android.add("modi", modifier);
//		st_android.add("type", type);
//		st_android.add("name", name);
//		st_android.add("value", init);
		
		STGroup gggg = new STGroupFile("/Users/kijin/Documents/workspace_mars/GumTreeTester/newtemp/java.stg");

		ST program = gggg.getInstanceOf("program");
		program.add("globals", "int a");
		program.add("functions", "void foo1() {}");
		program.add("functions", "void foo2() {}");
		program.add("functions", "void foo3() {}");
		
		System.out.println(program.render());
	}
}
