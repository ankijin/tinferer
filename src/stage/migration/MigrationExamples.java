package stage.migration;

import static org.hamcrest.CoreMatchers.instanceOf;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.misc.TestRig;
import org.apache.commons.collections4.ListUtils;

import com.googlecode.concurrenttrees.radix.node.concrete.DefaultCharSequenceNodeFactory;
import com.googlecode.concurrenttrees.solver.LCSubstringSolver;

import alignment.common.CommonOperators;
import alignment.common.OpProperty;
import antlr_parsers.javaparser.JavaLexer;
import antlr_parsers.javaparser.JavaParser;
import antlr_parsers.javaparser.JavaParser.StatementContext;
import migration.StructureMappingAntlr4.Pair;
import migration.SwiftParser.Return_statementContext;
import sqldb.DBTablePrinter;
import sqldb.SQLiteJDBC;
import templatechecker.DSLErrorListener;
import templatechecker.TemplateCLexer;
import templatechecker.TemplateCParser;


import org.apache.commons.collections4.ListUtils;

public class MigrationExamples {
	private int i2;
	public static int idd;
	private static boolean flip = true;
	//		public static final String db_name = "test_record11.db";
	//	public static final String db_name = "test_record0208.db";

	//	public static final String db_name = "train_record8.db";
	//	public static final String db_name = "test_record0210.db";
//	public static final String db_name = "train_record7.db";
	public static final String db_name = "test_record_charts.db";
	
	private static boolean templateOk = false;
	public static String query(String tableName, int android, int swift){
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, count from "+tableName+ " where parent_type="+swift +" order by count;";
		return query;
	}


	public static ResultSet queryExampleResult(String tableName, int android, int swift) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where ast_type="+android +" and parent_type="+swift +" order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		//		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);
		return resultSet;
	}

	public static ResultSet queryExampleResultbyID(String tableName, int id) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where id="+id+ " order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;
	}

	public static ResultSet queryExampleResultbyExample(String tableName, String a_example, String s_example) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, parent_type, count from "+tableName+ " where template=\'"+a_example +"\' and example=\'"+s_example +"\' order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println(query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;

	}

	public static ResultSet queryExampleResultbyExample(String tableName, String a_example) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, parent_type, count from "+tableName+ " where template=\'"+a_example +"\' order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println(query);
		//		test_record11.db
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;

	}

	/*
	public static ResultSet queryExampleResultbyExample(String tableName, String example) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, parent_type, count from "+tableName+ " where template=\'"+example +"\' order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println(query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+"train_record.db");
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;

	}
	 */



	public static String getLongestCommonSubstring(Collection<String> strings) {
		LCSubstringSolver solver = new LCSubstringSolver(new DefaultCharSequenceNodeFactory());

		for (String s: strings) {
			solver.add(s);
		}
		return solver.getLongestCommonSubstring().toString();
	}


	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		// TODO Auto-generated method stub
		//query examples
		//generates template to except for bindings
		//when there are no binding .. just syntax change
		//Goals: removal syntax errors

		//set of training
		//query .. 



		Class.forName("org.sqlite.JDBC");
		Connection connection = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		connection.setAutoCommit(true);
		Statement stmt = connection.createStatement();
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift"));

		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_binding_stat"));

		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroidStat());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwiftStat());
		stmt.executeUpdate(SQLiteJDBC.createBindingStat());


		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroid());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwift());


		//		String q1= query("grammar_swift_stat", 70, 36);
		//		System.err.println(q1);

		//		int atype 	= 18, stype	= 70;
		//constant
//				int atype 	= 68, stype	= 76;
		//epxress		
				int atype 	= 70, stype	= 214;

		//var func
		//								int atype 	= 19, stype	= 80;
		//var func
//		int atype 	= 19, stype	= 97;
		//expession
//		int atype 	= 19, stype	= 97;
		//return
		//										int atype 	= 70, stype	= 36;
		//		19, 27
		//if..
		//				int atype 	= 70, stype	= 18;
		//for
		//		int atype 	= 70, stype	= 6;
		// 70 6
		//76, 68


		//						ResultSet resultSet2 =  queryExampleResult("grammar_swift_stat", 70, 36);
//						ResultSet resultSet2 =  queryExampleResult("grammar_swift_stat", atype, stype);
		//				ResultSet resultSet2 =  queryExampleResultbyExample("grammar_swift_stat", "@Override    protected float[] getMarkerPosition(Highlight high) {        return new float[]{high.getDrawY(), high.getDrawX()};    }");
		// 13273, 3151, 14136
		// transpose, 15898, 14271, 14030
		// { errr 13765, 15898
		// func mis matched .. compo 13311
		ResultSet resultSet2 =  queryExampleResultbyID("grammar_swift_stat", 3141);
		//				org.antlr.v4.parse.ScopeParser;
		//				ResultSet resultSet2 =  queryExampleResultbyExample("grammar_swift_stat", "@Override    public boolean isHighlightFullBarEnabled() {        return mHighlightFullBarEnabled;    }","public var isHighlightFullBarEnabled: Bool { return highlightFullBarEnabled }");
		//		ResultSet resultSet2 =  queryExampleResultbyExample("grammar_swift_stat", "private RadarChart mChart;","private weak var chart: RadarChartView?");

		//		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+"test_record.db");
		//		connection2.setAutoCommit(true);
		//		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		//		ResultSet resultSet2 = stmt2.executeQuery(q1);
	
						
		int num 	= 0;
		int count 	= 0;
		int cycle 	= 10;

		while(resultSet2.next()){
			templateOk = false;
			flip = true;
			double c1 = 1.0000, c2 = 1.0001;

			//			for(int q=0;q<=cycle && !templateOk;q++){

			//				black_a =new LinkedList<OpProperty>();
			//				black_s=new LinkedList<OpProperty>();
			num++;
			String aaa = resultSet2.getString("template");
			String bbb = resultSet2.getString("example");
			atype = Integer.parseInt(resultSet2.getString("AST_TYPE"));
			stype = Integer.parseInt(resultSet2.getString("PARENT_TYPE"));
			idd = Integer.parseInt(resultSet2.getString("ID"));

			int ccc = Integer.parseInt(resultSet2.getString("count"));
			count = count + ccc;
			//			aaa = aaa.replaceAll("\\.\\.\\.", "\\<arg10\\>");
			//			bbb = bbb.replaceAll("\\.\\.\\.", "\\<arg10\\>");
			//				System.out.println(bbb);
			//				System.err.println(aaa);
			//			item_map.put(aaa, bbb);

			SwiftLexer lexer1 = new SwiftLexer((CharStream)new ANTLRInputStream(bbb));
			CommonTokenStream swiftCommonStream = new CommonTokenStream(lexer1);
			SwiftParser parser = new SwiftParser(swiftCommonStream);


			JavaLexer lexerj = new JavaLexer((CharStream)new ANTLRInputStream(aaa));
			CommonTokenStream javaCommonStream = new CommonTokenStream(lexerj);
			JavaParser parserj = new JavaParser(javaCommonStream);
			//				parser.setErrorHandler(error_handler);
			ParserRuleContext com = null, comj=null;
			//				System.out.println(atype+" java rule	"+JavaParser.ruleNames[atype]);
			//				System.out.println(stype+" swift rule	"+SwiftParser.ruleNames[stype]);


			if(atype==70 && stype==36){
				System.out.println("TTTT");
				System.out.println("java rule	"+JavaParser.ruleNames[stype]);
				System.out.println("swift rule	"+SwiftParser.ruleNames[atype]);
				com 	= parser.return_statement();
				comj 	= parserj.statement();
				//				parserj.getTokenStream().get
			}

			else if(atype==68 && stype==76){
				com 	= parser.constant_declaration();
				comj 	= parserj.localVariableDeclarationStatement();
				System.out.println("java rule	"+JavaParser.ruleNames[atype]);
				System.out.println("swift rule	"+SwiftParser.ruleNames[stype]);
				//					System.out.println("com:"+num+"\n"+com.getText());
				//					System.out.println(comj.getText());
			}

			else if(atype==69 && stype==76){
				com 	= parser.constant_declaration();
				comj 	= parserj.localVariableDeclaration();
				System.out.println("java rule	"+JavaParser.ruleNames[atype]);
				System.out.println("swift rule	"+SwiftParser.ruleNames[stype]);
				//					System.out.println("com:"+num+"\n"+com.getText());
				//					System.out.println(comj.getText());
			}


			else if(atype==18 && stype==70){
				System.out.println("java rule	"+JavaParser.ruleNames[atype]);
				System.out.println("swift rule	"+SwiftParser.ruleNames[stype]);
				com 	= parser.constant_declaration();
				comj 	= parserj.localVariableDeclaration();
				//					System.out.println("com:"+num+"\n"+com.getText());
				//					System.out.println(comj.getText());
			}
			
			


			else if(atype==19 && stype==80){
				System.out.println("java rule	"+JavaParser.ruleNames[atype]);
				System.out.println("swift rule	"+SwiftParser.ruleNames[stype]);
				com 	= parser.variable_declaration();
				comj 	= parserj.classBodyDeclaration();
				//					System.out.println("com:"+num+"\n"+com.getText());
				//					System.out.println(comj.getText());
			}

			else if(atype==19 && stype==97){
				System.out.println("java rule	"+JavaParser.ruleNames[atype]);
				System.out.println("swift rule	"+SwiftParser.ruleNames[stype]);
				com 	= parser.function_declaration();
				comj 	= parserj.classBodyDeclaration();
				//					System.out.println("com:"+num+"\n"+com.getText());
				//					System.out.println(comj.getText());
			}

			else if(atype==70 && stype==6){
				System.out.println("java rule	"+JavaParser.ruleNames[atype]);
				System.out.println("swift rule	"+SwiftParser.ruleNames[stype]);
				try{
					System.out.println("testing"+swiftCommonStream.getText(new Interval(7, 13)));

					com 	= parser.for_in_statement();
					comj 	= parserj.statement();
					System.out.println("com:"+num+"\n"+com.getText());
					System.out.println(comj.getText());
				} catch(Exception e){
					e.printStackTrace();
				}

			}

			else if(atype==70 && stype==18){
				System.out.println("java rule	"+JavaParser.ruleNames[atype]);
				System.out.println("swift rule	"+SwiftParser.ruleNames[stype]);
				try{
					com 	= parser.if_statement();
					comj 	= parserj.statement();
				} catch(Exception e){
					e.printStackTrace();
				}
				//	System.out.println("com:"+num+"\n"+com.getText());
				//	System.out.println(comj.getText());
			}

			
			else if(atype==70 && stype==214){
				System.out.println("java rule	"+JavaParser.ruleNames[atype]);
				System.out.println("swift rule	"+SwiftParser.ruleNames[stype]);
				try{
					com 	= parser.postfix_expression();
					comj 	= parserj.statement();
				} catch(Exception e){
					e.printStackTrace();
				}
				//	System.out.println("com:"+num+"\n"+com.getText());
				//	System.out.println(comj.getText());
			}
			
			
			else if(atype==70 && stype==180){
				System.out.println("java rule	"+JavaParser.ruleNames[atype]);
				System.out.println("swift rule	"+SwiftParser.ruleNames[stype]);
				try{
					com 	= parser.expression();
					comj 	= parserj.statement();
				} catch(Exception e){
					e.printStackTrace();
				}
				//	System.out.println("com:"+num+"\n"+com.getText());
				//	System.out.println(comj.getText());
			}
			
			
			//			System.err.println("ast	java	"+comj.getText());
			//			String sss = ListUtils.longestCommonSubsequence(comj.getText(), com.getText());
			//			System.out.println("ssss>>\n"+sss);
			StructureMappingAntlr4 mapping=null;
			if(comj!=null &&com!=null){
				try {
					mapping = new StructureMappingAntlr4(comj, com);
					mapping.swiftCommonStream = swiftCommonStream;
					mapping.androidCommonStream = javaCommonStream;
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}



				List<OpProperty> 	black_a =new LinkedList<OpProperty>(), black_s=new LinkedList<OpProperty>();

				//				for(int q=0;q<=cycle && mapping!=null && c1!=c2 ;q++){÷
				for(int q=0;q<=cycle && mapping!=null ;q++){
					try {
						System.out.println("test"+q);
						//initialize blacklists
						//						if(black_a.size()>0){
						//							mapping.android_listener.blacklist = black_a;
						//							System.out.println(black_a.size()+"blcklista"+black_a);
						//						}
						//						if(black_s.size()>0){
						//							mapping.swift_listner.blacklist = black_s;
						//							System.out.println(black_s.size()+"blcklists"+black_s);
						//						}
						//initialize get next candidates
						//						System.out.println("blcklists"+mapping.swift_listner.blacklist);
						mapping.initialize();
						//						System.out.println("opaaa"+mapping.android_listener.op_a_nodes);
						//						System.out.println("opsss"+mapping.swift_listner.op_s_nodes);
						//mappings
						//						if(mapping.android_listener.op_a_nodes.size()==mapping.swift_listner.op_s_nodes.size())
						c2 =c1;
						c1=mapping.compute();

						//						System.out.println("c1:"+c1+"c2:"+c2+"min:"+Math.min(c1,c2));
						//						if(c2==c1)
						templateOk=mapping.templateSolver;

						Iterator<Pair> mapps 		= mapping.android_to_swift_point.keySet().iterator();
						Iterator<Interval> aaaaaa 	= mapping.amap_ct.keySet().iterator();
						List<Interval> listc 		= new LinkedList<Interval>();
						Set<String> set 			= new HashSet<String>();
						LinkedHashMap<String, List<Interval>> constraint = mapping.android_listener.scope_constraint;
						Iterator<String> decls = constraint.keySet().iterator();
						while(decls.hasNext()){
							listc = constraint.get(decls.next());
							//							listc.add(new Interval(5, 5));
							//							listc.add(new Interval(14, 14));
							//							listc.add(new Interval(29, 29));
							boolean isMet = true;



							//check if template have same args

							while(aaaaaa.hasNext()){
								Interval nnnnn = aaaaaa.next();
								for(int i = 0; i< listc.size();i++){

									if(!listc.get(i).disjoint(nnnnn)){
										System.out.println("nnnnn"+mapping.amap_ct.get(nnnnn));
										set.add(mapping.amap_ct.get(nnnnn));

									}
								}
							}



							System.out.println(set+" sizeof"+set.size()+"amap_ct"+mapping.amap_ct);
							//if .. not in the same arguments 
							if(set.size()>1){
								for(int i = 0; i< listc.size();i++){
									for(OpProperty op: mapping.android_listener.op_a_nodes)
										if(!listc.get(i).disjoint(op.interval) && !listc.get(i).equals(op.interval)){
											mapping.android_listener.blacklist.add(op);
										}
								}


								if(mapping.android_to_swift_point.size()>0){
									Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
										public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
											return entry1.getValue().compareTo(entry2.getValue());
										}
									});

									if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(min.getKey().b)))
										mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
									System.out.println("black_s"+mapping.swift_listner.blacklist);

								}

							}
						} // un-met .. cases

						if(mapping.android_listener.op_a_nodes.size() < mapping.swift_listner.op_s_nodes.size()){
							//							if(flip){
							//							System.out.println("android mapping	"+mapping.android_to_swift_point);
							//							System.out.println(mapping.android_listener.op_a_nodes);
							//							System.out.println(mapping.swift_listner.op_s_nodes);

							//							System.out.println("mapping	"+mapping.android_to_swift_point);
							if(mapping.android_to_swift_point.size()>0){

								Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
									public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
										return entry1.getValue().compareTo(entry2.getValue());
									}
								});


								//								System.out.println(min.getKey()+"\n"+);
								//								black_a.clear();
								System.out.println("min	"+min+"  "+min.getKey()+"\n"+mapping.swift_listner.op_s_nodes.get(min.getKey().b)+"\n"+mapping.android_listener.op_a_nodes.get(min.getKey().a));
								//								
								//								black_a.add(mapping.android_listener.op_a_nodes.get(min.getKey().a));
								if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(min.getKey().a)))
									mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(min.getKey().a));
								//								System.out.println("black_a"+mapping.android_listener.blacklist);
								//								black_a.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
								//								if(min!=null)
								//									mapping.android_listener.op_a_nodes.remove(min.getKey().a);
							}
							//							black_s.addAll(mapping.swift_listner.op_s_nodes);

							//							black_a.addAll(mapping.android_listener.op_a_nodes);
							flip = false;
						}

						/** return <>; return <>
						 * for ( <arg0> <arg1> : <arg2> ) {...}  to for <arg1> in <arg2> {...}
						 * 없으면 이대로 .. 있으면 .. 계속. best matching
						 */
						else if(mapping.android_listener.op_a_nodes.size() >= mapping.swift_listner.op_s_nodes.size()){
							//							System.out.println("swift mapping	"+mapping.android_to_swift_point);
							//							System.out.println(mapping.android_listener.op_a_nodes);
							//							System.out.println(mapping.swift_listner.op_s_nodes);
							if(mapping.android_to_swift_point.size()>0){
								Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
									public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
										return entry1.getValue().compareTo(entry2.getValue());
									}
								});
								//								System.out.println("|a|<|s|"+min.getKey()+"\n"+mapping.swift_listner.op_s_nodes.get(min.getKey().b)+"\n"+mapping.android_listener.op_a_nodes.get(min.getKey().a));
								//								black_s.clear();
								//								black_s.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));


								Iterator<Integer> a_keys = mapping.android_to_swift.keySet().iterator();
								while(a_keys.hasNext()){
									int aa=a_keys.next();
									int ss = mapping.android_to_swift.get(aa);
									//									ParserRuleContext adrd = (ParserRuleContext)mapping.android_listener.op_a_nodes.get(aa).node;
									//									ParserRuleContext swft = (ParserRuleContext)mapping.swift_listner.op_s_nodes.get(ss).node;
									/*
									if(aa>=0 && ss>=0){
										OpProperty black = CommonOperators.determineGoDown(mapping.android_listener.op_a_nodes.get(aa), mapping.swift_listner.op_s_nodes.get(ss));
										if(black!=null){
											System.out.println("BBBB"+black);
											if(!mapping.swift_listner.blacklist.contains(black)&& mapping.swift_listner.op_s_nodes.get(ss).interval.equals(black.interval)){
												mapping.swift_listner.blacklist.add(black);
												System.out.println("SSSBBBB"+black);
											}
											else if(!mapping.android_listener.blacklist.contains(black) && mapping.android_listener.op_a_nodes.get(aa).equals(black)){
												mapping.android_listener.blacklist.add(black);
											}
										}
									}
									 */

									if(aa>=0 && ss>=0){
										int decision = CommonOperators.determineGoDownbyNum(mapping.android_listener.op_a_nodes.get(aa), mapping.swift_listner.op_s_nodes.get(ss));
										if(decision!=-1){
											System.out.println("BBBB"+decision);
											if(decision ==2){
												if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
													mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
												}
												if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
													mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
												}
												//												System.out.println("SSSBBBB"+mapping.swift_listner.op_s_nodes.get(ss));
											}
											else if(decision ==1){
												if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
													mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
												}
											}
											else{
												if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
													mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
												}
												if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
													mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
												}
											}
										}
									}

								}

								//								black_s.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
								//								if(min!=null)
								//									mapping.swift_listner.op_s_nodes.remove(min.getKey().b);
							}
							//							black_s.clear();
							//							black_s.addAll(mapping.swift_listner.op_s_nodes);
							//							System.out.println("black_s.addAll(mapping.swift_listner.op_s_nodes)");
							flip = true;
						}
						else{
							//							mapping.swift_listner.op_s_nodes.get
							Iterator<Integer> a_keys = mapping.android_to_swift.keySet().iterator();
							while(a_keys.hasNext()){
								int aa=a_keys.next();
								int ss = mapping.android_to_swift.get(aa);
								ParserRuleContext adrd = (ParserRuleContext)mapping.android_listener.op_a_nodes.get(aa).node;
								//
								//hasPunc
								//startsWithKeywords
								//								adrd.getText().substring(adrd.getText().length()-1, adrd.getText().length()-1);
								//								System.out.println("###"+adrd.getText().substring(0, 1)+",  "+adrd.getText().substring(adrd.getText().length()-1, adrd.getText().length()));
								//								for(int i=0; i< adrd.children.size();i++){
								//									System.out.println("child"+adrd.children.get(i).getText());
								//								}
								ParserRuleContext swft = (ParserRuleContext)mapping.swift_listner.op_s_nodes.get(ss).node;
								//								System.out.println("s###"+swft.children.size());
								//								if(swft.children.size()==1){
								//									System.out.println("schild"+swft.children.get(0).getText());

								if(CommonOperators.hasPunc(adrd.getText(), true) && 
										!CommonOperators.hasPunc(swft.getText(), false)){
									//									mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
								}

								if(!CommonOperators.hasPunc(adrd.getText(), true) && 
										CommonOperators.hasPunc(swft.getText(), false)){
									//									mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
								}

							}
						}//if same # of
						//					black_a = mapping.android_listener.op_a_nodes;
						//						System.out.println("black_a"+black_a);
						//						System.err.println("black_s"+black_s);

						//					black_s = mapping.swift_listner.op_s_nodes;

					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}



		}
		DBTablePrinter.printTable(connection, "grammar_android_stat");
		DBTablePrinter.printTable(connection, "grammar_swift_stat");
		DBTablePrinter.printTable(connection, "grammar_binding_stat");
		
		System.out.println(num);
		System.out.println(count);





		//		}
		//				System.err.println(varDeclSwift);
		//				System.out.println(varDeclAndroid);
		//				System.err.println(methodDecliOS);
		//				System.err.println(methodDeclAndroid);
		connection.close();
		//return . (-,-);
		//1 + 1 + 1, 1, 1
		//return . (-,-)
		/*
//		String sss = ListUtils.longestCommonSubsequence("Math.hypot(x1 - x2, y1 - y2)", "hypot(x1 - x2, y1 - y2)");
		//(-, -) (-, -)
//		char[] cccc = sss.toCharArray();
		List<String> aaaa = new LinkedList<String>();
		System.out.println(cccc);
		for(Character c:cccc){
//			System.out.println(c);
			if(StructureMappingAntlr4.java_keywords_list.contains(c.toString()) && StructureMappingAntlr4.swift_keywords_list.contains(c.toString()))
			{
				aaaa.add(c.toString());
			}
			if(StructureMappingAntlr4.java_punctuations_list.contains(c.toString()) && 
					StructureMappingAntlr4.swift_punctuations_list.contains(c.toString())){
				aaaa.add(c.toString());
			}


		}
		 */
		//		System.out.println(aaaa);
		//		System.out.println(ListUtils.longestCommonSubsequence("test.a(aa,aa,asdf)", "bb(cc,dd,as)"));


		//		StructureMappingAntlr4.java_keywords_list;


		//		a.add("test.a(aa,aa,asdf)".toCharArray());
		//		a.
		/*


		String arg [] ={"Swift", SwiftParser.ruleNames[36],"return test"};
//		try{
		TestRig aa;
		try {
			aa = new TestRig(arg);
//			aa.
			aa.process();
//			System.err.println("migrations:"+st_android.render());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
//			System.out.println("Exception"+e.getMessage());
		}

//		} catch (){
//			
//		}
		System.out.println("\n");

		//set of testing
		//query .. 
		 * 
		 */

		/*

		String test = "return mDrawAngles ;";
		String testB = "return value / yValueSum * mMaxAngle ;";
		String testC = "return set.isVisible() && set.isDrawValuesEnabled() ;";

		List<String> tttt = new LinkedList<String>();
		//		tttt.add(test);
		//		tttt.add(testB);
		//		tttt.add(testC);
		tttt.add("let rotationAngle:CGFloat = chart.rotationAngle");
		tttt.add("let phaseX:CGFloat = animator.phaseX");
		tttt.add("let qqqqq:wwww = animator.phaseY");
		tttt.add("let stackSpace = 1");

		String first = tttt.get(0);
		String ssss="";
		for(int i = 1; i<tttt.size();i++){
			if(ssss.equals(""))
				ssss = first;
			ssss = ListUtils.longestCommonSubsequence(ssss, tttt.get(i));
		}

		System.out.print(ssss);
		System.out.println("<END>");
		 */
	}

}
